#pragma once

namespace ether_drifters {

	class ISelectable {
	public:
		virtual void select() = 0;
		virtual void unselect() = 0;
	};

}
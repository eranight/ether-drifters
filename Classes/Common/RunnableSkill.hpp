#pragma once

namespace ether_drifters
{

	class RunnableSkill {
	public:
		virtual void run() = 0;
		virtual void stop() = 0;
		virtual bool isRunning() = 0;
	};

}
#include "ViewNode.hpp"

using namespace cocos2d;
using namespace ether_drifters;

const std::string ViewNode::NAME = "view";

bool ViewNode::init()
{
	if (!Node::init()) {
		return false;
	}

	setUserObject(ViewObject::create());

	setName(NAME);
	return true;
}

ViewObject* ViewNode::getUserObject()
{
	return dynamic_cast<ViewObject*>(Node::getUserObject());
}

void ViewNode::setOpacity(GLubyte opacity)
{
	for (auto child : getChildren()) {
		child->setOpacity(opacity);
	}
}

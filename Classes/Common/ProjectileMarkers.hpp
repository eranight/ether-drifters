#pragma once

namespace ether_drifters
{

	class OneTimeShot {

	};

	class FiniteTimeShot {
	public:
		virtual float duration() = 0;
	};

	class CallbackEndShot {
	public:
		virtual void end(const std::function<void ()>& callback) = 0;
	};

}
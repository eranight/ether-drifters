#pragma once
#include "cocos2d.h"
#include "ViewObject.hpp"

namespace ether_drifters
{

	class ViewNode : public cocos2d::Node {
	public:
		CREATE_FUNC(ViewNode);
		bool init() override;
		ViewObject* getUserObject() override;
		void setOpacity(GLubyte opacity) override;
	public:
		static const std::string NAME;
	};

}
#pragma once
#include "cocos2d.h"

namespace ether_drifters
{

	class ViewObject : public cocos2d::Ref {
	public:
		static ViewObject* create() { return new (std::nothrow) ViewObject(); }
	public:
		cocos2d::Size getContentSize() { return contentSize; }
		void setContentSize(const cocos2d::Size& contentSize) { this->contentSize = contentSize; }
	private:
		cocos2d::Size contentSize;
	};

}
#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class PointsBarConfig {
	public:
		float width;
		float maxPoints;
		float startPoints;
		cocos2d::Color4F color;
	};

	class AbstractPointsBar : public cocos2d::Node {
	protected:
		AbstractPointsBar(const PointsBarConfig& config) : config(config) {}
	public:
		bool init() override;
	protected:
		void updateState(float newPoints);
	private:
		float length(float newPoints) { return config.width * newPoints / config.maxPoints; }
	private:
		PointsBarConfig config;
		cocos2d::DrawNode* background;
		cocos2d::DrawNode* foreground;
	};

}
#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters
{

	class WeaponSkillInfo {
	public:
		Board board;
		cocos2d::Node* target = nullptr;
		cocos2d::Vec2 toPoint;
	};

	class WeaponSkill {
	public:
		virtual void fire(const WeaponSkillInfo& weaponSkillInfo) = 0;
	};

}
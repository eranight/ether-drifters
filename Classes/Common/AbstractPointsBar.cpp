#include "AbstractPointsBar.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

static const float POINTS_BAR_HEIGHT = 7.0f;
static const Color4F POINTS_BAR_BACKGROUND_COLOR = Color4F(Color4B(Color3B::BLACK, 35));

bool AbstractPointsBar::init() {
	if (!Node::init()) {
		return false;
	}

	background = DrawNode::create();
	background->drawSolidRect(Vec2::ZERO, Vec2(config.width, POINTS_BAR_HEIGHT), POINTS_BAR_BACKGROUND_COLOR);
	addChild(background);

	foreground = DrawNode::create();
	updateState(config.startPoints);
	addChild(foreground);

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

void AbstractPointsBar::updateState(float newPoints) {
	foreground->clear();
	foreground->drawSolidRect(Vec2::ZERO, Vec2(length(newPoints), POINTS_BAR_HEIGHT), config.color);
}
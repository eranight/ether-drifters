#ifndef __EHTER_H__
#define __EHTER_H__

#include "cocos2d.h"
#include "json/document.h"
#include "Prefubs/Domain.h"

namespace ether_drifters {

	class Ether;

	class EtherDestroyer {
	public:
		~EtherDestroyer();
		void init(Ether * ether);
	private:
		Ether * ether;
	};

	class Ether : public cocos2d::Ref {
	private:
		static Ether * s_ether;
		static EtherDestroyer s_destroyer;
	protected:
		Ether() {}
		virtual ~Ether();
		friend class EtherDestroyer;
	public:
		static Ether * getInstance();
		void init(const rapidjson::Value& val);
		void loadDomain(const std::string &, const std::string &, const std::string&);
		Domain* getCurrentDomain() { return domain; }
	private:
		cocos2d::Scene * createScene(const rapidjson::Value& val, const std::string& portalId);
		std::string addToDomain(cocos2d::Node* node, const rapidjson::Value& val, int localZ);
	private:
		Domain* domain;
		std::unordered_map<std::string, std::set<std::string>> clusters; // cluster on domains
		// target on map of action name on key codes
		std::unordered_map<std::string, std::unordered_map<std::string, std::set<cocos2d::EventKeyboard::KeyCode>>> actionMaps;
	};

}

#endif //__EHTER_H__
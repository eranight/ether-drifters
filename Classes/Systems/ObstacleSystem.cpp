#include "ObstacleSystem.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Obstacle.h"
#include "Events/ObstacleEventData.h"
#include "Ether.h"
#include "Prefubs/Domain.h"
#include "CircleExcluder.h"
#include "Utils/CircularObstaclePathfinding/Util.h"

using namespace cocos2d;
using namespace ether_drifters;
using namespace circular_obstacle_pathfinding;

ObstacleSystem::~ObstacleSystem() {
	CC_SAFE_RELEASE_NULL(addObstacleEventListener);
	CC_SAFE_RELEASE_NULL(removeObstacleEventListener);
#if ED_DEBUG
	CC_SAFE_RELEASE_NULL(forestDrawerNode);
#endif
}

bool ObstacleSystem::init() {
	if (!Node::init()) {
		return false;
	}

	forest = std::make_shared<Forest>();

#if ED_DEBUG
	forestDrawerNode = ForestDrawerNode::create();
	forestDrawerNode->setCameraMask(DOMAIN_UI_CAMERA_MASK);
	forestDrawerNode->retain();
	forestDrawerNode->setForest(forest);
	forestDrawerNode->enableHuggingEdgesDrawing(true);
	forestDrawerNode->enableSurfingEdgesDrawing(true);
	forestDrawerNode->enableVerticesDrawing(true);
	forestDrawerNode->setAddtitionalWidth(18.5);
#endif

	addObstacleEventListener = EventListenerCustom::create(ADD_OBSTACLE_EVENT, [this](EventCustom* event) {
		EventData* eventData = reinterpret_cast<EventData*>(event->getUserData());
		addObstacle(eventData->getOwner());
	});
	addObstacleEventListener->retain();

	removeObstacleEventListener = EventListenerCustom::create(REMOVE_OBSTACLE_EVENT, [this](EventCustom* event) {
		EventData* eventData = reinterpret_cast<EventData*>(event->getUserData());
		Obstacle* obstacle = HelperMethodsUtil::findComponent<Obstacle>(eventData->getOwner());
		removeObstacle(obstacle);
	});
	removeObstacleEventListener->retain();

	return true;
}

void ObstacleSystem::onEnter() {
	Node::onEnter();
	_eventDispatcher->addEventListenerWithFixedPriority(addObstacleEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(removeObstacleEventListener, 1);

	Domain* domain = Ether::getInstance()->getCurrentDomain();
	if (domain != nullptr) {
		for (auto child : domain->getChildren()) {
			addObstacle(child);
		}
#if ED_DEBUG
		domain->addChild(forestDrawerNode);
#endif
		domainRadius = domain->getRadius();
	}
}

void ObstacleSystem::onExit() {
	Node::onExit();
	_eventDispatcher->removeEventListener(addObstacleEventListener);
	_eventDispatcher->removeEventListener(removeObstacleEventListener);
#if ED_DEBUG
	Domain* domain = Ether::getInstance()->getCurrentDomain();
	if (domain != nullptr) {
		domain->removeChild(forestDrawerNode);
	}
#endif
	unscheduleUpdate();
}

void ObstacleSystem::update(float dt) {
	Node::update(dt);

	for (auto obstacle : obstacles) {
		Node* owner = obstacle->getOwner();
		Vec2 ownerPosition = owner->getPosition();
		for (auto& shape : obstacle->getShapes()) {
			Vec2 pos = calculateObstacleShapePosition(owner, shape);
			forest->changeObstaclePosition(shape.id, pos.x, pos.y);
		}
	}
}

Vec2 ObstacleSystem::calculatePointOnRadius(const Vec2& sourcePoint, const Vec2& centerPoint, float radius, float halfWidth, Obstacle* excludedObstacle) {
	Vec2 transformedSourcePoint = sourcePoint - centerPoint;
	Vec2 toDomainPoint = -centerPoint;
	CircleExcluder circleExcluder(radius);
	circleExcluder.excludeOutsidePoint(toDomainPoint, domainRadius - halfWidth);
	Vec2 transformedObstaclePosition;
	for (Obstacle* obstacle : obstacles) {
		if (obstacle == excludedObstacle || (obstacle->getMask() & Obstacle::PATH_OBSTACLE) == 0) continue;
		for (ObstacleShape& shape : obstacle->getShapes()) {
			transformedObstaclePosition = calculateObstacleShapePosition(obstacle->getOwner(), shape) - centerPoint;
			circleExcluder.excludeInsidePoint(transformedObstaclePosition, shape.radius + halfWidth);
		}
	}
	float sourceAngle = transformedSourcePoint.getAngle();
	sourceAngle = circular_obstacle_pathfinding::Util::convertAngleTo2Pi(sourceAngle);
	float newAngle = circleExcluder.approximate(sourceAngle);
	if (isnan(newAngle)) {
		return Vec2(NAN, NAN);
	}
	newAngle = circular_obstacle_pathfinding::Util::convertAngleToPi(newAngle);
	return centerPoint + Vec2(radius * cosf(newAngle), radius * sinf(newAngle));
}

ether_drifters::Path ObstacleSystem::getPath(const Vec2& start, const Vec2& goal, float additionalWidth, Obstacle* excludedObstacle)
{
	ether_drifters::Path resultPath;
	auto forest = Forest();
	for (auto obstacle : obstacles) {
		if (obstacle == excludedObstacle || (obstacle->getMask() & Obstacle::PATH_OBSTACLE) == 0) continue;
		Node* owner = obstacle->getOwner();
		Vec2 ownerPosition = owner->getPosition();
		for (ObstacleShape& shape : obstacle->getShapes()) {
			Vec2 pos = calculateObstacleShapePosition(owner, shape);
			forest.addObstacle(pos.x, pos.y, shape.radius);
		}
	}
	auto graph = forest.getGraph(additionalWidth);
	//forestDrawerNode->setForest(forest);
#if ED_DEBUG
	forestDrawerNode->setGraph(graph);
#endif
	uint32_t startId = graph->addEndpoint(start.x, start.y);
	uint32_t goalId = graph->addEndpoint(goal.x, goal.y);
	auto path = graph->findPath(startId, goalId);
	if (!path.parts.empty()) {
		for (auto partsIter = path.parts.begin(); partsIter != path.parts.end() - 1; ++partsIter) {
			auto& edge = graph->getEdges().at(partsIter->second);
			auto& vertexFrom = graph->getVertices().at(partsIter->first);
			auto& vertexTo = graph->getVertices().at(edge.getNot(partsIter->first));
			ether_drifters::Path::PathPart pathPart;
			pathPart.vertexFrom = Vec2(vertexFrom.x, vertexFrom.y);
			pathPart.vertexTo = Vec2(vertexTo.x, vertexTo.y);
			pathPart.length = edge.weight;
			pathPart.isHugging = edge.hugging;
			if (edge.hugging) {
				auto& obstacle = graph->getObstacles().at(edge.obstacle);
				pathPart.center = Vec2(obstacle.getX(), obstacle.getY());
				pathPart.radius = obstacle.getRadius();
				pathPart.clockwise = edge.getDirection(partsIter->first) == EdgeDirection::clockwise;
			}
			resultPath.parts.push_back(pathPart);
		}
	}
	return resultPath;
}

std::vector<ObstacleShape> ObstacleSystem::rayCast(const Vec2& from, const Vec2& to, float additionalWidth, Obstacle* excludedObstacle)
{
	std::vector<ObstacleShape> result;
	Vec2 ray = to - from;
	Vec2 reverseRay = from - to;
	for (auto obstacle : obstacles) {
		if (obstacle == excludedObstacle || (obstacle->getMask() & Obstacle::FIRE_OBSTACLE) == 0) continue;
		for (ObstacleShape& shape : obstacle->getShapes()) {
			Vec2 center = calculateObstacleShapePosition(obstacle->getOwner(), shape);
			float radiusSq = shape.radius + additionalWidth;
			radiusSq *= radiusSq;

			Vec2 toCenter = center - from;
			float dotProductResult = ray.dot(toCenter);
			if (dotProductResult <= 0.0f) {
				if (toCenter.lengthSquared() < radiusSq) {
					shape.position = center;
					result.push_back(shape);
				}
				continue;
			}

			toCenter = center - to;
			dotProductResult = reverseRay.dot(toCenter);
			if (dotProductResult <= 0.0f) {
				if (toCenter.lengthSquared() < radiusSq) {
					shape.position = center;
					result.push_back(shape);
				}
				continue;
			}

			float s = ray.cross(toCenter);
			float h = s * s / ray.lengthSquared();
			if (h <= radiusSq) {
				shape.position = center;
				result.push_back(shape);
			}
		}
	}
	return result;
}

Vec2 ObstacleSystem::calculateObstacleShapePosition(Node* node, ObstacleShape& shape)
{
	auto view = HelperMethodsUtil::getViewIfExists(node);
	Domain* domain = Ether::getInstance()->getCurrentDomain();
	return domain->convertToNodeSpace(view->convertToWorldSpace(shape.offset));
}

void ObstacleSystem::addObstacle(Node* node) {
	Obstacle* obstacle = HelperMethodsUtil::findComponent<Obstacle>(node);
	if (obstacle != nullptr && !obstacles.contains(obstacle)) {
		CCLOG("add new obstacle from node %p", node);
		if (obstacles.empty()) {
			scheduleUpdate();
		}
		obstacles.pushBack(obstacle);
		Vec2 position = node->getPosition();
		for (ObstacleShape& shape : obstacle->getShapes()) {
			Vec2 pos = calculateObstacleShapePosition(obstacle->getOwner(), shape);
			uint32_t id = forest->addObstacle(pos.x, pos.y, shape.radius);
			shape.id = id;
		}
	}
}

void ether_drifters::ObstacleSystem::removeObstacle(Obstacle* obstacle)
{
	if (obstacle != nullptr) {
		obstacles.eraseObject(obstacle);
		for (auto& shape : obstacle->getShapes()) {
			forest->removeObstacle(shape.id);
		}
		if (obstacles.empty()) {
			unscheduleUpdate();
		}
	}
}

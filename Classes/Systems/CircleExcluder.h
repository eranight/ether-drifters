#pragma once

#include "Utils/SegmentSet.h"
#include "cocos2d.h"
#include <vector>
#include <functional>

namespace ether_drifters {

	class CircleExcluder {
	public:
		CircleExcluder(float radius);
	public:
		void excludeOutsidePoint(const cocos2d::Vec2& pos, float radius);
		void excludeInsidePoint(const cocos2d::Vec2& pos, float radius);
		float approximate(float source);
		bool isEmpty() { return segmentSet.isEmpty(); }
	private:
		void exclude(const cocos2d::Vec2& pos, float radius, bool outsideMode = false);
	private:
		float radius;
		SegmentSet segmentSet;
	};

}
#pragma once

#include "cocos2d.h"

namespace ether_drifters {
	class Path {
	public:
		class PathPart {
		public:
			cocos2d::Vec2 vertexFrom;
			cocos2d::Vec2 vertexTo;
			float length;
			bool isHugging = false;
			bool clockwise = false;
			cocos2d::Vec2 center;
			float radius;
		};
		std::vector<PathPart> parts;
	};
}
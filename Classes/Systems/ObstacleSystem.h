#pragma once

#include "cocos2d.h"
#include "Path.h"
#include "Components/Obstacle.h"
#include "Utils/CircularObstaclePathfinding/Forest.h"
#include "Prefubs/ForestDrawerNode.h"

namespace ether_drifters {

	class ObstacleSystem : public cocos2d::Node {
	private:
		ObstacleSystem() {}
		~ObstacleSystem();
	public:
		CREATE_FUNC(ObstacleSystem);
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	public:
		// warning: sourcePoint must be in domain coordinate system
		cocos2d::Vec2 calculatePointOnRadius(const cocos2d::Vec2& sourcePoint, const cocos2d::Vec2& centerPoint, float radius, float halfWidth, Obstacle* excludedObstacle = nullptr);
		Path getPath(const cocos2d::Vec2& start, const cocos2d::Vec2& goal, float additionalWidth, Obstacle* excludedObstacle = nullptr);
		std::vector<ObstacleShape> rayCast(const cocos2d::Vec2& from, const cocos2d::Vec2& to, float additionalWidth, Obstacle* excludedObstacle = nullptr);
	private:
		cocos2d::Vec2 calculateObstacleShapePosition(cocos2d::Node* node, ObstacleShape& shape);
		void addObstacle(cocos2d::Node* node);
		void removeObstacle(Obstacle* obstacle);
	private:
		cocos2d::Vector<Obstacle*> obstacles;
		cocos2d::EventListenerCustom* addObstacleEventListener = nullptr;
		cocos2d::EventListenerCustom* removeObstacleEventListener = nullptr;

		std::shared_ptr<circular_obstacle_pathfinding::Forest> forest;
		std::shared_ptr<circular_obstacle_pathfinding::Graph> graph;

		float domainRadius = 0.0f;
#if ED_DEBUG
		ForestDrawerNode* forestDrawerNode = nullptr;
#endif
	};

}
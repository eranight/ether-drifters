#pragma once

#include "cocos2d.h"
#include "Events/InputEventData.h"

namespace ether_drifters
{

	class InputSystemConfig
	{
	public:
		std::unordered_map<std::string, std::set<cocos2d::EventKeyboard::KeyCode>> actionsOnKeys;
	};

	class InputSystem : public cocos2d::Node
	{
	private:
		InputSystem(const InputSystemConfig& config) : config(config) {}
		~InputSystem();
	public:
		static InputSystem* create(const InputSystemConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
		void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
		void processKey(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event, InputState state);
	private:
		InputSystemConfig config;
		std::unordered_map<cocos2d::EventKeyboard::KeyCode, std::set<std::string>> keysOnAction;
		cocos2d::EventListenerKeyboard* keyboardListener;
	};

}
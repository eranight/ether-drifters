#include "InputSystem.hpp"

using namespace cocos2d;
using namespace ether_drifters;

InputSystem::~InputSystem()
{
	CC_SAFE_RELEASE_NULL(keyboardListener);
}

InputSystem* InputSystem::create(const InputSystemConfig& config)
{
	auto ret = new (std::nothrow) InputSystem(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool InputSystem::init()
{
	if (!Node::init()) {
		return false;
	}

	for (auto& pair : config.actionsOnKeys) {
		for (EventKeyboard::KeyCode keyCode : pair.second) {
			auto& actions = keysOnAction[keyCode];
			actions.insert(pair.first);
		}
	}

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->retain();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(InputSystem::onKeyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(InputSystem::onKeyReleased, this);

	return true;
}

void InputSystem::onEnter()
{
	Node::onEnter();

	getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
}

void InputSystem::onExit()
{
	Node::onExit();

	getEventDispatcher()->removeEventListener(keyboardListener);
}

void InputSystem::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	processKey(keyCode, event, InputState::pressed);
}

void InputSystem::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	processKey(keyCode, event, InputState::released);
}

void InputSystem::processKey(EventKeyboard::KeyCode keyCode, Event* event, InputState state)
{
	auto& iter = keysOnAction.find(keyCode);
	if (iter != keysOnAction.end()) {
		for (auto& action : iter->second) {
			InputEventData eventData{ this, action, state };
			getEventDispatcher()->dispatchCustomEvent(action, &eventData);
			CCLOG("key event %s", action);
		}
	}
}

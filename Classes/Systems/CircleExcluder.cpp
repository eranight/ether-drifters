#include "CircleExcluder.h"
#include "Utils/CircularObstaclePathfinding/Util.h"

using namespace cocos2d;
using namespace ether_drifters;
using namespace circular_obstacle_pathfinding;

CircleExcluder::CircleExcluder(float radius) : radius(radius), segmentSet(0.0f, M_PI * 2.0f, 0.0001f)
{}

void CircleExcluder::excludeOutsidePoint(const Vec2& pos, float radius) {
	exclude(pos, radius, true);
}

void CircleExcluder::excludeInsidePoint(const Vec2& pos, float radius) {
	exclude(pos, radius);
}

void CircleExcluder::exclude(const cocos2d::Vec2& pos, float radius, bool outsideMode)
{
	float length = pos.length();
	int overlap = Util::overlap(this->radius, radius, length);
	if (overlap == 0) {
		if (!outsideMode && this->radius < radius) {
			segmentSet.exclude(0.0f, M_PI * 2.0f);
		}
		return;
	}
	else if (overlap == 2) {
		if (outsideMode) {
			segmentSet.exclude(0.0f, M_PI * 2.0f);
		}
		return;
	}
	if (overlap != 1) return;
	float theta = Util::theta(this->radius, radius, length);
	if (abs(theta) < FLT_EPSILON) return;
	float facing = Util::facing(0.0f, 0.0f, pos.x, pos.y);
	float angleFrom = facing - theta;
	if (abs(angleFrom) < FLT_EPSILON) angleFrom = 0.0f;
	float angleTo = facing + theta;
	if (abs(angleTo) < FLT_EPSILON) angleTo = 0.0f;
	angleFrom = Util::convertAngleTo2Pi(angleFrom);
	angleTo = Util::convertAngleTo2Pi(angleTo);
	if (outsideMode) {
		std::swap(angleFrom, angleTo);
	}
	segmentSet.exclude(angleFrom, angleTo);
}

float CircleExcluder::approximate(float source) {
	return segmentSet.approximate(source);
}
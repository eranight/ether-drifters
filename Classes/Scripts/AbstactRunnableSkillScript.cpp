#include "AbstactRunnableSkillScript.hpp"

using namespace cocos2d;
using namespace ether_drifters;

AbstactRunnableSkillScript::~AbstactRunnableSkillScript()
{
	stop();
}

bool AbstactRunnableSkillScript::run()
{
	if (isBusy()) {
		return false;
	}

	running = true;

	Director::getInstance()->getScheduler()->schedule(
		schedule_selector(AbstactRunnableSkillScript::update),
		this,
		getScheduleInterval(),
		false
	);

	return true;
}

void AbstactRunnableSkillScript::stop()
{
	if (!running) {
		return;
	}

	running = false;

	Director::getInstance()->getScheduler()->unschedule(schedule_selector(AbstactRunnableSkillScript::update), this);
}

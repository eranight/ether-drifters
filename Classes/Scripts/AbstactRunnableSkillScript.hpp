#pragma once
#include "cocos2d.h"

namespace ether_drifters
{

	class AbstactRunnableSkillScript : public cocos2d::Ref {
	public:
		~AbstactRunnableSkillScript();
		virtual bool run();
		virtual void stop();
		bool isRunning() { return running; }
	protected:
		virtual bool isBusy() { return running; }
		virtual float getScheduleInterval() = 0;
		virtual void update(float dt) = 0;
	private:
		bool running = false;
	};

}
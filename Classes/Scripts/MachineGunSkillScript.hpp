#pragma once
#include "cocos2d.h"
#include "AbstactRunnableSkillScript.hpp"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters
{

	class Cannon;
	class MachineGunBurst;
	class DamageDealer;
	class Tracked;

	class MachineGunSkillScriptConfig {
	public:
		Cannon* cannon;
		float cannonVisionAngle;
		float damage;
		int quantity;
		float delay;
		float velocity;
		float lifeTime;
		float dispersionAngle;
		DamageDealer* dealer = nullptr;
		bool debug = false;
	};

	class MachineGunSkillScript : public AbstactRunnableSkillScript {
		CREATE_FUNC_WITH_CONFIG(MachineGunSkillScript, MachineGunSkillScriptConfig);
	public:
		~MachineGunSkillScript();
		bool init();
		bool run() override;
		void stop() override;
	protected:
		bool isBusy() override;
		float getScheduleInterval() override;
		void update(float dt) override;
	private:
		void stop(float dt);
	private:
		MachineGunBurst* burst = nullptr;
		Tracked* tracked = nullptr;
	};

}
#include "MachineGunSkillScript.hpp"
#include "Prefubs/Cannon.hpp"
#include "Prefubs/Skills/MachineGunBurst.hpp"
#include "Components/Tracked.h"
#include "Ether.h"
#include "Components/PlayerController.hpp"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

MachineGunSkillScript::~MachineGunSkillScript()
{
	burst->release();
	Director::getInstance()->getScheduler()->unschedule(schedule_selector(MachineGunSkillScript::stop), this);
}

bool MachineGunSkillScript::init()
{
	MachineGunBurstConfig machineGunBursConfig;
	machineGunBursConfig.damage = config.damage;
	machineGunBursConfig.debug = config.debug;
	machineGunBursConfig.lifeTime = config.lifeTime;
	machineGunBursConfig.velocity = config.velocity;
	machineGunBursConfig.quantity = config.quantity;
	machineGunBursConfig.delay = config.delay;
	machineGunBursConfig.dispersionAngle = config.dispersionAngle;
	machineGunBursConfig.dealer = config.dealer;
	burst = MachineGunBurst::create(machineGunBursConfig);
	burst->retain();

	return true;
}

bool MachineGunSkillScript::run()
{
	if (!AbstactRunnableSkillScript::run()) {
		return false;
	}

	auto player = Ether::getInstance()->getCurrentDomain()->getChildByName(PLAYER);
	auto controller = HelperMethodsUtil::findComponent<PlayerController>(player);
	auto possibleTracked = controller->getSelectedNode() != nullptr ?
		HelperMethodsUtil::findComponent<Tracked>(controller->getSelectedNode()) :
		nullptr;
	if (possibleTracked != nullptr && possibleTracked->getOwner() != player) {
		tracked = possibleTracked;
	}
	else {
		config.cannon->reset();
	}

	update(0.0f);

	config.cannon->fire([this]() { return burst; });
	Director::getInstance()->getScheduler()->schedule(
		schedule_selector(MachineGunSkillScript::stop),
		this,
		config.quantity * config.delay + Director::getInstance()->getAnimationInterval(),
		0,
		0.0f,
		false
	);

	return true;
}

void MachineGunSkillScript::stop()
{
	AbstactRunnableSkillScript::stop();
	
	config.cannon->reset();

	tracked = nullptr;
}

bool MachineGunSkillScript::isBusy()
{
	return AbstactRunnableSkillScript::isBusy() || config.cannon->isRecharging();
}

float MachineGunSkillScript::getScheduleInterval()
{
	return Director::getInstance()->getAnimationInterval();
}

void MachineGunSkillScript::update(float dt)
{
	float nextCannonAngle;
	if (tracked != nullptr) {
		auto domain = Ether::getInstance()->getCurrentDomain();
		Vec2 trackedPosition = config.cannon->convertToNodeSpace(domain->convertToWorldSpace(tracked->getOwner()->getPosition()));
		float angle = config.cannon->getRotation();
		float trackedAngle = CC_RADIANS_TO_DEGREES(trackedPosition.getAngle());
		nextCannonAngle = trackedAngle - angle;
	}
	else {
		nextCannonAngle = config.cannonVisionAngle / config.quantity / config.delay * dt;
	}
	config.cannon->rotate(nextCannonAngle);
}

void MachineGunSkillScript::stop(float dt)
{
	stop();
}

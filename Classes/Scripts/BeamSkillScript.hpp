#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"
#include "AbstactRunnableSkillScript.hpp"

namespace ether_drifters 
{

	class Cannon;
	class DamageDealer;
	class BeamBurst;

	class BeamSkillScriptConfig {
	public:
		Cannon* cannon;
		float deployDuration = 0.0f;
		float activeDuration = 0.0f;
		float foldDuration = 0.0f;
		float damage = 0.0f;
		float length = 0.0f;
		float width = 0.0f;
		DamageDealer* dealer = nullptr;
	};

	class BeamSkillScript : public AbstactRunnableSkillScript {
		CREATE_FUNC_WITH_CONFIG(BeamSkillScript, BeamSkillScriptConfig);
	public:
		~BeamSkillScript();
		bool init();
		bool run() override;
	protected:
		bool isBusy() override;
		float getScheduleInterval() override;
		void update(float dt) override {}
	private:
		BeamBurst* burst = nullptr;
	};

}
#include "BeamSkillScript.hpp"
#include "Prefubs/Cannon.hpp"
#include "Prefubs/Skills/BeamBurst.hpp"

using namespace cocos2d;
using namespace ether_drifters;

BeamSkillScript::~BeamSkillScript()
{
	CC_SAFE_RELEASE_NULL(burst);
}

bool BeamSkillScript::init()
{
	BeamBurstConfig burstConfig;
	burstConfig.activeDuration = config.activeDuration;
	burstConfig.damage = config.damage;
	burstConfig.dealer = config.dealer;
	burstConfig.deployDuration = config.deployDuration;
	burstConfig.foldDuration = config.foldDuration;
	burstConfig.length = config.length;
	burstConfig.width = config.width;

	burst = BeamBurst::create(burstConfig);
	burst->retain();

	return true;
}

bool BeamSkillScript::run()
{
	if (!AbstactRunnableSkillScript::run()) {
		return false;
	}

	config.cannon->fire([this]() { return burst; });

	return true;
}

bool BeamSkillScript::isBusy()
{
	return AbstactRunnableSkillScript::isBusy() || config.cannon->isRecharging();
}

float BeamSkillScript::getScheduleInterval()
{
	return std::numeric_limits<float>::infinity();
}

#pragma once

#include <map>
#include <vector>
#include <memory>

#include "IdGenerator.h"
#include "Obstacle.h"
#include "Graph.h"

namespace circular_obstacle_pathfinding {
	class Forest
	{
	public:
		Forest() {}
	public:
		const Obstacle* getObstacle(uint32_t id);
		std::vector<Obstacle> getObstacles();
		uint32_t addObstacle(float x, float y, float radius);
		bool removeObstacle(uint32_t id);
		bool changeObstaclePosition(uint32_t id, float x, float y);
		bool changeObstacleRadius(uint32_t id, float radius);
		std::shared_ptr<Graph> getGraph(float additionalWidth);
	private:
		std::map<uint32_t, Obstacle> obstacles;
		IdGenerator idGenerator;
	};
}
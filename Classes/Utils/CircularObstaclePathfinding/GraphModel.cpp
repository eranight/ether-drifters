#include "GraphModel.h"
#include "Graph.h"

using namespace circular_obstacle_pathfinding;

GraphModel::GraphModel(GraphModel&& model) :
	obstacles(std::move(model.obstacles)),
	vertices(std::move(model.vertices)),
	edges(std::move(model.edges)),
	endpoints(std::move(model.endpoints)),
	obstacleIdGenerator(model.obstacleIdGenerator),
	verticesIdGenerator(model.verticesIdGenerator),
	edgesIdGenerator(model.edgesIdGenerator)
{
}

GraphModel& GraphModel::operator=(GraphModel&& model)
{
	obstacles = std::move(model.obstacles);
	vertices = std::move(model.vertices);
	edges = std::move(model.edges);
	endpoints = std::move(model.endpoints);
	obstacleIdGenerator = model.obstacleIdGenerator;
	verticesIdGenerator = model.verticesIdGenerator;
	edgesIdGenerator = model.edgesIdGenerator;
	return *this;
}

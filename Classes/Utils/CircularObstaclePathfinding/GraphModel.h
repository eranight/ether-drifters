#pragma once

#include <map>
#include "ObstacleModel.h"
#include "EdgeModel.h"
#include "IdGenerator.h"

namespace circular_obstacle_pathfinding {

	class AStarPathfinder;
	class GraphModelBuilder;
	class Graph;

	class GraphModel {
	private:
		friend AStarPathfinder;
		friend GraphModelBuilder;
		friend Graph;
	public:
		GraphModel() {}
		GraphModel(GraphModel&& model);
		GraphModel& operator=(GraphModel&& model);
	private:
		std::map<uint32_t, ObstacleModel> obstacles;
		std::map<uint32_t, VertexModel> vertices;
		std::map<uint32_t, EdgeModel> edges;
		std::map<uint32_t, std::set<uint32_t>> endpoints; // ids of obstacle models on obstacle models
		IdGenerator obstacleIdGenerator;
		IdGenerator verticesIdGenerator;
		IdGenerator edgesIdGenerator;
	};

}
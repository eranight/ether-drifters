#pragma once
#include <map>
#include <vector>
#include "VertexModel.h"

namespace circular_obstacle_pathfinding {

	class GraphModelBuilder;

	class ObstacleModel {
	private:
		friend GraphModelBuilder;
	public:
		ObstacleModel() { }
		ObstacleModel(float x, float y, float radius, bool endpoint) :
			x(x), y(y), radius(radius), endpoint(endpoint)
		{}
		ObstacleModel(ObstacleModel&& model);
		ObstacleModel(const ObstacleModel& model) = default;
		ObstacleModel& operator=(const ObstacleModel& model) = default;
		ObstacleModel& operator=(ObstacleModel&& model);
	public:
		inline float getX() const { return x; }
		inline void setX(float x) { this->x = x; }
		inline float getY() const { return y; }
		inline void setY(float y) { this->y = y; }
		inline float getRadius() const { return radius; }
		inline void setRadius(float radius) { this->radius = radius; }
	public:
		float x;
		float y;
		float radius;
		bool endpoint;
		std::set<uint32_t> vertices;      // ids of vertices for using with GraphModel
		std::set<uint32_t> huggingEdges;  // ids of hugging edges for using with GraphModel
		std::set<float> intersections; // facing angles from center to intersection point
	};

}
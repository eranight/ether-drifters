#pragma once

#include <memory>

#include "AStarPathfinder.h"
#include "ObstacleModel.h"
#include "VertexModel.h"
#include "EdgeModel.h"

namespace circular_obstacle_pathfinding {

	class GraphModel;
	class GraphModelBuilder;

	class Graph {
	public:
		Graph(std::shared_ptr<GraphModel> model);
	public:
		uint32_t addEndpoint(float x, float y);
		void removeEndpoint(uint32_t id);
		Path findPath(uint32_t from, uint32_t to);
		std::map<uint32_t, ObstacleModel>& getObstacles();
		std::map<uint32_t, VertexModel>& getVertices();
		std::map<uint32_t, EdgeModel>& getEdges();
	private:
		std::shared_ptr<GraphModel> model;
		std::shared_ptr<AStarPathfinder> pathfinder;
	};
}
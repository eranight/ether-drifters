#pragma once
#include <stdint.h>

namespace circular_obstacle_pathfinding {

	class IdGenerator {
	public:
		uint32_t generate() { return nextValue++; }
	private:
		uint32_t nextValue = 0;
	};

	class Id {

	};

}
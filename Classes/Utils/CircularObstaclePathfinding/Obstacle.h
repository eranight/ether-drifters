#pragma once

#include <cmath>

namespace circular_obstacle_pathfinding {

	class Obstacle
	{
	public:
		Obstacle() : x(NAN), y(NAN), radius(NAN) {}
		Obstacle(float x, float y, float radius) :
			x(x), y(y), radius(radius) {}
		Obstacle(const Obstacle& copy) :
			x(copy.x), y(copy.y), radius(copy.radius) {}
	public:
		float getX() const { return x; }
		void setX(float x) { this->x = x; }

		float getY() const { return y; }
		void setY(float y) { this->y = y; }

		float getRadius() const { return radius; }
		void setRadius(float radius) { this->radius = radius; }
	private:
		float x;
		float y;
		float radius;
	};

}
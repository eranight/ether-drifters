#pragma once
#include <string>
#include "cocos2d.h"

namespace ether_drifters {

	enum ContactMask {
		body = 0x01,
		mined = 0x02,
		mining = 0x04,
		tracked = 0x08,
		tracking = 0x10,
		portable = 0x20,
		portal = 0x40,
		hit = 0x80,
		drop = 0x100,
		picker = 0x200,
		domain = 0x40000000
	};

	enum class DomainObjectLevel {
		portals = 1,
		drop = 2,
		player = 3,
		enemies = 4,
		projectiles = 5
	};

	enum class DropType {
		none,
		ether_essence,
		survivor,
		iron_ore
	};

	enum class DamageTargetMode {
		none,
		any,
		target
	};

	enum class Board { 
		left,
		right 
	};

	const std::string VIEW = "view";
	const std::string VISION = "vision";
	const std::string PLAYER = "player";

	const std::string RADIUS = "radius";
	const int DOMAIN_TAG = 0xf0f0;

	const std::string PLAYER_CAMERA_NAME = "player_camera";
	const std::string DOMAIN_UI_CAMERA_NAME = "domain_ui_camera";

	const std::string NODE_MENU = "node_menu";

	const std::string OBSTACLE_SYSTEM = "obstacle_system";
	const std::string INPUT_SYSTEM = "input_system";

	const unsigned short DOMAIN_CAMERA_MASK = static_cast<unsigned short>(cocos2d::CameraFlag::USER1);
	const unsigned short DOMAIN_UI_CAMERA_MASK =
		static_cast<unsigned short>(cocos2d::CameraFlag::USER1) | static_cast<unsigned short>(cocos2d::CameraFlag::USER2);
	
}
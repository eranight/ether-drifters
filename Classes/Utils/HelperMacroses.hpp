#pragma once

#define CREATE_FUNC_WITH_CONFIG(__TYPE__, __CONFIG__) \
private: \
__TYPE__(const __CONFIG__& config) : config(config) {} \
private: \
__CONFIG__ config; \
public: \
static __TYPE__* create(__CONFIG__ config) \
{ \
    __TYPE__ *pRet = new(std::nothrow) __TYPE__(config); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = nullptr; \
        return nullptr; \
    } \
}
#include "JsonConvertersUtil.h"

using namespace cocos2d;
using namespace rapidjson;
using namespace ether_drifters;

Vec2 JsonConvertersUtil::convertCoordinates(const rapidjson::Value& val) {
	if (val.HasMember("x")) {
		return extractAsCartesianCoodrinates(val);
	}
	else if (val.HasMember("phi")) {
		return extractAsPolarCoordinates(val);
	}
	CCASSERT(false, "unknown coordinates");
	return Vec2::ZERO;
}

Color3B JsonConvertersUtil::convertColor(const rapidjson::Value& val) {
	byte r = val.GetArray()[0].GetInt();
	byte g = val.GetArray()[1].GetInt();
	byte b = val.GetArray()[2].GetInt();
	return Color3B(r, g, b);
}

Vec2 JsonConvertersUtil::extractAsCartesianCoodrinates(const rapidjson::Value& val) {
	float x = val["x"].GetFloat();
	float y = val["y"].GetFloat();
	return Vec2(x, y);
}

Vec2 JsonConvertersUtil::extractAsPolarCoordinates(const rapidjson::Value& val) {
	float r = val["r"].GetFloat();
	float phi = CC_DEGREES_TO_RADIANS(val["phi"].GetFloat());
	return Vec2(r * std::cosf(phi), r * std::sinf(phi));
}
#pragma once

#include "cocos2d.h"


namespace ether_drifters
{

	class StringToCocosKeyCodeConverterUtil
	{
	private:
		StringToCocosKeyCodeConverterUtil() = delete;
	public:
		static cocos2d::EventKeyboard::KeyCode convert(const std::string& code);
	};

}
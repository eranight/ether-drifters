#pragma once

#include <vector>
#include <cmath>

namespace ether_drifters {


	class Segment {
	public:
		Segment(float min, float max) : min(min), max(max) {}
	public:
		float getMin() const { return min; }
		void setMin(float min) { this->min = min; }
		float getMax() const { return max; }
		void setMax(float max) { this->max = max; }
		bool isEmpty(float epsilon) const { return std::isnan(min) || std::isnan(max) || length() < epsilon; }
		bool isIncluding(const Segment& other, float epsilon) const;
		bool isIncluding(const float val, float epsilon) const;
		float length() const { return max - min; }
		float approximate(float val, float epsilon);
	private:
		float min;
		float max;
	};

	class SegmentSet {
	public:
		SegmentSet(float min, float max, float epsilon);
	public:
		void exclude(float min, float max);
		bool isEmpty();
		// void include(float min, float max);
		float approximate(float val);
	private:
		float epsilon;
		Segment initSegment;
		std::vector<Segment> segments;
		bool empty;
	};

}
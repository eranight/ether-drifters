#include "SegmentSet.h"

using namespace std;
using namespace ether_drifters;

bool Segment::isIncluding(const Segment& other, float epsilon) const {
	return (abs(min - other.min) < epsilon || min < other.min) &&
		(abs(max - other.max) < epsilon || max > other.max);
}

bool Segment::isIncluding(const float val, float epsilon) const {
	return (abs(min - val) < epsilon || min < val) &&
		(abs(max - val) < epsilon || max > val);
}

float Segment::approximate(float val, float epsilon)
{
	if (length() < epsilon) {
		return val;
	}
	if (abs(val - min) < epsilon) {
		return min + epsilon;
	}
	if (abs(max - val) < epsilon) {
		return max - epsilon;
	}
	return val;
}

SegmentSet::SegmentSet(float min, float max, float epsilon) :
	epsilon(epsilon), initSegment(min, max) {
	segments.emplace_back(min, max);
	empty = initSegment.isEmpty(epsilon);
}

void SegmentSet::exclude(float min, float max) {

	if (abs(min - max) < epsilon) {
		return;
	}

	if (min > max) {
		exclude(min, initSegment.getMax());
		exclude(initSegment.getMin(), max);
		return;
	}

	Segment comparedSegment(min, max);
	int firstRedundantSegmentIndex = -1;
	int lastRedundantSegmentIndex = -1;
	int minIncludingSegmentIndex = -1;
	int maxIncludignSegmentIndex = -1;
	for (int index = 0; index < segments.size(); ++index) {
		Segment segment = segments[index];
		if (minIncludingSegmentIndex == -1 && segment.isIncluding(min, epsilon)) {
			minIncludingSegmentIndex = index;
		}
		if (maxIncludignSegmentIndex == -1 && segment.isIncluding(max, epsilon)) {
			maxIncludignSegmentIndex = index;
			break;
		}
		if (segment.getMin() > max) {
			break;
		}
		if (comparedSegment.isIncluding(segment, epsilon)) {
			if (firstRedundantSegmentIndex == -1) {
				firstRedundantSegmentIndex = index;
			}
			lastRedundantSegmentIndex = index;
		}
	}

	if (minIncludingSegmentIndex == maxIncludignSegmentIndex && minIncludingSegmentIndex != -1) {
		bool k = false;
		Segment dividableSegment = segments[minIncludingSegmentIndex];
		Segment currentSegment(dividableSegment.getMin(), comparedSegment.getMin());
		if (!currentSegment.isEmpty(epsilon)) {
			segments[minIncludingSegmentIndex].setMax(comparedSegment.getMin());
			k = true;
		}
		Segment nextSegment(comparedSegment.getMax(), dividableSegment.getMax());
		if (!nextSegment.isEmpty(epsilon)) {
			if (k) {
				segments.insert(segments.begin() + minIncludingSegmentIndex + 1, nextSegment);
			}
			else {
				segments[minIncludingSegmentIndex].setMin(comparedSegment.getMax());
			}
		}
		empty = currentSegment.isEmpty(epsilon) && nextSegment.isEmpty(epsilon);
	}
	else {
		if (minIncludingSegmentIndex != -1) {
			segments[minIncludingSegmentIndex].setMax(comparedSegment.getMin());
		}
		if (maxIncludignSegmentIndex != -1) {
			segments[maxIncludignSegmentIndex].setMin(comparedSegment.getMax());
		}
	}
	// must be after previous if-statement
	if (firstRedundantSegmentIndex != -1) {
		segments.erase(segments.begin() + firstRedundantSegmentIndex,
			segments.begin() + lastRedundantSegmentIndex + 1);
	}

}

bool SegmentSet::isEmpty()
{
	return empty || segments.empty();
}

float SegmentSet::approximate(float val) {
	if (isEmpty() || !initSegment.isIncluding(val, epsilon)) {
		return NAN;
	}
	Segment segment = segments[0];
	if (segment.isIncluding(val, epsilon)) {
		return segment.approximate(val, epsilon);
	}
	Segment lastSegment = segments[segments.size() - 1];
	if (segment.getMin() > val || lastSegment.getMax() < val) {
		Segment gap(0.0f, (segment.getMin() - initSegment.getMin()) + (initSegment.getMax() - lastSegment.getMax()));
		float nVal = segment.getMin() > val ? (val + (initSegment.getMax() - lastSegment.getMax())) : (val - lastSegment.getMax());
		float m = (nVal - gap.getMin()) / gap.length();
		if (m < 0.5f || abs(m - 0.5f) < epsilon) {
			return lastSegment.getMax() - epsilon;
		}
		else {
			return segment.getMin() + epsilon;
		}
	}
	for (int index = 1; index < segments.size(); ++index) {
		segment = segments[index];
		Segment gap(segments[index - 1].getMax(), segment.getMin());
		if (gap.isIncluding(val, epsilon)) {
			//return gap.approximate(val, epsilon);
			float m = (val - gap.getMin()) / gap.length();
			if (m < 0.5f || abs(m - 0.5f) < epsilon) {
				return gap.getMin() - epsilon;
			}
			else {
				return gap.getMax() + epsilon;
			}
		}
		if (segment.isIncluding(val, epsilon)) {
			return segment.approximate(val, epsilon);
		}
	}

	return NAN;
}

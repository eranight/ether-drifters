#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class EventData;

	class HelperMethodsUtil {
	private:
		HelperMethodsUtil() = delete;
	public:
		static cocos2d::Vector<cocos2d::Node*> fetchNodesByContactMasks(cocos2d::PhysicsContact& contact, int firstMask, int secondMask);
		// return emtpy vector if contact does not have thisNode as one of contacted bodies
		// otherwise return vector of 2 elemetns where first (0) is always thisNode
		static cocos2d::Vector<cocos2d::Node*> fetchNodesFromPhysicsContact(cocos2d::PhysicsContact& contact, cocos2d::Node* thisNode, int thisNodeMask);
		// return direct node or parent node if direct node is "view"
		static cocos2d::Node* getRightNode(cocos2d::Node* node);
		// return child "view" node from direct node or direct node otherwise
		static cocos2d::Node* getViewIfExists(cocos2d::Node* node);
		// return content size that retains in view user object field
		static cocos2d::Size getContentSize(cocos2d::Node* node);
		static void adjustScaleToSize(cocos2d::Node* node, const cocos2d::Size& size);

		static cocos2d::Vector<cocos2d::Node*> fetchNodesWithNames(cocos2d::PhysicsContact& contact, const std::string& nameA, const std::string& nameB);
		static cocos2d::Component* fetchComponentByName(cocos2d::Node* node, const std::string & name);
		static cocos2d::Vec2 randomDirectionToPoint(const cocos2d::Vec2& point, const cocos2d::Vec2& currentPosition, float maxAngle);
		static cocos2d::Vec2 randomDirectionToPoint(const cocos2d::Vec2& point, const cocos2d::Vec2& currentPosition, float minAngle, float maxAngle);
		static float randomAngle(float minAngle, float maxAngle);
		static float randomExplicitAngle(float minAngle, float maxAngle);
		
		template<typename T>
		static T * findComponent(cocos2d::Node* node) {
			return dynamic_cast<T *>(node->getComponent(T::NAME));
		}

		template<class T>
		static T* convertEventData(cocos2d::EventCustom* event, cocos2d::Node* expectedOwner = nullptr) {
			T* eventData = reinterpret_cast<T*>(event->getUserData());
			cocos2d::Node* owner = eventData->getOwner();
			return expectedOwner == nullptr || owner == expectedOwner ? eventData : nullptr;
		}

		static cocos2d::PhysicsShape* createVisionPhysicsShape(const cocos2d::Vec2& startPoint, int vertexNum, float visionAngle, float visionRadius);

		static cocos2d::Texture2D* createLoopTexture(const std::string& fileName);

		static std::vector<cocos2d::Vec2> calcCircleIntersection(const cocos2d::Vec2& firstPos, float firstRadius, const cocos2d::Vec2& secondPos, float secondRadius);
		static std::vector<cocos2d::Vec2> calcCircleIntersection(float firstRadius, const cocos2d::Vec2& secondPos, float secondRadius);

		static cocos2d::Vec2 recalculatePoint(const cocos2d::Vec2& base, const cocos2d::Vec2& current, const std::vector<cocos2d::Vec2>& possiblePoints);

		// return calculated point on circle if circle includes sourcePoint
		// otherwise return sourcePoint
		static cocos2d::Vec2 calcPointOnCircle(const cocos2d::Vec2 sourcePoint, const cocos2d::Vec2 circlePosition, float radius);

		static float calcFullAngle(const cocos2d::Vec2& base, const cocos2d::Vec2& point);
		static std::vector<float> calcRotAngles(float mainRadius, const cocos2d::Vec2& circlePosition, float circleRadius, const cocos2d::Vec2& point);
	};

}
#pragma once
#include "cocos2d.h"
#include "json/document.h"

namespace ether_drifters {

	class JsonConvertersUtil {
	private:
		JsonConvertersUtil() {}
	public:
		static cocos2d::Vec2 convertCoordinates(const rapidjson::Value& val);
		static cocos2d::Color3B convertColor(const rapidjson::Value& val);
	private:
		static cocos2d::Vec2 extractAsCartesianCoodrinates(const rapidjson::Value& val);
		static cocos2d::Vec2 extractAsPolarCoordinates(const rapidjson::Value& val);
	};

}
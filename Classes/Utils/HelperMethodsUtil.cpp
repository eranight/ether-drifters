#include "HelperMethodsUtil.h"
#include "Constants.h"
#include "Common/ViewObject.hpp"

using namespace cocos2d;
using namespace ether_drifters;

Vector<Node*> HelperMethodsUtil::fetchNodesByContactMasks(PhysicsContact& contact, int firstMask, int secondMask) {
	PhysicsBody* bodyA = contact.getShapeA()->getBody();
	int catA = bodyA->getCategoryBitmask();
	PhysicsBody* bodyB = contact.getShapeB()->getBody();
	int catB = bodyB->getCategoryBitmask();

	Vector<Node*> nodes(2);
	if ((catA & firstMask) != 0 && (catB & secondMask) != 0) {
		nodes.pushBack(getRightNode(bodyA->getNode()));
		nodes.pushBack(getRightNode(bodyB->getNode()));
	}
	else if ((catA & secondMask) != 0 && (catB & firstMask) != 0) {
		nodes.pushBack(getRightNode(bodyB->getNode()));
		nodes.pushBack(getRightNode(bodyA->getNode()));
	}

	return nodes;
}

Vector<Node*> HelperMethodsUtil::fetchNodesFromPhysicsContact(PhysicsContact& contact, Node* thisNode, int thisNodeMask) {
	int val = contact.getShapeA()->getBody()->getCategoryBitmask() & thisNodeMask;
	if (val != 0) {
		Node* node = getRightNode(contact.getShapeA()->getBody()->getNode());
		if (node == thisNode) {
			return { node, getRightNode(contact.getShapeB()->getBody()->getNode()) };
		}
	}

	val = contact.getShapeB()->getBody()->getCategoryBitmask() & thisNodeMask;
	if (val != 0) {
		Node* node = getRightNode(contact.getShapeB()->getBody()->getNode());
		if (node == thisNode) {
			return { node, getRightNode(contact.getShapeA()->getBody()->getNode()) };
		}
	}

	return {};
}

Node* HelperMethodsUtil::getRightNode(Node* node) {
	if (node->getTag() == DOMAIN_TAG) {
		return node;
	}
	while (node->getParent()->getTag() != DOMAIN_TAG) {
		node = node->getParent();
	}
	return node;
}

Node* HelperMethodsUtil::getViewIfExists(Node* node) {
	Node* viewNode = node->getChildByName(VIEW);
	if (viewNode == nullptr) {
		viewNode = node;
	}
	return viewNode;
}

Size HelperMethodsUtil::getContentSize(Node* node)
{
	return dynamic_cast<ViewObject*>(getViewIfExists(node)->getUserObject())->getContentSize();
}

void HelperMethodsUtil::adjustScaleToSize(Node* node, const Size& size)
{
	node->setScaleX(size.width / node->getContentSize().width);
	node->setScaleY(size.height / node->getContentSize().height);
}

Vector<Node*> HelperMethodsUtil::fetchNodesWithNames(PhysicsContact& contact, const std::string& nameA, const std::string& nameB) {
	Node* nodeA = getRightNode(contact.getShapeA()->getBody()->getNode());
	Node* nodeB = getRightNode(contact.getShapeB()->getBody()->getNode());
	if (nodeA->getName() == nameA && nodeB->getName() == nameB) {
		return Vector<Node*>({ nodeA, nodeB });
	}
	else if (nodeA->getName() == nameB && nodeB->getName() == nameA) {
		return Vector<Node*>({ nodeB, nodeA });
	}
	return Vector<cocos2d::Node*>();
}

Component* HelperMethodsUtil::fetchComponentByName(Node* node, const std::string& name) {
	Component* component = node->getComponent(name);
	if (component == nullptr) {
		Node * child = node->getChildByName(VIEW);
		if (child != nullptr) {
			return child->getComponent(name);
		}
	}
	return component;
}

Vec2 HelperMethodsUtil::randomDirectionToPoint(const Vec2& point, const Vec2& currentPosition, float maxAngle) {
	return randomDirectionToPoint(point, currentPosition, 0.0f, maxAngle);
}

Vec2 HelperMethodsUtil::randomDirectionToPoint(const Vec2& point, const Vec2& currentPosition, float minAngle, float maxAngle) {
	Vec2 direction = (point - currentPosition).getNormalized();
	float angle = randomAngle(minAngle, maxAngle);
	direction.rotate(Vec2::ZERO, angle);
	return direction;
}

float HelperMethodsUtil::randomAngle(float minAngle, float maxAngle)
{
	float minRandomAngle = CC_DEGREES_TO_RADIANS(minAngle);
	float maxRandomAngle = CC_DEGREES_TO_RADIANS(maxAngle);
	float randomSign = RandomHelper::random_int(1, 10) % 2 == 0 ? 1.0 : -1.0f;
	float randomAngle = randomSign * RandomHelper::random_real(minRandomAngle, maxRandomAngle);
	return randomAngle;
}

float HelperMethodsUtil::randomExplicitAngle(float minAngle, float maxAngle)
{
	float minRandomAngle = CC_DEGREES_TO_RADIANS(minAngle);
	float maxRandomAngle = CC_DEGREES_TO_RADIANS(maxAngle);
	float randomAngle = RandomHelper::random_real(minRandomAngle, maxRandomAngle);
	return randomAngle;
}

PhysicsShape* HelperMethodsUtil::createVisionPhysicsShape(const Vec2& startPoint, int vertexNum, float visionAngle, float visionRadius) {
	vertexNum += 3;
	std::vector<Vec2> points(vertexNum);
	points[0] = startPoint; points[0].scale(Vec2(1.0f, 0.0f));
	points[1] = startPoint;
	points[vertexNum - 1] = startPoint; points[vertexNum - 1].scale(Vec2(1.0f, -1.0f));
	float dAngle = CC_DEGREES_TO_RADIANS(visionAngle) / (vertexNum - 4);
	float angle = CC_DEGREES_TO_RADIANS(visionAngle) * 0.5f;
	for (int vertexesCounter = 2; vertexesCounter < vertexNum - 1; ++vertexesCounter, angle -= dAngle) {
		points[vertexesCounter].x = visionRadius * cos(angle);
		points[vertexesCounter].y = visionRadius * sin(angle);
	}
	PhysicsShape* visionShape = PhysicsShapePolygon::create(points.data(), vertexNum, PhysicsMaterial(0.0f, 0.0f, 0.0f));
	return visionShape;
}

static const Texture2D::TexParams LOOP_TEXTURE_PARAMS = { GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT };

Texture2D* HelperMethodsUtil::createLoopTexture(const std::string& fileName) {
	Texture2D* texture = TextureCache::getInstance()->addImage(fileName);
	texture->setTexParameters(LOOP_TEXTURE_PARAMS);
	return texture;
}

std::vector<Vec2> HelperMethodsUtil::calcCircleIntersection(const Vec2& firstPos, float firstRadius, const Vec2& secondPos, float secondRadius) {
	Vec2 secondPosNew = secondPos - firstPos;
	auto result = calcCircleIntersection(firstRadius, secondPosNew, secondRadius);
	for (auto& pos : result) {
		pos += firstPos;
	}
	return result;
}

std::vector<Vec2> HelperMethodsUtil::calcCircleIntersection(float firstRadius, const Vec2& secondPos, float secondRadius) {
	std::vector<Vec2> result;
	result.reserve(2);

	float d = secondPos.length();
	if (d <= MATH_EPSILON || firstRadius + secondRadius < d || abs(firstRadius - secondRadius) > d) {
		return result;
	}

	float sqFirstRadius = firstRadius * firstRadius;
	float a = (sqFirstRadius - secondRadius * secondRadius + d * d) / (2.0f * d);
	float h = sqrtf(sqFirstRadius - a * a);

	Vec2 p3 = secondPos * (a / d);
	if (h <= MATH_EPSILON) {
		result.push_back(p3);
		return result;
	}

	float hd = h / d;
	result.push_back(Vec2(p3.x + hd * secondPos.y, p3.y - hd * secondPos.x));
	result.push_back(Vec2(p3.x - hd * secondPos.y, p3.y + hd * secondPos.x));

	return result;
}

Vec2 HelperMethodsUtil::recalculatePoint(const Vec2& base, const Vec2& current, const std::vector<Vec2>& possiblePoints) {
	if (possiblePoints.empty()) {
		return current;
	}
	Vec2 pointNew = possiblePoints.at(0);
	float minAngle = abs(base.getAngle(pointNew));
	for (int index = 1; index < possiblePoints.size(); ++index) {
		auto point = possiblePoints.at(index);
		float angle = abs(base.getAngle(point));
		if (angle < minAngle) {
			pointNew = point;
			minAngle = angle;
		}
	}
	return pointNew;
}

Vec2 HelperMethodsUtil::calcPointOnCircle(Vec2 sourcePoint, Vec2 circlePosition, float radius) {
	float squaredRadius = radius * radius;
	if ((sourcePoint - circlePosition).lengthSquared() > squaredRadius) {
		return sourcePoint;
	}
	Vec2 m = sourcePoint.getNormalized();
	Vec2 b = circlePosition.dot(m) * m;
	float squaredH = circlePosition.lengthSquared() - b.lengthSquared();
	if (squaredH > squaredRadius) {
		return sourcePoint;
	}
	float factor = 0.0f;
	if (squaredH < MATH_EPSILON) {
		factor = radius;
	}
	else {
		factor = sqrt(squaredRadius - squaredH);
	}
	return m * abs(b.length() - factor);
}

float HelperMethodsUtil::calcFullAngle(const cocos2d::Vec2& base, const cocos2d::Vec2& point) {
	float angle = point.getAngle(base);
	if (point.y < 0.0f) {
		angle += (M_PI - angle) * 2.0f;
	}
	return angle;
}

std::vector<float> HelperMethodsUtil::calcRotAngles(float mainRadius, const Vec2& circlePosition, float circleRadius, const Vec2& point) {
	std::vector<Vec2> points = calcCircleIntersection(mainRadius, circlePosition, circleRadius);
	//circleRadius * circleRadius < (circlePosition - point).lengthSquared()
	if (points.size() <= 1 || !Vec2::isSegmentIntersect(points.at(0), points.at(1), Vec2::ZERO, point)) {
		return {};
	}
	std::vector<float> result;
	for (auto& p : points) {
		float angle = point.getAngle(p);
		result.push_back(angle);
	}
	return result;
}

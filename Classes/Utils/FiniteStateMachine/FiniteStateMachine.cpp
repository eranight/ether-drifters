#include "FiniteStateMachine.h"
#include "State.h"
#include "Transition.h"
#include <algorithm>

using namespace fsm;

FiniteStateMachine::FiniteStateMachine(const FiniteStateMachine& copy) :
	initialState(copy.initialState), currentState(copy.currentState), states(copy.states) {

}

FiniteStateMachine::~FiniteStateMachine() {

}

void FiniteStateMachine::addState(std::shared_ptr<State> state, bool initial) {
	auto iter = std::find_if(states.begin(), states.end(), [&state](std::shared_ptr<State> st) {return st.get() == state.get(); });
	if (iter == states.end()) {
		states.push_back(state);
		if (initial) {
			initialState = state;
		}
	}
}

void FiniteStateMachine::enter() {
	currentState = initialState;
	currentState->enter();
}

void FiniteStateMachine::exit() {
	currentState->exit();
}

void FiniteStateMachine::update() {
	for (auto transition : currentState->getTransitions()) {
		if (transition->isTriggered()) {
			currentState->exit();
			currentState = transition->getTargetState().lock();
			currentState->enter();
			break;
		}
	}
	currentState->update();
}

#include "State.h"
#include "Transition.h"

using namespace fsm;

void State::enter() {
	for (auto transition : transitions) {
		transition->enter();
	}
}

void State::exit() {
	for (auto transition : transitions) {
		transition->exit();
	}
}
#pragma once
#include <vector>
#include <memory>
#include "EntryPoint.h"

namespace fsm {

	class Transition;

	class State : public EntryPoint {
	public:
		State() {}
		virtual ~State() {}
	public:
		virtual void enter() override;
		virtual void exit() override;
		virtual void update() = 0;
		void addTransition(std::shared_ptr<Transition> transition) { transitions.push_back(transition); }
		const std::vector<std::shared_ptr<Transition>>& getTransitions() { return transitions; }
	protected:
		std::vector<std::shared_ptr<Transition>> transitions;
	};

	class CompletableState : public State {
	public:
		virtual bool isCompleted() = 0;
	};

}
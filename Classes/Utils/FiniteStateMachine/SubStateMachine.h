#pragma once
#include <vector>
#include <memory>

#include "State.h"
#include "FiniteStateMachine.h"

namespace fsm {

	class SubStateMachine : public State {
	public:
		SubStateMachine(const FiniteStateMachine& finiteStateMachine) : finiteStateMachine(finiteStateMachine) {}
		virtual ~SubStateMachine() {}
	public:
		virtual void enter() override { State::enter(); finiteStateMachine.enter(); }
		virtual void exit() override { State::exit(); finiteStateMachine.exit(); }
		virtual void update() override { finiteStateMachine.update(); }
	private:
		FiniteStateMachine finiteStateMachine;
	};

}
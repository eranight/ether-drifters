#pragma once
#include <memory>
#include "Condition.h"

namespace fsm {

	class State;

	class Transition : public EntryPoint {
	public:
		Transition(std::shared_ptr<Condition> condition, std::weak_ptr<State> targetState) : condition(condition), targetState(targetState) {}
		virtual ~Transition() {}
	public:
		bool isTriggered() { return condition->test(); }
		std::weak_ptr<State> getTargetState() { return targetState; }
		virtual void enter() override { condition->enter(); }
		virtual void exit() override { condition->exit(); }
	private:
		std::shared_ptr<Condition> condition;
		std::weak_ptr<State> targetState;
	};

}
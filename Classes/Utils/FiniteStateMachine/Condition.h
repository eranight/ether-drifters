#pragma once
#include "EntryPoint.h"
#include "State.h"

#include <memory>
#include <functional>

namespace fsm {

	class Condition : public EntryPoint {
	public:
		virtual ~Condition() {}
		virtual bool test() = 0;
	};

	class ConditionNot : public Condition {
	public:
		ConditionNot(std::shared_ptr<Condition> condition) : condition(condition) {}
		~ConditionNot() {}
		void enter() { condition->enter(); }
		void exit() { condition->exit(); }
		bool test() { return !condition->test(); }
	private:
		std::shared_ptr<Condition> condition;
	};

	class ConditionIf : public Condition {
	public:
		ConditionIf(std::function<bool()> predicate) : predicate(predicate) {}
		~ConditionIf() {}
		void enter() {}
		void exit() {}
		bool test() { return predicate(); }
	private:
		std::function<bool()> predicate;
	};

	class CompletableState;

	class ConditionComplete : public Condition {
	public:
		ConditionComplete(const std::weak_ptr<CompletableState>& state) : state(state) {}
	public:
		void enter() override {}
		void exit() override {}
		bool test() override { return state.lock()->isCompleted(); }
	private:
		std::weak_ptr<CompletableState> state;
	};

}
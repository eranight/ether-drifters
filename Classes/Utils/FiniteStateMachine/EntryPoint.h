#pragma once

namespace fsm {

	class EntryPoint {
	public:
		virtual void enter() = 0;
		virtual void exit() = 0;
	};

}
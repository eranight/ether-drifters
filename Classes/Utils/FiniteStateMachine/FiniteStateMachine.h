#pragma once
#include "EntryPoint.h"

#include<vector>
#include <memory>

namespace fsm {

	class State;

	class FiniteStateMachine : EntryPoint {
	public:
		FiniteStateMachine() {}
		FiniteStateMachine(const FiniteStateMachine& copy);
		~FiniteStateMachine();
	public:
		void enter() override;
		void exit() override;
		void addState(std::shared_ptr<State> state, bool initial = false);
		void update();
	private:
		std::shared_ptr<State> initialState;
		std::shared_ptr<State> currentState;
		std::vector<std::shared_ptr<State>> states;
	};

}
#include "Ether.h"
#include "Prefubs/DomainObjectCreator.h"
#include "Prefubs/Anomalies/AnomalyCreator.h"
#include "Prefubs/Player.h"
#include "Prefubs/Meteor.h"
#include "Prefubs/Skeleton.h"
#include "Prefubs/SimpleMonster.h"
#include "Prefubs/Island.h"
#include "Utils/Constants.h"
#include "Utils/JsonConvertersUtil.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/PortalEventData.h"
#include "Systems/ObstacleSystem.h"
#include "Systems/InputSystem.hpp"
#include "Utils/StringToCocosKeyCodeConverterUtil.hpp"

USING_NS_CC;
using namespace ether_drifters;

static const std::string CLUSTERS_FOLDER_PATH = "clusters/";

Ether * Ether::s_ether = nullptr;
EtherDestroyer Ether::s_destroyer;

EtherDestroyer::~EtherDestroyer() {
	if (ether != nullptr) {
		ether->release();
	}
}

void EtherDestroyer::init(Ether * ether) {
	this->ether = ether;
}

Ether * Ether::getInstance() {
	if (!s_ether) {
		s_ether = new (std::nothrow) Ether();
		CCASSERT(s_ether, "FATAL: Not enough memory");
		s_destroyer.init(s_ether);
	}
	return s_ether;
}

Ether::~Ether() {
	CC_ASSERT(this == s_ether);
	s_ether = nullptr;
}

Scene * Ether::createScene(const rapidjson::Value& val, const std::string& portalId) {

	auto scene = Scene::createWithPhysics();
	auto physicsWorld = scene->getPhysicsWorld();
	//physicsWorld->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	physicsWorld->setGravity(Vec2::ZERO);

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto obstacleSystem = ObstacleSystem::create();
	scene->addChild(obstacleSystem, 0, OBSTACLE_SYSTEM);

	InputSystemConfig config;
	config.actionsOnKeys = actionMaps["domain"];
	auto inputSystem = InputSystem::create(config);
	scene->addChild(inputSystem, 0, INPUT_SYSTEM);

	domain = DomainObjectCreator::createDomain(val);
	float radius = domain->getRadius();
	domain->setPosition(origin + Vec2(radius, radius));
	scene->addChild(domain, 0, val["id"].GetString());

	int portalLevel = static_cast<int>(DomainObjectLevel::portals);

	Portal* pivotPortal = nullptr;
	if (val.HasMember("portals")) {
		auto portals = val["portals"].GetArray();
		for (rapidjson::SizeType index = 0; index < portals.Size(); ++index) {
			Portal* portal = DomainObjectCreator::createPortal(portals[index]);
			std::string id = addToDomain(portal, portals[index], portalLevel);
			if (id == portalId) {
				pivotPortal = portal;
			}
		}
	}

	if (val.HasMember("anomalies")) {
		auto anomalies = val["anomalies"].GetArray();
		for (rapidjson::SizeType index = 0; index < anomalies.Size(); ++index) {
			Node* anomaly = AnomalyCreator::create(anomalies[index]);
			addToDomain(anomaly, anomalies[index], portalLevel);
		}
	}

	if (val.HasMember("islands")) {
		auto islands = val["islands"].GetArray();
		for (rapidjson::SizeType index = 0; index < islands.Size(); ++index) {
			Node* island = Island::create();
			addToDomain(island, islands[index], portalLevel);
		}
	}

	if (val.HasMember("mineds")) {
		auto mineds = val["mineds"].GetArray();
		for (rapidjson::SizeType index = 0; index < mineds.Size(); ++index) {
			Node* mined = nullptr;
			std::string type = mineds[index]["type"].GetString();
			if (type == "meteor") {
				mined = DomainObjectCreator::createMeteor(mineds[index]);
			}
			else if (type == "skeleton") {
				mined = DomainObjectCreator::createSkeleton(mineds[index]);
			}
			else {
				std::string assertMessage = "unknown type of mineds " + type;
				CCASSERT(false, assertMessage.c_str());
			}
			addToDomain(mined, mineds[index], portalLevel);
		}
	}

	if (val.HasMember("enemies")) {
		auto enemies = val["enemies"].GetArray();
		for (rapidjson::SizeType index = 0; index < enemies.Size(); ++index) {
			Node* enemy = DomainObjectCreator::createSimpleMonster(enemies[index]);
			addToDomain(enemy, enemies[index], static_cast<int>(DomainObjectLevel::enemies));
		}
	}

	PlayerConfig playerConfig;
	playerConfig.velocity = 120.0f;
	playerConfig.acceleration = 50.0f;
	playerConfig.rotation = MATH_DEG_TO_RAD(45.0f);
	playerConfig.hp = std::make_pair<double, double>(1600.0, 1600.0);
	playerConfig.damage = 30.0;
	playerConfig.lifeTime = 5.0f;
	playerConfig.shellVelocity = 300.0f;
	playerConfig.cooldown = 2.0f * std::powf(1 - 5.0f / 100.0f, 10); // ~1.2
	playerConfig.shellViewFilename = "particles/fire_ball.plist";
	playerConfig.visionAreaRadius = 400.0f;
	playerConfig.miningAreaRadius = 100.0f;
	playerConfig.miningRate = 3.0f * std::powf(1 - 3.0f / 100.0f, 10); // ~2.21
	playerConfig.miningPower = 3.0;
	playerConfig.repairPower = 3.0;
	playerConfig.repairInterval = 4.0f * std::powf(1 - 5.0f / 100.0f, 10); // ~2.4
	playerConfig.repairDelay = 3.0f;
	playerConfig.crew = std::make_pair<int, int>(10, 10);
	playerConfig.crewRepairBonus = 5;
	playerConfig.crewCanonBonus = 5;
	playerConfig.crewMiningBonus = 3;
	playerConfig.crewLooseChances = std::map<double, double>({ { 75, 10 }, { 50, 20 }, { 25, 30 } });
	Player* player = Player::create(playerConfig);
	if (pivotPortal != nullptr) {
		Vec2 portalPos = pivotPortal->getPosition();
		auto portalSize = HelperMethodsUtil::getContentSize(pivotPortal);
		auto playerSize = HelperMethodsUtil::getContentSize(player);

		float playerPosLength = (std::max(portalSize.width, portalSize.height) + std::max(playerSize.width, playerSize.height)) * 0.5f;

		portalPos.scale((portalPos.length() - playerPosLength) / portalPos.length());
		player->setPosition(portalPos);
	}
	else {
		player->setPosition(Vec2::ZERO);
	}
	domain->addChild(player, static_cast<int>(DomainObjectLevel::player), PLAYER);

	Vec2 playerWorldPosition = domain->convertToWorldSpace(player->getPosition());
	auto camera = Camera::create();
	camera->setPosition(playerWorldPosition);
	camera->setCameraFlag(CameraFlag::USER1);
	scene->addChild(camera, 0, PLAYER_CAMERA_NAME);

	camera = Camera::create();
	camera->setPosition(playerWorldPosition);
	camera->setCameraFlag(CameraFlag::USER2);
	scene->addChild(camera, 0, DOMAIN_UI_CAMERA_NAME);

	// schedule once to overwrite camera mask of physics world
	auto setPhysicsCameraMaskCallback = [scene](float) {
		auto iter = std::find_if(scene->getChildren().begin(), scene->getChildren().end(), [](Node* child) { return dynamic_cast<DrawNode*>(child) != nullptr; });
		if (iter != scene->getChildren().end()) {
			(*iter)->setCameraMask(DOMAIN_CAMERA_MASK);
		}
	};
	scene->getScheduler()->schedule(setPhysicsCameraMaskCallback, scene, 0.2f, 1, 0.0f, false, ";sdjgf;gtenrtkkjg");

	return scene;
}

std::string Ether::addToDomain(Node* node, const rapidjson::Value& val, int localZ) {
	Vec2 position = JsonConvertersUtil::convertCoordinates(val["coordinates"]);
	node->setPosition(position);
	std::string id = val["id"].GetString();
	domain->addChild(node, localZ, id);
	return id;
}

void Ether::init(const rapidjson::Value& val) {
	auto clustersArr = val["ether"]["clusters"].GetArray();
	for (rapidjson::SizeType clusterIndex = 0; clusterIndex < clustersArr.Size(); ++clusterIndex) {
		std::string clusterId = clustersArr[clusterIndex]["id"].GetString();
		clusters[clusterId] = std::set<std::string>();
		auto domainsArr = clustersArr[clusterIndex]["domains"].GetArray();
		for (rapidjson::SizeType domainIndex = 0; domainIndex < domainsArr.Size(); ++domainIndex) {
			std::string domainId = domainsArr[domainIndex].GetString();
			clusters[clusterId].insert(domainId);
		}
	}
	EventListenerCustom* portalEventListener = EventListenerCustom::create(PORTAL_EVENT, [this](EventCustom* custom) {
		PortalEventData* eventData = reinterpret_cast<PortalEventData*>(custom->getUserData());
		auto pp = eventData->getPortalPath();
		loadDomain(pp.cluster, pp.domain, pp.portal);
	});
	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(portalEventListener, 0xfffff);

	if (val["ether"].HasMember("action_maps")) {
		auto actionMaps = val["ether"]["action_maps"].GetArray();
		for (rapidjson::SizeType actionMapIndex = 0; actionMapIndex < actionMaps.Size(); ++actionMapIndex) {
			std::string target = actionMaps[actionMapIndex]["target"].GetString();
			auto actions = actionMaps[actionMapIndex]["actions"].GetArray();
			for (rapidjson::SizeType actionIndex = 0; actionIndex < actions.Size(); ++actionIndex) {
				std::string name = actions[actionIndex]["name"].GetString();
				auto codes = actions[actionIndex]["codes"].GetArray();
				for (rapidjson::SizeType codeIndex = 0; codeIndex < codes.Size(); ++codeIndex) {
					std::string code = codes[codeIndex].GetString();
					auto keyCode = StringToCocosKeyCodeConverterUtil::convert(code);
					this->actionMaps[target][name].insert(keyCode);
				}
			}
		}
	}

}

void Ether::loadDomain(const std::string& cluster, const std::string& domain, const std::string& portal) {
	std::string domainFileFullPath = CLUSTERS_FOLDER_PATH + cluster + "/" + domain + ".json";
	std::string domainAsString = FileUtils::getInstance()->getStringFromFile(domainFileFullPath);
	rapidjson::Document domainAsDocument;
	domainAsDocument.Parse(domainAsString.c_str());
	if (domainAsDocument.HasParseError()) {
		std::string errorString = "some errors occurred during parsing file " + domainFileFullPath;
		CCASSERT(!domainAsDocument.HasParseError(), errorString.c_str());
	}
	
	auto director = Director::getInstance();
	Scene* scene = createScene(domainAsDocument, portal);
	Director::getInstance()->replaceScene(scene);
}
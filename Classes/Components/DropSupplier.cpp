#include "DropSupplier.h"
#include "Events/EventData.h"
#include "Prefubs/Drop.h"
#include "Ether.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string DropSupplier::NAME = "loot_supplier";

DropSupplier* DropSupplier::create(const DropSupplierConfig& config) {
	auto ref = new (std::nothrow) DropSupplier(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

DropSupplier::~DropSupplier() {
	CC_SAFE_RELEASE_NULL(eventListener);
}

bool DropSupplier::init() {
	if (!Component::init()) {
		return false;
	}

	eventListener = EventListenerCustom::create(config.eventName, [this] (EventCustom* custom) {
		EventData* eventData = reinterpret_cast<EventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner) {
			return;
		}
		generateDrop(1);
	});
	eventListener->retain();

	setName(NAME);
	return true;
}

void DropSupplier::onEnter() {
	Component::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(eventListener, 1);
}

void DropSupplier::onExit() {
	Component::onExit();

	_owner->getEventDispatcher()->removeEventListener(eventListener);
}

void DropSupplier::generateDrop(int num) {
	Node* domain = Ether::getInstance()->getCurrentDomain();
	Node* view = HelperMethodsUtil::getViewIfExists(_owner);
	for (int count = 0; count < num; ++count) {
		Vec2 point = domain->convertToNodeSpace(view->convertToWorldSpace(config.points[nextPointIndex]));
		std::string spriteFrame = config.spriteFrames[RandomHelper::random_int<ssize_t>(0, config.spriteFrames.size() - 1)];
		DropConfig dropConfig;
		dropConfig.type = config.type;
		dropConfig.quantity = config.quantity;
		dropConfig.lifeTime = config.lifeTime;
		dropConfig.physicsBodyDelay = config.physicsBodyDelay;
		dropConfig.spriteFrame = spriteFrame;
		Drop* drop = Drop::create(dropConfig);
		drop->setPosition(point);
		domain->addChild(drop, static_cast<int>(DomainObjectLevel::drop));

		nextPointIndex = (nextPointIndex + 1) % config.points.size();
	}
}
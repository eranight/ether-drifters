#include "DropController.h"
#include "Dropped.h"
#include "Events/PickedEventData.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

DropController* DropController::create(const DropControllerConfig& config) {
	auto ref = new (std::nothrow) DropController(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

DropController::~DropController() {
	CC_SAFE_RELEASE_NULL(pickedEventListener);
}

bool DropController::init() {
	if (!Controller::init()) {
		return false;
	}

	pickedEventListener = EventListenerCustom::create(PICKED_DROP_EVENT, [this](EventCustom* custom) {
		PickedEventData* eventData = reinterpret_cast<PickedEventData*>(custom->getUserData());
		if (eventData->getDropped()->getOwner() != _owner) {
			return;
		}
		_owner->runAction(RemoveSelf::create());
	});
	pickedEventListener->retain();

	return true;
}

void DropController::onEnter() {
	Controller::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(pickedEventListener, 1);

	Node* view = HelperMethodsUtil::getViewIfExists(_owner);

	if (config.offsetY > 0.0f) {
		float y = RandomHelper::random_real<float>(0.0f, 1.0f) * config.offsetY  - config.offsetY;
		view->setPosition(Vec2(0.0f, y));
		view->runAction(
			Sequence::createWithTwoActions(
				MoveTo::create((config.offsetY - y) / (config.offsetY * 2.0f), Vec2(0.0f, config.offsetY * 2)),
				CallFuncN::create([this](Node* vw) {
					vw->runAction(
						RepeatForever::create(
							Sequence::createWithTwoActions(
								MoveTo::create(1.0f, Vec2(0.0f, -config.offsetY * 2)),
								MoveTo::create(1.0f, Vec2(0.0f, config.offsetY * 2))
							)
						)
					);
				})
			)
		);
	}

	if (config.lifeTime > -1.0) {
		FiniteTimeAction* beforeRemoveAction = nullptr;
		if (config.fadeEffect) {
			beforeRemoveAction = Sequence::createWithTwoActions(
				DelayTime::create(config.lifeTime * 0.8),
				FadeOut::create(config.lifeTime * 0.2f)
			);
		}
		else {
			beforeRemoveAction = DelayTime::create(config.lifeTime);
		}
		view->runAction(
			Sequence::createWithTwoActions(
				beforeRemoveAction,
				RemoveSelf::create()
			)
		);
	}

	if (config.physicsBodyDelay > 0.0f) {
		view->getPhysicsBody()->setEnabled(false);
		view->runAction(
			Sequence::createWithTwoActions(
				DelayTime::create(config.physicsBodyDelay),
				CallFuncN::create([](Node* n) { n->getPhysicsBody()->setEnabled(true); })
			)
		);
	}

}

void DropController::onExit() {
	Controller::onExit();
	_owner->getEventDispatcher()->removeEventListener(pickedEventListener);
	HelperMethodsUtil::getViewIfExists(_owner)->stopAllActions();
}
#include "Mining.h"
#include "Utils/HelperMethodsUtil.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Mining::NAME = "mining";

Mining* Mining::create(const MiningConfig& config) {
	auto ref = new (std::nothrow) Mining(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

Mining::~Mining() {
	CC_SAFE_RELEASE_NULL(physicsListener);
	CC_SAFE_RELEASE_NULL(wastedMinedEventListener);
	CC_SAFE_RELEASE_NULL(startMiningEventListener);
	CC_SAFE_RELEASE_NULL(stopdMiningEventListener);
}

bool Mining::init() {
	if (!Component::init()) {
		return false;
	}

	physicsListener = EventListenerPhysicsContact::create();
	if (physicsListener == nullptr) {
		return false;
	}
	physicsListener->onContactBegin = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesByContactMasks(contact, ContactMask::mining, ContactMask::mined);
		if (nodes.empty() || nodes.at(0) == nodes.at(1) || nodes.at(0) != _owner) {
			return false;
		}
		Mined* mined = HelperMethodsUtil::findComponent<Mined>(nodes.at(1));
		if (mined != nullptr && !mined->isEmpty()) {
			addMined(mined);
			return true;
		}
		return false;
	};
	physicsListener->onContactSeparate = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesFromPhysicsContact(contact, _owner, ContactMask::mining);
		if (nodes.empty()) {
			return;
		}
		Mined* mined = HelperMethodsUtil::findComponent<Mined>(nodes.at(1));
		if (mined != nullptr) {
			removeMined(mined, InterruptMiningReason::unfocused, false);
		}
	};
	physicsListener->retain();

	wastedMinedEventListener = EventListenerCustom::create(WASTE_MINED_RESOURCES_EVENT, [this](EventCustom* custom) {
		MinedEventData* eventData = reinterpret_cast<MinedEventData*>(custom->getUserData());
		removeMined(eventData->getMined(), InterruptMiningReason::wasted, true);
	});
	wastedMinedEventListener->retain();

	startMiningEventListener = EventListenerCustom::create(START_MINING_EVENT, [this](EventCustom* custom) {
		MinedEventData* minedEventData = reinterpret_cast<MinedEventData*>(custom->getUserData());
		if (minedEventData == nullptr) {
			CCLOG("received object is not Mined");
			return;
		}
		if (minedEventData->getOwner() != _owner) {
			return;
		}
		Mined* mined = minedEventData->getMined();
		int count = std::count(possibleMineds.begin(), possibleMineds.end(), mined);
		if (count > 0) {
			possibleMineds.pushBack(currentMined);
			currentMined.clear();
			for (int iter = 0; iter < count; ++iter) {
				currentMined.pushBack(mined);
			}
			possibleMineds.eraseObject(mined, true);
			CCLOG("start mining 0x%x", mined);
			setMined(mined, InterruptMiningReason::switched);
		}
	});
	startMiningEventListener->retain();

	stopdMiningEventListener = EventListenerCustom::create(WASTE_MINED_RESOURCES_EVENT, [this](EventCustom* custom) {
		MinedEventData* minedEventData = reinterpret_cast<MinedEventData*>(custom->getUserData());
		if (minedEventData == nullptr) {
			CCLOG("received object is not Mined");
			return;
		}
		if (minedEventData->getOwner() != _owner) {
			return;
		}
		Mined* mined = minedEventData->getMined();
		if (this->mined == mined) {
			possibleMineds.pushBack(currentMined);
			currentMined.clear();
			CCLOG("stop mining 0x%x", mined);
			setMined(nullptr, InterruptMiningReason::switched);
		}
	});
	stopdMiningEventListener->retain();

	setName(NAME);
	return true;
}

void Mining::onEnter() {
	Component::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithSceneGraphPriority(physicsListener, _owner);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(wastedMinedEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(startMiningEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(stopdMiningEventListener, 1);
}

void Mining::onExit() {
	Component::onExit();

	_owner->getEventDispatcher()->removeEventListener(physicsListener);
	_owner->getEventDispatcher()->removeEventListener(wastedMinedEventListener);
	_owner->getEventDispatcher()->removeEventListener(startMiningEventListener);
	_owner->getEventDispatcher()->removeEventListener(stopdMiningEventListener);

	_owner->getScheduler()->unschedule(schedule_selector(Mining::mining), this);
}

void Mining::addMined(Mined* mined) {
	if (this->mined == mined) {
		currentMined.pushBack(mined);
		return;
	}
	bool needToDispatchEvent = possibleMineds.find(mined) == possibleMineds.end();
	possibleMineds.pushBack(mined);
	if (needToDispatchEvent) {
		CCLOG("add mined 0x%0x", mined);
		// generate FocusNewMinedEvent
		MinedEventData eventData(_owner, mined);
		_owner->getEventDispatcher()->dispatchCustomEvent(FOCUS_MINED_EVENT, &eventData);
	}
}

void Mining::removeMined(Mined* mined, InterruptMiningReason reason, bool removeAll) {
	if (this->mined == mined) {
		currentMined.eraseObject(mined);
		if (currentMined.empty()) {
			CCLOG("remove mined 0x%0x", mined);
			setMined(nullptr, reason);
		}
		return;
	}
	int count = std::count(possibleMineds.begin(), possibleMineds.end(), mined);
	if (count > 0) {
		possibleMineds.eraseObject(mined);
		if (count == 1) {
			CCLOG("remove mined 0x%0x", mined);
			// generate UnfocusMinedEvent
			MinedEventData eventData(_owner, mined);
			_owner->getEventDispatcher()->dispatchCustomEvent(UNFOCUS_MINED_EVENT, &eventData);
		}
	}
}

void Mining::setMined(Mined* mined, InterruptMiningReason reason) {
	if (this->mined != nullptr) {
		if (mined == nullptr) {
			_owner->getScheduler()->unschedule(schedule_selector(Mining::mining), this);
		}
		// generate InterruptMiningEvent
		InterruptMiningEventData eventData(_owner, this->mined, reason);
		_owner->getEventDispatcher()->dispatchCustomEvent(INTERRUPT_MINING_EVENT, &eventData);
	}
	this->mined = mined;
	if (this->mined != nullptr) {
		// this code will reiniting scheduler if it exists
		_owner->getScheduler()->schedule(schedule_selector(Mining::mining), this, config.rate, CC_REPEAT_FOREVER, config.rate, false);
	}
}

void Mining::setRate(float rate) {
	if (std::abs(config.rate - rate) > MATH_EPSILON) {
		dirty = true;
		config.rate = rate;
	}
}

void Mining::mining(float dt) {
	if (mined == nullptr) {
		return;
	}

	Mined* localMined = mined;
	MinedResources minedResources = localMined->mining(config.power);
	CCLOG("mining %d", minedResources.getPoints());
	MinedResourcesEvent eventData(_owner, localMined, &minedResources);
	_owner->getEventDispatcher()->dispatchCustomEvent(MINED_RESOURCES_EVENT, &eventData);

	if (mined != nullptr && dirty) {
		dirty = false;
		_owner->getScheduler()->schedule(schedule_selector(Mining::mining), this, config.rate, CC_REPEAT_FOREVER, config.rate, false);
	}
}
#include "Crew.h"
#include "Health.h"
#include "Dropped.h"
#include "Regeneration.h"
#include "Mining.h"
#include "Events/CrewEventData.h"
#include "Events/HealthEventData.h"
#include "Events/PickedEventData.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Crew::NAME = "crew";

Crew* Crew::create(const CrewConfig& config) {
	auto ref = new (std::nothrow) Crew(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

Crew::~Crew() {
	CC_SAFE_RELEASE_NULL(damageEventListener);
	CC_SAFE_RELEASE_NULL(pickedDropEventListener);
}

bool Crew::init() {
	if (!Component::init()) {
		return false;
	}

	damageEventListener = EventListenerCustom::create(TAKING_DAMAGE_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner || config.currentQuantity == 0) {
			return;
		}
		takeDamage(eventData->getHealthInfo());
	});
	damageEventListener->retain();

	pickedDropEventListener = EventListenerCustom::create(PICKED_DROP_EVENT, [this](EventCustom* custom) {
		PickedEventData* eventData = reinterpret_cast<PickedEventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner || eventData->getDrop()->type != DropType::survivor) {
			return;
		}
		changeQuantity(eventData->getDrop());
	});
	pickedDropEventListener->retain();

	setName(NAME);
	return true;
}

void Crew::onEnter() {
	Component::onEnter();
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(damageEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(pickedDropEventListener, 1);
}

void Crew::onExit() {
	Component::onExit();
	_owner->getEventDispatcher()->removeEventListener(damageEventListener);
	_owner->getEventDispatcher()->removeEventListener(pickedDropEventListener);
}

void Crew::takeDamage(HealthInfo* info) {
	double maxHP = info->maxHitPoints;
	double currentHP = info->currentHitPoints;
	auto iter = std::find_if(config.looseCrewChances.rbegin(), config.looseCrewChances.rend(), [maxHP, currentHP](auto& pair) {
		return maxHP * pair.first * 0.01 >= currentHP;
	});
	if (iter != config.looseCrewChances.rend()) {
		double chance = iter->second;
		int random = RandomHelper::random_int<int>(1, 100);
		if (random <= chance) {
			int prevQuantity = config.currentQuantity--;
			recalculateBonuses(-1);
			CrewEventData eventData(_owner, prevQuantity, config.currentQuantity);
			_owner->getEventDispatcher()->dispatchCustomEvent(CHANGED_CREW_QUANTITY_EVENT, &eventData);
		}
	}
}

void Crew::changeQuantity(PickedResource* drop) {
	if (config.currentQuantity == config.maxQuantity) {
		return;
	}
	int prevQuantity = config.currentQuantity++;
	recalculateBonuses(1);
	CrewEventData eventData(_owner, prevQuantity, config.currentQuantity);
	_owner->getEventDispatcher()->dispatchCustomEvent(CHANGED_CREW_QUANTITY_EVENT, &eventData);
}

void Crew::recalculateBonuses(int num) {
	auto regeneration = HelperMethodsUtil::findComponent<Regeneration>(_owner);
	if (regeneration != nullptr) {
		float newInterval = calc(regeneration->getInterval(), config.repairRateBonus, num);
		regeneration->setInterval(newInterval);
	}
	auto mining = HelperMethodsUtil::findComponent<Mining>(_owner);
	if (mining != nullptr) {
		float newRate = calc(mining->getRate(), config.miningRateBonus, num);
		mining->setRate(newRate);
	}
}

float ether_drifters::Crew::calc(float src, float bonus, int num) {
	if (num > 0) {
		return src * std::powf(1 - bonus / 100.0f, num);
	}
	else {
		return src * std::powf(100 / (100 - bonus), -num);
	}
}

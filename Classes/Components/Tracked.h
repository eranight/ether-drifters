#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class TrackedInfo {
	public:
		TrackedInfo(bool reachable) : reachable(reachable) {}
	public:
		bool isReachable() { return reachable; }
	private:
		bool reachable;
	};

	class Tracked : public cocos2d::Component {
	public:
		CREATE_FUNC(Tracked);
		virtual ~Tracked();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	public:
		bool isReachable() { return reachable; }
		TrackedInfo track();
	private:
		bool reachable;
		cocos2d::EventListenerCustom* deathEventListener = nullptr;
		cocos2d::EventListenerCustom* portalEventListener = nullptr;
		cocos2d::EventListenerCustom* outOfDomainEventListener = nullptr;
	public:
		static const std::string NAME;
	};

}
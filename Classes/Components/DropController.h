#pragma once
#include "Controller.h"

namespace ether_drifters {

	class DropControllerConfig {
	public:
		float offsetY;
		float lifeTime;
		bool fadeEffect;
		float physicsBodyDelay;
	};

	class DropController : public Controller {
	private:
		DropController(const DropControllerConfig& config) :
			config(config),
			pickedEventListener(nullptr)
		{}
	public:
		static DropController* create(const DropControllerConfig& config);
		~DropController();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		DropControllerConfig config;
		cocos2d::EventListenerCustom* pickedEventListener;
	};

}
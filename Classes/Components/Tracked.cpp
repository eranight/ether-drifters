#include "Tracked.h"
#include "Events/TrackedEventData.h"
#include "Events/HealthEventData.h"
#include "Events/PortalEventData.h"
#include "Events/DomainEventData.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Tracked::NAME = "tracked";

Tracked::~Tracked() {
	CC_SAFE_RELEASE_NULL(deathEventListener);
	CC_SAFE_RELEASE_NULL(portalEventListener);
	CC_SAFE_RELEASE_NULL(outOfDomainEventListener);
}

bool Tracked::init() {
	if (!Component::init()) {
		return false;
	}

	reachable = true;

	deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() == _owner) {
			reachable = false;
			TrackedEventData eventData(_owner, this);
			_owner->getEventDispatcher()->dispatchCustomEvent(UNREACHABLE_TRACKED_EVENT, &eventData);
		}
	});
	deathEventListener->retain();

	portalEventListener = EventListenerCustom::create(PORTAL_EVENT, [this](EventCustom* custom) {
		PortalEventData* eventData = reinterpret_cast<PortalEventData*>(custom->getUserData());
		if (eventData->getPortedNode() == _owner) {
			TrackedEventData eventData(_owner, this);
			_owner->getEventDispatcher()->dispatchCustomEvent(UNREACHABLE_TRACKED_EVENT, &eventData);
		}
	});
	portalEventListener->retain();

	outOfDomainEventListener = EventListenerCustom::create(END_TOUCH_DOMAIN_BORDER_EVENT, [this](EventCustom* custom) {
		DomainEventData* eventData = reinterpret_cast<DomainEventData*>(custom->getUserData());
		if (eventData->getConnectedNode() == _owner) {
			if (eventData->isInside()) {
				reachable = true;
			}
			else {
				reachable = false;
				TrackedEventData eventData(_owner, this);
				_owner->getEventDispatcher()->dispatchCustomEvent(UNREACHABLE_TRACKED_EVENT, &eventData);
			}
		}
	});
	outOfDomainEventListener->retain();

	setName(NAME);
	return true;
}

void Tracked::onEnter() {
	Component::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(portalEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(outOfDomainEventListener, 1);
}

void Tracked::onExit() {
	Component::onExit();

	_owner->getEventDispatcher()->removeEventListener(deathEventListener);
	_owner->getEventDispatcher()->removeEventListener(portalEventListener);
	_owner->getEventDispatcher()->removeEventListener(outOfDomainEventListener);
}

TrackedInfo Tracked::track() {
	return TrackedInfo(reachable);
}
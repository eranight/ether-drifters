#pragma once
#include "cocos2d.h"
#include "Dropped.h"

namespace ether_drifters {

	class Picker : public cocos2d::Component {
	public:
		CREATE_FUNC(Picker);
		~Picker();
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		cocos2d::Vector<Dropped*> contactedDroppeds;
		cocos2d::EventListenerPhysicsContact* contactEventListener;
	public:
		static const std::string NAME;
	};

}
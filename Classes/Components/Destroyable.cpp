#include "Destroyable.h"
#include "Health.h"
#include "Events/HealthEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Destroyable::NAME = "destroyable";

Destroyable* Destroyable::create(const std::function<void()>& destroyCallback)
{
	auto ret = new (std::nothrow) Destroyable();
	if (ret != nullptr && ret->init(destroyCallback)) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

Destroyable::~Destroyable() {
	CC_SAFE_RELEASE_NULL(deathEventListener);
}

bool Destroyable::init(const std::function<void()>& destroyCallback) {
	if (!Component::init()) {
		return false;
	}

	this->destroyCallback = destroyCallback;

	deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner) {
			return;
		}
		this->destroyCallback();
	});
	deathEventListener->retain();

	setName(NAME);
	return true;
}

void Destroyable::onEnter() {
	Component::onEnter();
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
}

void Destroyable::onExit() {
	Component::onExit();
	_owner->getEventDispatcher()->removeEventListener(deathEventListener);
}

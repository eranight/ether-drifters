#pragma once
#include "Controller.h"

namespace ether_drifters {

	class ShellControllerConfig {
	public:
		std::function<void()> lifeTimeOverReaction = nullptr;
		std::function<void(cocos2d::Node*)> touchBorderReaction = nullptr;
		std::function<void(cocos2d::Node*)> touchNodeReaction = nullptr;
		float lifeTime = 0.0f;
		float velocity = 0.0f;
		cocos2d::Vec2 direction;
		cocos2d::Node* target = nullptr;
	};

	class AbstractShellController : public Controller {
	public:
		AbstractShellController(const ShellControllerConfig& config) : config(config) {}
		virtual ~AbstractShellController() {}
	protected:
		ShellControllerConfig config;
	};

	class SimpleShellController : public AbstractShellController {
	public:
		static SimpleShellController* create(const ShellControllerConfig& config);
		SimpleShellController(const ShellControllerConfig& config) : AbstractShellController(config) {}
		virtual ~SimpleShellController();
		bool init() override;
		void onEnter();
		void onExit();
	private:
		cocos2d::EventListenerCustom* domainBorderListener;
		cocos2d::EventListenerCustom* damageEventListener;
		cocos2d::Action* lifeTimeAction;
	};

}
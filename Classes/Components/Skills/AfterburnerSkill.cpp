#include "AfterburnerSkill.hpp"
#include "Components/Movement.h"
#include "Events/EventData.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string AfterburnerSkill::NAME = "afterburner_skill";

AfterburnerSkill* AfterburnerSkill::create(const AfterburnerSkillConfig& config)
{
	auto ret = new (std::nothrow) AfterburnerSkill(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool AfterburnerSkill::init()
{
	if (!Component::init()) {
		return false;
	}

	setName(NAME);
	return true;
}

void AfterburnerSkill::update(float dt)
{
	Component::update(dt);

	if (!running) {
		return;
	}

	timer -= dt;
	if (timer <= 0.0f) {
		timer = 0.0f;
		ensureStopping();
		return;
	}

	if (timer > startSlowDownTime) {
		accelerate(dt);
	}
	else {
		slowDown(dt);
	}
}

void AfterburnerSkill::run()
{
	if (running) {
		return;
	}

	timer = config.duration;
	startSlowDownTime = (config.velocity - config.endVelocity) / config.acceleration;

	if (movement == nullptr) {
		movement = HelperMethodsUtil::findComponent<Movement>(_owner);
	}

	EventData eventData{ _owner };
	_owner->getEventDispatcher()->dispatchCustomEvent(START_AFTERBURNER_SKILL, &eventData);

	running = true;
}

void AfterburnerSkill::stop()
{
	if (!running) {
		return;
	}

	timer = clampf(timer, 0.0f, startSlowDownTime);
}

void AfterburnerSkill::ensureStopping()
{
	if (movement == nullptr) {
		movement = HelperMethodsUtil::findComponent<Movement>(_owner);
	}
	movement->setVelocity(config.endVelocity);

	EventData eventData{ _owner };
	_owner->getEventDispatcher()->dispatchCustomEvent(STOP_AFTERBURNER_SKILL, &eventData);

	running = false;
}

void AfterburnerSkill::accelerate(float dt)
{
	float currentVelocity = clampf(movement->getVelocity(), 0.0f, config.velocity);
	if (currentVelocity < config.velocity) {
		float nextVelocity = currentVelocity + config.acceleration * dt;
		nextVelocity = clampf(nextVelocity, 0.0f, config.velocity);
		movement->setVelocity(nextVelocity);
	}
	else if (currentVelocity > config.velocity) {
		movement->setVelocity(config.velocity);
	}
}

void AfterburnerSkill::slowDown(float dt)
{
	float currentVelocity = clampf(movement->getVelocity(), config.endVelocity, config.velocity);
	if (currentVelocity > config.endVelocity) {
		float nextVelocity = currentVelocity - config.acceleration * dt;
		nextVelocity = clampf(nextVelocity, config.endVelocity, config.velocity);
		movement->setVelocity(nextVelocity);
	}
	else if (currentVelocity < config.endVelocity) {
		movement->setVelocity(config.endVelocity);
	}
}

#pragma once

#include "cocos2d.h"
#include "Common/RunnableSkill.hpp"
#include "Modification/Modification.h"

namespace ether_drifters
{

	class PlayerController;
	class Direction;
	class Movement;

	class SharpReversalSkill : public cocos2d::Component, public RunnableSkill {
	private:
		SharpReversalSkill(float rotVelocity) :
			rotVelocity(rotVelocity) {}
	public:
		static SharpReversalSkill* create(float rotVelocity);
		bool init() override;
		void update(float dt) override;
	public:
		void run() override;
		void stop() override;
		bool isRunning() override { return running; }
	private:
		bool running = false;
		float rotVelocity = 0.0f;
		float rotTimer = 0.0f;
		Direction* direction = nullptr;
		Movement* movement = nullptr;
	public:
		static const std::string NAME;
	};

	static const std::string START_SHARP_REVERSAL_SKILL = "start_sharp_reversal_skill";
	static const std::string STOP_SHARP_REVERSAL_SKILL = "stop_sharp_reversal_skill";

}
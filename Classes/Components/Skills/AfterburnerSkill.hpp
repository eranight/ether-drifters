#pragma once

#include "cocos2d.h"
#include "Common/RunnableSkill.hpp"

namespace ether_drifters
{

	class Movement;

	class AfterburnerSkillConfig {
	public:
		float velocity;
		float acceleration;
		float duration;
		float endVelocity;
	};

	class AfterburnerSkill : public cocos2d::Component, public RunnableSkill {
	private:
		AfterburnerSkill(const AfterburnerSkillConfig& config) : config(config) {}
	public:
		static AfterburnerSkill* create(const AfterburnerSkillConfig& config);
		bool init() override;
		void update(float dt) override;
	public:
		void run() override;
		void stop() override;
		bool isRunning() override { return running; }
	private:
		void ensureStopping();
		void accelerate(float dt);
		void slowDown(float dt);
	private:
		AfterburnerSkillConfig config;

		float timer = 0.0f;
		float startSlowDownTime = 0.0f;

		bool running = false;

		Movement* movement = nullptr;
	public:
		static const std::string NAME;
	};

	static const std::string START_AFTERBURNER_SKILL = "start_afterburner_skill";
	static const std::string STOP_AFTERBURNER_SKILL = "stop_afterburner_skill";

}

#include "SharpReversalSkill.hpp"
#include "Components/Direction.h"
#include "Components/Movement.h"
#include "Events/EventData.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string SharpReversalSkill::NAME = "sharp_reversal_skill";

SharpReversalSkill* SharpReversalSkill::create(float rotVelocity)
{
	auto ret = new (std::nothrow) SharpReversalSkill(rotVelocity);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool SharpReversalSkill::init()
{
	if (!Component::init()) {
		return false;
	}

	setName(NAME);
	return true;
}

void SharpReversalSkill::update(float dt)
{
	Component::update(dt);
	
	if (!running) {
		return;
	}

	movement->setDirection(direction->getDirection());

	rotTimer -= dt;
	if (rotTimer <= 0.0f) {
		rotTimer = 0.0f;
		stop();
	}

}

void SharpReversalSkill::run()
{
	if (running) {
		return;
	}
	
	rotTimer = 2 * M_PI / rotVelocity;

	if (direction == nullptr) {
		direction = HelperMethodsUtil::findComponent<Direction>(_owner);
	}
	direction->setVelocity(rotVelocity);
	direction->setNextAngle(2 * M_PI);

	if (movement == nullptr) {
		movement = HelperMethodsUtil::findComponent<Movement>(_owner);
	}

	EventData eventData{ _owner };
	_owner->getEventDispatcher()->dispatchCustomEvent(START_SHARP_REVERSAL_SKILL, &eventData);

	running = true;
}

void SharpReversalSkill::stop()
{
	if (!running) {
		return;
	}

	if (direction == nullptr) {
		direction = HelperMethodsUtil::findComponent<Direction>(_owner);
	}
	direction->setVelocity(rotVelocity);
	direction->syncNextAndCurrentAngles();

	EventData eventData{ _owner };
	_owner->getEventDispatcher()->dispatchCustomEvent(STOP_SHARP_REVERSAL_SKILL, &eventData);

	running = false;
}

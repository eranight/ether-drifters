#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class HealthConfig {
	public:
		double hitPoints;
		double currentHitPoints;
	};

	class HealthInfo {
	public:
		HealthInfo(double maxHitPoints, double previousHitPoints, double currentHitPoints, double appliedHit, bool isDead) :
			maxHitPoints(maxHitPoints), previousHitPoints(previousHitPoints), currentHitPoints(currentHitPoints), appliedHit(appliedHit), isDead(isDead) {}
		double maxHitPoints;
		double previousHitPoints;
		double currentHitPoints;
		double appliedHit;
		bool isDead;
	};

	class Health : public cocos2d::Component {
	public:
		static Health* create(const HealthConfig& config);
		virtual ~Health() {}
		bool init(const HealthConfig& config);
		void update(float dt) override;
	public:
		HealthInfo getCurrentState();
		HealthInfo damage(double hit);
		HealthInfo heal(double hit);
	private:
		double maxHitPoints;
		double currentHitPoints;

		class HealthInfoEvent {
		public:
			HealthInfoEvent(const std::string& name, const HealthInfo& info) : name(name), info(info) {}
			std::string name;
			HealthInfo info;
		};
		std::deque<HealthInfoEvent> events;
	public:
		static const std::string NAME;
	};

}
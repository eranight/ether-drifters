#pragma once
#include "cocos2d.h"
#include "Controller.h"

namespace ether_drifters {

	class Movement;

	class StrayControllerConfig {
	public:
		bool randomizeDirection;
		cocos2d::Vec2 direction;
	};

	class StrayController : public Controller {
	private:
		StrayController(const StrayControllerConfig& config) : config(config), movement(nullptr), domainBorderEventListener(nullptr) {}
	public:
		static StrayController* create(const StrayControllerConfig& config);
		~StrayController();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		void onContactWithEdge(cocos2d::Node* domain);
	private:
		StrayControllerConfig config;
		Movement* movement;
		cocos2d::EventListenerCustom* domainBorderEventListener;
	};

}
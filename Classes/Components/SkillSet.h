#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class Skill;
	class SkillSet;

	class UsableSkill {
	public:
		virtual Skill* use() = 0;
	};

	enum class SkillFilter {
		attack,
		passive_buf
	};

	class SkillWrapper : public cocos2d::Ref, public UsableSkill {
	private:
		SkillWrapper(cocos2d::Node* owner, Skill * skill, float reloadTime);
	public:
		virtual ~SkillWrapper();
		static SkillWrapper* create(cocos2d::Node* owner, Skill* skill, float reloadTime);
		void onEnter() {}
		void onExit();
	public:
		bool isReady() { return ready; }
		Skill* use();
		void reload(float time);
	private:
		void finishReloading(float dt);
	private:
		cocos2d::Node* owner;
		Skill* skill;
		float reloadTime;
		bool ready;
		SkillFilter filter;
		friend class SkillSet;
	};

	class SkillSet : public cocos2d::Component {
	public:
		CREATE_FUNC(SkillSet);
		virtual ~SkillSet();
		bool init() override;
		void onAdd() override;
		void onEnter() override;
		void onExit() override;
	public:
		void addSkill(Skill* skill);
		UsableSkill* getFirstReadySkill(const SkillFilter& filter);
	private:
		cocos2d::Vector<SkillWrapper*> skills;
		bool globalReload;
		cocos2d::EventListenerCustom* startReloadingEventListener;
		cocos2d::Action* globalReloadAction;
	public:
		static const std::string NAME;
	};

}
#pragma once

#include "cocos2d.h"
#include "Controller.h"
#include "Events/InputEventData.h"
#include "Modification/Modification.h"
#include "Modification/Modifiable.h"
#include "Events/FocusEventListener.h"
#include "Utils/Constants.h"
#include "Utils/HelperMacroses.hpp"
#include "Scripts/AbstactRunnableSkillScript.hpp"

namespace ether_drifters{

	class Movement;
	class Direction;
	class RunnableSkill;
	class Tracking;
	class Domain;
	class WeaponSkill;
	class Cannon;

	class PlayerControllerConfig {
	public:
		float maxVelocity;
		float acceleration;
		float rotVelocity;
		cocos2d::Map<Board, Cannon*> cannons;
		std::vector<std::function<cocos2d::Node* ()>> shellSuppliers;
		cocos2d::Vector<AbstactRunnableSkillScript*> specialSkills;
	};

	class PlayerController : public Controller {
		CREATE_FUNC_WITH_CONFIG(PlayerController, PlayerControllerConfig);
	public:
		~PlayerController();
		bool init() override;
		void onAdd() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	public:
		Modifiable<std::string, PlayerController*>* getInputActioModifiable() { return inputActioModifiable; }
		void resetMovement();
		void resetRotation();
		cocos2d::Node* getSelectedNode() { return selectedNode; }
	private:
		void updateRotation(float dt);
		void updateMovement(float dt);
		void processEvent(cocos2d::EventCustom* event);
		void processUp(InputState state);
		void processDown(InputState state);
		void processRight(InputState state);
		void processLeft(InputState state);
		void processFireRight(InputState state);
		void processFireLeft(InputState state);
		void processFire(InputState state, const Board& board);
		void processChangeAmmunition(InputState state);
		void processChangeSpecialSkill(InputState state);
		void processActivateSpecialSkill(InputState state);
		void processSharpReversal(InputState state);
		void processAfterburner(InputState state);
		void processRunnableSkill(RunnableSkill* runnableSkill, const std::set<std::string>& blockedEvents);
		void processDebugRotation180(InputState state);
		void processStopShapReversalEvent(cocos2d::EventCustom* event);
		void processStopAfterburnerEvent(cocos2d::EventCustom* event);
		void processStopFrontalLaserEvent(cocos2d::EventCustom* event);
		void processTouchEvent(cocos2d::EventCustom* event);
		void processSelect(cocos2d::Node* node, bool select);

		void block(const std::set<std::string>& blockedEvents);
		void unblock(const std::set<std::string>& unblockedEvents);

		void focus(TrackedEventData* eventData);
		void unfocus(TrackedEventData* eventData);
		void changeCrosshairColorForTouchedNode(TrackedEventData* eventData, cocos2d::Color3B color);
	private:

		bool isRotating;
		bool isAccelerating;

		float rotDirection; //-1.0f - cw-rotation, 1.0f - ccw-rotation
		float movVelocity;
		float accDirection;
		float acceleration;

		std::unordered_map<std::string, uint32_t> blockedEvents;

		Movement* movement = nullptr;
		Direction* direction = nullptr;
		Tracking* tracking = nullptr;

		Domain* domain = nullptr;

		cocos2d::Map<std::string, cocos2d::EventListenerCustom*> inputListeners;

		Modifiable<std::string, PlayerController*>* inputActioModifiable = nullptr;

		cocos2d::Node* selectedNode = nullptr;

		TrackedEventListener* trackedEventListener = nullptr;

		int currentShellSupplierIndex = 0;
		int currentSpecialSkillIndex = 0;
	};

}
#pragma once
#include "Controller.h"
#include "Utils/FiniteStateMachine/FiniteStateMachine.h"

namespace ether_drifters {

	class SimpleEnemyContext;
	class Cannon;
	class Health;

	class SimpleEnemyControllerConfig {
	public:
		cocos2d::Vec2 fromPoint;
		float cooldown;
		Cannon* cannon;
		std::function<cocos2d::Node* (Health* target)> skillNodeSupplier;
	};

	class SimpleEnemyController : public Controller {
	private:
		SimpleEnemyController(const SimpleEnemyControllerConfig& config) :
			config(config),
			isDead(false),
			deathEventListener(nullptr),
			context(nullptr) {}
	public:
		static SimpleEnemyController* create(const SimpleEnemyControllerConfig& config);
		virtual ~SimpleEnemyController();
		bool init() override;
		void onAdd() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		void updateCooldown(float dt);
		void fire(Health* targetHealth);
	private:
		SimpleEnemyControllerConfig config;
		fsm::FiniteStateMachine finiteStateMachine;
		SimpleEnemyContext* context;
		cocos2d::EventListenerCustom* deathEventListener;
		bool isDead;
	};
}
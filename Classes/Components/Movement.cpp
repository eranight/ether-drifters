#include "Movement.h"
#include "Utils/Constants.h"

USING_NS_CC;
using namespace ether_drifters;

static const float EPSILON = 0.0001f;

const std::string Movement::NAME = "movement";

Movement::~Movement() {
	CC_SAFE_RELEASE_NULL(velocityVectorModifiable);
#if ED_DEBUG
	CC_SAFE_RELEASE_NULL(debugNode);
#endif
}

bool Movement::init() {
	if (!Component::init()) {
		return false;
	}

	velocity = 0.0f;
	modifiedVelocity = -1.0f;

	velocityVectorModifiable = Modifiable<Vec2, Movement*>::create();
	velocityVectorModifiable->retain();

	setName(NAME);
	return true;
}

void Movement::onAdd() {
#if ED_DEBUG
	debugNode = DrawNode::create();
	debugNode->setCameraMask(DOMAIN_UI_CAMERA_MASK);
	_owner->addChild(debugNode, 5);
#endif
}

void Movement::update(float dt) {
	Component::update(dt);

	if (path.parts.empty()) {
		updatePositionWithDirection(dt);
	}
	else {
		updatePositionWithPath(dt);
	}
#if ED_DEBUG
	debugNode->clear();
	debugNode->drawLine(Vec2::ZERO, direction * 100.0f, Color4F::BLACK);
#endif
}

void Movement::setVelocity(float velocity) {
	if (std::abs(this->velocity - velocity) < EPSILON) {
		return;
	}
	float prevVelocity = this->velocity;
	this->velocity = velocity > MATH_EPSILON ? velocity : 0.0f;
	ChangeVelocityMovementEventData eventData(_owner, ChangeVelocityMovementEventData::ChangeVelocityType::base, prevVelocity, this->velocity);
	_owner->getEventDispatcher()->dispatchCustomEvent(CHANGE_VELOCITY_EVENT, &eventData);
}

void Movement::setDirection(const Vec2 & direction) {
	this->direction = direction.getNormalized();
}

void Movement::updatePositionWithDirection(float dt)
{
	Vec2 nextVelocityVector = direction * velocity;
	applyModificationOn(nextVelocityVector);
	Vec2 nextPosition = _owner->getPosition() + nextVelocityVector * dt;
	_owner->setPosition(nextPosition);
}

void Movement::updatePositionWithPath(float dt)
{
	float maxPathLength = velocity * dt;
	float approachedPath = maxPathLength;
	Vec2 nextPosition;
	int index = 0;
	while (index < path.parts.size() && path.parts[index].length <= approachedPath) {
		approachedPath = clampf(approachedPath - path.parts[index].length, 0.0f, maxPathLength);
		//_owner->setPosition(path.parts[index++].vertexTo);
		nextPosition = path.parts[index++].vertexTo;
	}
	if (index == path.parts.size()) {
		Vec2 movementUnit = nextPosition - _owner->getPosition();
		applyModificationOn(movementUnit);
		_owner->setPosition(_owner->getPosition() + movementUnit);
		FinishPathMovementEventData eventData = FinishPathMovementEventData(_owner);
		_owner->getEventDispatcher()->dispatchCustomEvent(FINISH_PATH_EVENT, &eventData);
		CCLOG("dispatches finish path event");
		return;
	}
	auto pathPart = path.parts[index];
	if (pathPart.isHugging) {
		float angle = approachedPath / pathPart.radius;
		if (pathPart.clockwise) {
			angle *= -1;
		}
		Vec2 from = pathPart.vertexFrom - pathPart.center;
		Vec2 to = from.rotateByAngle(Vec2::ZERO, angle);
		nextPosition = pathPart.center + to;
	}
	else {
		Vec2  movementUnit = pathPart.vertexTo - pathPart.vertexFrom;
		movementUnit.normalize();
		movementUnit *= approachedPath;
		nextPosition = _owner->getPosition() + movementUnit;
	}
	Vec2 movementUnit = nextPosition - _owner->getPosition();
	setDirection(movementUnit);
	applyModificationOn(movementUnit);
	_owner->setPosition(_owner->getPosition() + movementUnit);
}

void Movement::applyModificationOn(Vec2& modifiableVelocityVector) {
	if (velocityVectorModifiable->getSize() > 0) {
		modifiableVelocityVector = velocityVectorModifiable->applyModifications(modifiableVelocityVector, this);
		float nextModVelocity = modifiableVelocityVector.length();
		if (modifiedVelocity < 0.0f || std::abs(modifiedVelocity - nextModVelocity) > EPSILON) {
			float prevModVelocity = modifiedVelocity;
			modifiedVelocity = nextModVelocity;
			ChangeVelocityMovementEventData eventData(_owner, ChangeVelocityMovementEventData::ChangeVelocityType::mod, prevModVelocity, modifiedVelocity);
			_owner->getEventDispatcher()->dispatchCustomEvent(CHANGE_VELOCITY_EVENT, &eventData);
		}
	}
}
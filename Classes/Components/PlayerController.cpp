#include "PlayerController.hpp"
#include "Movement.h"
#include "Direction.h"
#include "Events/InputEventData.h"
#include "Events/TouchEventData.h"
#include "Events/HealthEventData.h"
#include "Events/TrackedEventData.h"
#include "Events/FocusEventListener.h"
#include "Events/SelectEventData.h"
#include "Touchable.h"
#include "Prefubs/Crosshair.h"
#include "Utils/HelperMethodsUtil.h"
#include "Mined.h"
#include "Health.h"
#include "Tracked.h"
#include "Tracking.h"
#include "Skills/SharpReversalSkill.hpp"
#include "Skills/AfterburnerSkill.hpp"
#include "Prefubs/ResourceBar.h"
#include "Prefubs/HealthBar.h"
#include "Prefubs/Cannon.hpp"
#include "Ether.h"
#include "Common/WeaponSkill.hpp"

using namespace cocos2d;
using namespace ether_drifters;

static const std::set<std::string> SHARP_REVERSAL_BLOCKED_EVENTS = { INPUT_UP, INPUT_DOWN, INPUT_RIGHT, INPUT_LEFT, INPUT_AFTERBURNER };
static const std::set<std::string> FORSAGE_BLOCKED_EVENTS = { INPUT_UP, INPUT_DOWN, INPUT_RIGHT, INPUT_LEFT, INPUT_SHART_REVERSAL };
static const std::set<std::string> FRONTAL_LASER_BLOCKED_EVENTS = { INPUT_AFTERBURNER };

PlayerController::~PlayerController()
{
	CC_SAFE_RELEASE_NULL(inputActioModifiable);
	CC_SAFE_RELEASE_NULL(trackedEventListener);
}

bool PlayerController::init() {
	if (!Controller::init()) {
		return false;
	}

	isRotating = false;
	isAccelerating = false;

	rotDirection = 0.0f;
	movVelocity = 0.0f;
	accDirection = 0.0f;
	acceleration = 0.0f;

	auto inputListener = EventListenerCustom::create(INPUT_UP, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_UP, inputListener);

	inputListener = EventListenerCustom::create(INPUT_DOWN, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_DOWN, inputListener);

	inputListener = EventListenerCustom::create(INPUT_RIGHT, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_RIGHT, inputListener);

	inputListener = EventListenerCustom::create(INPUT_LEFT, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_LEFT, inputListener);

	inputListener = EventListenerCustom::create(INPUT_FIRE_RIGHT_BOARD, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_FIRE_RIGHT_BOARD, inputListener);

	inputListener = EventListenerCustom::create(INPUT_FIRE_LEFT_BOARD, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_FIRE_LEFT_BOARD, inputListener);

	inputListener = EventListenerCustom::create(INPUT_CHANGE_FIRE_SKILL, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_CHANGE_FIRE_SKILL, inputListener);

	inputListener = EventListenerCustom::create(INPUT_CHANGE_SPECIAL_SKILL, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_CHANGE_SPECIAL_SKILL, inputListener);

	inputListener = EventListenerCustom::create(INPUT_ACTIVATE_SPECIAL_SKILL, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_ACTIVATE_SPECIAL_SKILL, inputListener);

	inputListener = EventListenerCustom::create(INPUT_SHART_REVERSAL, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_SHART_REVERSAL, inputListener);

	inputListener = EventListenerCustom::create(STOP_SHARP_REVERSAL_SKILL, CC_CALLBACK_1(PlayerController::processStopShapReversalEvent, this));
	inputListeners.insert(STOP_SHARP_REVERSAL_SKILL, inputListener);

	inputListener = EventListenerCustom::create(INPUT_AFTERBURNER, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_AFTERBURNER, inputListener);

	inputListener = EventListenerCustom::create(STOP_AFTERBURNER_SKILL, CC_CALLBACK_1(PlayerController::processStopAfterburnerEvent, this));
	inputListeners.insert(STOP_AFTERBURNER_SKILL, inputListener);

	inputListener = EventListenerCustom::create(INPUT_DEBUG_ROTATION_180, CC_CALLBACK_1(PlayerController::processEvent, this));
	inputListeners.insert(INPUT_DEBUG_ROTATION_180, inputListener);

	inputListener = EventListenerCustom::create(INPUT_EXIT, [](EventCustom* event) { Director::getInstance()->end(); });
	inputListeners.insert(INPUT_EXIT, inputListener);

	inputActioModifiable = Modifiable<std::string, PlayerController*>::create();
	inputActioModifiable->retain();

	auto touchEventListener = EventListenerCustom::create(TOUCH_EVENT, CC_CALLBACK_1(PlayerController::processTouchEvent, this));
	inputListeners.insert(TOUCH_EVENT, touchEventListener);

	auto deathTouchedNodeEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* event) {
		if (selectedNode == nullptr) {
			return;
		}
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(event->getUserData());
		if (eventData->getHealth()->getOwner() == selectedNode) {
			selectedNode->removeChildByName(Crosshair::NAME);
			selectedNode = nullptr;
		}
	});

	inputListeners.insert(DEATH_EVENT, deathTouchedNodeEventListener);

	return true;
}

void PlayerController::onAdd()
{
	Controller::onAdd();

	trackedEventListener = TrackedEventListener::create(_owner, false);
	trackedEventListener->retain();
	trackedEventListener->onFocusedEvent = CC_CALLBACK_1(PlayerController::focus, this);
	trackedEventListener->onUnfocusedEvent = CC_CALLBACK_1(PlayerController::unfocus, this);
	trackedEventListener->onInterruptedEvent = CC_CALLBACK_1(PlayerController::unfocus, this);
}

void PlayerController::onEnter()
{
	Controller::onEnter();

	for (auto entry : inputListeners) {
		_owner->getEventDispatcher()->addEventListenerWithFixedPriority(entry.second, 1);
	}

	auto initDirection = Vec2::UNIT_Y;

	direction = HelperMethodsUtil::findComponent<Direction>(_owner);
	direction->setDirection(initDirection);

	movement = HelperMethodsUtil::findComponent<Movement>(_owner);
	movement->setDirection(initDirection);

	tracking = HelperMethodsUtil::findComponent<Tracking>(_owner);

	domain = Ether::getInstance()->getCurrentDomain();
}

void PlayerController::onExit()
{
	Controller::onEnter();

	for (auto entry : inputListeners) {
		_owner->getEventDispatcher()->removeEventListener(entry.second);
	}

	trackedEventListener->onFocusedEvent = nullptr;
	trackedEventListener->onUnfocusedEvent = nullptr;
	trackedEventListener->onInterruptedEvent = nullptr;
}

void PlayerController::update(float dt)
{
	Controller::update(dt);

	updateRotation(dt);
	updateMovement(dt);
}

void PlayerController::resetMovement()
{
	accDirection = 0.0f;
	acceleration = 0.0f;
	isAccelerating = false;
}

void PlayerController::resetRotation()
{
	rotDirection = 0.0f;
	isRotating = false;
}

void PlayerController::updateRotation(float dt)
{
	if (isRotating) {
		float rotAngle = config.rotVelocity * rotDirection * dt;
		auto nextDir = direction->getDirection().rotateByAngle(Vec2::ZERO, rotAngle);
		direction->setDirection(nextDir);
		movement->setDirection(nextDir);
	}
}

void PlayerController::updateMovement(float dt)
{
	if (isAccelerating) {
		float velocity = movVelocity;
		velocity += acceleration * dt;
		if (velocity > config.maxVelocity || std::abs(velocity - config.maxVelocity) < MATH_EPSILON) {
			velocity = config.maxVelocity;
			acceleration = 0.0f;
		}
		else if (velocity < 0.0f || std::abs(velocity) < MATH_EPSILON) {
			velocity = 0.0f;
			acceleration = 0.0f;
		}
		movVelocity = velocity;
		movement->setVelocity(movVelocity);
	}
}

void PlayerController::processEvent(EventCustom* event)
{
	InputEventData* eventData = reinterpret_cast<InputEventData*>(event->getUserData());
	auto action = inputActioModifiable->applyModifications(eventData->getAction(), this);
	auto state = eventData->getState();
	if (action == INPUT_UP) {
		processUp(state);
	}
	else if (action == INPUT_DOWN) {
		processDown(state);
	}
	else if (action == INPUT_RIGHT) {
		processRight(state);
	}
	else if (action == INPUT_LEFT) {
		processLeft(state);
	}
	else if (action == INPUT_FIRE_RIGHT_BOARD) {
		processFireRight(state);
	}
	else if (action == INPUT_FIRE_LEFT_BOARD) {
		processFireLeft(state);
	}
	else if (action == INPUT_CHANGE_FIRE_SKILL) {
		processChangeAmmunition(state);
	}
	else if (action == INPUT_CHANGE_SPECIAL_SKILL) {
		processChangeSpecialSkill(state);
	}
	else if (action == INPUT_ACTIVATE_SPECIAL_SKILL) {
		processActivateSpecialSkill(state);
	}
	else if (action == INPUT_SHART_REVERSAL) {
		processSharpReversal(state);
	}
	else if (action == INPUT_AFTERBURNER) {
		processAfterburner(state);
	}
	else if (action == INPUT_DEBUG_ROTATION_180) {
		processDebugRotation180(state);
	}
}

void PlayerController::processUp(InputState state)
{
	if (state == InputState::pressed && !isAccelerating) {
		accDirection = 1.0f;
		acceleration = config.acceleration * accDirection;
		isAccelerating = true;
	}
	else if (state == InputState::released && isAccelerating && accDirection == 1.0f) {
		resetMovement();
	}
}

void PlayerController::processDown(InputState state)
{
	if (state == InputState::pressed && !isAccelerating) {
		accDirection = -1.0f;
		acceleration = config.acceleration * accDirection;
		isAccelerating = true;
	}
	else if (state == InputState::released && isAccelerating && accDirection == -1.0f) {
		resetMovement();
	}
}

void PlayerController::processRight(InputState state)
{
	if (state == InputState::pressed && !isRotating) {
		rotDirection = -1.0f;
		isRotating = true;
	}
	else if (state == InputState::released && isRotating && rotDirection == -1.0f) {
		resetRotation();
	}
}

void PlayerController::processLeft(InputState state)
{
	if (state == InputState::pressed && !isRotating) {
		rotDirection = 1.0f;
		isRotating = true;
	}
	else if (state == InputState::released && isRotating && rotDirection == 1.0f) {
		resetRotation();
	}
}

void PlayerController::processFireRight(InputState state)
{
	processFire(state, Board::right);
}

void PlayerController::processFireLeft(InputState state)
{
	processFire(state, Board::left);
}

void PlayerController::processFire(InputState state, const Board& board)
{
	if (state == InputState::pressed && !config.shellSuppliers.empty()) {
		auto currentCannon = config.cannons.at(board);
		currentCannon->reset();
		if (selectedNode != nullptr && selectedNode != _owner && HelperMethodsUtil::findComponent<Tracked>(selectedNode) != nullptr) {
			Vec2 targetPosition = currentCannon->convertToNodeSpace(domain->convertToWorldSpace(selectedNode->getPosition()));
			float angle = CC_RADIANS_TO_DEGREES(targetPosition.getAngle());
			currentCannon->rotate(angle);
		}

		currentCannon->fire(config.shellSuppliers[currentShellSupplierIndex]);
	}
}

void PlayerController::processChangeAmmunition(InputState state)
{
	if (state == InputState::pressed && !config.shellSuppliers.empty()) {
		CCLOG("change board cannon shell");
		currentShellSupplierIndex = (currentShellSupplierIndex + 1) % config.shellSuppliers.size();
	}
}

void PlayerController::processChangeSpecialSkill(InputState state)
{
	if (state == InputState::pressed && !config.specialSkills.empty()) {
		auto currentSkill = config.specialSkills.at(currentSpecialSkillIndex);
		if (currentSkill->isRunning()) {
			CCLOG("cant change special skill cause current one is active");
		}
		else {
			CCLOG("change special skill");
			currentSpecialSkillIndex = (currentSpecialSkillIndex + 1) % config.specialSkills.size();
		}
	}
}

void PlayerController::processActivateSpecialSkill(InputState state)
{
	if (state == InputState::pressed && !config.specialSkills.empty()) {
		AbstactRunnableSkillScript* runnableSkill = config.specialSkills.at(currentSpecialSkillIndex);
		if (runnableSkill->isRunning()) {
			runnableSkill->stop();
		}
		else {
			runnableSkill->run();
		}
	}
}

void PlayerController::processSharpReversal(InputState state)
{
	if (state == InputState::pressed) {
		SharpReversalSkill* fastRotationSkill = HelperMethodsUtil::findComponent<SharpReversalSkill>(_owner);
		processRunnableSkill(fastRotationSkill, SHARP_REVERSAL_BLOCKED_EVENTS);
	}
}

void PlayerController::processAfterburner(InputState state)
{
	if (state == InputState::pressed) {
		AfterburnerSkill* forsageSkill = HelperMethodsUtil::findComponent<AfterburnerSkill>(_owner);
		processRunnableSkill(forsageSkill, FORSAGE_BLOCKED_EVENTS);
	}
}

void PlayerController::processRunnableSkill(RunnableSkill* runnableSkill, const std::set<std::string>& blockedEvents)
{
	if (runnableSkill->isRunning()) {
		runnableSkill->stop();
	}
	else {
		resetMovement();
		resetRotation();
		runnableSkill->run();
		block(blockedEvents);
	}
}

void PlayerController::processDebugRotation180(InputState state)
{
	if (state == InputState::pressed) {
		auto nextDir = direction->getDirection().rotateByAngle(Vec2::ZERO, -MATH_DEG_TO_RAD(180.0f));
		direction->setDirection(nextDir);
		movement->setDirection(nextDir);
	}
}

void PlayerController::processStopShapReversalEvent(EventCustom* event)
{
	EventData* eventData = reinterpret_cast<EventData*>(event->getUserData());
	if (eventData->getOwner() == _owner) {
		unblock(SHARP_REVERSAL_BLOCKED_EVENTS);
	}
}

void PlayerController::processStopAfterburnerEvent(EventCustom* event)
{
	EventData* eventData = reinterpret_cast<EventData*>(event->getUserData());
	if (eventData->getOwner() == _owner) {
		unblock(FORSAGE_BLOCKED_EVENTS);
		movVelocity = movement->getVelocity();
	}
}

void PlayerController::processStopFrontalLaserEvent(cocos2d::EventCustom* event)
{
	EventData* eventData = reinterpret_cast<EventData*>(event->getUserData());
	if (eventData->getOwner() == _owner) {
		unblock(FRONTAL_LASER_BLOCKED_EVENTS);
	}
}

void PlayerController::processTouchEvent(EventCustom* event)
{
	TouchEventData* eventData = reinterpret_cast<TouchEventData*>(event->getUserData());
	if (eventData->getTouchable() == nullptr) {
		if (selectedNode != nullptr) {
			processSelect(selectedNode, false);
			selectedNode->removeChildByName(Crosshair::NAME);
			Node* affectedNode = selectedNode;
			selectedNode = nullptr;
			SelectEventData selectEventData(_owner, affectedNode, false);
			_owner->getEventDispatcher()->dispatchCustomEvent(SELECT_EVENT, &selectEventData);
		}
		return;
	}

	Node* touchableNode = eventData->getTouchable()->getOwner();
	if (touchableNode == selectedNode) {
		return;
	}
	if (selectedNode != nullptr) {
		processSelect(selectedNode, false);
		selectedNode->removeChildByName(Crosshair::NAME);
	}
	selectedNode = touchableNode;
	CrosshairConfig config;
	if (HelperMethodsUtil::findComponent<Mined>(selectedNode) != nullptr) {
		config.color = Color3B::WHITE;
	}
	else if (HelperMethodsUtil::findComponent<Health>(selectedNode) != nullptr && selectedNode != _owner) {
		auto tracked = HelperMethodsUtil::findComponent<Tracked>(touchableNode);
		if (tracking->containInVision(tracked)) {
			config.color = Color3B::RED;
		}
		else {
			config.color = Color3B::GRAY;
		}
	}
	else {
		config.color = Color3B::WHITE;
	}
	auto crosshair = Crosshair::create(config);
	selectedNode->addChild(crosshair);
	processSelect(touchableNode, true);
	SelectEventData selectEventData(_owner, selectedNode, true);
	_owner->getEventDispatcher()->dispatchCustomEvent(SELECT_EVENT, &selectEventData);
}

void PlayerController::processSelect(Node* node, bool select)
{
	auto bar = node->getChildByName(HealthBar::NAME);
	if (bar != nullptr) {
		bar->setVisible(select);
		return;
	}
	bar = node->getChildByName(ResourceBar::NAME);
	if (bar != nullptr) {
		bar->setVisible(select);
	}
}

void PlayerController::block(const std::set<std::string>& blockedEvents)
{
	for (auto entry : inputListeners) {
		if (blockedEvents.find(entry.first) != blockedEvents.end()) {
			entry.second->setEnabled(false);
			auto currentBlockedEventIter = this->blockedEvents.find(entry.first);
			if (currentBlockedEventIter == this->blockedEvents.end()) {
				this->blockedEvents[entry.first] = 1;
			}
			else {
				this->blockedEvents[entry.first]++;
			}
		}
	}
}

void PlayerController::unblock(const std::set<std::string>& unblockedEvents)
{
	for (auto entry : inputListeners) {
		if (unblockedEvents.find(entry.first) != unblockedEvents.end()) {
			auto currentBlockedEventIter = this->blockedEvents.find(entry.first);
			if (currentBlockedEventIter != this->blockedEvents.end()) {
				uint32_t val = --this->blockedEvents[entry.first];
				if (val == 0) {
					entry.second->setEnabled(true);
				}
			}
			else {
				entry.second->setEnabled(true);
			}
		}
	}
}

void PlayerController::focus(TrackedEventData* eventData)
{
	changeCrosshairColorForTouchedNode(eventData, Color3B::RED);
}

void PlayerController::unfocus(TrackedEventData* eventData)
{
	changeCrosshairColorForTouchedNode(eventData, Color3B::GRAY);
}

void PlayerController::changeCrosshairColorForTouchedNode(TrackedEventData* eventData, Color3B color) {
	Node* trackedNode = eventData->getTracked()->getOwner();
	if (selectedNode == trackedNode) {
		Node* crosshair = selectedNode->getChildByName(Crosshair::NAME);
		if (crosshair != nullptr) {
			crosshair->setColor(color);
		}
		else
		{
			CCLOG("selected node must have crosshair child!");
		}
	}
}


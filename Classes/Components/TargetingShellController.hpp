#pragma once
#include "ShellControllers.h"

namespace ether_drifters
{
	class Tracked;
	class Movement;
	class Direction;

	class TargetingShellController : public AbstractShellController {
	private:
		TargetingShellController(const ShellControllerConfig& config) :
			AbstractShellController(config) {}
	public:
		~TargetingShellController();
		static TargetingShellController* create(const ShellControllerConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		cocos2d::Node* trackedNode = nullptr;
		Movement* movement = nullptr;
		Direction* direction = nullptr;

		cocos2d::EventListener* domainBorderListener = nullptr;
		cocos2d::EventListener* damageEventListener = nullptr;
		cocos2d::EventListener* deathEventListener = nullptr;
	};

}
#include "Touchable.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Touchable::NAME = "touchable";

Touchable::~Touchable() {
	CC_SAFE_RELEASE_NULL(debugMask);
}

Touchable* Touchable::create(const Size& touchSize, const Vec2& offset, bool debugMode) {
	auto ref = new (std::nothrow) Touchable();
	if (ref != nullptr && ref->init(touchSize, offset, debugMode)) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Touchable::init(const Size& touchSize, const Vec2& offset, bool debugMode) {
	if (!Component::init()) {
		return false;
	}

	Vec2 halfSize = touchSize * 0.5f;
	Vec2 negationHalfSize = -halfSize;

	rect = Rect(negationHalfSize + offset, touchSize);

	if (debugMode) {
		debugMask = DrawNode::create();
		if (debugMask == nullptr) {
			CCLOG("cannot create debugMask for Touchable component");
			return false;
		}
		debugMask->setColor(Color3B::MAGENTA);
		debugMask->drawSolidRect(negationHalfSize, halfSize, Color4F(Color4B(Color3B::MAGENTA, 128)));
		debugMask->retain();
		debugMask->setCameraMask(DOMAIN_CAMERA_MASK);
	}
	setName(NAME);
	return true;
}

void Touchable::onAdd() {
	Component::onAdd();
	if (debugMask != nullptr) {
		auto view = HelperMethodsUtil::getViewIfExists(_owner);
		view->addChild(debugMask, 99);
		auto dot = DrawNode::create();
		dot->drawDot(Vec2::ZERO, 5.0f, Color4F::RED);
		dot->setCameraMask(DOMAIN_CAMERA_MASK);
		view->addChild(dot, 100);
	}

}

void Touchable::onRemove() {
	Component::onRemove();
	if (debugMask != nullptr) {
		auto view = HelperMethodsUtil::getViewIfExists(_owner);
		view->removeChild(debugMask);
	}
}
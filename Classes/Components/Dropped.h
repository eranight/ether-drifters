#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters {

	class DroppedConfig {
	public:
		DropType type;
		int quantity;
	};

	class Dropped : public cocos2d::Component {
	private:
		Dropped(const DroppedConfig& config) : config(config) {}
	public:
		static Dropped* create(const DroppedConfig& drop);
		bool init() override;
	public:
		bool empty() { return config.type == DropType::none; }
		DroppedConfig pick();
	private:
		DroppedConfig config;
	public:
		static const std::string NAME;
	};

}
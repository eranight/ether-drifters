#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class HealthInfo;
	class Health;

	class RegenerationConfig {
	public:
		double heal;
		float interval;
		float delay;
	};

	class Regeneration : public cocos2d::Component {
	private:
		Regeneration(const RegenerationConfig& config) :
			config(config),
			health(nullptr),
			deathEventListener(nullptr),
			isRunning(false),
			dirty(false) {}
	public:
		static Regeneration* create(const RegenerationConfig & config);
		~Regeneration();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	public:
		double getHeal() { return config.heal; }
		void setHeal(double heal) { config.heal = heal; }
		float getInterval() { return config.interval; }
		void setInterval(float interval);
		float getDelay() { return config.delay; }
		void setDelay(float delay) { config.delay = delay; }
	private:
		void startRegeneration();
		void stopRegeneration();
		void regenerate(float);
	private:
		RegenerationConfig config;
		Health* health;
		cocos2d::EventListenerCustom* startRegenerationEventListener;
		cocos2d::EventListenerCustom* stopRegenerationEventListener;
		cocos2d::EventListenerCustom* deathEventListener;
		bool isRunning;
		bool dirty;
	public:
		static const std::string NAME;
	};

}
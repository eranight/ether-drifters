#include "Obstacle.h"
#include "Events/ObstacleEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Obstacle::NAME = "obstacle";
const uint32_t Obstacle::PATH_OBSTACLE = 0x01;
const uint32_t Obstacle::FIRE_OBSTACLE = 0x02;

Obstacle* Obstacle::create(uint32_t mask, std::vector<ObstacleShape> shapes)
{
	auto ref = new (std::nothrow) Obstacle(mask, shapes);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Obstacle::init() {
	if (!Component::init()) {
		return false;
	}
	setName(NAME);
	return true;
}

void Obstacle::onAdd() {
	Component::onAdd();

	for (auto& shape : shapes) {
		shape.owner = _owner;
	}

	EventData eventData(_owner);
	_owner->getEventDispatcher()->dispatchCustomEvent(ADD_OBSTACLE_EVENT, &eventData);
}

void Obstacle::onRemove() {
	Component::onRemove();

	EventData eventData(_owner);
	_owner->getEventDispatcher()->dispatchCustomEvent(REMOVE_OBSTACLE_EVENT, &eventData);
}
#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class HealthInfo;
	class PickedResource;

	class CrewConfig {
	public:
		int maxQuantity;
		int currentQuantity;
		float repairRateBonus;
		float canonCooldownBonus;
		float miningRateBonus;
		std::map<double, double> looseCrewChances;
	};

	class Crew : public cocos2d::Component {
	private:
		Crew(const CrewConfig& config) : 
			config(config),
			damageEventListener(nullptr),
			pickedDropEventListener(nullptr) 
		{}
	public:
		static Crew* create(const CrewConfig& config);
		~Crew();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	public:
		int getMaxQuantity() { return config.maxQuantity; }
		int getCurrentQuantity() { return config.currentQuantity; }
	private:
		void takeDamage(HealthInfo* info);
		void changeQuantity(PickedResource* drop);
		void recalculateBonuses(int num);
		float calc(float src, float bonus, int num);
	private:
		CrewConfig config;
		cocos2d::EventListenerCustom* damageEventListener;
		cocos2d::EventListenerCustom* pickedDropEventListener;
	public:
		static const std::string NAME;
	};

}
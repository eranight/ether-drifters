#include "Direction.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

USING_NS_CC;
using namespace ether_drifters;

const std::string Direction::NAME = "direction";

Direction::~Direction()
{
#if ED_DEBUG
	CC_SAFE_RELEASE_NULL(debugNode);
#endif
}

Direction * Direction::create(const Vec2 & direction) {
	auto ret = new (std::nothrow) Direction(direction);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool Direction::init() {
	if (!Component::init()) {
		return false;
	}

#if ED_DEBUG
	debugNode = DrawNode::create();
	debugNode->setCameraMask(DOMAIN_UI_CAMERA_MASK);
	debugNode->retain();
#endif

	setName(NAME);
	return true;
}

void Direction::onAdd()
{
	Component::onAdd();
#if ED_DEBUG
	_owner->addChild(debugNode, 5);
#endif
}

void Direction::onEnter()
{
	Component::onEnter();
	updateDirection();
}

void Direction::update(float dt)
{
	Component::update(dt);

	/*if (ccw != 0) {
		float remainingAngle = nextDirection.getAngle(direction);
		float dAngle = velocity * dt;
		dAngle = clampf(dAngle, 0.0f, abs(remainingAngle));
		direction.rotate(Vec2::ZERO, dAngle * ccw);
		updateDirection();
		if (abs(remainingAngle - dAngle) < MATH_EPSILON) {
			ccw = 0;
		}
	}*/

	if (ccw != 0) {
		float dAngle = clampf(velocity * dt, 0.0f, remainedAngle);
		remainedAngle = clampf(remainedAngle - dAngle, 0.0f, nextAngle);
		direction.rotate(Vec2::ZERO, dAngle * ccw);
		updateDirection();
		if (remainedAngle == 0.0f) {
			ccw = 0;
		}
	}

#if ED_DEBUG
	debugNode->clear();
	debugNode->drawLine(Vec2::ZERO, direction * 100.0f, Color4F::BLUE);
#endif
}

void Direction::setDirection(const cocos2d::Vec2 & direction) {
	this->direction = direction;
	this->direction.normalize();
	updateDirection();
}

void Direction::setNextAngle(float angle)
{
	nextAngle = remainedAngle = abs(angle);
	ccw = angle < 0.0f ? 1.0f : -1.0f;
}

void Direction::updateDirection()
{
	Node * node = HelperMethodsUtil::getViewIfExists(_owner);
	node->setRotation(-MATH_RAD_TO_DEG(this->direction.getAngle()));
}

#include "Controller.h"

using namespace ether_drifters;

const std::string Controller::NAME = "controller";

bool Controller::init() {
	if (!Component::init()) {
		return false;
	}
	setName(NAME);
	return true;
}
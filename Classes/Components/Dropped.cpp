#include "Dropped.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Dropped::NAME = "dropped";

Dropped* Dropped::create(const DroppedConfig& config) {
	auto ref = new (std::nothrow) Dropped(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Dropped::init() {
	if (!Component::init()) {
		return false;
	}

	setName(NAME);
	return true;
}

DroppedConfig Dropped::pick() {
	DroppedConfig retDrop = config;
	if (config.type != DropType::none) {
		config = DroppedConfig();
		config.type = DropType::none;
		config.quantity = 0;
	}
	return retDrop;
}
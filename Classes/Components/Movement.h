#ifndef __MOVEMENT_H__
#define __MOVEMENT_H__

#include "cocos2d.h"
#include "Modification/Modification.h"
#include "Modification/Modifiable.h"
#include "Events/MovementEventData.h"
#include "Systems/Path.h"

namespace ether_drifters {

	class Movement : public cocos2d::Component
	{
	protected:
		Movement() : velocity(0.0f), direction(cocos2d::Vec2::ZERO) {}

	public:
		virtual ~Movement();
		CREATE_FUNC(Movement)
		bool init() override;
		void onAdd() override;
		void update(float dt) override;

		float getVelocity() const { return velocity; }
		void setVelocity(float velocity);

		cocos2d::Vec2 getDirection() const { return cocos2d::Vec2(direction); }
		void setDirection(const cocos2d::Vec2 & direction);

		void setPath(Path path) { this->path = path; }

		Modifiable<cocos2d::Vec2, Movement*>* getVelocityVectorModifiable() { return velocityVectorModifiable; }

	private:
		void updatePositionWithDirection(float dt);
		void updatePositionWithPath(float dt);
		void applyModificationOn(cocos2d::Vec2& modifiableVelocityVector);

	private:
		float velocity;
		float modifiedVelocity;
		cocos2d::Vec2 direction;
		Path path;

		Modifiable<cocos2d::Vec2, Movement*>* velocityVectorModifiable;

#if ED_DEBUG
		cocos2d::DrawNode* debugNode = nullptr;
#endif

	public:
		static const std::string NAME;
	};

}

#endif //__MOVEMENT_H__
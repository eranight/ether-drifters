#pragma once
#include <unordered_map>
#include <functional>

#include "cocos2d.h"
#include "json/document.h"

namespace ether_drifters {

	class ComponentCreator {
	private:
		ComponentCreator();
	public:
		static cocos2d::Component* create(const rapidjson::Value& val);
	private:
		cocos2d::Component* createDamageDealer(const rapidjson::Value& val);
		cocos2d::Component* createDamage(const rapidjson::Value& val);
		cocos2d::Component* createDirection(const rapidjson::Value& val);
		cocos2d::Component* createHealth(const rapidjson::Value& val);
		cocos2d::Component* createMined(const rapidjson::Value& val);
		cocos2d::Component* createMining(const rapidjson::Value& val);
		cocos2d::Component* createMovement(const rapidjson::Value& val);
		cocos2d::Component* createRegeneration(const rapidjson::Value& val);
		cocos2d::Component* createTracked(const rapidjson::Value& val);
		cocos2d::Component* createTracking(const rapidjson::Value& val);
	private:
		static ComponentCreator componentCreator;
		std::unordered_map<std::string, std::function<cocos2d::Component* (const rapidjson::Value&)>> mapper;
	};

}
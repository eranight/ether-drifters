#include "DamageDealer.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string DamageDealer::NAME = "damage_dealer";

bool DamageDealer::init() {
	if (!Component::init()) {
		return false;
	}
	setName(NAME);
	return true;
}

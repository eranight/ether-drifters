#include "SimpleEnemyController.h"
#include "FSM/SimpleEnemy/SimpleEnemyFSMHeaders.h"
#include "FSM/SimpleEnemy/SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/SubStateMachine.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/FocusEventListener.h"
#include "Events/HealthEventData.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"
#include "Prefubs/Cannon.hpp"

using namespace cocos2d;
using namespace ether_drifters;

SimpleEnemyController* SimpleEnemyController::create(const SimpleEnemyControllerConfig& config) {
	auto ref = new (std::nothrow) SimpleEnemyController(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

SimpleEnemyController::~SimpleEnemyController() {
	CC_SAFE_RELEASE_NULL(context);
	CC_SAFE_RELEASE_NULL(deathEventListener);
}

bool SimpleEnemyController::init() {
	if (!Controller::init()) {
		return false;
	}

	deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() == _owner) {
			isDead = true;
			finiteStateMachine.exit();
		}
	});
	deathEventListener->retain();

	return true;
}

void SimpleEnemyController::onAdd() {
	Controller::onAdd();

	context = SimpleEnemyContext::create(_owner);
	context->retain();

	auto trackedEventListener = TrackedEventListener::create(_owner);
	context->setTrackedEventListener(trackedEventListener);

	context->setMaxVelocity(180.0f);
	context->setRotVelocity(MATH_DEG_TO_RAD(720.0f));

	float minAttackDist = 200.0f;
	float maxAttackDist = 400.0f;
	context->setMinAttackDistance(minAttackDist);
	context->setMaxAttackDistance(maxAttackDist);

	// STRAY state
	auto restState = std::make_shared<RestState>();
	auto flyState = std::make_shared<FlyState>(context);

	auto conditionFromRestToFly = std::make_shared<RandomTimeCondition>(1.0f, 2.0f);
	auto transitionFromRestToFly = std::make_shared<fsm::Transition>(conditionFromRestToFly, flyState);
	restState->addTransition(transitionFromRestToFly);

	auto conditionFromFlyToRest = std::make_shared<RandomTimeCondition>(2.0f, 3.0f);
	auto transitionFromFlyToRest = std::make_shared<fsm::Transition>(conditionFromFlyToRest, restState);
	flyState->addTransition(transitionFromFlyToRest);

	auto strayFiniteStateMachine = fsm::FiniteStateMachine();
	strayFiniteStateMachine.addState(restState, true);
	strayFiniteStateMachine.addState(flyState);

	
	// ATTACK state
	
	auto chaseState = std::make_shared<ChaseNextPositionState>(context);
	auto hitState = std::make_shared<HitState>(context, CC_CALLBACK_1(SimpleEnemyController::fire, this));
	auto dislocationState = std::make_shared<DislocationState>(context);

	auto conditionFromChaseToHitByDistance = std::make_shared<DistanceCondition>(context, minAttackDist);
	auto transitionFromChaseToHitDistance = std::make_shared<fsm::Transition>(conditionFromChaseToHitByDistance, hitState);
	chaseState->addTransition(transitionFromChaseToHitDistance);

	auto conditionFromChaseToHitByChaseComplete = std::make_shared<fsm::ConditionComplete>(chaseState);
	auto transitionFromChaseToHitByChaseComplete = std::make_shared<fsm::Transition>(conditionFromChaseToHitByChaseComplete, hitState);
	chaseState->addTransition(transitionFromChaseToHitByChaseComplete);

	auto conditionFromHitToChase = std::make_shared<fsm::ConditionNot>(std::make_shared<DistanceCondition>(context, maxAttackDist));
	auto transitionFromHitToChase = std::make_shared<fsm::Transition>(conditionFromHitToChase, chaseState);
	hitState->addTransition(transitionFromHitToChase);

	auto conditionFromHitToDislocation = std::make_shared<TimeCondition>(2.0f);
	auto transitionFromHitToDislocation = std::make_shared<fsm::Transition>(conditionFromHitToDislocation, dislocationState);
	hitState->addTransition(transitionFromHitToDislocation);

	auto conditionFromHitToDislocationByComplete = std::make_shared<fsm::ConditionComplete>(hitState);
	auto transitionFromHitToDislocationByComplete = std::make_shared<fsm::Transition>(conditionFromHitToDislocationByComplete, dislocationState);
	hitState->addTransition(transitionFromHitToDislocationByComplete);

	auto conditionFromDislocationToHit = std::make_shared<fsm::ConditionComplete>(dislocationState);
	auto transitionFromDislocationToHit = std::make_shared<fsm::Transition>(conditionFromDislocationToHit, hitState);
	dislocationState->addTransition(transitionFromDislocationToHit);

	auto conditionFromDislocationToHit1 = std::make_shared<TimeCondition>(3.0f);
	auto transitionFromDislocationToHit1 = std::make_shared<fsm::Transition>(conditionFromDislocationToHit1, hitState);
	dislocationState->addTransition(transitionFromDislocationToHit1);

	auto conditionFromDislocationToChase = std::make_shared<fsm::ConditionNot>(std::make_shared<DistanceCondition>(context, maxAttackDist));
	auto transitionFromDislocationToChase = std::make_shared<fsm::Transition>(conditionFromDislocationToChase, chaseState);
	dislocationState->addTransition(transitionFromDislocationToChase);

	auto attackFiniteStateMachine = fsm::FiniteStateMachine();
	attackFiniteStateMachine.addState(chaseState, true);
	attackFiniteStateMachine.addState(hitState);
	attackFiniteStateMachine.addState(dislocationState);

	// MAIN FSM
	auto strayBrick = std::make_shared<StrayState>(strayFiniteStateMachine, context);
	auto attackBrick = std::make_shared<fsm::SubStateMachine>(attackFiniteStateMachine);
	auto panicBrick = std::make_shared<PanicState>(context);

	auto conditionFromStrayToAttack = std::make_shared<TargetInSightCodition>(context);
	auto transitionFromStrayToAttack = std::make_shared<fsm::Transition>(conditionFromStrayToAttack, attackBrick);
	strayBrick->addTransition(transitionFromStrayToAttack);

	auto conditionFromStrayToAttackByDamage = std::make_shared<TakingDamageCondition>(context);
	auto transitionFromStrayToAttackByDamage = std::make_shared<fsm::Transition>(conditionFromStrayToAttackByDamage, attackBrick);
	strayBrick->addTransition(transitionFromStrayToAttackByDamage);

	auto conditionFromStrayToPanic = std::make_shared<OutOfDomainCondition>(context);
	auto transitionFromStrayToPanic = std::make_shared<fsm::Transition>(conditionFromStrayToPanic, panicBrick);
	strayBrick->addTransition(transitionFromStrayToPanic);
	
	auto conditionFromAttackToRestTargetIsOut = std::make_shared<TargetOutSightCodition>(context);
	auto transitionFromAttackToRestTargetIsOut = std::make_shared<fsm::Transition>(conditionFromAttackToRestTargetIsOut, strayBrick);
	attackBrick->addTransition(transitionFromAttackToRestTargetIsOut);

	auto conditionFromAttackToPanic = std::make_shared<OutOfDomainCondition>(context);
	auto transitionFromAttackToPanic = std::make_shared<fsm::Transition>(conditionFromAttackToPanic, panicBrick);
	attackBrick->addTransition(transitionFromAttackToPanic);

	auto conditionFromPanicToStray = std::make_shared<fsm::ConditionComplete>(panicBrick);
	auto transitionFromPanicToStray = std::make_shared<fsm::Transition>(conditionFromPanicToStray, strayBrick);
	panicBrick->addTransition(transitionFromPanicToStray);

	finiteStateMachine = fsm::FiniteStateMachine();
	finiteStateMachine.addState(strayBrick, true);
	finiteStateMachine.addState(attackBrick);
	finiteStateMachine.addState(panicBrick);
}

void SimpleEnemyController::onEnter() {
	Controller::onEnter();

	finiteStateMachine.enter();
	context->setDomain(Ether::getInstance()->getCurrentDomain());
	context->setObstacleSystem(dynamic_cast<ObstacleSystem*>(Director::getInstance()->getRunningScene()->getChildByName(OBSTACLE_SYSTEM)));
	context->setObstacle(HelperMethodsUtil::findComponent<Obstacle>(_owner));
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
}

void SimpleEnemyController::onExit() {
	Controller::onExit();
	finiteStateMachine.exit();
	_owner->getEventDispatcher()->removeEventListener(deathEventListener);
	_owner->getScheduler()->unschedule(schedule_selector(SimpleEnemyController::updateCooldown), this);
}

void SimpleEnemyController::update(float dt) {
	Controller::update(dt);
	if (!isDead) {
		finiteStateMachine.update();
	}
}

void SimpleEnemyController::updateCooldown(float dt)
{
	context->setShotReady(true);
}

void SimpleEnemyController::fire(Health* targetHealth)
{
	context->setShotReady(false);
	auto skillNode = config.skillNodeSupplier(targetHealth);
	config.cannon->fire([skillNode]() { return skillNode; });

	_owner->getScheduler()->schedule(schedule_selector(SimpleEnemyController::updateCooldown), this, config.cooldown, 0, config.cooldown, false);
}

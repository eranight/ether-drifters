#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class Touchable : public cocos2d::Component {
	public:
		~Touchable();
		static Touchable* create(const cocos2d::Size& touchSize, const cocos2d::Vec2& offset = cocos2d::Vec2::ZERO, bool debugMode = false);
		bool init(const cocos2d::Size& touchSize, const cocos2d::Vec2& offset, bool debugMode);
		void onAdd() override;
		void onRemove() override;
	public:
		cocos2d::Rect getRect() { return rect; }
	private:
		cocos2d::DrawNode* debugMask = nullptr;
		cocos2d::Rect rect;
	public:
		static const std::string NAME;
	};

}
#include "Tracking.h"
#include "Tracked.h"
#include "Utils/HelperMethodsUtil.h"
#include "Utils/Constants.h"
#include "Events/TrackedEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Tracking::NAME = "tracking";

Tracking::~Tracking() {
	CC_SAFE_RELEASE_NULL(physicsListener);
	CC_SAFE_RELEASE_NULL(unreachableTrackedListener);
	CC_SAFE_RELEASE_NULL(startTrackingListener);
	CC_SAFE_RELEASE_NULL(stopTrackingListener);
}

bool Tracking::init() {
	if (!Component::init()) {
		return false;
	}

	physicsListener = EventListenerPhysicsContact::create();
	if (physicsListener == nullptr) {
		return false;
	}
	physicsListener->onContactBegin = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesByContactMasks(contact, ContactMask::tracking, ContactMask::tracked);
		if (nodes.empty() || nodes.at(0) == nodes.at(1) || nodes.at(0) != _owner) {
			return false;
		}
		Tracked* tracked = HelperMethodsUtil::findComponent<Tracked>(nodes.at(1));
		if (tracked != nullptr) {
			if (tracked->isReachable()) {
				focus(tracked);
				return true;
			}
		}
		return false;
	};
	physicsListener->onContactSeparate = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesFromPhysicsContact(contact, _owner, ContactMask::tracking);
		if (nodes.empty()) {
			return;
		}
		Tracked* tracked = HelperMethodsUtil::findComponent<Tracked>(nodes.at(1));
		if (tracked != nullptr) {
			unfocus(tracked);
		}
	};
	physicsListener->retain();

	unreachableTrackedListener = EventListenerCustom::create(UNREACHABLE_TRACKED_EVENT, [this](EventCustom* custom) {
		TrackedEventData* eventData = reinterpret_cast<TrackedEventData*>(custom->getUserData());
		interrupt(eventData->getTracked());
	});
	unreachableTrackedListener->retain();

	startTrackingListener = EventListenerCustom::create(START_TRACKING_EVENT, [this](EventCustom* custom) {
		TrackedEventData* eventData = reinterpret_cast<TrackedEventData*>(custom->getUserData());
		if (eventData == nullptr) {
			return;
		}
		if (eventData->getOwner() != _owner) {
			return;
		}
		Tracked* tracked = eventData->getTracked();
		if (currentTrackeds.find(tracked) == currentTrackeds.end()) {
			currentTrackeds.pushBack(tracked);
			if (visibleTrackeds.find(tracked) == visibleTrackeds.end()) {
			}
		}
	});
	startTrackingListener->retain();

	stopTrackingListener = EventListenerCustom::create(STOP_TRACKING_EVENT, [this](EventCustom* custom) {
		TrackedEventData* eventData = reinterpret_cast<TrackedEventData*>(custom->getUserData());
		if (eventData == nullptr) {
			return;
		}
		if (eventData->getOwner() != _owner) {
			return;
		}
		Tracked* tracked = eventData->getTracked();
		if (currentTrackeds.find(tracked) != currentTrackeds.end()) {
			currentTrackeds.eraseObject(tracked, true);
		}
	});
	stopTrackingListener->retain();

	setName(NAME);
	return true;
}

void Tracking::onEnter() {
	Component::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithSceneGraphPriority(physicsListener, _owner);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(unreachableTrackedListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(startTrackingListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(stopTrackingListener, 1);
}

void Tracking::onExit() {
	Component::onExit();

	_owner->getEventDispatcher()->removeEventListener(physicsListener);
	_owner->getEventDispatcher()->removeEventListener(unreachableTrackedListener);
	_owner->getEventDispatcher()->removeEventListener(startTrackingListener);
	_owner->getEventDispatcher()->removeEventListener(stopTrackingListener);
}

bool Tracking::containInVision(Tracked* tracked)
{
	return visibleTrackeds.contains(tracked);
}

void Tracking::focus(Tracked* tracked) {
	bool needToDispatchEvent = visibleTrackeds.find(tracked) == visibleTrackeds.end();
	visibleTrackeds.pushBack(tracked);
	if (needToDispatchEvent) {
		// generate FocusTrackedEvent
		TrackedEventData eventData(_owner, tracked);
		_owner->getEventDispatcher()->dispatchCustomEvent(FOCUS_TRACKED_EVENT, &eventData);
	}
}

void Tracking::unfocus(Tracked* tracked) {
	int count = std::count(visibleTrackeds.begin(), visibleTrackeds.end(), tracked);
	if (count > 0) {
		visibleTrackeds.eraseObject(tracked);
		if (count == 1) {
			// generate UnfocusTrackedEvent
			TrackedEventData eventData(_owner, tracked);
			_owner->getEventDispatcher()->dispatchCustomEvent(UNFOCUS_TRACKED_EVENT, &eventData);
		}
	}
}

void Tracking::interrupt(Tracked* tracked) {
	if (currentTrackeds.find(tracked) != currentTrackeds.end()) {
		currentTrackeds.eraseObject(tracked, true);
		// generate InterruptTrackingEvent
		InterruptTrackingEventData eventData(_owner, tracked, InterruptTrackingReason::unreachable);
		_owner->getEventDispatcher()->dispatchCustomEvent(INTERRUPT_TRACKING_EVENT, &eventData);
		return;
	}
}
#pragma once

#include "cocos2d.h"

namespace ether_drifters {

	class ObstacleSystem;

	class ObstacleShape {
	public:
		cocos2d::Vec2 offset;
		float radius;
		cocos2d::Vec2 position;
		cocos2d::Node* owner = nullptr;
		uint32_t id;
	};

	class Obstacle : public cocos2d::Component {
	private:
		friend ObstacleSystem;
		Obstacle(uint32_t mask, std::vector<ObstacleShape> shapes) : mask(mask), shapes(shapes) {}
	public:
		static Obstacle* create(uint32_t mask, std::vector<ObstacleShape> shapes);
		bool init() override;
		void onAdd() override;
		void onRemove() override;
	public:
		float getRadius() { return radius; }
		uint32_t getMask() { return mask; }
		std::vector<ObstacleShape>& getShapes() { return shapes; }
	private:
		uint32_t getId() const { return id; }
		void setId(uint32_t id) { this->id = id; }
	private:
		float radius;
		uint32_t mask;
		uint32_t id; // set by obstacle system
		std::vector<ObstacleShape> shapes;
	public:
		static const std::string NAME;
		static const uint32_t PATH_OBSTACLE;
		static const uint32_t FIRE_OBSTACLE;
	};

}
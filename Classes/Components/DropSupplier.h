#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters {

	class DropSupplierConfig {
	public:
		std::vector<cocos2d::Vec2> points;
		std::vector<std::string> spriteFrames;
		std::string eventName;
		DropType type;
		int quantity;
		float physicsBodyDelay;
		float lifeTime;
	};

	class DropSupplier : public cocos2d::Component {
	private:
		DropSupplier(const DropSupplierConfig& config) : config(config), nextPointIndex(0), eventListener(nullptr) {}
	public:
		static DropSupplier* create(const DropSupplierConfig& config);
		~DropSupplier();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		void generateDrop(int num);
	private:
		DropSupplierConfig config;
		int nextPointIndex;
		cocos2d::EventListenerCustom* eventListener;
	public:
		static const std::string NAME;
	};

}
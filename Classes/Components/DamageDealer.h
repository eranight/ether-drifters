#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters {

	class DamageDealerConfig {
	public:

	};

	class DamageDealer : public cocos2d::Component {
		CREATE_FUNC_WITH_CONFIG(DamageDealer, DamageDealerConfig);
	public:
		bool init() override;
	public:
		static const std::string NAME;
	};

}
#include "Regeneration.h"
#include "Health.h"
#include "Events/HealthEventData.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Regeneration::NAME = "regeneration";

Regeneration::~Regeneration() {
	CC_SAFE_RELEASE_NULL(startRegenerationEventListener);
	CC_SAFE_RELEASE_NULL(stopRegenerationEventListener);
	CC_SAFE_RELEASE_NULL(deathEventListener);
	CC_SAFE_RELEASE_NULL(health);
}

Regeneration* Regeneration::create(const RegenerationConfig& config) {
	auto ref = new (std::nothrow) Regeneration(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Regeneration::init() {
	if (!Component::init()) {
		return false;
	}

	startRegenerationEventListener = EventListenerCustom::create(START_REGENERATION_EVENT, [this](EventCustom* custom) {
		EventData* eventData = reinterpret_cast<EventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner) {
			return;
		}
		startRegeneration();
	});
	startRegenerationEventListener->retain();

	stopRegenerationEventListener = EventListenerCustom::create(STOP_REGENERATION_EVENT, [this](EventCustom* custom) {
		EventData* eventData = reinterpret_cast<EventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner) {
			return;
		}
		stopRegeneration();
	});
	stopRegenerationEventListener->retain();

	deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() != _owner) {
			return;
		}
		stopRegeneration();
	});
	deathEventListener->retain();

	setName(NAME);
	return true;
}

void Regeneration::onEnter() {
	Component::onEnter();
	health = HelperMethodsUtil::findComponent<Health>(_owner);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(startRegenerationEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(stopRegenerationEventListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
}

void Regeneration::onExit() {
	Component::onExit();
	_owner->getEventDispatcher()->removeEventListener(startRegenerationEventListener);
	_owner->getEventDispatcher()->removeEventListener(stopRegenerationEventListener);
	_owner->getEventDispatcher()->removeEventListener(deathEventListener);
	_owner->getScheduler()->unschedule(schedule_selector(Regeneration::regenerate), this);
}

void Regeneration::setInterval(float interval) {
	config.interval = interval;
	dirty = true;
}

void Regeneration::startRegeneration() {
	HealthInfo info = health->getCurrentState();
	if (std::abs(info.maxHitPoints - info.currentHitPoints) < MATH_EPSILON) {
		return;
	}
	if (!isRunning) {
		isRunning = true;
		dirty = false;
		_owner->getScheduler()->schedule(schedule_selector(Regeneration::regenerate), this, config.interval, CC_REPEAT_FOREVER, config.delay, false);
	}
}

void Regeneration::stopRegeneration() {
	if (isRunning) {
		isRunning = false;
		_owner->getScheduler()->unschedule(schedule_selector(Regeneration::regenerate), this);
	}
}

void Regeneration::regenerate(float) {
	HealthInfo info = health->heal(config.heal);
	if (std::abs(info.maxHitPoints - info.currentHitPoints) < MATH_EPSILON) {
		stopRegeneration();
		return;
	}
	if (dirty) {
		dirty = false;
		_owner->getScheduler()->schedule(schedule_selector(Regeneration::regenerate), this, config.interval, CC_REPEAT_FOREVER, config.delay, false);
	}
}
#include "StrayController.h"
#include "Movement.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/DomainEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

StrayController* StrayController::create(const StrayControllerConfig& config) {
	auto ref = new (std::nothrow) StrayController(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

StrayController::~StrayController() {
	CC_SAFE_RELEASE_NULL(domainBorderEventListener);
}

bool StrayController::init() {
	if (!Controller::init()) {
		return false;
	}

	domainBorderEventListener = EventListenerCustom::create(BEGIN_TOUCH_DOMAIN_BORDER_EVENT, [this](EventCustom* custom) {
		DomainEventData* eventData = reinterpret_cast<DomainEventData*>(custom->getUserData());
		if (eventData->getConnectedNode() == _owner) {
			onContactWithEdge(nullptr);
		}
	});
	domainBorderEventListener->retain();

	return true;
}

void StrayController::onEnter() {
	Controller::onEnter();
	Node * view = HelperMethodsUtil::getViewIfExists(_owner);
	view->runAction(RepeatForever::create(RotateBy::create(36.0f, 360.0f)));

	movement = dynamic_cast<Movement*>(_owner->getComponent(Movement::NAME));
	CCASSERT(movement != nullptr, "node with stray controller must have a movement component");
	movement->setVelocity(1.0f);
	if (config.randomizeDirection) {
		float angle = RandomHelper::random_real(0.0f, 1.0f) * M_PI * 2.0f;
		movement->setDirection(Vec2::UNIT_X.rotateByAngle(Vec2::ZERO, angle));
	}
	else {
		movement->setDirection(config.direction);
	}
	
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(domainBorderEventListener, 1);
}

void StrayController::onExit() {
	Controller::onExit();
	_owner->getEventDispatcher()->removeEventListener(domainBorderEventListener);
}

void StrayController::onContactWithEdge(Node* domain) {
	Vec2 toCenter = HelperMethodsUtil::randomDirectionToPoint(Vec2::ZERO, _owner->getPosition(), 30.0f);
	movement->setDirection(toCenter);
}
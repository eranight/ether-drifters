#pragma once
#include "cocos2d.h"
#include "Events/TrackedEventData.h"

namespace ether_drifters {

	class Tracked;

	class Tracking : public cocos2d::Component {
	public:
		CREATE_FUNC(Tracking);
		virtual ~Tracking();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	public:
		bool containInVision(Tracked* tracked);
	private:
		void focus(Tracked* tracked);
		void unfocus(Tracked* tracked);
		void interrupt(Tracked* tracked);
	private:
		cocos2d::Vector<Tracked*> currentTrackeds;
		cocos2d::Vector<Tracked*> visibleTrackeds; // trackeds are in vision
		cocos2d::EventListenerPhysicsContact* physicsListener;
		cocos2d::EventListenerCustom* unreachableTrackedListener;
		cocos2d::EventListenerCustom* startTrackingListener;
		cocos2d::EventListenerCustom* stopTrackingListener;
	public:
		static const std::string NAME;
	};

}
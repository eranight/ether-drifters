#include "Damage.h"
#include "Health.h"
#include "DamageDealer.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/DamageEventData.h"
#include "Events/HealthEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Damage::NAME = "damage";

Damage::~Damage() {
	CC_SAFE_RELEASE_NULL(physicsListener);
	CC_SAFE_RELEASE_NULL(deathEventListener);
}

bool Damage::init() {
	if (!Component::init()) {
		return false;
	}

	if (config.damageTargetMode != DamageTargetMode::none) {
		physicsListener = EventListenerPhysicsContact::create();
		physicsListener->onContactBegin = [this](PhysicsContact& contact) {
			auto nodes = HelperMethodsUtil::fetchNodesByContactMasks(contact, ContactMask::hit, ContactMask::body);
			if (nodes.empty() || nodes.at(0) == nodes.at(1) || nodes.at(0) != _owner) {
				return false;
			}

			Health* health = HelperMethodsUtil::findComponent<Health>(nodes.at(1));
			if (health == nullptr ||
				config.damageTargetMode == DamageTargetMode::target && health != config.target ||
				config.dealer != nullptr && config.dealer->getOwner() == health->getOwner()) {

				return false;
			}

			DamageActionConfig actionConfig;
			actionConfig.damage = config.hit;
			actionConfig.interval = config.interval;
			actionConfig.quantity = config.interval > MATH_EPSILON ? CC_REPEAT_FOREVER : 1;
			actionConfig.target = health;
			actionConfig.owner = _owner;
			actionConfig.damageComponent = this;

			auto damageAction = DamageAction::create(actionConfig);
			damageActions.pushBack(damageAction);
			damageAction->start();

			return true;
		};
		physicsListener->onContactSeparate = [this](PhysicsContact& contact) {
			auto nodes = HelperMethodsUtil::fetchNodesByContactMasks(contact, ContactMask::hit, ContactMask::body);
			if (nodes.empty() || nodes.at(0) == nodes.at(1) || nodes.at(0) != _owner) {
				return;
			}
			Health* health = HelperMethodsUtil::findComponent<Health>(nodes.at(1));
			if (health == nullptr) {
				return;
			}
			for (auto action : damageActions) {
				action->loseTarget(health);
			}
		};
		physicsListener->retain();
	}
	
	deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* event) {
		auto eventData = HelperMethodsUtil::convertEventData<HealthEventData>(event);
		if (eventData != nullptr) {
			if (this->config.dealer != nullptr && this->config.dealer->getOwner() == eventData->getOwner()) {
				this->config.dealer = nullptr;
			}
			else {
				for (auto action : damageActions) {
					action->loseTarget(eventData->getHealth());
				}
			}
		}
	});
	deathEventListener->retain();

	setName(NAME);
	return true;
}

void Damage::onEnter() {
	Component::onEnter();

	if (physicsListener != nullptr) {
		_owner->getEventDispatcher()->addEventListenerWithSceneGraphPriority(physicsListener, _owner);
	}

	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
	
}

void Damage::onExit() {
	Component::onExit();

	if (physicsListener != nullptr) {
		_owner->getEventDispatcher()->removeEventListener(physicsListener);
	}

	_owner->getEventDispatcher()->removeEventListener(deathEventListener);
}

void Damage::update(float dt)
{
	Component::update(dt);

	std::remove_if(damageActions.begin(), damageActions.end(), [](DamageAction* damageAction) { return !damageAction->isRunning(); });
}

DamageAction::~DamageAction()
{
	if (config.quantity > 1) {
		Director::getInstance()->getScheduler()->unschedule(schedule_selector(DamageAction::update), this);
	}
}

void DamageAction::start()
{
	if (running) {
		return;
	}

	uint32_t quantity = config.quantity == CC_REPEAT_FOREVER ? config.quantity : std::clamp(config.quantity - 1, (uint32_t) 0, CC_REPEAT_FOREVER);

	dealDamage();

	if (quantity == 0) {
		running = false;
		return;
	}

	Director::getInstance()->getScheduler()->schedule(
		schedule_selector(DamageAction::update), 
		this, 
		config.interval, 
		quantity,
		0.0f, 
		false);
	running = true;
}

void DamageAction::loseTarget(Health* health)
{
	if (running && health != nullptr && config.target == health) {
		config.target = nullptr;
		running = false;
		Director::getInstance()->getScheduler()->unschedule(schedule_selector(DamageAction::update), this);
	}
}

void DamageAction::update(float dt)
{
	if (!running || config.target == nullptr) {
		return;
	}

	dealDamage();
	if (running && config.quantity != CC_REPEAT_FOREVER) {
		running = ++counter < config.quantity;
	}
}

void DamageAction::dealDamage()
{
	Health* cachedHealth = config.target;
	HealthInfo healthInfo = cachedHealth->damage(config.damage);
	DamageEventData eventData(config.owner, cachedHealth, config.damageComponent);
	config.owner->getEventDispatcher()->dispatchCustomEvent(DAMAGE_EVENT, &eventData);
	healthInfo = cachedHealth->getCurrentState();
	if (healthInfo.isDead) {
		running = false;
	}
}

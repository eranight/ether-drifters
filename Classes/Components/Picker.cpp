#include "Picker.h"
#include "Events/PickedEventData.h"
#include "Utils/HelperMethodsUtil.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Picker::NAME = "picker";

Picker::~Picker() {
	CC_SAFE_RELEASE_NULL(contactEventListener);
}

bool Picker::init() {
	if (!Component::init()) {
		return false;
	}

	contactEventListener = EventListenerPhysicsContact::create();
	contactEventListener->onContactBegin = [this] (PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesByContactMasks(contact, ContactMask::picker, ContactMask::drop);
		if (nodes.empty() || nodes.at(0) == nodes.at(1) || nodes.at(0) != _owner) {
			return false;
		}
		Dropped* dropped = HelperMethodsUtil::findComponent<Dropped>(nodes.at(1));
		if (dropped == nullptr || dropped->empty()) {
			return false;
		}
		contactedDroppeds.pushBack(dropped);
		return true;
	};
	contactEventListener->retain();

	setName(NAME);
	return true;
}

void Picker::onEnter() {
	Component::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactEventListener, _owner);
}

void Picker::onExit() {
	Component::onExit();

	_owner->getEventDispatcher()->removeEventListener(contactEventListener);
}

void Picker::update(float dt) {
	Component::update(dt);

	for (auto dropped : contactedDroppeds) {
		DroppedConfig drop = dropped->pick();
		if (drop.type != DropType::none) {
			PickedResource pickedResource;
			pickedResource.type = drop.type;
			pickedResource.quantity = drop.quantity;
			PickedEventData eventData(_owner, dropped, &pickedResource);
			_owner->getEventDispatcher()->dispatchCustomEvent(PICKED_DROP_EVENT, &eventData);
		}
	}
	contactedDroppeds.clear();
}
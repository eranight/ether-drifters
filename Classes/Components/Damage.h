#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters {

	class Health;
	class DamageDealer;
	class DamageAction;

	class DamageConfig {
	public:
		double hit = 0.0f;
		float interval = 0.0f;
		DamageDealer* dealer = nullptr;
		DamageTargetMode damageTargetMode = DamageTargetMode::none;
		Health* target;
	};

	class Damage : public cocos2d::Component {
		CREATE_FUNC_WITH_CONFIG(Damage, DamageConfig);
	public:
		virtual ~Damage();
		bool init();
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	public:
		DamageDealer* getDealer() { return config.dealer; }
	private:
		cocos2d::EventListenerPhysicsContact* physicsListener = nullptr;
		cocos2d::EventListenerCustom* deathEventListener = nullptr;
		cocos2d::Vector<DamageAction*> damageActions;
	public:
		static const std::string NAME;
	};

	class DamageActionConfig {
	public:
		double damage;
		float interval;
		uint32_t quantity;
		Health* target;
		cocos2d::Node* owner;
		Damage* damageComponent;
	};

	// IMPORTANT: it is not a cocos action!
	class DamageAction : public cocos2d::Ref {
		CREATE_FUNC_WITH_CONFIG(DamageAction, DamageActionConfig);
	public:
		~DamageAction();
		bool init() { return true; }
	public:
		void start();
		bool isRunning() { return running; }
		void loseTarget(Health* health);
	private:
		void update(float dt);
		void dealDamage();
	private:
		bool running = false;
		int counter = 0;
	};

}
#include "SkillSet.h"
#include "Events/SkillEventData.h"
#include "Prefubs/Skills/Skill.h"

using namespace cocos2d;
using namespace ether_drifters;

SkillWrapper::SkillWrapper(Node* owner, Skill * skill, float reloadTime) :
	owner(owner), skill(skill), reloadTime(reloadTime), ready(true) {
	if (owner != nullptr) {
		owner->retain();
	}
	skill->retain();

	filter = skill->goal == Goal::target && skill->type == PrefubConverter::PrefubType::node ?
		SkillFilter::attack :
		SkillFilter::passive_buf;
}

SkillWrapper::~SkillWrapper() {
	CC_SAFE_RELEASE_NULL(owner);
	CC_SAFE_RELEASE_NULL(skill);
}

SkillWrapper* SkillWrapper::create(Node* owner, Skill* skill, float reloadTime) {
	auto ref = new (std::nothrow) SkillWrapper(owner, skill, reloadTime);
	if (ref != nullptr) {
		ref->autorelease();
	}
	else {
		ref = nullptr;
	}
	return ref;
}

void SkillWrapper::onExit() {
	owner->getScheduler()->unschedule(schedule_selector(SkillWrapper::finishReloading), this);
}

Skill* SkillWrapper::use() {
	if (!ready) return nullptr;
	reload(reloadTime);
	return skill;
}

void SkillWrapper::reload(float time) {
	if (!owner->getScheduler()->isScheduled(schedule_selector(SkillWrapper::finishReloading), this)) {
		ready = false;
		owner->getScheduler()->schedule(schedule_selector(SkillWrapper::finishReloading), this, reloadTime, 0, 0, false);
		owner->getEventDispatcher()->dispatchCustomEvent(START_RELOAD_SKILL_EVENT);
	}
}

void SkillWrapper::finishReloading(float dt) {
	ready = true;
	owner->getEventDispatcher()->dispatchCustomEvent(FINISH_RELOAD_SKILL_EVENT);
}

const std::string SkillSet::NAME = "skill_set";

SkillSet::~SkillSet() {
	CC_SAFE_RELEASE_NULL(globalReloadAction);
}

bool SkillSet::init() {
	if (!Component::init()) {
		return false;
	}

	globalReload = false;
	globalReloadAction = Sequence::createWithTwoActions(
		DelayTime::create(0.3f),
		CallFunc::create([this]() { this->globalReload = false; })
	);
	globalReloadAction->retain();

	startReloadingEventListener = EventListenerCustom::create(START_RELOAD_SKILL_EVENT, [this](EventCustom * custom) {
		SkillEventData* eventData = reinterpret_cast<SkillEventData*>(custom->getUserData());
		if (!globalReload) {
			globalReload = true;
			_owner->runAction(globalReloadAction->clone());
		}
	});

	setName(NAME);
	return true;
}

void SkillSet::onAdd() {
	Component::onAdd();
	for (auto skill : skills) {
		skill->owner = _owner;
	}
}

void SkillSet::onEnter() {
	Component::onEnter();
}

void SkillSet::onExit() {
	Component::onExit();
}

void SkillSet::addSkill(Skill* skill) {
	skills.pushBack(SkillWrapper::create(_owner, skill, skill->cooldown));
}

UsableSkill* SkillSet::getFirstReadySkill(const SkillFilter& filter) {
	auto iter = std::find_if(skills.begin(), skills.end(), [filter](SkillWrapper* skill) { return skill->isReady() && skill->filter == filter; });
	return iter != skills.end() ? *iter : nullptr;
}
#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class MinedResources {
	public:
		MinedResources(float points=0.0f) : points(points) {}
		int getPoints() const { return points; }
	private:
		int points;
	};

	class MinedConfig {
	public:
		int resourcePoints;
		double strengthPoints;
	};

	class Mined : public cocos2d::Component {
	private:
		Mined(const MinedConfig& config) :
			config(config),
			resourcePoints(config.resourcePoints),
			strengthPoints(config.strengthPoints),
			deathEventListener(nullptr)
		{}
	public:
		static Mined* create(const MinedConfig& config);
		~Mined();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	public:
		MinedResources mining(double hit);
		bool isEmpty() { return resourcePoints == 0; }
		int getRemainingResourcePoints() { return resourcePoints; }
	private:
		MinedConfig config;
		int resourcePoints;
		double strengthPoints;
		cocos2d::EventListenerCustom* deathEventListener;
	public:
		static const std::string NAME;
	};

}
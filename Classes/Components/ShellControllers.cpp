#include "ShellControllers.h"
#include "Movement.h"
#include "Direction.h"
#include "Events/DomainEventData.h"
#include "Events/DamageEventData.h"
#include "Utils/HelperMethodsUtil.h"
#include "Health.h"

using namespace cocos2d;
using namespace ether_drifters;

SimpleShellController* SimpleShellController::create(const ShellControllerConfig& config) {
	auto ref = new (std::nothrow) SimpleShellController(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

SimpleShellController::~SimpleShellController() {
	CC_SAFE_RELEASE_NULL(domainBorderListener);
	CC_SAFE_RELEASE_NULL(lifeTimeAction);
	CC_SAFE_RELEASE_NULL(damageEventListener);
}

bool SimpleShellController::init() {
	if (!AbstractShellController::init()) {
		return false;
	}

	domainBorderListener = EventListenerCustom::create(BEGIN_TOUCH_DOMAIN_BORDER_EVENT, [this](EventCustom* custom) {
		DomainEventData* eventData = reinterpret_cast<DomainEventData*>(custom->getUserData());
		if (eventData->getConnectedNode() == _owner) {
			_owner->stopAction(lifeTimeAction);
			this->config.touchBorderReaction(eventData->getOwner());
		}
	});
	domainBorderListener->retain();

	damageEventListener = EventListenerCustom::create(DAMAGE_EVENT, [this](EventCustom* custom) {
		DamageEventData* eventData = reinterpret_cast<DamageEventData*>(custom->getUserData());
		if (eventData->getOwner() == _owner) {
			_owner->stopAction(lifeTimeAction);
			this->config.touchNodeReaction(HelperMethodsUtil::getRightNode(eventData->getHealth()->getOwner()));
		}
	});
	damageEventListener->retain();

	lifeTimeAction = Sequence::createWithTwoActions(
		DelayTime::create(config.lifeTime),
		CallFunc::create(config.lifeTimeOverReaction)
	);
	lifeTimeAction->retain();

	return true;
}

void SimpleShellController::onEnter() {
	Controller::onEnter();
	_owner->runAction(lifeTimeAction);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(domainBorderListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(damageEventListener, 1);

	Vec2 dir = config.target != nullptr ? (config.target->getPosition() - _owner->getPosition()) : config.direction;

	Movement* shellMovement = HelperMethodsUtil::findComponent<Movement>(_owner);
	shellMovement->setDirection(dir);
	shellMovement->setVelocity(config.velocity);

	HelperMethodsUtil::findComponent<Direction>(_owner)->setDirection(dir);
}

void SimpleShellController::onExit() {
	Controller::onExit();

	_owner->getEventDispatcher()->removeEventListener(domainBorderListener);
	_owner->getEventDispatcher()->removeEventListener(damageEventListener);
}
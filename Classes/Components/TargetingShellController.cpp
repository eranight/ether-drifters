#include "TargetingShellController.hpp"
#include "Movement.h"
#include "Direction.h"
#include "Health.h"
#include "Events/DomainEventData.h"
#include "Events/DamageEventData.h"
#include "Events/HealthEventData.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

TargetingShellController::~TargetingShellController()
{
	CC_SAFE_RELEASE_NULL(domainBorderListener);
	CC_SAFE_RELEASE_NULL(damageEventListener);
	CC_SAFE_RELEASE_NULL(deathEventListener);
	CC_SAFE_RELEASE_NULL(trackedNode);
}

TargetingShellController* TargetingShellController::create(const ShellControllerConfig& config)
{
	auto ret = new (std::nothrow) TargetingShellController(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool TargetingShellController::init()
{
	if (!AbstractShellController::init()) {
		return false;
	}

	trackedNode = config.target;
	if (trackedNode != nullptr) {
		trackedNode->retain();
	}

	domainBorderListener = EventListenerCustom::create(BEGIN_TOUCH_DOMAIN_BORDER_EVENT, [this](EventCustom* custom) {
		DomainEventData* eventData = reinterpret_cast<DomainEventData*>(custom->getUserData());
		if (eventData->getConnectedNode() == _owner) {
			this->config.touchBorderReaction(eventData->getOwner());
		}
	});
	domainBorderListener->retain();

	damageEventListener = EventListenerCustom::create(DAMAGE_EVENT, [this](EventCustom* custom) {
		DamageEventData* eventData = reinterpret_cast<DamageEventData*>(custom->getUserData());
		if (eventData->getOwner() == _owner) {
			this->config.touchNodeReaction(HelperMethodsUtil::getRightNode(eventData->getHealth()->getOwner()));
		}
	});
	damageEventListener->retain();

	if (trackedNode != nullptr) {
		deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* custom) {
			if (trackedNode == nullptr) {
				return;
			}
			HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
			if (eventData->getOwner() == trackedNode) {
				deathEventListener->setEnabled(false);
				CC_SAFE_RELEASE_NULL(trackedNode);
			}
			});
		deathEventListener->retain();
	}

	return true;
}

void TargetingShellController::onEnter()
{
	AbstractShellController::onEnter();

	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(domainBorderListener, 1);
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(damageEventListener, 1);
	if (deathEventListener != nullptr) {
		_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
	}

	movement = HelperMethodsUtil::findComponent<Movement>(_owner);
	movement->setVelocity(config.velocity);
	direction = HelperMethodsUtil::findComponent<Direction>(_owner);

	if (trackedNode == nullptr) {
		movement->setDirection(config.direction);
		direction->setDirection(config.direction);
	}
}

void TargetingShellController::onExit()
{
	AbstractShellController::onExit();

	_owner->getEventDispatcher()->removeEventListener(domainBorderListener);
	_owner->getEventDispatcher()->removeEventListener(damageEventListener);
	if (deathEventListener != nullptr) {
		_owner->getEventDispatcher()->removeEventListener(deathEventListener);
	}
}

void TargetingShellController::update(float dt)
{
	AbstractShellController::update(dt);

	if (trackedNode == nullptr) {
		return;
	}

	Vec2 dir = trackedNode->getPosition() - _owner->getPosition();
	direction->setDirection(dir);
	movement->setDirection(dir);
}

#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class Destroyable : public cocos2d::Component {
	private:
		Destroyable() : deathEventListener(nullptr) {}
	public:
		static Destroyable* create(const std::function<void()>& destroyCallback);
		~Destroyable();
		bool init(const std::function<void()>& destroyCallback);
		void onEnter() override;
		void onExit() override;
	private:
		std::function<void()> destroyCallback;
		cocos2d::EventListenerCustom* deathEventListener;
	public:
		static const std::string NAME;
	};

}
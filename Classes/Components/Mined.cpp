#include "Mined.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/MinedEventData.h"
#include "Events/HealthEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Mined::NAME = "mined";

Mined* Mined::create(const MinedConfig& config) {
	auto ref = new (std::nothrow) Mined(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

Mined::~Mined() {
	CC_SAFE_RELEASE_NULL(deathEventListener);
}

bool Mined::init() {
	if (!Component::init()) {
		return false;
	}

	deathEventListener = EventListenerCustom::create(DEATH_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() == _owner) {
			MinedEventData eventData(_owner, this);
			_owner->getEventDispatcher()->dispatchCustomEvent(WASTE_MINED_RESOURCES_EVENT, &eventData);
		}
	});
	deathEventListener->retain();

	setName(NAME);
	return true;
}

void Mined::onEnter() {
	Component::onEnter();
	_owner->getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);
}

void Mined::onExit() {
	Component::onExit();
	_owner->getEventDispatcher()->removeEventListener(deathEventListener);
}

MinedResources Mined::mining(double hit) {
	double maxHit = std::min(strengthPoints, hit);
	double beforeHitStrenghtPoints = std::ceil(strengthPoints);
	strengthPoints -= maxHit;
	if (strengthPoints <= MATH_EPSILON) {
		strengthPoints = 0.0;
		MinedEventData eventData(_owner, this);
		_owner->getEventDispatcher()->dispatchCustomEvent(WASTE_MINED_RESOURCES_EVENT, &eventData);
	}
	int resources = std::min((int)std::floor(beforeHitStrenghtPoints - std::ceil(strengthPoints)), resourcePoints);
	resourcePoints -= resources;
	return MinedResources(resources);
}
#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "cocos2d.h"

namespace ether_drifters {

	class Controller : public cocos2d::Component {
	public:
		bool init() override;
		static const std::string NAME;
	};

}

#endif //__CONTROLLER_H__
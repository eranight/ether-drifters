#include "ComponentCreator.h"
#include "DamageDealer.h"
#include "Damage.h"
#include "Health.h"
#include "Tracked.h"
#include "Tracking.h"
#include "Mined.h"
#include "Mining.h"
#include "Direction.h"
#include "Movement.h"
#include "Regeneration.h"

using namespace cocos2d;
using namespace rapidjson;
using namespace ether_drifters;

ComponentCreator ComponentCreator::componentCreator;

ComponentCreator::ComponentCreator() {
	mapper["damage_dealer"] = std::bind(&ComponentCreator::createDamageDealer, this, std::placeholders::_1);
	mapper["damage"] = std::bind(&ComponentCreator::createDamage, this, std::placeholders::_1);
	mapper["tracking"] = std::bind(&ComponentCreator::createTracking, this, std::placeholders::_1);
	mapper["tracked"] = std::bind(&ComponentCreator::createTracked, this, std::placeholders::_1);
	mapper["mining"] = std::bind(&ComponentCreator::createMining, this, std::placeholders::_1);
	mapper["mined"] = std::bind(&ComponentCreator::createMined, this, std::placeholders::_1);
	mapper["direction"] = std::bind(&ComponentCreator::createDirection, this, std::placeholders::_1);
	mapper["health"] = std::bind(&ComponentCreator::createHealth, this, std::placeholders::_1);
	mapper["movement"] = std::bind(&ComponentCreator::createMovement, this, std::placeholders::_1);
	mapper["regeneration"] = std::bind(&ComponentCreator::createRegeneration, this, std::placeholders::_1);
}

Component* ComponentCreator::create(const rapidjson::Value& val) {
	CCASSERT(val.HasMember("type"), "jsonNode must has member type");
	std::string type = val["type"].GetString();
	auto funcIter = componentCreator.mapper.find(type);
	if (funcIter != componentCreator.mapper.end()) {
		return funcIter->second(val);
	}
	CCLOG("component type %s not found", type);
	return nullptr;
}

Component* ComponentCreator::createDamageDealer(const rapidjson::Value& val) {
	return DamageDealer::create({});
}

Component* ComponentCreator::createDamage(const rapidjson::Value& val) {
	DamageConfig config;
	config.hit = val["hit"].GetDouble();
	config.interval = val["interval"].GetFloat();
	std::string target = val["target"].GetString();
	if (target == "none") {
		config.damageTargetMode = DamageTargetMode::none;
	}
	else if (target == "any") {
		config.damageTargetMode = DamageTargetMode::any;
	} else if (target == "target") {
		config.damageTargetMode = DamageTargetMode::target;
	}
	else {
		CCLOG("unknown target checker type %s", target);
		config.damageTargetMode = DamageTargetMode::none;
	}
	return Damage::create(config);
}

Component* ComponentCreator::createTracking(const rapidjson::Value& val) {
	return Tracking::create();
}

Component* ComponentCreator::createTracked(const rapidjson::Value& val) {
	return Tracked::create();
}

Component* ComponentCreator::createMined(const rapidjson::Value& val) {
	return Mined::create({});
}

Component* ComponentCreator::createMining(const rapidjson::Value& val) {
	return Mining::create({});
}

Component* ComponentCreator::createHealth(const rapidjson::Value& val) {
	HealthConfig config;
	config.hitPoints = val["hitpoints"].GetDouble();
	config.currentHitPoints = val.HasMember("current_hitpoints") ? val["current_hitpoints"].GetDouble() : config.hitPoints;
	return Health::create(config);
}

Component* ComponentCreator::createRegeneration(const rapidjson::Value& val) {
	RegenerationConfig config;
	config.heal = val["heal"].GetDouble();
	config.interval = val["interval"].GetDouble();
	config.delay = val["delay"].GetDouble();
	return Regeneration::create(config);
}

Component* ComponentCreator::createDirection(const rapidjson::Value& val) {
	float x = val["x"].GetFloat();
	float y = val["y"].GetFloat();
	return Direction::create(Vec2(x, y));
}

Component* ComponentCreator::createMovement(const rapidjson::Value& val) {
	return Movement::create();
}
#include "Health.h"
#include "Events/HealthEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Health::NAME = "health";

Health* Health::create(const HealthConfig& config) {
	auto ref = new (std::nothrow) Health();
	if (ref != nullptr && ref->init(config)) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Health::init(const HealthConfig& config) {
	if (!Component::init()) {
		return false;
	}

	maxHitPoints = config.hitPoints;
	this->currentHitPoints = config.currentHitPoints;

	setName(NAME);
	return true;
}

void Health::update(float dt) {
	Component::update(dt);

	while (!events.empty()) {
		auto& h = events.front();
		HealthEventData eventData(_owner, this, &h.info);
		_owner->getEventDispatcher()->dispatchCustomEvent(h.name, &eventData);
		events.pop_front();
	}
}

HealthInfo Health::getCurrentState() {
	return HealthInfo(maxHitPoints, currentHitPoints, currentHitPoints, 0.0, std::abs(currentHitPoints) < MATH_EPSILON);
}

HealthInfo Health::damage(double hit) {
	if (currentHitPoints <= 0.0f) {
		return HealthInfo(maxHitPoints, 0.0f, 0.0f, 0.0f, true);
	}

	double prev = currentHitPoints;
	hit = std::min(currentHitPoints, hit);
	currentHitPoints -= hit;
	
	bool isDead = std::abs(currentHitPoints) < MATH_EPSILON;

	HealthInfo healthInfo(maxHitPoints, prev, currentHitPoints, hit, isDead);
	events.push_back(Health::HealthInfoEvent(TAKING_DAMAGE_EVENT, healthInfo));

	if (isDead) {
		events.push_back(Health::HealthInfoEvent(DEATH_EVENT, healthInfo));
	}

	return healthInfo;
}

HealthInfo Health::heal(double hit) {
	double prev = currentHitPoints;
	hit = std::min(maxHitPoints - currentHitPoints, hit);
	currentHitPoints += hit;

	HealthInfo healthInfo(maxHitPoints, prev, currentHitPoints, hit, false);
	events.push_back(Health::HealthInfoEvent(TAKING_HEALING_EVENT, healthInfo));

	return healthInfo;
}
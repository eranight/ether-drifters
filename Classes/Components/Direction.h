#ifndef __DIRECTION_H__
#define __DIRECTION_H__

#include "cocos2d.h"

namespace ether_drifters
{

	class Direction : public cocos2d::Component
	{
	protected:
		Direction(const cocos2d::Vec2 & direction) : direction(direction) {}
	public:
		~Direction();
		static Direction * create(const cocos2d::Vec2 & direction);
		bool init() override;
		void onAdd() override;
		void onEnter() override;
		void update(float dt) override;
	public:
		float getVelocity() { return velocity; }
		void setVelocity(float velocity) { this->velocity = velocity; }

		cocos2d::Vec2 const getDirection() { return direction; }
		void setDirection(const cocos2d::Vec2 & direction);

		float getNextAngle() { return nextAngle; }
		void setNextAngle(float angle);
		bool isNextAndCurrentAnglesSync() { return ccw == 0; }
		void syncNextAndCurrentAngles() { ccw = 0; }
	private:
		void updateDirection();
	private:
		float velocity = 0.0f;
		cocos2d::Vec2 direction;
		float nextAngle = 0.0f;
		float remainedAngle = 0.0f;
		int ccw = 0;
#if ED_DEBUG
		cocos2d::DrawNode* debugNode = nullptr;
#endif
	public:
		static const std::string NAME;
	};

}

#endif //__DIRECTION_H__
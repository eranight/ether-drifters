#pragma once
#include "cocos2d.h"
#include "Mined.h"
#include "Events/MinedEventData.h"

namespace ether_drifters {

	class MiningConfig {
	public:
		float rate;
		double power;
	};

	class Mining : public cocos2d::Component {
	private:
		Mining(const MiningConfig& config) :
			config(config),
			dirty(false), 
			mined(nullptr), 
			physicsListener(nullptr),
			wastedMinedEventListener(nullptr),
			startMiningEventListener(nullptr), 
			stopdMiningEventListener(nullptr) {}
	public:
		static Mining* create(const MiningConfig& config);
		virtual ~Mining();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	public:
		float getRate() { return config.rate; }
		void setRate(float rate);
		double getPower() { return config.power; }
		void setPower(double power) { config.power = power; }
	private:
		void addMined(Mined* mined);
		void removeMined(Mined* mined, InterruptMiningReason reason, bool removeAll);
		void setMined(Mined* mined, InterruptMiningReason reason);
		void mining(float);
	private:
		MiningConfig config;
		bool dirty;
		Mined* mined;
		cocos2d::Vector<Mined*> currentMined;
		cocos2d::Vector<Mined*> possibleMineds;
		cocos2d::EventListenerPhysicsContact* physicsListener;
		cocos2d::EventListenerCustom* wastedMinedEventListener;
		cocos2d::EventListenerCustom* startMiningEventListener;
		cocos2d::EventListenerCustom* stopdMiningEventListener;
	public:
		static const std::string NAME;
	};

}
#pragma once
#include "cocos2d.h"
#include "Modification.h"

namespace ether_drifters {

	template<typename T, typename U>
	class Modifiable : public cocos2d::Ref {
	public:
		static Modifiable<T, U>* create() {
			Modifiable<T, U>* ref = new (std::nothrow) Modifiable<T, U>();
			if (ref != nullptr) {
				ref->autorelease();
			}
			else {
				CC_SAFE_DELETE(ref);
			}
			return ref;
		}

		void addModification(Modification<T, U>* modification) {
			if (modifications.find(modification) == modifications.end()) {
				modifications.pushBack(modification);
			}
		}

		void removeModification(Modification<T, U>* modification) {
			auto iter = std::find(modifications.begin(), modifications.end(), modification);
			if (iter != modifications.end()) {
				modifications.erase(iter);
			}
		}

		T applyModifications(T source, U component) {
			T result = source;
			for (Modification<T, U>* modification : modifications) {
				result = modification->apply(source, result, component);
			}
			return result;
		}

		int getSize() {
			return modifications.size();
		}

	private:
		cocos2d::Vector<Modification<T, U>*> modifications;
	};

}
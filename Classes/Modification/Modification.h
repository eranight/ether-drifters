#pragma once
#include <functional>
#include "cocos2d.h"

namespace ether_drifters {

	template<typename T, typename L>
	class Modification : public cocos2d::Ref {
	private:
		Modification(const std::function<T(T, T, L)>& func) :
			func(func) {}
	public:
		static Modification<T, L>* create(const std::function<T(T, T, L)>& func) {
			Modification<T, L>* ref = new (std::nothrow) Modification<T, L>(func);
			if (ref != nullptr) {
				ref->autorelease();
			}
			else {
				CC_SAFE_DELETE(ref);
			}
			return ref;
		}

		T apply(T source, T result, L component) {
			return func(source, result, component);
		}

	private:
		std::function<T(T, T, L)> func;
	};

}
#pragma once
#include "EventData.h"
#include <string>

namespace ether_drifters {

	enum class InputState {
		pressed, released
	};

	class InputEventData : public EventData {
	public:
		InputEventData(cocos2d::Node* owner, const std::string& action, InputState state) : 
			EventData(owner), action(action), state(state) {}
	public:
		const std::string& getAction() { return action; }
		InputState getState() { return state; }
	private:
		std::string action;
		InputState state;
	};

	// domain scene inputs
	static const std::string INPUT_UP = "input_up";
	static const std::string INPUT_DOWN = "input_down";
	static const std::string INPUT_RIGHT = "input_right";
	static const std::string INPUT_LEFT = "input_left";
	static const std::string INPUT_FIRE_RIGHT_BOARD = "input_fire_right_board";
	static const std::string INPUT_FIRE_LEFT_BOARD = "input_fire_left_board";
	static const std::string INPUT_CHANGE_FIRE_SKILL = "input_change_fire_skill";
	static const std::string INPUT_CHANGE_SPECIAL_SKILL = "input_change_special_skill";
	static const std::string INPUT_START_REPAIR = "input_start_repair";
	static const std::string INPUT_STOP_REPAIR = "input_stop_repair";
	static const std::string INPUT_SHART_REVERSAL = "input_sharp_reversal";
	static const std::string INPUT_AFTERBURNER = "input_afterburner";
	static const std::string INPUT_ACTIVATE_SPECIAL_SKILL = "input_activate_special_skill";

	// for domain debug purposes
	static const std::string INPUT_DEBUG_ROTATION_180 = "input_debug_rotation_180";

	// common inputs
	static const std::string INPUT_EXIT = "input_exit";
}
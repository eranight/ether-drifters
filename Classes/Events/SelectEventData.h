#pragma once
#include "EventData.h"

namespace ether_drifters
{

	class SelectEventData : public EventData {
	public:
		SelectEventData(cocos2d::Node* owner, cocos2d::Node* affected, bool selected) : 
			EventData(owner), affected(affected), selected(selected) {}
	public:
		cocos2d::Node* getAffected() { return affected; }
		bool isSelected() { return selected; }
	private:
		cocos2d::Node* affected;
		bool selected;
	};

	static const std::string SELECT_EVENT = "select_event";

}
#pragma once
#include "EventData.h"
#include <string>

namespace ether_drifters {

	class DomainEventData : public EventData {
	public:
		DomainEventData(cocos2d::Node* owner, cocos2d::Node* connectedNode, bool inside) :
			EventData(owner), connectedNode(connectedNode), inside(inside) {}
	public:
		cocos2d::Node* getConnectedNode() { return connectedNode; }
		bool isInside() { return inside; }
	private:
		cocos2d::Node* connectedNode;
		bool inside;
	};

	static const std::string BEGIN_TOUCH_DOMAIN_BORDER_EVENT = "begin_touch_domain_border_event";
	static const std::string END_TOUCH_DOMAIN_BORDER_EVENT = "end_touch_domain_border_event";

}
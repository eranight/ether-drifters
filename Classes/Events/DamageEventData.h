#pragma once
#include "EventData.h"

namespace ether_drifters {

	class Health;
	class Damage;

	class DamageEventData : public EventData {
	public:
		DamageEventData(cocos2d::Node* owner, Health * health, Damage* damage) : EventData(owner), health(health), damage(damage) {}
	public:
		Health* getHealth() { return health; }
		Damage* getDamage() { return damage; }
	private:
		Health* health;
		Damage* damage;
	};

	static const std::string DAMAGE_EVENT = "damage_event";

}
#pragma once
#include "EventData.h"

namespace ether_drifters {

	class Health;
	class HealthInfo;

	class HealthEventData : public EventData {
	public:
		HealthEventData(cocos2d::Node* owner, Health* health, HealthInfo* healthInfo) :
			EventData(owner), health(health), healthInfo(healthInfo) {}
	public:
		Health* getHealth() { return health; }
		HealthInfo* getHealthInfo() { return healthInfo; }
	private:
		Health* health;
		HealthInfo* healthInfo;
	};

	static const std::string DEATH_EVENT = "death_event";
	static const std::string TAKING_DAMAGE_EVENT = "taking_damage_event";
	static const std::string TAKING_HEALING_EVENT = "taking_healing_event";

}
#pragma once
#include "EventData.h"
#include <string>

namespace ether_drifters {

	class SkillWrapper;

	class SkillEventData : public EventData {
	public:
		SkillEventData(cocos2d::Node* owner, SkillWrapper* skill) : EventData(owner), skill(skill) {}
	public:
		SkillWrapper* getSkill() { return skill; }
	private:
		SkillWrapper* skill;
	};

	static const std::string START_RELOAD_SKILL_EVENT = "start_reload_skill_event";
	static const std::string FINISH_RELOAD_SKILL_EVENT = "finish_reload_skill_event";

}
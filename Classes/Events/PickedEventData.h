#pragma once
#include "EventData.h"
#include "Utils/Constants.h"

namespace ether_drifters {

	class Dropped;
	
	class PickedResource {
	public:
		DropType type;
		int quantity;
	};

	class PickedEventData : public EventData {
	public:
		PickedEventData(cocos2d::Node* owner, Dropped* dropped, PickedResource* drop) : EventData(owner), dropped(dropped), drop(drop) {}
	public:
		Dropped* getDropped() { return dropped; }
		PickedResource* getDrop() { return drop; }
	private:
		Dropped* dropped;
		PickedResource* drop;
	};


	static const std::string PICKED_DROP_EVENT = "picked_drop_event";
}
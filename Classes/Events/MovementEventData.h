#pragma once
#include "EventData.h"

namespace ether_drifters {

	class ChangeVelocityMovementEventData : public EventData {
	public:
		enum class ChangeVelocityType {
			base, // base velocity changed
			mod   // modifiable velocity changed
		};

		ChangeVelocityMovementEventData(cocos2d::Node* owner, const ChangeVelocityType& type, float prevVelocity,float nextVelocity) :
			EventData(owner), type(type), prevVelocity(prevVelocity), nextVelocity(nextVelocity) {}
	public:
		ChangeVelocityType getType() { return type; }
		float getPrevVelocity() { return prevVelocity; }
		float getNextVelocity() { return nextVelocity; }
	private:
		ChangeVelocityType type;
		float prevVelocity;
		float nextVelocity;
	};

	class FinishPathMovementEventData : public EventData {
	public:
		FinishPathMovementEventData(cocos2d::Node* owner) : EventData(owner) {}
	};

	static const std::string CHANGE_VELOCITY_EVENT = "change_velocity_event";
	static const std::string FINISH_PATH_EVENT = "finish_path_event";

}
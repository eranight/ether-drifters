#pragma once
#include "EventData.h"

namespace ether_drifters {

	class Touchable;

	class TouchEventData : public EventData {
	public:
		TouchEventData(cocos2d::Node* owner, Touchable* touchable) :
			EventData(owner), touchable(touchable) {}
	public:
		Touchable* getTouchable() { return touchable; }
	private:
		Touchable* touchable;
	};

	static const std::string TOUCH_EVENT = "touch_event";

}
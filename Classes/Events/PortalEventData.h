#pragma once
#include "EventData.h"
#include <string>

namespace ether_drifters {

	class PortalPath {
	public:
		PortalPath(const std::string& cluster, const std::string& domain, const std::string& portal) :
			cluster(cluster), domain(domain), portal(portal) {}
	public:
		std::string cluster;
		std::string domain;
		std::string portal;
	};

	class PortalEventData : public EventData {
	public:
		PortalEventData(cocos2d::Node* owner,
			cocos2d::Node* portedNode,
			const PortalPath& portalPath)
			: EventData(owner), portedNode(portedNode), portalPath(portalPath) {}
	public:
		cocos2d::Node* getPortedNode() { return portedNode; }
		PortalPath getPortalPath() { return portalPath; }
	private:
		cocos2d::Node* portedNode;
		PortalPath portalPath;
	};

	static const std::string PORTAL_EVENT = "portal_event";

}
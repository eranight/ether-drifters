#pragma once
#include <string>
#include "EventData.h"

namespace ether_drifters {

	class Tracked;

	class TrackedEventData : public EventData {
	public:
		TrackedEventData(cocos2d::Node* owner, Tracked* tracked) :
			EventData(owner), tracked(tracked) {}
	public:
		Tracked* getTracked() { return tracked; }
	private:
		Tracked* tracked;
	};

	enum class InterruptTrackingReason {
		unreachable  // tracked object is unreachable
	};

	class InterruptTrackingEventData : public TrackedEventData {
	public:
		InterruptTrackingEventData(cocos2d::Node* owner, Tracked* tracked, InterruptTrackingReason reason) :
			TrackedEventData(owner, tracked), reason(reason) {}
	public:
		InterruptTrackingReason getReason() { return reason; }
	private:
		InterruptTrackingReason reason;
	};

	static const std::string FOCUS_TRACKED_EVENT = "focus_tracked_event";
	static const std::string UNFOCUS_TRACKED_EVENT = "unfocus_tracked_event";
	static const std::string INTERRUPT_TRACKING_EVENT = "interrupt_tracking_event";
	static const std::string START_TRACKING_EVENT = "start_tracking_event";
	static const std::string STOP_TRACKING_EVENT = "stop_tracking_event";
	static const std::string UNREACHABLE_TRACKED_EVENT = "unreachable_tracked_event";

}
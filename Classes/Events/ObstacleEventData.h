#pragma once

#include "EventData.h"

namespace ether_drifters {

	static const std::string ADD_OBSTACLE_EVENT = "add_obstacle_event";
	static const std::string REMOVE_OBSTACLE_EVENT = "remove_obstacle_event";

}
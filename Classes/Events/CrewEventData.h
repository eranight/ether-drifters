#pragma once
#include "EventData.h"

namespace ether_drifters {

	class CrewEventData : public EventData {
	public:
		CrewEventData(cocos2d::Node* owner, int prevQuantity, int quantity) :
			EventData(owner), prevQuantity(prevQuantity), quantity(quantity)
		{}
	public:
		int getPrevQuantity() { return prevQuantity; }
		int getQuantity() { return quantity; }
	private:
		int prevQuantity;
		int quantity;
	};

	static const std::string CHANGED_CREW_QUANTITY_EVENT = "changed_crew_quantity_event";

}
#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class EventData {
	public:
		EventData(cocos2d::Node* owner) : owner(owner) {}
	public:
		cocos2d::Node* getOwner() { return owner; }
	private:
		cocos2d::Node* owner;
	};


	static const std::string START_REGENERATION_EVENT = "start_regerenation_event";
	static const std::string STOP_REGENERATION_EVENT = "stop_regeneration_event";
}
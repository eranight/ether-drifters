#pragma once
#include "cocos2d.h"
#include "MinedEventData.h"
#include "TrackedEventData.h"
#include "Utils/HelperMethodsUtil.h"

namespace ether_drifters {

	// use this classes instead of FocusEventListener

	template<typename T, typename U>
	class FocusEventListener : public cocos2d::Ref {
	protected:
		FocusEventListener() : onFocusedEvent(nullptr), onUnfocusedEvent(nullptr), onInterruptedEvent(nullptr)  {
		}
	public:
		~FocusEventListener() {
			if (owner != nullptr) {
				for (auto listener : listeners) {
					owner->getEventDispatcher()->removeEventListener(listener);
				}
				owner = nullptr;
			}
		}

		bool init(cocos2d::Node * owner, bool isDirectOwner, const std::string& focusEventName, const std::string& unfocusEventName, const std::string& interruptEventName ) {
			this->owner = owner;
			this->isDirectOwner = isDirectOwner;
			listeners.pushBack(owner->getEventDispatcher()->addCustomEventListener(focusEventName, [this](cocos2d::EventCustom* custom) {
				if (onFocusedEvent == nullptr) {
					return;
				}
				T* eventData = reinterpret_cast<T*>(custom->getUserData());
				if (eventData->getOwner() == getOwner()) {
					onFocusedEvent(eventData);
				}
			}));
			listeners.pushBack(owner->getEventDispatcher()->addCustomEventListener(unfocusEventName, [this](cocos2d::EventCustom* custom) {
				if (onUnfocusedEvent == nullptr) {
					return;
				}
				T* eventData = reinterpret_cast<T*>(custom->getUserData());
				if (eventData->getOwner() == getOwner()) {
					onUnfocusedEvent(eventData);
				}
			}));
			listeners.pushBack(owner->getEventDispatcher()->addCustomEventListener(interruptEventName, [this](cocos2d::EventCustom* custom) {
				if (onInterruptedEvent == nullptr) {
					return;
				}
				U* eventData = reinterpret_cast<U*>(custom->getUserData());
				if (eventData->getOwner() == getOwner()) {
					onInterruptedEvent(eventData);
				}
			}));
			return true;
		}
	public:
		std::function<void(T*)> onFocusedEvent;
		std::function<void(T*)> onUnfocusedEvent;
		std::function<void(U*)> onInterruptedEvent;
	private:
		inline cocos2d::Node* getOwner() {
			return isDirectOwner ? owner : HelperMethodsUtil::getRightNode(owner);
		}
	protected:
		cocos2d::Node* owner;
		bool isDirectOwner;
		cocos2d::Vector<cocos2d::EventListenerCustom*> listeners;
	};

	class MinedEventListener : public FocusEventListener<MinedEventData, InterruptMiningEventData> {
	public:
		// if isDirectOwner is false then we use owner->getParent()
		static MinedEventListener* create(cocos2d::Node* owner, bool isDirectOwner = true) {
			auto ref = new (std::nothrow) MinedEventListener();
			if (ref != nullptr && ref->init(owner, isDirectOwner)) {
				ref->autorelease();
			}
			else {
				CC_SAFE_DELETE(ref);
			}
			return ref;
		}
		bool init(cocos2d::Node* owner, bool isDirectOwner) {
			return FocusEventListener::init(owner, isDirectOwner, FOCUS_MINED_EVENT, UNFOCUS_MINED_EVENT, INTERRUPT_MINING_EVENT);
		}

	};

	class TrackedEventListener : public FocusEventListener<TrackedEventData, InterruptTrackingEventData> {
	public:
		static TrackedEventListener* create(cocos2d::Node* owner, bool isDirectOwner = true) {
			auto ref = new (std::nothrow) TrackedEventListener();
			if (ref != nullptr && ref->init(owner, isDirectOwner)) {
				ref->autorelease();
			}
			else {
				CC_SAFE_DELETE(ref);
			}
			return ref;
		}
		bool init(cocos2d::Node* owner, bool isDirectOwner) {
			return FocusEventListener::init(owner, isDirectOwner, FOCUS_TRACKED_EVENT, UNFOCUS_TRACKED_EVENT, INTERRUPT_TRACKING_EVENT);
		}

	};

}
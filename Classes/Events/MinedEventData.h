#pragma once
#include <string>
#include "EventData.h"

namespace ether_drifters {

	class Mined;
	class MinedResources;

	class MinedEventData : public EventData {
	public:
		MinedEventData(cocos2d::Node* owner, Mined* mined) : EventData(owner), mined(mined) {}
	public:
		Mined* getMined() { return mined; }
	private:
		Mined* mined;
	};
	
	enum class InterruptMiningReason {
		unfocused, // mined object is out of mining area
		wasted,    // mined object is empty
		switched   // start minint another mined object
	};

	class InterruptMiningEventData : public MinedEventData {
	public:
		InterruptMiningEventData(cocos2d::Node* owner, Mined* mined, InterruptMiningReason reason) :
			MinedEventData(owner, mined), reason(reason) {}
	public:
		InterruptMiningReason getReason() { return reason; }
	private:
		InterruptMiningReason reason;
	};

	class MinedResourcesEvent : public MinedEventData {
	public:
		MinedResourcesEvent(cocos2d::Node* owner, Mined* mined, MinedResources* resources) :
			MinedEventData(owner, mined), resources(resources) {}
	public:
		MinedResources* getMinedResources() { return resources; }
	private:
		MinedResources* resources;
	};

	static const std::string FOCUS_MINED_EVENT = "focus_mined_event";
	static const std::string UNFOCUS_MINED_EVENT = "unfocus_mined_event";
	static const std::string INTERRUPT_MINING_EVENT = "interrupt_mining_event";
	static const std::string START_MINING_EVENT = "start_mining_event";
	static const std::string STOP_MINING_EVENT = "stop_mining_event";
	static const std::string WASTE_MINED_RESOURCES_EVENT = "waste_mined_resources_event";
	static const std::string MINED_RESOURCES_EVENT = "mined_resources_event";
}
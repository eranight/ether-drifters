#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/State.h"
#include "cocos2d.h"

namespace ether_drifters {

	class Movement;
	class Direction;

	class FlyState : public fsm::State {
	public:
		FlyState(SimpleEnemyContext* context);
		virtual ~FlyState();
	public:
		void enter() override;
		void exit() override;
		void update() override;
	private:
		void onContactWithEdge(cocos2d::Node* domain);
		void updateRot(float dt);
		void updateMove(float dt);
	private:
		enum class FlyMode {
			rot, move
		} mode;
		// ���� dirty � dir �����, ����� ������ ������
		bool dirty;
		cocos2d::Vec2 dir;
		SimpleEnemyContext* context;
		Movement* movement;
		Direction* direction;
		cocos2d::EventListenerCustom* domainBorderEventListener;
	};

}
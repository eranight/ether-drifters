#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/Condition.h"

namespace ether_drifters {

	class InterruptTrackingEventData;
	class Domain;
	class ObstacleSystem;

	class TargetOutSightCodition : public fsm::Condition {
	public:
		TargetOutSightCodition(SimpleEnemyContext* context);
		virtual ~TargetOutSightCodition();
	public:
		void enter() override;
		void exit() override;
		bool test() override;
	private:
		void interrupt(InterruptTrackingEventData* eventData);
		void resetTracked();
	private:
		SimpleEnemyContext* context;
	};

}
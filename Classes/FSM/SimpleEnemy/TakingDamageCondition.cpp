#include "TakingDamageCondition.h"
#include "Components/Damage.h"
#include "Components/DamageDealer.h"
#include "Components/Health.h"
#include "Components/Tracked.h"
#include "Events/DamageEventData.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"

using namespace cocos2d;
using namespace ether_drifters;

TakingDamageCondition::TakingDamageCondition(SimpleEnemyContext* context) : context(context) {
	context->retain();
	takingDamageEventListener = EventListenerCustom::create(DAMAGE_EVENT, [this](EventCustom* event) {
		DamageEventData* eventData = reinterpret_cast<DamageEventData*>(event->getUserData());
		if (eventData->getHealth()->getOwner() != this->context->getOwner()) {
			return;
		}
		DamageDealer* dealer = eventData->getDamage()->getDealer();
		if (dealer != nullptr) {
			Tracked* tracked = HelperMethodsUtil::findComponent<Tracked>(dealer->getOwner());
			if (tracked != nullptr) {
				Domain* domain = Ether::getInstance()->getCurrentDomain();
				if (domain->isInside(tracked->getOwner())) {
					auto obstacleSystem = dynamic_cast<ObstacleSystem*>(Director::getInstance()->getRunningScene()->getChildByName(OBSTACLE_SYSTEM));
					Obstacle* obstacle = HelperMethodsUtil::findComponent<Obstacle>(this->context->getOwner());
					Vec2 trackedPosition = tracked->getOwner()->getPosition();
					Vec2 ownPosition = this->context->getOwner()->getPosition();
					Vec2 toPoint = (ownPosition - trackedPosition).getNormalized() * this->context->getMinAttackDistance();
					toPoint = domain->convertToNodeSpace(tracked->getOwner()->convertToWorldSpace(toPoint));
					toPoint = obstacleSystem->calculatePointOnRadius(toPoint, trackedPosition, this->context->getMinAttackDistance(), this->context->getHalfWidth(), obstacle);
					if (!isnan(toPoint.x) && !isnan(toPoint.y)) {
						Path path = this->context->getObstacleSystem()->getPath(ownPosition, toPoint, this->context->getHalfWidth(), obstacle);
						if (!path.parts.empty()) {
							this->context->setPath(path);
							this->context->setNextPosition(tracked->getOwner()->convertToNodeSpace(this->context->getDomain()->convertToWorldSpace(toPoint)));
							this->context->setValidNextPosition(true);
							this->context->setTracked(tracked);
							TrackedEventData eventData(this->context->getOwner(), this->context->getTracked());
							this->context->getOwner()->getEventDispatcher()->dispatchCustomEvent(START_TRACKING_EVENT, &eventData);
						}
					}
				}
			}
		}
		});
	takingDamageEventListener->retain();
}

TakingDamageCondition::~TakingDamageCondition() {
	CC_SAFE_RELEASE_NULL(context);
	CC_SAFE_RELEASE_NULL(takingDamageEventListener);
}

void TakingDamageCondition::enter() {
	context->getOwner()->getEventDispatcher()->addEventListenerWithFixedPriority(takingDamageEventListener, 1);
}

void TakingDamageCondition::exit() {
	context->getOwner()->getEventDispatcher()->removeEventListener(takingDamageEventListener);
}

bool TakingDamageCondition::test() {
	return context->getTracked() != nullptr;
}
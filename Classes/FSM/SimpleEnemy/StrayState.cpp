#include "StrayState.h"
#include "cocos2d.h"

using namespace cocos2d;
using namespace ether_drifters;

void StrayState::enter() {
	SubStateMachine::enter();
	context->getOwner()->getEventDispatcher()->dispatchCustomEvent(START_REGENERATION_EVENT, &EventData(context->getOwner()));
}

void StrayState::exit() {
	SubStateMachine::exit();
	context->getOwner()->getEventDispatcher()->dispatchCustomEvent(STOP_REGENERATION_EVENT, &EventData(context->getOwner()));
}
#pragma once

#include "ChaseNextPositionState.h"

namespace ether_drifters {

	class DislocationState : public ChaseNextPositionState {
	public:
		DislocationState(SimpleEnemyContext* context) : ChaseNextPositionState(context) {}
	public:
		void enter() override;
	protected:
		float getMinAngle() { return 60.0f; }
		float getMaxAngle() { return 90.0f; }
	};

}
#include "TargetOutSightCodition.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Tracked.h"
#include "Events/TrackedEventData.h"
#include "Events/DomainEventData.h"
#include "Systems/ObstacleSystem.h"
#include "Ether.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

TargetOutSightCodition::TargetOutSightCodition(SimpleEnemyContext* context) : context(context) {
	this->context->retain();
}

TargetOutSightCodition::~TargetOutSightCodition() {
	CC_SAFE_RELEASE_NULL(context);
}

void TargetOutSightCodition::enter() {
	context->getTrackedEventListener()->onInterruptedEvent = CC_CALLBACK_1(TargetOutSightCodition::interrupt, this);
}

void TargetOutSightCodition::exit() {
	context->getTrackedEventListener()->onInterruptedEvent = nullptr;
}

bool TargetOutSightCodition::test() {
	if (context->getTracked() == nullptr) {
		return true;
	}
	if (context->isValidNextPosition()) {
		Tracked* tracked = context->getTracked();
		Obstacle* obstacle = context->getObstacle();
		Vec2 trackedPosition = tracked->getOwner()->getPosition();
		Vec2 ownPosition = context->getOwner()->getPosition();
		Vec2 toPoint = context->getNextPosition();
		toPoint = context->getDomain()->convertToNodeSpace(tracked->getOwner()->convertToWorldSpace(toPoint));
		toPoint = context->getObstacleSystem()->calculatePointOnRadius(toPoint, trackedPosition, context->getMinAttackDistance(), context->getHalfWidth(), obstacle);
		if (!isnan(toPoint.x) && !isnan(toPoint.y)) {
			Path path = context->getObstacleSystem()->getPath(ownPosition, toPoint, context->getHalfWidth(), obstacle);
			if (!path.parts.empty()) {
				context->setPath(path);
				context->setNextPosition(tracked->getOwner()->convertToNodeSpace(context->getDomain()->convertToWorldSpace(toPoint)));
				context->setValidNextPosition(true);
				return false;
			}
		}
	}
	resetTracked();
	return true;
}

void TargetOutSightCodition::interrupt(InterruptTrackingEventData* eventData) {
	if (eventData->getTracked() == this->context->getTracked()) {
		resetTracked();
	}
}

void TargetOutSightCodition::resetTracked()
{
	context->setValidNextPosition(false);
	this->context->setTracked(nullptr);
	TrackedEventData eventData(context->getOwner(), context->getTracked());
	this->context->getOwner()->getEventDispatcher()->dispatchCustomEvent(STOP_TRACKING_EVENT, &eventData);
}

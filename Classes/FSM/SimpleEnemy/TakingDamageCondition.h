#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/Condition.h"

namespace ether_drifters {

	class TakingDamageCondition : public fsm::Condition {
	public:
		TakingDamageCondition(SimpleEnemyContext* context);
		~TakingDamageCondition();
	public:
		void enter() override;
		void exit() override;
		bool test() override;
	private:
		SimpleEnemyContext* context;
		cocos2d::EventListenerCustom* takingDamageEventListener;
	};

}
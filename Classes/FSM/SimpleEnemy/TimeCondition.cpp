#include "TimeCondition.h"
#include "cocos2d.h"

using namespace cocos2d;
using namespace ether_drifters;

TimeCondition::TimeCondition(float seconds) : maxTime(seconds), timer(0.0f) {

}

void TimeCondition::enter() {
	timer = 0.0f;
}

bool TimeCondition::test() {
	timer += Director::getInstance()->getDeltaTime();
	return timer >= maxTime;
}

RandomTimeCondition::RandomTimeCondition(float minSeconds, float maxSeconds) :
	TimeCondition(0.0f), minSeconds(minSeconds), maxSeconds(maxSeconds) {

}

void RandomTimeCondition::enter() {
	TimeCondition::enter();
	maxTime = RandomHelper::random_real<float>(minSeconds, maxSeconds);
}
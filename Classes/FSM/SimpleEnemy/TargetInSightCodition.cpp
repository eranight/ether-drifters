#include "TargetInSightCodition.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Tracked.h"
#include "Events/TrackedEventData.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

TargetInSightCodition::TargetInSightCodition(SimpleEnemyContext* context) : context(context) {
	context->retain();
}

TargetInSightCodition::~TargetInSightCodition() {
	CC_SAFE_RELEASE_NULL(context);
}

void TargetInSightCodition::enter() {
	context->getTrackedEventListener()->onFocusedEvent = CC_CALLBACK_1(TargetInSightCodition::focus, this);
	//context->getTrackedEventListener()->onUnfocusedEvent = CC_CALLBACK_1(TargetInSightCodition::unfocus, this);
}

void TargetInSightCodition::exit() {
	possibleTargets.clear();
	context->getTrackedEventListener()->onFocusedEvent = nullptr;
	context->getTrackedEventListener()->onUnfocusedEvent = nullptr;
}

bool TargetInSightCodition::test() {
	if (context->getTracked() != nullptr) {
		return true;
	}
	for (auto tracked : possibleTargets) {
		if (checkTargetReachable(tracked)) {
			possibleTargets.eraseObject(tracked);
			return true;
		}
	}
	return false;
}

void TargetInSightCodition::focus(TrackedEventData* eventData) {
	// ����� �������� ����� ������, ���� ���-�� ��� ����������������, ��� ������� ������ �� tracking �����
	if (eventData->getTracked()->getOwner()->getName() == PLAYER) {
		if (possibleTargets.find(eventData->getTracked()) == possibleTargets.end()) {
			possibleTargets.pushBack(eventData->getTracked());
		}
	}
}

void TargetInSightCodition::unfocus(TrackedEventData* eventData) {
	possibleTargets.eraseObject(eventData->getTracked());
}

bool TargetInSightCodition::checkTargetReachable(Tracked* tracked)
{
	if (context->getDomain()->isInside(tracked->getOwner())) {
		Obstacle* obstacle = context->getObstacle();
		Vec2 trackedPosition = tracked->getOwner()->getPosition();
		Vec2 ownPosition = context->getOwner()->getPosition();
		Vec2 toPoint = (ownPosition - trackedPosition).getNormalized() * context->getMinAttackDistance();
		toPoint = context->getDomain()->convertToNodeSpace(tracked->getOwner()->convertToWorldSpace(toPoint));
		toPoint = context->getObstacleSystem()->calculatePointOnRadius(toPoint, trackedPosition, context->getMinAttackDistance(), context->getHalfWidth(), obstacle);
		if (!isnan(toPoint.x) && !isnan(toPoint.y)) {
			Path path = context->getObstacleSystem()->getPath(ownPosition, toPoint, context->getHalfWidth(), obstacle);
			if (!path.parts.empty()) {
				context->setPath(path);
				context->setTracked(tracked);
				context->setNextPosition(tracked->getOwner()->convertToNodeSpace(context->getDomain()->convertToWorldSpace(toPoint)));
				context->setValidNextPosition(true);
				TrackedEventData eventData(this->context->getOwner(), this->context->getTracked());
				this->context->getOwner()->getEventDispatcher()->dispatchCustomEvent(START_TRACKING_EVENT, &eventData);
				return true;
			}
		}
	}
	context->setValidNextPosition(false);
	return false;
}

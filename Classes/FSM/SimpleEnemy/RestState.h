#pragma once

#include "Utils/FiniteStateMachine/State.h"

namespace ether_drifters {

	class RestState : public fsm::State {
	public:
		virtual ~RestState() {}
	public:
		void update() override {}
	};

}
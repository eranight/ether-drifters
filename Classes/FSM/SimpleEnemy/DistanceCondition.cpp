#include "DistanceCondition.h"
#include "Components/Tracked.h"
#include "cocos2d.h"

using namespace cocos2d;
using namespace ether_drifters;

DistanceCondition::DistanceCondition(SimpleEnemyContext* context, float distance) : context(context), distance(distance) {
	context->retain();
}

DistanceCondition::~DistanceCondition() {
	CC_SAFE_RELEASE_NULL(context);
}

bool DistanceCondition::test() {
	return context->getTracked()->getOwner()->getPosition().distance(context->getOwner()->getPosition()) <= distance;
}
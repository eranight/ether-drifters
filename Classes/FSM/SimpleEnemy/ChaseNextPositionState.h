#pragma once

#include "Utils/FiniteStateMachine/State.h"
#include "Prefubs/PathDebugDrawer.h"

#include "cocos2d.h"

namespace ether_drifters {

	class SimpleEnemyContext;
	class Direction;
	class Movement;
	class Domain;
	class ObstacleSystem;

	class ChaseNextPositionState : public fsm::CompletableState {
	public:
		ChaseNextPositionState(SimpleEnemyContext* context);
		~ChaseNextPositionState();
	public:
		void enter() override;
		void exit() override;
		void update() override;
		bool isCompleted() override { return dirty; }
	private:
		void processFinishPathEvent(cocos2d::EventCustom* event);
	protected:
		SimpleEnemyContext* context;
	private:
		bool dirty;

		Direction* direction;
		Movement* movement;
		cocos2d::Vec2 fromPoint;

		cocos2d::EventListenerCustom* finishPathEventListener = nullptr;

#if ED_DEBUG
		cocos2d::DrawNode* toPointDraw = nullptr;
		cocos2d::DrawNode* fromOwnerToTargetLine = nullptr;
		cocos2d::DrawNode* toPointLine = nullptr;
		PathDebugDrawer* pathDrawer = nullptr;
#endif
	};

}
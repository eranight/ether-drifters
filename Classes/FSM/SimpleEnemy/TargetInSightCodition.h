#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/Condition.h"

namespace ether_drifters {

	class Domain;
	class ObstacleSystem;

	class TargetInSightCodition : public fsm::Condition {
	public:
		TargetInSightCodition(SimpleEnemyContext* context);
		virtual ~TargetInSightCodition();
	public:
		void enter() override;
		void exit() override;
		bool test() override;
	private:
		void focus(TrackedEventData* eventData);
		void unfocus(TrackedEventData* eventData);
		bool checkTargetReachable(Tracked* tracked);
	private:
		SimpleEnemyContext* context;
		cocos2d::Vector<Tracked*> possibleTargets;
	};

}
#pragma once
#include "cocos2d.h"
#include "Events/FocusEventListener.h"
#include "Systems/Path.h"

namespace ether_drifters {

	class Tracked;
	class Domain;
	class ObstacleSystem;
	class Obstacle;

	class SimpleEnemyContext : public cocos2d::Ref {
	public:
		static SimpleEnemyContext* create(cocos2d::Node * owner);
		SimpleEnemyContext(cocos2d::Node* owner);
		virtual ~SimpleEnemyContext();
	public:
		cocos2d::Node* getOwner() { return owner; }

		Tracked* getTracked() { return tracked; }
		void setTracked(Tracked* tracked);

		TrackedEventListener* getTrackedEventListener() { return trackedEventListener; }
		void setTrackedEventListener(TrackedEventListener* trackedEventListener);

		float getMaxVelocity() { return maxVelocity; }
		void setMaxVelocity(float maxVelocity) { this->maxVelocity = maxVelocity; }

		float getRotVelocity() { return rotVelocity; }
		void setRotVelocity(float rotVelocity) { this->rotVelocity = rotVelocity; }

		bool isContactedWithDomainBorder() { return contactedWithDomainBorder; }
		void setContactedWithDomainBorder(bool contactedWithDomainBorder) { this->contactedWithDomainBorder = contactedWithDomainBorder; }

		cocos2d::Vec2 getNextPosition() { return nextPosition; }
		void setNextPosition(const cocos2d::Vec2& position) { this->nextPosition = position; }

		bool isValidNextPosition() { return validNextPosition; }
		void setValidNextPosition(bool validNextPosition) { this->validNextPosition = validNextPosition; }

		float getHalfWidth() { return halfWidth; }

		float getMinAttackDistance() { return minAttackDistance; }
		void setMinAttackDistance(float minAttackDistance) { this->minAttackDistance = minAttackDistance; }

		float getMaxAttackDistance() { return maxAttackDistance; }
		void setMaxAttackDistance(float maxAttackDistance) { this->maxAttackDistance = maxAttackDistance; }

		Domain* getDomain() { return domain; }
		void setDomain(Domain* domain) { this->domain = domain; }

		ObstacleSystem* getObstacleSystem() { return obstacleSystem; }
		void setObstacleSystem(ObstacleSystem* obstacleSystem) { this->obstacleSystem = obstacleSystem; }

		Obstacle* getObstacle() { return obstacle; }
		void setObstacle(Obstacle* obstacle) { this->obstacle = obstacle; }

		Path& getPath() { return path; }
		void setPath(const Path& path) { this->path = path; }

		bool isShotReady() { return shotReady; }
		void setShotReady(bool shotReady) { this->shotReady = shotReady; }

	private:
		cocos2d::Node* owner;
		float halfWidth;
		Tracked* tracked;
		TrackedEventListener* trackedEventListener;
		float maxVelocity;
		float rotVelocity;
		bool contactedWithDomainBorder;
		float minAttackDistance;
		float maxAttackDistance;
		cocos2d::Vec2 nextPosition;
		bool validNextPosition;
		Domain* domain;
		ObstacleSystem* obstacleSystem;
		Obstacle* obstacle;
		Path path;
		bool shotReady = true;
	};

}
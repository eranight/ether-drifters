#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/Condition.h"

namespace ether_drifters {

	class Tracking;
	class Domain;

	class DistanceCondition : public fsm::Condition {
	public:
		DistanceCondition(SimpleEnemyContext* context, float distance);
		virtual ~DistanceCondition();
	public:
		void enter() override {}
		void exit() override {}
		bool test() override;
	private:
		SimpleEnemyContext* context;
		float distance;
	};

}
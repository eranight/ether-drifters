#pragma once
#include "SimpleEnemyContext.h"

#include "Utils/FiniteStateMachine/Condition.h"
#include "Utils/FiniteStateMachine/Transition.h"
#include "Utils/FiniteStateMachine/State.h"
#include "Utils/FiniteStateMachine/SubStateMachine.h"

#include "cocos2d.h"

namespace ether_drifters {

	class StrayState : public fsm::SubStateMachine {
	public:
		StrayState(const fsm::FiniteStateMachine& finiteStateMachine, SimpleEnemyContext * context)
			: fsm::SubStateMachine(finiteStateMachine), context(context) {
			context->retain();
		}
		virtual ~StrayState() {
			CC_SAFE_RELEASE_NULL(context);
		}
	public:
		virtual void enter() override;
		virtual void exit() override;
	private:
		SimpleEnemyContext* context;
	};

}
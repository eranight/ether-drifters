#include "DislocationState.h"
#include "Utils/HelperMethodsUtil.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"
#include "Components/Tracked.h"
#include "SimpleEnemyContext.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

void DislocationState::enter()
{
	ChaseNextPositionState::enter();

	Tracked* tracked = context->getTracked();
	Obstacle* obstacle = context->getObstacle();
	Vec2 trackedPosition = tracked->getOwner()->getPosition();
	Vec2 ownPosition = context->getOwner()->getPosition();
	Vec2 toPoint = (ownPosition - trackedPosition).getNormalized() * context->getMinAttackDistance();
	toPoint = context->getDomain()->convertToNodeSpace(tracked->getOwner()->convertToWorldSpace(toPoint));
	toPoint = util::randomDirectionToPoint(toPoint, trackedPosition, getMinAngle(), getMaxAngle()) * context->getMinAttackDistance();
	toPoint = context->getDomain()->convertToNodeSpace(tracked->getOwner()->convertToWorldSpace(toPoint));
	toPoint = context->getObstacleSystem()->calculatePointOnRadius(toPoint, trackedPosition, context->getMinAttackDistance(), context->getHalfWidth(), obstacle);
	if (!isnan(toPoint.x) && !isnan(toPoint.y)) {
		Path path = context->getObstacleSystem()->getPath(ownPosition, toPoint, context->getHalfWidth(), obstacle);
		if (!path.parts.empty()) {
			context->setTracked(tracked);
			context->setPath(path);
			context->setNextPosition(tracked->getOwner()->convertToNodeSpace(context->getDomain()->convertToWorldSpace(toPoint)));
			context->setValidNextPosition(true);
		}
	}

}

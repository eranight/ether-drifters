#include "PanicState.h"
#include "Components/Direction.h"
#include "Components/Movement.h"
#include "Ether.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

OutOfDomainCondition::OutOfDomainCondition(SimpleEnemyContext* context) : context(context) {
	context->retain();
}

OutOfDomainCondition::~OutOfDomainCondition() {
	CC_SAFE_RELEASE_NULL(context);
}

void OutOfDomainCondition::enter() {
	domain = Ether::getInstance()->getCurrentDomain();
}

bool OutOfDomainCondition::test() {
	return !domain->isInside(context->getOwner());
}

PanicState::PanicState(SimpleEnemyContext* context) : context(context), dirty(false) {
	context->retain();
}

PanicState::~PanicState() {
	CC_SAFE_RELEASE_NULL(context);
}

void PanicState::enter() {
	State::enter();

	dirty = false;

	domain = Ether::getInstance()->getCurrentDomain();
	Vec2 ownerPos = context->getOwner()->getPosition();
	toPoint = ownerPos * domain->getRadius() * 0.8f / ownerPos.length();

	Vec2 dir = toPoint - ownerPos;
	direction = util::findComponent<Direction>(context->getOwner());
	direction->setDirection(dir);
	movement = util::findComponent<Movement>(context->getOwner());
	movement->setDirection(dir);
	movement->setVelocity(context->getMaxVelocity());
}

void PanicState::exit() {
	State::exit();

	movement->setVelocity(0.0f);
}

void PanicState::update() {
	float dt = Director::getInstance()->getDeltaTime();
	Vec2 ownerPos = context->getOwner()->getPosition();
	Vec2 pathDir = toPoint - ownerPos;
	if (pathDir.length() < context->getMaxVelocity() * dt) {
		dirty = true;
	}
}
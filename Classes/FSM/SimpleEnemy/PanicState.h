#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/Condition.h"
#include "Utils/FiniteStateMachine/State.h"

namespace ether_drifters {

	class Domain;

	class OutOfDomainCondition : public fsm::Condition {
	public:
		OutOfDomainCondition(SimpleEnemyContext* context);
		virtual ~OutOfDomainCondition();
	public:
		void enter() override;
		void exit() override {}
		bool test() override;
	private:
		SimpleEnemyContext* context;
		Domain* domain;
	};

	class Direction;
	class Movement;

	class PanicState : public fsm::CompletableState {
	public:
		PanicState(SimpleEnemyContext* context);
		virtual ~PanicState();
	public:
		void enter() override;
		void exit() override;
		void update() override;
		bool isCompleted() override { return dirty; }
	private:
		SimpleEnemyContext* context;
		Direction* direction;
		Movement* movement;
		Domain* domain;
		cocos2d::Vec2 toPoint;
		bool dirty;
	};

}
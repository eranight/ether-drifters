#include "SimpleEnemyContext.h"
#include "Components/Tracked.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

SimpleEnemyContext* SimpleEnemyContext::create(Node* owner) {
	auto ref = new (std::nothrow) SimpleEnemyContext(owner);
	if (ref != nullptr) {
		ref->autorelease();
	}
	return ref;
}

SimpleEnemyContext::SimpleEnemyContext(Node* owner)
	: owner(owner), tracked(nullptr), trackedEventListener(nullptr), maxVelocity(0.0f), contactedWithDomainBorder(false) {
	auto halfContentSize = HelperMethodsUtil::getContentSize(owner) * 0.52f;
	this->halfWidth = MAX(halfContentSize.width, halfContentSize.height);
}

SimpleEnemyContext::~SimpleEnemyContext() {
	CC_SAFE_RELEASE_NULL(tracked);
	CC_SAFE_RELEASE_NULL(trackedEventListener);
}

void SimpleEnemyContext::setTracked(Tracked* tracked) {
	if (this->tracked != nullptr) {
		this->tracked->release();
	}
	this->tracked = tracked;
	if (this->tracked != nullptr) {
		this->tracked->retain();
	}
}

void ether_drifters::SimpleEnemyContext::setTrackedEventListener(TrackedEventListener* trackedEventListener) {
	if (this->trackedEventListener != nullptr) {
		this->trackedEventListener->release();
	}
	this->trackedEventListener = trackedEventListener;
	if (this->trackedEventListener != nullptr) {
		this->trackedEventListener->retain();
	}
}

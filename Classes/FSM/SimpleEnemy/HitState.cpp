#include "HitState.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Direction.h"
#include "Components/Tracked.h"
#include "Components/Health.h"
#include "Components/DamageDealer.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

HitState::HitState(SimpleEnemyContext* context, const std::function<void(Health*)>& fireCallback) : 
	context(context), fireCallback(fireCallback) {
	context->retain();
}

HitState::~HitState() {
	CC_SAFE_RELEASE_NULL(context);
	domain = nullptr;
}

void HitState::enter() {
	State::enter();
	dirty = false;
	timer = 1.0f;
	Node* owner = context->getOwner();
	direction = util::findComponent<Direction>(owner);
	damageDealer = HelperMethodsUtil::findComponent<DamageDealer>(owner);
	domain = Ether::getInstance()->getCurrentDomain();
}

void HitState::update() {
	float dt = Director::getInstance()->getDeltaTime();
	Vec2 toTargetDirectin = (context->getTracked()->getOwner()->getPosition() - context->getOwner()->getPosition());
	direction->setDirection(toTargetDirectin);

	if (hasFireObstacle()) {
		timer -= dt;
		if (timer <= 0.0f) {
			dirty = true;
		}
	}
	else if (context->isShotReady()) {
		Health* targetHealth = HelperMethodsUtil::findComponent<Health>(context->getTracked()->getOwner());
		fireCallback(targetHealth);
	}
}

bool HitState::hasFireObstacle()
{
	Obstacle* selfObstacle = util::findComponent<Obstacle>(context->getOwner());
	Node* trackedOwner = context->getTracked()->getOwner();
	Obstacle* targetObstacle = util::findComponent<Obstacle>(trackedOwner);

	Vec2 realFromPoint = domain->convertToNodeSpace(HelperMethodsUtil::getViewIfExists(context->getOwner())->convertToWorldSpace(Vec2::ZERO));
	Vec2 toPoint = trackedOwner->getPosition();
	auto obstacles = context->getObstacleSystem()->rayCast(realFromPoint, toPoint, 7.5f, selfObstacle);
	
	if (targetObstacle != nullptr) {
		std::remove_if(obstacles.begin(), obstacles.end(), [trackedOwner](ObstacleShape& shape) { return shape.owner == trackedOwner; });
	}
	
	return !obstacles.empty();
}

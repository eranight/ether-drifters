#pragma once

#include "SimpleEnemyContext.h"
#include "Utils/FiniteStateMachine/State.h"

namespace ether_drifters {

	class Direction;
	class DamageDealer;
	class Domain;
	class Health;

	class HitState : public fsm::CompletableState {
	public:
		HitState(SimpleEnemyContext* context, const std::function<void(Health*)>& fireCallback);
		virtual ~HitState();
	public:
		void enter() override;
		void update() override;
		bool isCompleted() override { return dirty; }
	private:
		bool hasFireObstacle();
	private:
		SimpleEnemyContext* context;
		Direction* direction;
		DamageDealer* damageDealer;
		Domain* domain;
		std::function<void(Health*)> fireCallback;

		bool dirty;
		float timer;
	};

}
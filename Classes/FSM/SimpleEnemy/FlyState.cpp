#include "FlyState.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Movement.h"
#include "Components/Direction.h"
#include "Events/DomainEventData.h"
#include "Systems/ObstacleSystem.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

FlyState::FlyState(SimpleEnemyContext* context) : context(context) {
	context->retain();

	domainBorderEventListener = EventListenerCustom::create(BEGIN_TOUCH_DOMAIN_BORDER_EVENT, [this](EventCustom* custom) {
		DomainEventData* eventData = reinterpret_cast<DomainEventData*>(custom->getUserData());
		if (eventData->getConnectedNode() == this->context->getOwner()) {
			onContactWithEdge(nullptr);
		}
		});
	domainBorderEventListener->retain();

	direction = util::findComponent<Direction>(context->getOwner());
	CCASSERT(direction != nullptr, "owner must has direction component");
	movement = util::findComponent<Movement>(context->getOwner());
	CCASSERT(movement != nullptr, "owner must has movement component");
}

FlyState::~FlyState() {
	CC_SAFE_RELEASE_NULL(context);
	CC_SAFE_RELEASE_NULL(domainBorderEventListener);
}

void FlyState::enter() {
	State::enter();

	dirty = false;
	context->setContactedWithDomainBorder(false);
	mode = FlyMode::rot;

	Vec2 nextDirection;
	float angle;
	if (context->isContactedWithDomainBorder()) {
		angle = util::randomAngle(0.0f, 30.0f);
	}
	else {
		angle = util::randomAngle(30.0f, 180.0f);
	}
	dir = nextDirection;
	direction->setNextAngle(angle);
	direction->setVelocity(context->getRotVelocity());

	context->getOwner()->getEventDispatcher()->addEventListenerWithFixedPriority(domainBorderEventListener, 1);
}

void FlyState::exit() {
	State::exit();

	movement->setVelocity(0.0f);
	context->getOwner()->getEventDispatcher()->removeEventListener(domainBorderEventListener);
}

void FlyState::onContactWithEdge(Node* domain) {
	dirty = true;
	dir = util::randomDirectionToPoint(Vec2::ZERO, context->getOwner()->getPosition(), 30.0f);
	context->setContactedWithDomainBorder(true);
	// �� �������� ����������� ���, ��� ��� ���������� ������ ��������� rotation � ������� ���������
	// 	   ��� ����� ����� ����� update
	// 	   movement ����� ������ � update ������ ����� ��� ���� ������, � ��� movement �������� (������ ��� �� �������� ����, � �� �� ���)
	// direction->setDirection(toCenter);
	// movement->setDirection(dir);
}

void FlyState::update() {
	float dt = Director::getInstance()->getDeltaTime();
	if (mode == FlyMode::rot) {
		updateRot(dt);
	}
	else {
		if (dirty) {
			direction->setDirection(dir);
			movement->setDirection(dir);
			dirty = false;
			context->setContactedWithDomainBorder(false);
		}
		updateMove(dt);
	}
}

void FlyState::updateRot(float dt) {
	if (direction->isNextAndCurrentAnglesSync()) {
		movement->setVelocity(std::min(40.0f, context->getMaxVelocity()));
		movement->setDirection(direction->getDirection());
		mode = FlyMode::move;
	}
}

void FlyState::updateMove(float dt) {
	Vec2 movementUnit = direction->getDirection() * (movement->getVelocity() * dt + context->getHalfWidth());
	Vec2 from = context->getOwner()->getPosition();
	Vec2 to = from + movementUnit;
	Obstacle* selfObstacle = util::findComponent<Obstacle>(context->getOwner());
	auto obstacles = context->getObstacleSystem()->rayCast(from, to, context->getHalfWidth(), selfObstacle);
	if (!obstacles.empty()) {
		dir = util::randomDirectionToPoint(from - movementUnit, obstacles[0].position, 30.0f);
		direction->setDirection(dir);
		movement->setDirection(dir);
	}
}
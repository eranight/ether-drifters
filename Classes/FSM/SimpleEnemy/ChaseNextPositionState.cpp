#include "ChaseNextPositionState.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "SimpleEnemyContext.h"
#include "Components/Direction.h"
#include "Components/Movement.h"
#include "Components/Tracked.h"
#include "Components/Obstacle.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"
#include "Events/TrackedEventData.h"
#include "Events/MovementEventData.h"
#include "Systems/Path.h"

#define CC_SAFE_REMOVE_CHILD(parent, child) do { if(parent && child) { (parent)->removeChild((child)); (child) = nullptr; } } while(0)

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;


ChaseNextPositionState::ChaseNextPositionState(SimpleEnemyContext* context) :
	context(context) {
	CC_SAFE_RETAIN(context);
}

ChaseNextPositionState::~ChaseNextPositionState() {
#if ED_DEBUG
	CC_SAFE_REMOVE_CHILD(context->getDomain(), toPointDraw);
	CC_SAFE_REMOVE_CHILD(context->getDomain(), fromOwnerToTargetLine);
	CC_SAFE_REMOVE_CHILD(context->getDomain(), toPointLine);
	CC_SAFE_REMOVE_CHILD(context->getDomain(), pathDrawer);
#endif
	CC_SAFE_RELEASE_NULL(context);
	CC_SAFE_RELEASE_NULL(finishPathEventListener);
}

void ChaseNextPositionState::enter() {
	State::enter();

	direction = util::findComponent<Direction>(context->getOwner());
	movement = util::findComponent<Movement>(context->getOwner());
	movement->setVelocity(context->getMaxVelocity());

	dirty = false;

	Node* owner = context->getOwner();
	fromPoint = owner->getPosition() - context->getTracked()->getOwner()->getPosition();

	if (finishPathEventListener == nullptr) {
		finishPathEventListener = EventListenerCustom::create(FINISH_PATH_EVENT, CC_CALLBACK_1(ChaseNextPositionState::processFinishPathEvent, this));
		finishPathEventListener->retain();
	}
	owner->getEventDispatcher()->addEventListenerWithFixedPriority(finishPathEventListener, 1);

#if ED_DEBUG
	if (toPointDraw == nullptr) {
		toPointDraw = DrawNode::create();
		toPointDraw->setCameraMask(DOMAIN_UI_CAMERA_MASK);
		context->getDomain()->addChild(toPointDraw, 5);
	}

	if (fromOwnerToTargetLine == nullptr) {
		fromOwnerToTargetLine = DrawNode::create();
		fromOwnerToTargetLine->setCameraMask(DOMAIN_UI_CAMERA_MASK);
		context->getDomain()->addChild(fromOwnerToTargetLine, 5);
	}

	if (toPointLine == nullptr) {
		toPointLine = DrawNode::create();
		toPointLine->setCameraMask(DOMAIN_UI_CAMERA_MASK);
		context->getDomain()->addChild(toPointLine, 5);
	}

	if (pathDrawer == nullptr) {
		pathDrawer = PathDebugDrawer::create();
		pathDrawer->setCameraMask(DOMAIN_UI_CAMERA_MASK);
		context->getDomain()->addChild(pathDrawer, 5);
	}
#endif
}

void ChaseNextPositionState::exit() {
	State::exit();
	movement->setVelocity(0.0f);
	movement->setPath(Path());
	context->getOwner()->getEventDispatcher()->removeEventListener(finishPathEventListener);
#if ED_DEBUG
	if (toPointDraw == nullptr) {
		return;
	}
	toPointDraw->clear();
	fromOwnerToTargetLine->clear();
	toPointLine->clear();
	pathDrawer->clear();
#endif
}

void ChaseNextPositionState::update() {
	Path& path = context->getPath();
	Vec2 to;
	if (path.parts.empty()) {
		movement->setPath(Path());
	}
	else {
		to = path.parts[0].vertexTo;
		movement->setPath(path);
	}

	direction->setDirection(movement->getDirection());

#if ED_DEBUG
	if (pathDrawer != nullptr && !context->getPath().parts.empty()) {
		pathDrawer->setPath(context->getPath());
	}

	toPointLine->clear();
	toPointLine->drawLine(context->getTracked()->getOwner()->getPosition(), to, Color4F::YELLOW);

	toPointDraw->clear();
	toPointDraw->drawDot(to, 5, Color4F::GREEN);
#endif
}

void ChaseNextPositionState::processFinishPathEvent(EventCustom* event)
{
	FinishPathMovementEventData* eventData = reinterpret_cast<FinishPathMovementEventData*>(event->getUserData());
	if (eventData->getOwner() == context->getOwner()) {
		this->dirty = true;
	}
}

#pragma once

#include "Utils/FiniteStateMachine/Condition.h"

namespace ether_drifters {

	class TimeCondition : public fsm::Condition {
	public:
		TimeCondition(float seconds);
		virtual ~TimeCondition() {}
	public:
		void enter() override;
		void exit() override {}
		bool test() override;
	protected:
		float maxTime;
		float timer;
	};

	class RandomTimeCondition : public TimeCondition {
	public:
		RandomTimeCondition(float minSeconds, float maxSeconds);
		virtual ~RandomTimeCondition() {}
	public:
		void enter() override;
	private:
		float minSeconds;
		float maxSeconds;
	};

}
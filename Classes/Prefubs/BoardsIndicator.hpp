#pragma once
#include "cocos2d.h"

namespace ether_drifters
{

	class BoardIndicator;
	class Direction;
	class Tracking;
	class ViewNode;

	class BoardsIndicatorConfig {
	public:
		cocos2d::Vec2 rightBoardPosition;
		std::string rightBoardText;
		cocos2d::Vec2 leftBoardPosition;
		std::string leftBoardText;
		float length;
	};

	class BoardsIndicator : public cocos2d::Node {
	private:
		BoardsIndicator(const BoardsIndicatorConfig& config) : config(config) {}
	public:
		~BoardsIndicator();
		static BoardsIndicator* create(const BoardsIndicatorConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		void processSelectEvent(cocos2d::EventCustom* event);
		void select();
		void processDeathEvent(cocos2d::EventCustom* event);
		float calculateAngle(cocos2d::Node* selectedNode);
	private:
		BoardsIndicatorConfig config;
		BoardIndicator* rightBoard = nullptr;
		BoardIndicator* leftBoard = nullptr;
		BoardIndicator* currentActiveBoard = nullptr;

		cocos2d::Node* owner = nullptr;
		ViewNode* ownerView = nullptr;

		Direction* direction = nullptr;
		Tracking* tracking = nullptr;
		cocos2d::Node* currentTarget = nullptr;

		cocos2d::EventListener* selectEventListener = nullptr;
		cocos2d::EventListener* deathEventListener = nullptr;
	};

	class BoardIndicator : public cocos2d::Node {
	public:
		static BoardIndicator* create(float lenght, const std::string& labelText, bool rightSide);
		bool init(float length, const std::string& labelText, bool rightSide);
		void update(float dt) override;
	public:
		void setViewOwner(ViewNode* ownerView) { this->ownerView = ownerView; }
		void activate(bool activate);
	private:
		float length = 0.0f;
		ViewNode* ownerView = nullptr;
		cocos2d::DrawNode* view = nullptr;
		cocos2d::Label* label = nullptr;
	};

}
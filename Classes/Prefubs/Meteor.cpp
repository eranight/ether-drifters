#include "Meteor.h"
#include "Common/ViewNode.hpp"
#include "Drop.h"
#include "ResourceBar.h"
#include "Components/Movement.h"
#include "Components/Mined.h"
#include "Components/StrayController.h"
#include "Components/Health.h"
#include "Components/Destroyable.h"
#include "Components/Touchable.h"
#include "Components/Obstacle.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Ether.h"

using namespace cocos2d;
using namespace ether_drifters;

Meteor* Meteor::create(const MeteorConfig& config) {
	auto ref = new (std::nothrow) Meteor(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

Meteor::~Meteor() {
	CCLOG("meteor destroyed");
}

bool Meteor::init() {
	if (!Node::init()) {
		return false;
	}

	Size contentSize;
	switch (config.size)
	{
	case MeteorSize::s_size:
		contentSize = Size(29.0f, 26.0f);
		break;
	case MeteorSize::m_size:
		contentSize = Size(45.0f, 40.0f);
		break;
	case MeteorSize::l_size:
		contentSize = Size(89.0f, 82.0f);
		break;
	default:
		break;
	}

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	auto sprite = Sprite::createWithSpriteFrameName(config.spriteFrame);
	HelperMethodsUtil::adjustScaleToSize(sprite, contentSize);
	view->addChild(sprite);

	float radius = std::min(contentSize.width, contentSize.height) * 0.4f;
	auto physicsBody = PhysicsBody::createCircle(radius);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::body | ContactMask::mined);
	physicsBody->setContactTestBitmask(ContactMask::domain | ContactMask::mining | ContactMask::hit);
	view->setPhysicsBody(physicsBody);

	// ������ ����� addComponent �������� scheduleUpdate, ������� ������� � ��� �������������� warning: don't update it again
	// ��� ������� ���������� ������, ��� ����� ����������
	// ������ ����� addComponent ������� _componentContainer (lazy alloc),
	// ������� ������ ��������� ��������� ����� addComponent, � ��������� �� ������
	addComponent(Movement::create());
	MinedConfig minedConfig;
	minedConfig.resourcePoints = config.resourcePoints;
	minedConfig.strengthPoints = config.strengthPoints;
	_componentContainer->add(Mined::create(minedConfig));
	StrayControllerConfig strayControllerConfig;
	strayControllerConfig.randomizeDirection = config.randomizeDirection;
	strayControllerConfig.direction = config.direction;
	_componentContainer->add(StrayController::create(strayControllerConfig));
	_componentContainer->add(Destroyable::create(CC_CALLBACK_0(Meteor::destroy, this)));
	HealthConfig healthConfig;
	double healthHitPoints = config.strengthPoints > 0.0f ? config.strengthPoints : 1.0f;
	healthConfig.hitPoints = healthHitPoints;
	healthConfig.currentHitPoints = healthHitPoints;
	_componentContainer->add(Health::create(healthConfig));
	_componentContainer->add(Touchable::create(contentSize * 0.8f));
	_componentContainer->add(Obstacle::create( Obstacle::FIRE_OBSTACLE, { { Vec2::ZERO, radius } }));

	float width = std::min(contentSize.width, contentSize.height);
	PointsBarConfig resourceBarConfig;
	resourceBarConfig.width = width;
	resourceBarConfig.maxPoints = 20.0f;
	resourceBarConfig.startPoints = 20.0f;
	resourceBarConfig.color = Color4F::ORANGE;
	resourceBar = ResourceBar::create(resourceBarConfig);
	resourceBar->setPosition(Vec2(-width * 0.5f, std::max(contentSize.width, contentSize.height) * 0.7f));
	resourceBar->setVisible(false);
	addChild(resourceBar);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void Meteor::onEnter() {
	Node::onEnter();
}

void Meteor::onExit() {
	Node::onExit();
}

void Meteor::destroy() {
	auto exp = ParticleSun::create();
	exp->setAutoRemoveOnFinish(true);
	switch (config.size)
	{
	case MeteorSize::l_size:
		exp->setScale(2.0f);
		break;
	case MeteorSize::m_size:
	case MeteorSize::s_size:
		exp->setScale(1.0f);
		break;
	default:
		break;
	}
	exp->setDuration(0.7f);
	exp->setCameraMask(DOMAIN_CAMERA_MASK);

	auto view = HelperMethodsUtil::getViewIfExists(this);
	auto domain = Ether::getInstance()->getCurrentDomain();
	Vec2 pos = domain->convertToNodeSpace(this->convertToWorldSpace(view->getPosition()));
	exp->setPosition(pos);
	domain->addChild(exp, 3);

	runAction(
		Sequence::create({
			DelayTime::create(0.7f),
			CallFunc::create(CC_CALLBACK_0(Meteor::createPieces, this)),
			DelayTime::create(0.8f),
			RemoveSelf::create()
		})
	);
}

void Meteor::createPieces() {
	auto view = HelperMethodsUtil::getViewIfExists(this);
	view->getPhysicsBody()->setEnabled(false);
	view->setVisible(false);
	if (config.size == MeteorSize::s_size) {
		return;
	}
	auto domain = Ether::getInstance()->getCurrentDomain();
	Vec2 currentPosition = domain->convertToNodeSpace(this->convertToWorldSpace(view->getPosition()));
	
	int count;
	int mediumCount;
	int smallCount;
	if (config.size == MeteorSize::l_size) {
		count = RandomHelper::random_int(4, 5);
		mediumCount = count / 2;
		smallCount = count - mediumCount;
	}
	else {
		count = RandomHelper::random_int(1, 2);
		mediumCount = 0;
		smallCount = count;
	}

	auto contentSize = HelperMethodsUtil::getContentSize(view);
	float spawnPointsRadius = std::min(contentSize.width, contentSize.height) * 0.25f;
	float angle = 360.0f / count;
	float additionalAngleOffset = RandomHelper::random_real<float>(0.0f, 1.0f) * 12.0f;
	float shiftAngle = angle + additionalAngleOffset;

	float startAngle = shiftAngle;

	int remainingPoints = HelperMethodsUtil::findComponent<Mined>(this)->getRemainingResourcePoints();

	std::vector<std::tuple<std::string, MeteorSize, int>> names;
	for (int index = 0; index < mediumCount; ++index) {
		std::string name = RandomHelper::random_int(1, 10) % 2 == 0 ? "meteorBrown_med1" : "meteorBrown_med3";
		MeteorSize size = MeteorSize::m_size;
		int points = std::min(RandomHelper::random_int(3, 5), remainingPoints);
		remainingPoints -= points;
		names.push_back(std::make_tuple(name, size, points));
	}
	for (int index = 0; index < smallCount; ++index) {
		std::string name = RandomHelper::random_int(1, 10) % 2 == 0 ? "meteorBrown_small1" : "meteorBrown_small2";
		MeteorSize size = MeteorSize::s_size;
		int points = std::min(RandomHelper::random_int(1, 2), remainingPoints);
		remainingPoints -= points;
		names.push_back(std::make_tuple(name, size, points));
	}

	auto rng = std::default_random_engine{};
	std::shuffle(names.begin(), names.end(), rng);

	for (int index = 0; index < count; ++index, startAngle += shiftAngle) {
		MeteorSize size = std::get<1>(names[index]);
		int points = std::get<2>(names[index]);
		float radAngle = CC_DEGREES_TO_RADIANS(startAngle);
		float x = std::cosf(radAngle) * spawnPointsRadius;
		float y = std::sinf(radAngle) * spawnPointsRadius;
		Vec2 partMeteorPosition = domain->convertToNodeSpace(this->convertToWorldSpace(Vec2(x, y)));
		if (size == MeteorSize::s_size && RandomHelper::random_real(0.0, 1.0) * 100 < 20) {
			DropConfig dropConfig;
			dropConfig.type = DropType::iron_ore;
			dropConfig.lifeTime = -1.0f;
			dropConfig.physicsBodyDelay = 1.0f;
			dropConfig.quantity = points;
			dropConfig.spriteFrame = "meteorGrey_tiny1";
			Drop* drop = Drop::create(dropConfig);
			drop->setPosition(partMeteorPosition);
			domain->addChild(drop, 1);
		}
		else {
			MeteorConfig config;
			config.spriteFrame = std::get<0>(names[index]);
			config.size = size;
			config.resourcePoints = points;
			config.strengthPoints = config.resourcePoints;
			config.randomizeDirection = false;
			config.direction = (partMeteorPosition - currentPosition).getNormalized();
			auto meteor = Meteor::create(config);
			meteor->setPosition(partMeteorPosition);
			domain->addChild(meteor, 1);
		}
	}
}

#pragma once

#include "cocos2d.h"


namespace ether_drifters {

	class Path;

	class PathDebugDrawer : public cocos2d::Node {
	public:
		CREATE_FUNC(PathDebugDrawer);
		bool init() override;
		//void update(float dt) override;
		void clear();
	public:
		void setPath(Path& path);
	private:
		void drawCircleSegment(cocos2d::DrawNode* drawNode, const cocos2d::Vec2& center, float radius, float angleFrom, float length, int segments, const cocos2d::Color4F& color);
	private:
		cocos2d::DrawNode* drawNode;
	};

}
#include "DebugDrawNode.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Ether.h"
#include "Systems/ObstacleSystem.h"

using namespace cocos2d;
using namespace ether_drifters;

using util = HelperMethodsUtil;

#define RADIUS 200.0f
#define RADIUS_X_RADIUS (RADIUS * RADIUS)

bool DebugDrawNode::init() {
	if (!Node::init()) {
		return false;
	}

	auto domain = Ether::getInstance()->getCurrentDomain();

	domainRadius = domain->getRadius() * 0.97f;

	int maxCount = 72;
	float deltaAngle = 2.0f * M_PI / maxCount;
	float angle = 0.0f;
	for (int index = 0; index < maxCount; ++index, angle += deltaAngle) {
		coords.push_back(Vec2(RADIUS * cosf(angle), RADIUS * sinf(angle)));
	}

	drawNode = DrawNode::create();
	addChild(drawNode);

	scheduleUpdate();

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

void DebugDrawNode::update(float dt) {
	Node::update(dt);
	drawNode->clear();
	for (Vec2 point : coords) {
		recalculate(point);
		drawNode->drawDot(point, 3.0f, Color4F::GREEN);
		//drawNode->drawLine(Vec2::ZERO, point, Color4F::ORANGE);
	}
	drawNode->drawCircle(Vec2::ZERO, RADIUS, 360.0f, 72, false, Color4F::BLUE);
}

void DebugDrawNode::recalculate(Vec2& point) {
	Vec2 pos = getParent()->getPosition();
	float radius = 50.0f;
	auto os = dynamic_cast<ObstacleSystem*>(Director::getInstance()->getRunningScene()->getChildByName("obstacle_system"));
	point = os->calculatePointOnRadius(pos + point, pos, RADIUS, 0.0f) - pos;
}

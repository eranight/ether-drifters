#include "ResourceBar.h"
#include "Events/MinedEventData.h"
#include "Components/Mined.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string ResourceBar::NAME = "resource_bar";

ResourceBar::~ResourceBar() {
	CC_SAFE_RELEASE_NULL(minedResourceEventListener);
}

ResourceBar* ResourceBar::create(const PointsBarConfig& config) {
	auto ref = new (std::nothrow) ResourceBar(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool ResourceBar::init() {
	if (!AbstractPointsBar::init()) {
		return false;
	}

	minedResourceEventListener = EventListenerCustom::create(MINED_RESOURCES_EVENT, CC_CALLBACK_1(ResourceBar::changeState, this));
	minedResourceEventListener->retain();

	setName(NAME);
	return true;
}

void ResourceBar::onEnter() {
	Node::onEnter();

	_eventDispatcher->addEventListenerWithFixedPriority(minedResourceEventListener, 1);
}

void ResourceBar::onExit() {
	Node::onExit();

	_eventDispatcher->removeEventListener(minedResourceEventListener);
}

void ResourceBar::changeState(EventCustom* custom) {
	MinedEventData* eventData = reinterpret_cast<MinedEventData*>(custom->getUserData());
	if (eventData->getMined()->getOwner() == getParent()) {
		float newResourcePoints = eventData->getMined()->getRemainingResourcePoints();
		updateState(newResourcePoints);
	}
}

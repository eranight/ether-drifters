#include "Skeleton.h"
#include "Common/ViewNode.hpp"
#include "ResourceBar.h"
#include "Components/Mined.h"
#include "Components/Movement.h"
#include "Components/StrayController.h"
#include "Components/Touchable.h"
#include "Components/Obstacle.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

bool Skeleton::init() {

	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(113.0f, 66.0f);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	Node* sprite = Sprite::createWithSpriteFrameName("ship (24).png");
	sprite->setRotation(90.0f);
	view->addChild(sprite);

	auto physicsBody = PhysicsBody::createBox(Size(contentSize.width * 0.6f, contentSize.height * 0.8f));
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::body | ContactMask::mined);
	physicsBody->setContactTestBitmask(ContactMask::domain | ContactMask::mining);
	view->setPhysicsBody(physicsBody);

	addComponent(Movement::create());
	MinedConfig minedConfig;
	minedConfig.resourcePoints = 20;
	minedConfig.strengthPoints = 20.0;
	_componentContainer->add(Mined::create(minedConfig));
	StrayControllerConfig strayControllerConfig;
	strayControllerConfig.randomizeDirection = true;
	_componentContainer->add(StrayController::create(strayControllerConfig));
	Size touchSize = contentSize * 0.8f;
	_componentContainer->add(Touchable::create(touchSize));

	float diameter = contentSize.width / 3.0f;
	float y = contentSize.height * 0.5f;
	float radius = diameter * 0.5f;
	std::vector<ObstacleShape> shapes {
		{ Vec2(-diameter, 0.0f), radius },
		{ Vec2::ZERO, radius },
		{ Vec2(diameter, 0.0f), radius }
	};
	_componentContainer->add(Obstacle::create(Obstacle::FIRE_OBSTACLE, shapes));

	float width = std::min(view->getContentSize().width, view->getContentSize().height);
	PointsBarConfig resourceBarConfig;
	resourceBarConfig.width = width;
	resourceBarConfig.maxPoints = 20.0f;
	resourceBarConfig.startPoints = 20.0f;
	resourceBarConfig.color = Color4F::ORANGE;
	resourceBar = ResourceBar::create(resourceBarConfig);
	resourceBar->setPosition(Vec2(-width * 0.5f, std::max(contentSize.width, contentSize.height) * 0.7f));
	resourceBar->setVisible(false);
	addChild(resourceBar);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void Skeleton::onEnter() {
	Node::onEnter();
}

void Skeleton::onExit() {
	Node::onExit();
	unscheduleUpdate();
}

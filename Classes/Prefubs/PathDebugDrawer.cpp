#include "PathDebugDrawer.h"
#include "Systems/Path.h"
#include "Utils/Constants.h"
#include "Utils/CircularObstaclePathfinding/Util.h"

using namespace cocos2d;
using namespace ether_drifters;

bool PathDebugDrawer::init()
{
	if (!Node::init()) {
		return false;
	}

	drawNode = DrawNode::create();
	addChild(drawNode);

	scheduleUpdate();

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

void ether_drifters::PathDebugDrawer::clear()
{
	drawNode->clear();
}

void ether_drifters::PathDebugDrawer::setPath(Path& path)
{
	drawNode->clear();
	for (auto& part : path.parts) {
		if (part.isHugging) {
			float angleFrom = circular_obstacle_pathfinding::Util::convertAngleTo2Pi((part.vertexFrom - part.center).getAngle());
			float angleTo = circular_obstacle_pathfinding::Util::convertAngleTo2Pi((part.vertexTo - part.center).getAngle());
			if (angleFrom > angleTo) {
				if (2 * M_PI - angleFrom + angleTo > M_PI) {
					std::swap(angleFrom, angleTo);
				}
			}
			else if (angleTo > angleFrom) {
				if (2 * M_PI - angleTo + angleFrom < M_PI) {
					std::swap(angleFrom, angleTo);
				}
			}
			drawCircleSegment(
				drawNode,
				part.center,
				part.radius,
				angleFrom,
				part.length,
				36,
				Color4F::ORANGE);
		}
		else {
			drawNode->drawLine(part.vertexFrom, part.vertexTo, Color4F::ORANGE);
		}
	}
}

void PathDebugDrawer::drawCircleSegment(cocos2d::DrawNode* drawNode, const Vec2& center, float radius, float angleFrom, float length, int segments, const Color4F& color) {
	float angle = length / radius;
	if (angle < FLT_EPSILON) {
		return;
	}
	float unitStep = CC_DEGREES_TO_RADIANS(2 * M_PI * radius / segments);
	int realSegments = angle / unitStep;
	if (angle / unitStep - realSegments > FLT_EPSILON) realSegments++;
	if (realSegments == 0) {
		return;
	}
	if (realSegments == 1) {
		++realSegments;
	}
	unitStep = angle / realSegments;
	int pointNum = realSegments + 1;
	float rads = angleFrom;
	float x = center.x + radius * cosf(rads);
	float y = center.y + radius * sinf(rads);
	Vec2 s(x, y);
	for (int index = 1; index < pointNum; ++index) {
		rads = unitStep * index + angleFrom;
		x = center.x + radius * cosf(rads);
		y = center.y + radius * sinf(rads);
		Vec2 f(x, y);
		drawNode->drawLine(s, f, color);
		s = f;
	}
}

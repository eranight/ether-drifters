#include "Island.h"
#include "Common/ViewNode.hpp"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Touchable.h"
#include "Components/Obstacle.h"

using namespace cocos2d;
using namespace ether_drifters;

bool Island::init() {
	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(150.0f, 203.2f);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	auto moveUp = MoveBy::create(2.0f, Vec2(0.0f, 10.0f));
	view->runAction(RepeatForever::create(
		Sequence::createWithTwoActions(
			moveUp,
			moveUp->reverse()
		)
	));
	addChild(view);

	auto sprite = Sprite::create("island_test_dark_2.png");
	HelperMethodsUtil::adjustScaleToSize(sprite, contentSize);
	view->addChild(sprite);

	addComponent(Touchable::create(contentSize * 0.6f));
	auto center = view->getContentSize() * 0.5f;
	float radius = std::min(contentSize.width, contentSize.height) * 0.5f;
	_componentContainer->add(Obstacle::create(Obstacle::PATH_OBSTACLE | Obstacle::FIRE_OBSTACLE, 
		{ { Vec2::ZERO, radius } }));

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}
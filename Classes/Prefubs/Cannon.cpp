#include "Cannon.hpp"
#include "Ether.h"
#include "Utils/HelperMethodsUtil.h"
#include "Utils/Constants.h"
#include "Common/ProjectileMarkers.hpp"

using namespace cocos2d;
using namespace ether_drifters;

static const float DEBUG_LINE_SIZE = 100.0f;

bool Cannon::init() {
	if (!Node::init()) {
		return false;
	}

	view = Node::create();
	addChild(view);

	if (!config.spriteName.empty()) {
		auto sprite = Sprite::createWithSpriteFrameName(config.spriteName);
		HelperMethodsUtil::adjustScaleToSize(sprite, config.contentSize);
		view->addChild(sprite);
	}

	if (config.debug) {
		float halfAngle = CC_DEGREES_TO_RADIANS(config.visionAngle * 0.5f);

		auto visionAngleDrawNode = DrawNode::create();
		visionAngleDrawNode->drawLine(Vec2::ZERO, Vec2(cosf(halfAngle) * DEBUG_LINE_SIZE, sinf(halfAngle) * DEBUG_LINE_SIZE), Color4F::RED);
		visionAngleDrawNode->drawLine(Vec2::ZERO, Vec2(cosf(halfAngle) * DEBUG_LINE_SIZE, -sinf(halfAngle) * DEBUG_LINE_SIZE), Color4F::RED);

		addChild(visionAngleDrawNode);

		debugDrawNode = DrawNode::create();
		redrawDebugNode();
		view->addChild(debugDrawNode, 5);
	}

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void Cannon::onEnter()
{
	Node::onEnter();

	if (config.debug) {
		scheduleUpdate();
	}
}

void Cannon::rotate(float angle)
{
	float currentAngle = view->getRotation();
	float clampedAngle = fabsf(360.0f - config.visionAngle) < MATH_EPSILON ? currentAngle - angle : clampf(currentAngle - angle, -config.visionAngle * 0.5f, config.visionAngle * 0.5f);
	view->setRotation(clampedAngle);
	if (config.debug) {
		redrawDebugNode();
	}
}

float Cannon::getRotation()
{
	return -view->getRotation();
}

void Cannon::reset()
{
	view->setRotation(0.0f);
}

void Cannon::fire(const std::function<cocos2d::Node* ()>& shotNodeSupplier)
{
	if (recharging) {
		return;
	}
	shotNode = shotNodeSupplier();
	if (shotNode->getParent() == view) {
		CCLOG("WARNING: shot node is already added to this cannon");
		return;
	}
	shotNode->setPosition(Vec2(config.contentSize.width * 0.5f, 0.0f));
	view->addChild(shotNode);

	float rechargeTime = config.cooldown;

	auto callbackEndShot = dynamic_cast<CallbackEndShot*>(shotNode);
	if (callbackEndShot != nullptr) {
		callbackEndShot->end([this, rechargeTime]() {
			scheduleOnce(schedule_selector(Cannon::updateRecharged), rechargeTime);
		});
	}
	else {
		auto finiteTimeShot = dynamic_cast<FiniteTimeShot*>(shotNode);
		if (finiteTimeShot != nullptr) {
			rechargeTime += finiteTimeShot->duration();
		}

		scheduleOnce(schedule_selector(Cannon::updateRecharged), rechargeTime);
	}

	recharging = true;
}

void Cannon::redrawDebugNode()
{
	debugDrawNode->clear();
	debugDrawNode->drawLine(Vec2::ZERO, Vec2::UNIT_X * DEBUG_LINE_SIZE, Color4F::BLACK);
}

void Cannon::updateRecharged(float dt)
{
	recharging = false;

	view->removeChild(shotNode);
	shotNode = nullptr;
	//TODO: send recharged event (?)
}

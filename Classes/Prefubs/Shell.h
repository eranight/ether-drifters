#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters {

	class Health;
	class DamageDealer;
	class AbstractShellController;
	class ShellControllerConfig;

	class ShellConfig {
	public:
		double damage = 0.0f;
		float lifeTime = 0.0f;
		float velocity;
		cocos2d::Vec2 direction;
		Health* targetHealth = nullptr;
		DamageDealer* dealer = nullptr;
		DamageTargetMode targetMode = DamageTargetMode::any;
		std::string viewFilename;
		std::function<AbstractShellController* (const ShellControllerConfig&)> controllerCreator = nullptr;
	};

	class Shell : public cocos2d::Node {
	public:
		Shell(const ShellConfig& config) : config(config) {}
	public:
		static Shell* create(const ShellConfig& config);
		bool init() override;
		void onEnter() override;
	private:
		void fading();
		void explode(cocos2d::Node * node);
	private:
		ShellConfig config;
		cocos2d::ParticleSystemQuad* particles;
	};

}
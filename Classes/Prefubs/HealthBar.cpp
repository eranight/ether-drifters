#include "HealthBar.h"
#include "Events/HealthEventData.h"
#include "Components/Health.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string HealthBar::NAME = "health_bar";

HealthBar::~HealthBar() {
	CC_SAFE_RELEASE_NULL(takingDamageEventListener);
	CC_SAFE_RELEASE_NULL(takingHealingEventListener);
}

HealthBar* HealthBar::create(const PointsBarConfig& config) {
	auto ref = new (std::nothrow) HealthBar(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool HealthBar::init() {
	if (!AbstractPointsBar::init()) {
		return false;
	}

	takingDamageEventListener = EventListenerCustom::create(TAKING_DAMAGE_EVENT, CC_CALLBACK_1(HealthBar::changeState, this));
	CC_SAFE_RETAIN(takingDamageEventListener);
	takingHealingEventListener = EventListenerCustom::create(TAKING_HEALING_EVENT, CC_CALLBACK_1(HealthBar::changeState, this));
	CC_SAFE_RETAIN(takingHealingEventListener);

	setName(NAME);
	return true;
}

void HealthBar::onEnter() {
	Node::onEnter();

	_eventDispatcher->addEventListenerWithFixedPriority(takingDamageEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(takingHealingEventListener, 1);
}

void HealthBar::onExit() {
	Node::onExit();

	_eventDispatcher->removeEventListener(takingDamageEventListener);
	_eventDispatcher->removeEventListener(takingHealingEventListener);
}

void HealthBar::changeState(EventCustom* custom) {
	HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
	if (eventData->getOwner() == getParent()) {
		float newHitPoints = eventData->getHealthInfo()->currentHitPoints;
		updateState(newHitPoints);
	}
}
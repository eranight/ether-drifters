#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class Touchable;

	class DomainObjectTouchLayer : public cocos2d::Layer {
	private:
		enum class State {
			WAITING,
			TRACKING_TOUCH,
		};
		DomainObjectTouchLayer() : enabled(true), state(State::WAITING), selectedTouchable(nullptr), selectedWithCamera(nullptr) {}
	public:
		~DomainObjectTouchLayer();
		CREATE_FUNC(DomainObjectTouchLayer);
		bool init() override;
		
		bool isEnabled() const { return enabled; }
		void setEnabled(bool value) { enabled = value; };

		bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event) override;
		void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event) override;
		void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event) override;
		void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event) override;
	private:
		Touchable* getTouchable(cocos2d::Touch* touch, const cocos2d::Camera* camera);
	private:
		bool enabled;
		State state;
		Touchable* selectedTouchable;
		const cocos2d::Camera * selectedWithCamera;
	};

}
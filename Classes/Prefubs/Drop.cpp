#include "Drop.h"
#include "Common/ViewNode.hpp"
#include "Components/Dropped.h"
#include "Components/DropController.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

Drop* Drop::create(const DropConfig& config) {
	auto ref = new (std::nothrow) Drop(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Drop::init() {
	if (!Node::init()) {
		return false;
	}

	auto contentSize = getSizeForType(config.type);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	Sprite* sprite = Sprite::createWithSpriteFrameName(config.spriteFrame);
	HelperMethodsUtil::adjustScaleToSize(sprite, contentSize);
	view->addChild(sprite);

	auto physicsBody = PhysicsBody::createCircle(std::min(contentSize.width, contentSize.height) * 0.5f);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::drop);
	physicsBody->setContactTestBitmask(ContactMask::picker);
	view->setPhysicsBody(physicsBody);

	DroppedConfig droppedConfig;
	droppedConfig.type = config.type;
	droppedConfig.quantity = config.quantity;
	addComponent(Dropped::create(droppedConfig));
	DropControllerConfig dropControllerConfig;
	dropControllerConfig.offsetY = 1.0f;
	dropControllerConfig.lifeTime = config.lifeTime;
	dropControllerConfig.fadeEffect = config.lifeTime != -1.0;
	dropControllerConfig.physicsBodyDelay = config.physicsBodyDelay;
	_componentContainer->add(DropController::create(dropControllerConfig));

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

Size Drop::getSizeForType(const DropType& type)
{
	switch (type)
	{
	case DropType::ether_essence:
		return Size(25.6f, 25.6f);
	case DropType::survivor:
		return Size(33.0f, 30.0f);
	case DropType::iron_ore:
		return Size(27.0f, 27.0f);
	}
}

#pragma once
#include "cocos2d.h"
#include "Common/AbstractPointsBar.h"

namespace ether_drifters {
	
	class ResourceBar : public AbstractPointsBar {
	private:
		ResourceBar(const PointsBarConfig& config) : AbstractPointsBar(config) {}
	public:
		~ResourceBar();
		static ResourceBar* create(const PointsBarConfig& config);
		bool init();
		void onEnter() override;
		void onExit() override;
	private:
		void changeState(cocos2d::EventCustom* custom);
	private:
		cocos2d::EventListenerCustom* minedResourceEventListener;
	public:
		static const std::string NAME;
	};

}
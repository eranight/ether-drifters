#include "DomainObjectTouchLayer.h"
#include "Ether.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Touchable.h"
#include "Components/Health.h"
#include "Events/TouchEventData.h"
#include "Events/HealthEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

DomainObjectTouchLayer::~DomainObjectTouchLayer() {
	CCLOG("In the destructor of DomainObjectTouchLayer. %p", this);
}

bool DomainObjectTouchLayer::init() {
	if (!Layer::init()) {
		return false;
	}
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(DomainObjectTouchLayer::onTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(DomainObjectTouchLayer::onTouchMoved, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(DomainObjectTouchLayer::onTouchEnded, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(DomainObjectTouchLayer::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	_eventDispatcher->addCustomEventListener(DEATH_EVENT, [] (EventCustom* event) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(event->getUserData());
		eventData->getOwner()->removeComponent(Touchable::NAME);
	});

	return true;
}

bool DomainObjectTouchLayer::onTouchBegan(Touch* touch, Event* /*event*/) {
	auto camera = Camera::getVisitingCamera();
	if (state != DomainObjectTouchLayer::State::WAITING || !_visible || !enabled || !camera) {
		return false;
	}
	CCLOG("touch began");
	selectedTouchable = getTouchable(touch, camera);
	state = State::TRACKING_TOUCH;
	selectedWithCamera = camera;
	if (selectedTouchable != nullptr) {
		CCLOG("touchable node found %p", selectedTouchable->getOwner());
	}
	else {
		CCLOG("no touchable node found");
	}
	return true;
}

void DomainObjectTouchLayer::onTouchEnded(Touch* touch, Event* /*event*/) {
	auto currentNode = getTouchable(touch, selectedWithCamera);
	if (currentNode != selectedTouchable) {
		selectedTouchable = currentNode;
	}
	state = DomainObjectTouchLayer::State::WAITING;
	selectedWithCamera = nullptr;
	auto node = selectedTouchable != nullptr ? selectedTouchable->getOwner() : nullptr;
	CCLOG("touched node %p", node);
	TouchEventData eventData(this, selectedTouchable);
	_eventDispatcher->dispatchCustomEvent(TOUCH_EVENT, &eventData);
	selectedTouchable = nullptr;
}

void DomainObjectTouchLayer::onTouchCancelled(Touch* /*touch*/, Event* /*event*/) {
	state = DomainObjectTouchLayer::State::WAITING;
	selectedTouchable = nullptr;
	selectedWithCamera = nullptr;
}

void DomainObjectTouchLayer::onTouchMoved(Touch* touch, Event* /*event*/) {
	auto currentNode = getTouchable(touch, selectedWithCamera);
	if (currentNode != selectedTouchable) {
		selectedTouchable = currentNode;
	}
}

Touchable* DomainObjectTouchLayer::getTouchable(Touch* touch, const Camera* camera) {
	Vec2 touchLocation = touch->getLocation();
	auto domain = Ether::getInstance()->getCurrentDomain();
	for (const auto& child : domain->getChildren()) {
		auto touchable = HelperMethodsUtil::findComponent<Touchable>(child);
		if (!child->isVisible() || child->getComponent(Touchable::NAME) == nullptr) {
			continue;
		}
		auto view = HelperMethodsUtil::getViewIfExists(child);
		if (isScreenPointInRect(touchLocation, camera, view->getWorldToNodeTransform(), touchable->getRect(), nullptr)) {
			return touchable;
		}
	}
	return nullptr;
}
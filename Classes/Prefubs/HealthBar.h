#pragma once
#include "cocos2d.h"
#include "Common/AbstractPointsBar.h"

namespace ether_drifters {

	class HealthBar : public AbstractPointsBar {
	private:
		HealthBar(const PointsBarConfig& config) : AbstractPointsBar(config) {}
	public:
		~HealthBar();
		static HealthBar* create(const PointsBarConfig& config);
		bool init();
		void onEnter() override;
		void onExit() override;
	private:
		void changeState(cocos2d::EventCustom* custom);
	private:
		cocos2d::EventListenerCustom* takingDamageEventListener;
		cocos2d::EventListenerCustom* takingHealingEventListener;
	public:
		static const std::string NAME;
	};

}
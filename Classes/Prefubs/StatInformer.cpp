#include "StatInformer.h"
#include "Events/HealthEventData.h"
#include "Components/Health.h"
#include "Events/MinedEventData.h"
#include "Components/Mined.h"
#include "Utils/Constants.h"
#include "Components/Dropped.h"
#include "Events/PickedEventData.h"

using namespace cocos2d;
using namespace ether_drifters;

StatInformer::~StatInformer() {
	CC_SAFE_RELEASE_NULL(showStatAction);
	CC_SAFE_RELEASE_NULL(takingDamageEventListener);
	CC_SAFE_RELEASE_NULL(takingHealingEventListener);
	CC_SAFE_RELEASE_NULL(minedResourcesEventListener);
	CC_SAFE_RELEASE_NULL(pickedDropEventListener);
}

bool StatInformer::init() {
	if (!Node::init()) {
		return false;
	}

	showStatAction = Sequence::createWithTwoActions(
		Spawn::createWithTwoActions(
			MoveBy::create(3.0f, Vec2(0.0f, 40.0f)),
			FadeOut::create(3.0f)
		),
		RemoveSelf::create()
	);
	showStatAction->retain();

	takingDamageEventListener = EventListenerCustom::create(TAKING_DAMAGE_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() == getParent()) {
			pushStat(1, -static_cast<int>(eventData->getHealthInfo()->appliedHit), Color4B::RED);
		}
	});
	takingDamageEventListener->retain();

	takingHealingEventListener = EventListenerCustom::create(TAKING_HEALING_EVENT, [this](EventCustom* custom) {
		HealthEventData* eventData = reinterpret_cast<HealthEventData*>(custom->getUserData());
		if (eventData->getOwner() == getParent()) {
			pushStat(2, static_cast<int>(eventData->getHealthInfo()->appliedHit), Color4B::GREEN);
		}
	});
	takingHealingEventListener->retain();

	minedResourcesEventListener = EventListenerCustom::create(MINED_RESOURCES_EVENT, [this](EventCustom* custom) {
		MinedResourcesEvent* eventData = reinterpret_cast<MinedResourcesEvent*>(custom->getUserData());
		if (eventData->getOwner() == getParent()) {
			pushStat(3, static_cast<int>(eventData->getMinedResources()->getPoints()), Color4B::GRAY);
		}
	});
	minedResourcesEventListener->retain();

	pickedDropEventListener = EventListenerCustom::create(PICKED_DROP_EVENT, [this](EventCustom* custom) {
		PickedEventData* eventData = reinterpret_cast<PickedEventData*>(custom->getUserData());
		if (eventData->getOwner() == getParent()) {
			pushStat(4, eventData->getDrop()->quantity, Color4B::MAGENTA);
		}
	});
	pickedDropEventListener->retain();

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

void StatInformer::onEnter() {
	Node::onEnter();

	_eventDispatcher->addEventListenerWithFixedPriority(takingDamageEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(takingHealingEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(minedResourcesEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(pickedDropEventListener, 1);
}

void StatInformer::onExit() {
	Node::onExit();

	_eventDispatcher->removeEventListener(takingDamageEventListener);
	_eventDispatcher->removeEventListener(takingHealingEventListener);
	_eventDispatcher->removeEventListener(minedResourcesEventListener);
	_eventDispatcher->removeEventListener(pickedDropEventListener);
}

void StatInformer::pushStat(int code, int num, const Color4B& color) {
	auto iter = std::find_if(statInfoQueue.begin(), statInfoQueue.end(), [code](const StatInfo& statInfo) {return statInfo.code == code; });
	if (iter != statInfoQueue.end()) {
		iter->qunatity += num;
	}
	else {
		StatInfo statInfo;
		statInfo.code = code;
		statInfo.color = color;
		statInfo.qunatity = num;
		statInfoQueue.push_back(statInfo);
	}
	if (!_scheduler->isScheduled(schedule_selector(StatInformer::putStat), this)) {
		putStat(1.0f);
	}
}

void StatInformer::putStat(float dt) {
	if (this->statInfoQueue.empty()) {
		unschedule(schedule_selector(StatInformer::putStat));
		return;
	}
	StatInfo statInfo = statInfoQueue.at(0);
	std::string format = statInfo.qunatity != 0 ? "%+d" : "%d";
	auto label = Label::createWithTTF(StringUtils::format(format.c_str(), statInfo.qunatity), "fonts/arial.ttf", 20.0f);
	label->setTextColor(statInfo.color);
	label->runAction(this->showStatAction->clone());
	label->setCameraMask(getCameraMask());
	label->setPosition(Vec2(label->getContentSize().width * 0.5f, 0.0f));
	addChild(label);
	statInfoQueue.erase(statInfoQueue.begin());
	if (!_scheduler->isScheduled(schedule_selector(StatInformer::putStat), this)) {
		schedule(schedule_selector(StatInformer::putStat), 1.0f, CC_REPEAT_FOREVER, 0.0f);
	}
}
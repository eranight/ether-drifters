#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class Island : public cocos2d::Node {
	public:
		CREATE_FUNC(Island);
		bool init() override;
	};

}
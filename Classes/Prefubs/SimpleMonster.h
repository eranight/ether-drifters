#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class HealthBar;

	class SimpleMonster : public cocos2d::Node {
	public:
		CREATE_FUNC(SimpleMonster);
		~SimpleMonster();
		bool init() override;
	private:
		void destroy();
	private:
		cocos2d::Node* vision;
		HealthBar* healthBar;
	};

}
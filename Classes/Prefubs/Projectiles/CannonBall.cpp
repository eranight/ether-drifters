#include "CannonBall.hpp"
#include "Common/ViewNode.hpp"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Direction.h"
#include "Components/Movement.h"
#include "Components/ShellControllers.h"
#include "Components/Damage.h"

using namespace cocos2d;
using namespace ether_drifters;

CannonBall* CannonBall::create(const CannonBallConfig& config)
{
	auto ret = new (std::nothrow) CannonBall(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool CannonBall::init()
{
	if (!Node::init()) {
		return false;
	}

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(config.size);
	addChild(view);

	Sprite* tail = Sprite::create("Effects/speed.png");
	float scaleY = config.size.height / tail->getContentSize().height;
	tail->setScaleY(scaleY);
	tail->setScaleX(0.0f);
	tail->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
	tail->runAction(ScaleTo::create(0.5f, 1.0, scaleY));
	tail->setPosition(Vec2(config.size.width * 0.5f, 0.0f));
	view->addChild(tail);

	Sprite* shell = Sprite::createWithSpriteFrameName("cannonBall.png");
	HelperMethodsUtil::adjustScaleToSize(shell, config.size);
	view->addChild(shell);

	addComponent(Direction::create(Vec2::UNIT_X));

	_componentContainer->add(Movement::create());

	ShellControllerConfig shellControllerConfig;
	shellControllerConfig.target = config.target;
	shellControllerConfig.direction = config.direction;
	shellControllerConfig.velocity = config.velocity;
	shellControllerConfig.lifeTime = config.lifeTime;
	shellControllerConfig.lifeTimeOverReaction = CC_CALLBACK_0(CannonBall::fading, this);
	shellControllerConfig.touchBorderReaction = CC_CALLBACK_0(CannonBall::fading, this);
	shellControllerConfig.touchNodeReaction = CC_CALLBACK_1(CannonBall::explode, this);
	_componentContainer->add(SimpleShellController::create(shellControllerConfig));

	DamageConfig damageConfig;
	damageConfig.dealer = config.damageDealer;
	damageConfig.hit = config.damage;
	damageConfig.damageTargetMode = config.targetMode;
	_componentContainer->add(Damage::create(damageConfig));

	auto physicsBody = PhysicsBody::createCircle(config.size.width * 0.5f);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::hit);
	physicsBody->setContactTestBitmask(ContactMask::body | ContactMask::domain);
	view->setPhysicsBody(physicsBody);


	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void CannonBall::fading()
{
	auto view = HelperMethodsUtil::getViewIfExists(this);
	view->runAction(FadeOut::create(0.4f));
	runAction(Sequence::createWithTwoActions(DelayTime::create(0.4f), RemoveSelf::create()));
}

void CannonBall::explode(Node* node)
{
	auto view = HelperMethodsUtil::getViewIfExists(this);
	HelperMethodsUtil::findComponent<Movement>(this)->setVelocity(0.0f);

	auto exp = ParticleSun::create();
	exp->setAutoRemoveOnFinish(true);
	exp->setScale(1.0f);
	exp->setDuration(0.3f);
	exp->setCameraMask(DOMAIN_CAMERA_MASK);

	Vec2 pos = node->convertToNodeSpace(getParent()->convertToWorldSpace(getPosition()));
	exp->setPosition(pos);
	node->addChild(exp, 3);

	runAction(RemoveSelf::create());
}

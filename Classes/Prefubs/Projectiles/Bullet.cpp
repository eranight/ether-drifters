#include "Bullet.hpp"
#include "Common/ViewNode.hpp"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Direction.h"
#include "Components/Movement.h"
#include "Components/Damage.h"
#include "Components/ShellControllers.h"

using namespace cocos2d;
using namespace ether_drifters;

Bullet* Bullet::create(const BulletConfig& config)
{
	auto ret = new (std::nothrow) Bullet(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool Bullet::init()
{
	if (!Node::init()) {
		return false;
	}

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(config.contentSize);
	addChild(view);

	Sprite* sprite = Sprite::create("laserBlue07.png");
	HelperMethodsUtil::adjustScaleToSize(sprite, config.contentSize);
	view->addChild(sprite);

	addComponent(Direction::create(Vec2::UNIT_X));

	_componentContainer->add(Movement::create());
	
	DamageConfig damageConfig;
	damageConfig.dealer = config.dealer;
	damageConfig.hit = config.damage;
	damageConfig.damageTargetMode = config.targetMode;
	_componentContainer->add(Damage::create(damageConfig));

	ShellControllerConfig shellControllerConfig;
	shellControllerConfig.target = config.target;
	shellControllerConfig.direction = config.direction;
	shellControllerConfig.velocity = config.velocity;
	shellControllerConfig.lifeTime = config.lifeTime;
	shellControllerConfig.lifeTimeOverReaction = CC_CALLBACK_0(Bullet::fading, this);
	shellControllerConfig.touchBorderReaction = CC_CALLBACK_0(Bullet::fading, this);
	shellControllerConfig.touchNodeReaction = CC_CALLBACK_1(Bullet::collide, this);
	_componentContainer->add(SimpleShellController::create(shellControllerConfig));

	auto physicsBody = PhysicsBody::createBox(config.contentSize);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::hit);
	physicsBody->setContactTestBitmask(ContactMask::body | ContactMask::domain);
	view->setPhysicsBody(physicsBody);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void Bullet::fading()
{
	auto view = HelperMethodsUtil::getViewIfExists(this);
	view->runAction(FadeOut::create(0.1f));
	runAction(Sequence::createWithTwoActions(DelayTime::create(0.4f), RemoveSelf::create()));
}

void Bullet::collide(cocos2d::Node* node)
{
	auto view = HelperMethodsUtil::getViewIfExists(this);

	HelperMethodsUtil::findComponent<Movement>(this)->setVelocity(0.0f);

	auto exp = ParticleSun::create();
	exp->setAutoRemoveOnFinish(true);
	exp->setScale(0.5f);
	exp->setDuration(0.1f);
	exp->setEmissionRate(50);
	exp->setCameraMask(DOMAIN_CAMERA_MASK);

	Vec2 pos = node->convertToNodeSpace(getParent()->convertToWorldSpace(getPosition()));
	exp->setPosition(pos);
	node->addChild(exp, 3);

	runAction(RemoveSelf::create());
}

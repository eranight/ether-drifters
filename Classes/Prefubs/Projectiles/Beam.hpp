#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters
{

	class ViewNode;
	class DamageDealer;

	class BeamConfig {
	public:
		float deployDuration;
		float activeDuration;
		float foldDuration;
		float damage;
		float length;
		float width;
		DamageDealer* dealer = nullptr;
		std::function<void(cocos2d::Node*)> onFoldingCompletedCallback;
	};

	class Beam : public cocos2d::Node {
		CREATE_FUNC_WITH_CONFIG(Beam, BeamConfig);
	public:
		bool init() override;
		void update(float dt) override;
	private:
		void resize();
	private:

		enum class RayState {
			deploy, active, folding
		} state = RayState::deploy;

		float timer = 0.0f;
		float length = 0.0f;

		ViewNode* view = nullptr;
		cocos2d::Node* startPart = nullptr;
		cocos2d::Node* middlePart = nullptr;
		cocos2d::Node* endPart = nullptr;
	};

}
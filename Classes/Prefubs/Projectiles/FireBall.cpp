#include "FireBall.h"
#include "Common/ViewNode.hpp"
#include "Components/Movement.h"
#include "Components/Direction.h"
#include "Components/ShellControllers.h"
#include "Components/Health.h"
#include "Components/Damage.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

bool FireBall::init() {
	if (!Node::init()) {
		return false;
	}

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(config.contentSize);
	addChild(view);

	particles = ParticleSystemQuad::create(config.viewFilename);
	particles->setStartSize(config.contentSize.width);
	view->addChild(particles);

	auto physicsBody = PhysicsBody::createCircle(config.contentSize.width * 0.5f);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::hit);
	physicsBody->setContactTestBitmask(ContactMask::body | ContactMask::domain);
	view->setPhysicsBody(physicsBody);

	addComponent(Movement::create());
	_componentContainer->add(Direction::create(Vec2::UNIT_X));
	DamageConfig damageConfig;
	damageConfig.hit = this->config.damage;
	damageConfig.dealer = this->config.dealer;
	damageConfig.damageTargetMode = this->config.targetMode;
	damageConfig.target = this->config.targetHealth;
	Damage* damage = Damage::create(damageConfig);
	_componentContainer->add(damage);
	ShellControllerConfig shellControllerConfig;
	shellControllerConfig.lifeTime = this->config.lifeTime;
	shellControllerConfig.velocity = this->config.velocity;
	shellControllerConfig.direction = this->config.direction;
	shellControllerConfig.target = this->config.targetHealth != nullptr ? this->config.targetHealth->getOwner() : nullptr;
	shellControllerConfig.lifeTimeOverReaction = CC_CALLBACK_0(FireBall::fading, this);
	shellControllerConfig.touchBorderReaction = CC_CALLBACK_0(FireBall::fading, this);
	shellControllerConfig.touchNodeReaction = CC_CALLBACK_1(FireBall::collide, this);
	AbstractShellController* controller;
	if (this->config.controllerCreator == nullptr) {
		controller = SimpleShellController::create(shellControllerConfig);
	}
	else {
		controller = config.controllerCreator(shellControllerConfig);
	}
	_componentContainer->add(controller);

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void FireBall::onEnter() {
	Node::onEnter();

	particles->start();
}

void FireBall::fading() {
	auto view = HelperMethodsUtil::getViewIfExists(this);
	particles->stop();

	runAction(Sequence::createWithTwoActions(DelayTime::create(0.4f), RemoveSelf::create()));
}

void FireBall::collide(cocos2d::Node* node) {
	auto view = HelperMethodsUtil::getViewIfExists(this);
	particles->stop();
	HelperMethodsUtil::findComponent<Movement>(this)->setVelocity(0.0f);

	auto exp = ParticleSun::create();
	exp->setAutoRemoveOnFinish(true);
	exp->setScale(1.0f);
	exp->setDuration(0.3f);
	exp->setCameraMask(DOMAIN_CAMERA_MASK);

	Vec2 pos = node->convertToNodeSpace(getParent()->convertToWorldSpace(getPosition()));
	exp->setPosition(pos);
	node->addChild(exp, 3);

	runAction(RemoveSelf::create());
}
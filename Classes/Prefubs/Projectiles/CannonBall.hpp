#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters
{
	
	class DamageDealer;
	
	class CannonBallConfig {
	public:
		float damage;
		float velocity;
		float lifeTime;
		DamageDealer* damageDealer = nullptr;
		cocos2d::Node* target = nullptr;
		cocos2d::Vec2 direction;
		DamageTargetMode targetMode = DamageTargetMode::any;
		cocos2d::Size size = cocos2d::Size(15.0f, 15.0f);
	};

	class CannonBall : public cocos2d::Node {
	private:
		CannonBall(const CannonBallConfig& config) : config(config) {}
	public:
		static CannonBall* create(const CannonBallConfig& config);
		bool init() override;
	private:
		void fading();
		void explode(cocos2d::Node* node);
	private:
		CannonBallConfig config;
	};

}
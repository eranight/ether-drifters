#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters 
{

	class Health;
	class DamageDealer;
	class AbstractShellController;
	class ShellControllerConfig;

	class FireBallConfig {
	public:
		double damage = 0.0f;
		float lifeTime = 0.0f;
		float velocity;
		cocos2d::Vec2 direction;
		Health* targetHealth = nullptr;
		DamageDealer* dealer = nullptr;
		DamageTargetMode targetMode = DamageTargetMode::any;
		std::string viewFilename;
		std::function<AbstractShellController* (const ShellControllerConfig&)> controllerCreator = nullptr;
		cocos2d::Size contentSize;
	};

	class FireBall : public cocos2d::Node {
		CREATE_FUNC_WITH_CONFIG(FireBall, FireBallConfig);
	public:
		bool init() override;
		void onEnter() override;
	private:
		void fading();
		void collide(cocos2d::Node * node);
	private:
		cocos2d::ParticleSystem* particles;
	};

}
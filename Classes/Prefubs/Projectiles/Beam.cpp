#include "Beam.hpp"
#include "Common/ViewNode.hpp"
#include "Components/Damage.h"
#include "Components/DamageDealer.h"
#include "Components/Direction.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

bool ether_drifters::Beam::init()
{
	if (!Node::init()) {
		return false;
	}

	if (config.deployDuration == 0.0f) {
		length = config.length;
		state = RayState::active;
	}
	else {
		length = 0.001f;
		state = RayState::deploy;
	}

	Size contentSize = Size(length, config.width);

	view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	SpriteBatchNode* batchNode = SpriteBatchNode::create("Spritesheets/laser.png");

	startPart = Sprite::createWithSpriteFrameName("laserBlue05-start");
	startPart->setScaleY(config.width / startPart->getContentSize().height);
	startPart->setPosition(Vec2(startPart->getContentSize().width * 0.5f, 0.0f));
	batchNode->addChild(startPart);

	middlePart = Sprite::createWithSpriteFrameName("laserBlue05-middle");
	middlePart->setScaleX(length / middlePart->getContentSize().width);
	middlePart->setPosition(Vec2(startPart->getPosition().x + startPart->getContentSize().width * 0.5f, 0.0f));
	middlePart->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	middlePart->setScaleY(config.width / middlePart->getContentSize().height);
	batchNode->addChild(middlePart, 0, "middle");

	endPart = Sprite::createWithSpriteFrameName("laserBlue05-end");
	endPart->setPosition(Vec2(middlePart->getPosition().x + length + endPart->getContentSize().width * 0.5f, 0.0f));
	endPart->setScaleY(config.width / endPart->getContentSize().height);
	batchNode->addChild(endPart, 0, "end");

	view->addChild(batchNode);

	auto physicsBody = PhysicsBody::createBox(middlePart->getContentSize());
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::hit);
	physicsBody->setContactTestBitmask(ContactMask::body | ContactMask::domain);
	middlePart->setPhysicsBody(physicsBody);

	DamageConfig damageConfig;
	damageConfig.hit = this->config.damage;
	damageConfig.dealer = config.dealer;
	damageConfig.interval = 1.0f;
	damageConfig.damageTargetMode = DamageTargetMode::any;
	addComponent(Damage::create(damageConfig));

	_componentContainer->add(Direction::create(Vec2::UNIT_X));

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void ether_drifters::Beam::update(float dt)
{
	Node::update(dt);

	if (state == RayState::deploy) {
		length = std::clamp(length + config.length / config.deployDuration * dt, 0.0f, config.length);
		resize();
		endPart->setPosition(middlePart->getPosition() + Vec2(length + endPart->getContentSize().width * 0.5f, 0.0f));
		if (length == config.length) {
			timer = 0.0f;
			state = RayState::active;
		}
	}
	else if (state == RayState::active) {
		timer = std::clamp(timer + dt, 0.0f, config.activeDuration);
		if (timer == config.activeDuration) {
			state = RayState::folding;
			middlePart->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
			middlePart->setPosition(middlePart->getPosition() + Vec2(length, 0.0f));
		}
	}
	else {
		length = std::clamp(length - config.length / config.foldDuration * dt, 0.0f, config.length);
		resize();
		startPart->setPosition(middlePart->getPosition() - Vec2(length + startPart->getContentSize().width * 0.5f, 0.0f));
		if (length == 0.0f) {
			config.onFoldingCompletedCallback(this);
		}
	}
}

void ether_drifters::Beam::resize()
{
	view->getUserObject()->setContentSize(Size(length, config.width));
	float scale = middlePart->getContentSize().width == 0.0f ? 0.0f : (length / middlePart->getContentSize().width);
	middlePart->setScaleX(scale);
}

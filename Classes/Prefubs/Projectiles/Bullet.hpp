#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters
{

	class DamageDealer;

	class BulletConfig {
	public:
		cocos2d::Size contentSize = cocos2d::Size(16.444f, 4.0f);
		float damage = 0.0f;
		float velocity = 0.0f;
		float lifeTime = 0.0f;
		DamageDealer* dealer = nullptr;
		cocos2d::Node* target = nullptr;
		cocos2d::Vec2 direction;
		DamageTargetMode targetMode = DamageTargetMode::any;
	};

	class Bullet : public cocos2d::Node {
	private:
		Bullet(const BulletConfig& config) : config(config) {}
	public:
		static Bullet* create(const BulletConfig& config);
		bool init() override;
	private:
		void fading();
		void collide(cocos2d::Node* node);
	private:
		BulletConfig config;
	};

}
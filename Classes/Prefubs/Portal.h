#ifndef __PORTAL_H__
#define __PORTAL_H__

#include "cocos2d.h"

namespace ether_drifters {

	class PortalConfig {
	public:
		std::string destClasterId;
		std::string destDomainId;
		std::string outPortalId;
		cocos2d::Color3B color;
	};

	class Portal : public cocos2d::Node {
	protected:
		Portal(const PortalConfig& config): config(config) {}
	public:
		static Portal* create(const PortalConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		void teleport(cocos2d::Node* playerNode);
	private:
		PortalConfig config;
		cocos2d::Vec2 destination;
		cocos2d::EventListenerPhysicsContact* physicsListener;
		cocos2d::Vector<cocos2d::Node*> candidates;
	};

}

#endif //__PORTAL_H__
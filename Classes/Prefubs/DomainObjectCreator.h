#pragma once
#include "cocos2d.h"
#include "json/document.h"

namespace ether_drifters {

	class Domain;
	class Portal;
	class Meteor;
	class Skeleton;
	class SimpleMonster;

	class DomainObjectCreator {
	private:
		DomainObjectCreator() {}
	public:
		static Domain* createDomain(const rapidjson::Value& val);
		static Portal* createPortal(const rapidjson::Value& val);
		static Meteor* createMeteor(const rapidjson::Value& val);
		static Skeleton* createSkeleton(const rapidjson::Value& val);
		static SimpleMonster* createSimpleMonster(const rapidjson::Value& val);
	};

}
#include "Laser.hpp"
#include "Common/ViewNode.hpp"
#include "Components/Damage.h"
#include "Components/Direction.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

Laser* Laser::create(const LaserConfig& config)
{
	auto ret = new (std::nothrow) Laser(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool Laser::init()
{
	if (!Node::init()) {
		return false;
	}

	Size contentSize = Size(11.0f, config.width);

	view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);
	

	SpriteBatchNode* batchNode = SpriteBatchNode::create("Spritesheets/laser.png");
	startLaserPart = Sprite::createWithSpriteFrameName("laserBlue01-start");
	startLaserPart->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	startLaserPart->setScaleY(config.width / startLaserPart->getContentSize().height);
	batchNode->addChild(startLaserPart);

	middleLaserPart = Sprite::createWithSpriteFrameName("laserBlue01-middle");
	middleLaserPart->setScaleX(1.0f / middleLaserPart->getContentSize().width);
	middleLaserPart->setPosition(Vec2(startLaserPart->getContentSize().width, 0.0f));
	middleLaserPart->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	middleLaserPart->setScaleY(config.width / middleLaserPart->getContentSize().height);
	batchNode->addChild(middleLaserPart, 0, "middle");

	endLaserPart = Sprite::createWithSpriteFrameName("laserBlue01-end");
	endLaserPart->setPosition(Vec2(middleLaserPart->getPosition().x + 1.0f, 0.0f));
	endLaserPart->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	endLaserPart->setScaleY(config.width / endLaserPart->getContentSize().height);
	batchNode->addChild(endLaserPart, 0, "end");

	view->addChild(batchNode);

	auto physicsBody = PhysicsBody::createBox(middleLaserPart->getContentSize());
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::hit);
	physicsBody->setContactTestBitmask(ContactMask::body | ContactMask::domain);
	middleLaserPart->setPhysicsBody(physicsBody);

	DamageConfig damageConfig;
	damageConfig.hit = this->config.damage;
	damageConfig.dealer = this->config.damageDealer;
	damageConfig.interval = 1.0f;
	damageConfig.damageTargetMode = DamageTargetMode::any;
	addComponent(Damage::create(damageConfig));
	direction = Direction::create(Vec2::UNIT_X);
	_componentContainer->add(direction);

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void Laser::setLength(float length)
{
	if (abs(this->length - length) < MATH_EPSILON) {
		return;
	}
	this->length = length;
	view->getUserObject()->setContentSize(Size(10.0f + length, config.width));
	float scale = length / middleLaserPart->getContentSize().width;
	middleLaserPart->setScaleX(scale);
	endLaserPart->setPosition(middleLaserPart->getPosition() + Vec2(length, 0.0f));
}

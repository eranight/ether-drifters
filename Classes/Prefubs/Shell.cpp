#include "Shell.h"
#include "Common/ViewNode.hpp"
#include "Components/Movement.h"
#include "Components/Direction.h"
#include "Components/ShellControllers.h"
#include "Components/Health.h"
#include "Components/Damage.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

Shell* Shell::create(const ShellConfig& config) {
	auto ref = new (std::nothrow) Shell(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Shell::init() {
	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(15.0f, 15.0f);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	particles = ParticleSystemQuad::create(config.viewFilename);
	view->addChild(particles);

	auto physicsBody = PhysicsBody::createCircle(contentSize.width * 0.5f);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::hit);
	physicsBody->setContactTestBitmask(ContactMask::body | ContactMask::domain);
	view->setPhysicsBody(physicsBody);

	addComponent(Movement::create());
	_componentContainer->add(Direction::create(Vec2::UNIT_X));
	DamageConfig damageConfig;
	damageConfig.hit = this->config.damage;
	damageConfig.dealer = this->config.dealer;
	damageConfig.damageTargetMode = this->config.targetMode;
	damageConfig.target = this->config.targetHealth;
	Damage* damage = Damage::create(damageConfig);
	_componentContainer->add(damage);
	ShellControllerConfig config;
	config.lifeTime = this->config.lifeTime;
	config.velocity = this->config.velocity;
	config.direction = this->config.direction;
	config.target = this->config.targetHealth != nullptr ? this->config.targetHealth->getOwner() : nullptr;
	config.lifeTimeOverReaction = CC_CALLBACK_0(Shell::fading, this);
	config.touchBorderReaction = CC_CALLBACK_0(Shell::fading, this);
	config.touchNodeReaction = CC_CALLBACK_1(Shell::explode, this);
	AbstractShellController* controller;
	if (this->config.controllerCreator == nullptr) {
		controller = SimpleShellController::create(config);
	}
	else {
		controller = this->config.controllerCreator(config);
	}
	_componentContainer->add(controller);

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void Shell::onEnter() {
	Node::onEnter();

	particles->start();
}

void Shell::fading() {
	auto view = HelperMethodsUtil::getViewIfExists(this);
	particles->stop();
	
	runAction(Sequence::createWithTwoActions(DelayTime::create(0.4f), RemoveSelf::create()));
}

void Shell::explode(cocos2d::Node* node) {
	auto view = HelperMethodsUtil::getViewIfExists(this);
	particles->stop();
	HelperMethodsUtil::findComponent<Movement>(this)->setVelocity(0.0f);

	auto exp = ParticleSun::create();
	exp->setAutoRemoveOnFinish(true);
	exp->setScale(1.0f);
	exp->setDuration(0.3f);
	exp->setCameraMask(DOMAIN_CAMERA_MASK);

	Vec2 pos = node->convertToNodeSpace(getParent()->convertToWorldSpace(getPosition()));
	exp->setPosition(pos);
	node->addChild(exp, 3);

	runAction(RemoveSelf::create());
}
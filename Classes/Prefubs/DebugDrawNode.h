#pragma once

#include "cocos2d.h"

namespace ether_drifters {

	class DebugDrawNode : public cocos2d::Node {
	public:
		CREATE_FUNC(DebugDrawNode);
		bool init() override;
		void update(float dt) override;
	private:
		void recalculate(cocos2d::Vec2& point);
	private:
		std::vector<cocos2d::Vec2> coords;
		cocos2d::DrawNode* drawNode = nullptr;
		float domainRadius;
	};

}
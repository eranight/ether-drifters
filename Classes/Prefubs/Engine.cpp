#include "Engine.h"
#include "Events/MovementEventData.h"
#include "Components/Skills/AfterburnerSkill.hpp"
#include "Utils/HelperMethodsUtil.h"
#include "Ether.h"

using namespace cocos2d;
using namespace ether_drifters;

Engine::~Engine() {
	CC_SAFE_RELEASE_NULL(movementEventListener);
	CC_SAFE_RELEASE_NULL(startAfterburnerEventListener);
	CC_SAFE_RELEASE_NULL(stopAfterburnerEventListener);
	CC_SAFE_RELEASE_NULL(maxVelocityAction);
}

Engine* Engine::create(const EngineConfig& config) {
	auto ref = new (std::nothrow) Engine(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Engine::init() {
	if (!Node::init()) {
		return false;
	}
	isRunning = false;
	int num = config.num / 2;
	float startOffset = config.offset;

	leftFire = createFireSprite();
	leftFire->setPosition(Vec2(-startOffset, 0.0f));
	addChild(leftFire);

	rightFire = createFireSprite();
	rightFire->setPosition(Vec2(startOffset, 0.0f));
	addChild(rightFire);

	movementEventListener = EventListenerCustom::create(CHANGE_VELOCITY_EVENT, CC_CALLBACK_1(Engine::processChangeVelocity, this));
	movementEventListener->retain();

	startAfterburnerEventListener = EventListenerCustom::create(START_AFTERBURNER_SKILL, CC_CALLBACK_1(Engine::processStartAfterburner, this));
	startAfterburnerEventListener->retain();

	stopAfterburnerEventListener = EventListenerCustom::create(STOP_AFTERBURNER_SKILL, CC_CALLBACK_1(Engine::processStopAfterburner, this));
	stopAfterburnerEventListener->retain();

	maxVelocityAction = RepeatForever::create(
		Sequence::createWithTwoActions(
			ScaleBy::create(0.5f, 1.0 / 0.95),
			ScaleBy::create(0.5f, 0.95)
		)
	);
	maxVelocityAction->retain();

	scheduleUpdate();
	return true;
}

void Engine::onEnter() {
	Node::onEnter();
	_eventDispatcher->addEventListenerWithFixedPriority(movementEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(startAfterburnerEventListener, 1);
	_eventDispatcher->addEventListenerWithFixedPriority(stopAfterburnerEventListener, 1);
}

void Engine::onExit() {
	Node::onExit();

	_eventDispatcher->removeEventListener(movementEventListener);
	_eventDispatcher->removeEventListener(startAfterburnerEventListener);
	_eventDispatcher->removeEventListener(stopAfterburnerEventListener);
}

void Engine::update(float dt)
{
	Node::update(dt);
}

Sprite* Engine::createFireSprite()
{
	auto fireSprite = Sprite::createWithSpriteFrameName("fire_blue");
	fireSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
	fireSprite->setScaleY(0.0f);
	return fireSprite;
}

void Engine::processChangeVelocity(EventCustom* event)
{
	auto eventData = HelperMethodsUtil::convertEventData<ChangeVelocityMovementEventData>(event, HelperMethodsUtil::getRightNode(this));
	if (eventData == nullptr || eventData->getType() == ChangeVelocityMovementEventData::ChangeVelocityType::mod) {
		return;
	}
	float scaleFactor = eventData->getNextVelocity() / config.maxVelocity;
	bool isMaxVelocity = abs(1.0f - scaleFactor) < FLT_EPSILON;
	for (auto child : getChildren()) {
		child->setScaleY(scaleFactor);
		if (isMaxVelocity && child->getNumberOfRunningActions() == 0) {
			child->runAction(maxVelocityAction->clone());
		}
		else if (!isMaxVelocity && child->getNumberOfRunningActions() > 0) {
			child->stopAllActions();
		}
	}
}

void Engine::processStartAfterburner(EventCustom* event)
{
	leftFire->setSpriteFrame("fire_magenta");
	rightFire->setSpriteFrame("fire_magenta");
}

void Engine::processStopAfterburner(EventCustom* event)
{
	leftFire->setSpriteFrame("fire_blue");
	rightFire->setSpriteFrame("fire_blue");
}

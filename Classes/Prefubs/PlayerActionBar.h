#pragma once
#include "cocos2d.h"
#include "Events/FocusEventListener.h"

namespace ether_drifters {

	class PlayerActionBar : public cocos2d::Node {
	public:
		CREATE_FUNC(PlayerActionBar);
		virtual ~PlayerActionBar();
		bool init() override;
	private:
		cocos2d::Menu* getNodeMenu(cocos2d::Node* node);
		cocos2d::MenuItem* createMenuItem(const std::string& frameName);
		cocos2d::Node* getDialog(const std::string& dialogFrameName, const std::string& actionFrameName);
		void align(cocos2d::Menu* menu);
	private:
		float contentWidth;
		//cocos2d::Menu* menu;
		MinedEventListener* minedEventListener;
		TrackedEventListener* trackedEventListener;
	};

}
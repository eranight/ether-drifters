#include "SimpleMonster.h"
#include "Common/ViewNode.hpp"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Direction.h"
#include "Components/Movement.h"
#include "Components/SimpleEnemyController.h"
#include "Components/Tracked.h"
#include "Components/Tracking.h"
#include "Components/DamageDealer.h"
#include "Components/Health.h"
#include "Components/Regeneration.h"
#include "Components/DropSupplier.h"
#include "Components/Destroyable.h"
#include "Components/Touchable.h"
#include "Components/Obstacle.h"
#include "Components/ShellControllers.h"
#include "EmotesPanel.h"
#include "Events/TrackedEventData.h"
#include "Events/HealthEventData.h"
#include "HealthBar.h"
#include "StatInformer.h"
#include "Cannon.hpp"
#include "Projectiles/FireBall.h"
#include "Skills/SingleSalvo.hpp"
#include "json/document.h"

using namespace cocos2d;
using namespace ether_drifters;
using namespace rapidjson;

SimpleMonster::~SimpleMonster() {
	CCLOG("simple monster destroyed");
}

bool SimpleMonster::init() {
	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(37.0f, 37.0f);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	Sprite* sprite = Sprite::create("monster.png");
	HelperMethodsUtil::adjustScaleToSize(sprite, contentSize);
	view->addChild(sprite);

	PhysicsBody* physicsBody = PhysicsBody::createCircle(contentSize.width * 0.5f);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::body | ContactMask::tracked);
	physicsBody->setContactTestBitmask(ContactMask::domain | ContactMask::tracking | ContactMask::hit);
	view->setPhysicsBody(physicsBody);

	CannonConfig cannonConfig;
	cannonConfig.visionAngle = 0.0f;
	cannonConfig.debug = false;
	auto cannon = Cannon::create(cannonConfig);
	cannon->setPosition(Vec2(contentSize.width * 0.5f, 0.0f));
	view->addChild(cannon);

	addComponent(Direction::create(Vec2::UNIT_X));
	_componentContainer->add(Movement::create());
	auto damageDealer = DamageDealer::create({});
	_componentContainer->add(damageDealer);
	SimpleEnemyControllerConfig controllerConfig;
	controllerConfig.fromPoint = Vec2(contentSize.width * 0.7f, 0.0f);
	controllerConfig.cooldown = 1.2f;
	controllerConfig.cannon = cannon;
	controllerConfig.skillNodeSupplier = [damageDealer](Health* target) {
		SingleSalvoConfig skillConfig;
		skillConfig.shellContectSize = Size(10.0f, 10.0f);
		skillConfig.shellNodeSupplier = [damageDealer, target](const Vec2 dir) {
			FireBallConfig fireBallConfig;
			fireBallConfig.damage = 0.0f;
			fireBallConfig.lifeTime = 5.0f;
			fireBallConfig.velocity = 200.0f;
			fireBallConfig.targetMode = DamageTargetMode::target;
			fireBallConfig.targetHealth = target;
			fireBallConfig.dealer = damageDealer;
			fireBallConfig.direction = dir;
			fireBallConfig.viewFilename = "particles/fire_ball.plist";
			fireBallConfig.contentSize = Size(10.0f, 10.0f);
			fireBallConfig.controllerCreator = [](const ShellControllerConfig& shellControllerConfig) {
				return SimpleShellController::create(shellControllerConfig);
			};
			return FireBall::create(fireBallConfig);
		};
		return SingleSalvo::create(skillConfig);
	};
	_componentContainer->add(SimpleEnemyController::create(controllerConfig));
	_componentContainer->add(Tracked::create());
	_componentContainer->add(Tracking::create());
	HealthConfig healthConfig;
	healthConfig.hitPoints = 100.0f;
	healthConfig.currentHitPoints = 100.0f;
	_componentContainer->add(Health::create(healthConfig));
	RegenerationConfig regenerationConfig;
	regenerationConfig.heal = 2.0;
	regenerationConfig.interval = 1.0f;
	regenerationConfig.delay = 5.0f;
	_componentContainer->add(Regeneration::create(regenerationConfig));
	DropSupplierConfig dropSupplierConfig;
	dropSupplierConfig.eventName = DEATH_EVENT;
	dropSupplierConfig.points = { Vec2::ZERO };
	dropSupplierConfig.spriteFrames = { "ether_essence_red_large" };
	dropSupplierConfig.type = DropType::ether_essence;
	dropSupplierConfig.quantity = RandomHelper::random_int(1, 5);
	dropSupplierConfig.lifeTime = -1.0;
	dropSupplierConfig.physicsBodyDelay = 2.0f;
	_componentContainer->add(DropSupplier::create(dropSupplierConfig));
	_componentContainer->add(Destroyable::create(CC_CALLBACK_0(SimpleMonster::destroy, this)));
	_componentContainer->add(Touchable::create(contentSize));
	float radius = contentSize.width * 0.5f;

	vision = Node::create();
	vision->setPosition(contentSize * 0.5f);
	view->addChild(vision, 0, VISION);

	float rad = CC_DEGREES_TO_RADIANS(50.0f);
	float r = contentSize.width * 0.5f * 1.01f / cos(rad);
	Vec2 startVisionPoint = Vec2(r * cos(rad), r * sin(rad));
	auto visionPhysicsBody = PhysicsBody::create();
	visionPhysicsBody->addShape(HelperMethodsUtil::createVisionPhysicsShape(startVisionPoint, 12, 100.0f, 400.0f));
	visionPhysicsBody->setDynamic(false);
	visionPhysicsBody->setCategoryBitmask(ContactMask::tracking);
	visionPhysicsBody->setContactTestBitmask(ContactMask::tracked);
	vision->setPhysicsBody(visionPhysicsBody);


	auto emotesMap = std::map<std::string, std::pair<std::string, std::string>>();
	emotesMap[START_TRACKING_EVENT] = std::make_pair<std::string, std::string>("dialog_normal", "emote_anger");
	emotesMap[STOP_TRACKING_EVENT] = std::make_pair<std::string, std::string>("mind_normal", "emote_question");
	auto emotesPanel = EmotesPanel::create(emotesMap);
	emotesPanel->setPosition(Vec2(0.0f, contentSize.height * 1.1f + 8.0f));
	addChild(emotesPanel, 0, "emotes_panel");

	float width = contentSize.width * 1.1f;
	PointsBarConfig healthBarConfig;
	healthBarConfig.width = width;
	healthBarConfig.maxPoints = 100.0f;
	healthBarConfig.startPoints = 100.0f;
	healthBarConfig.color = Color4F::RED;
	healthBar = HealthBar::create(healthBarConfig);
	healthBar->setPosition(Vec2(-width * 0.5f, contentSize.height * 1.0f));
	healthBar->setVisible(false);
	addChild(healthBar);

	auto statInformer = StatInformer::create();
	statInformer->setPosition(Vec2(contentSize.width * 0.9f, contentSize.height * 0.6f));
	addChild(statInformer);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void SimpleMonster::destroy() {
	Node* view = HelperMethodsUtil::getViewIfExists(this);
	view->getPhysicsBody()->setEnabled(false);
	view->runAction(
		Sequence::createWithTwoActions(
			FadeOut::create(3.0f),
			CallFunc::create([this]() { this->runAction(RemoveSelf::create()); })
		)
	);
}

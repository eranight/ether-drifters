#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class CrosshairConfig {
	public:
		cocos2d::Color3B color;
	};

	class Crosshair : public cocos2d::Node {
	private:
		Crosshair(const CrosshairConfig& config) : config(config) {}
	public:
		static Crosshair* create(const CrosshairConfig& config);
		bool init() override;
		void setColor(const cocos2d::Color3B& color) override;
	private:
		CrosshairConfig config;
	public:
		static const std::string NAME;
	};

}
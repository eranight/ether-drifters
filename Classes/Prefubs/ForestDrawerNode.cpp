#include "ForestDrawerNode.h"
#include "Utils/CircularObstaclePathfinding/Util.h"
#include "Utils/CircularObstaclePathfinding/ObstacleModel.h"
#include "Utils/CircularObstaclePathfinding/VertexModel.h"
#include "Utils/CircularObstaclePathfinding/EdgeModel.h"

using namespace cocos2d;
using namespace circular_obstacle_pathfinding;

bool ForestDrawerNode::init() {
	if (!Node::init()) {
		return false;
	}

	needDrawVertices = false;
	needDrawSurfingEdges = false;
	needDrawHuggingEdges = false;
	additionalWidth = 0.0f;
	needToReinitGraph = false;

	drawObstaclesNode = DrawNode::create();
	if (drawObstaclesNode == nullptr) {
		return false;
	}
	addChild(drawObstaclesNode);

	drawGraphNode = DrawNode::create(1.0f);
	if (drawGraphNode == nullptr) {
		return false;
	}
	addChild(drawGraphNode);

	drawPathNode = DrawNode::create(3.0f);
	if (drawPathNode == nullptr) {
		return false;
	}
	addChild(drawPathNode);

	scheduleUpdate();
	return true;
}

void ForestDrawerNode::update(float dt) {
	Node::update(dt);
	if (forest == nullptr) return;
	drawObstaclesNode->clear();
	const Obstacle* drawedObstacle;
	for (auto& drawedObstacle : forest->getObstacles()) {
		auto obstaclePos = Vec2(drawedObstacle.getX(), drawedObstacle.getY());
		if (drawedObstacle.getRadius() < FLT_EPSILON) {
			drawObstaclesNode->drawDot(obstaclePos, 5.0f, Color4F::GREEN);
		}
		else {
			drawObstaclesNode->drawCircle(obstaclePos, drawedObstacle.getRadius(), 360.0f, 72, false, Color4F::GREEN);
		}
	}
	if (graph == nullptr) return;
	drawGraphNode->clear();
	auto& obstacles = graph->getObstacles();
	auto& vertices = graph->getVertices();
	auto& edges = graph->getEdges();
	Vec2 vec1, vec2;
	if (needDrawSurfingEdges || needDrawHuggingEdges || needDrawVertices) {
		if (needDrawSurfingEdges || needDrawHuggingEdges) {
			for (auto& keyValue : graph->getEdges()) {
				EdgeModel& edge = keyValue.second;
				if (edge.hugging && needDrawHuggingEdges) {
					auto& obstacle = obstacles[edge.obstacle];
					Vec2 center(obstacle.x, obstacle.y);
					vec1.set(vertices[edge.vertices.second].x, vertices[edge.vertices.second].y);
					vec1 -= center;
					drawCircleSegment(
						drawGraphNode,
						center,
						obstacle.radius,
						Util::convertAngleTo2Pi(vec1.getAngle()),
						edge.weight,
						36,
						Color4F::MAGENTA);
				}
				else if (!edge.hugging && needDrawSurfingEdges) {
					vec2.set(vertices[edge.vertices.first].x, vertices[edge.vertices.first].y);
					vec1.set(vertices[edge.vertices.second].x, vertices[edge.vertices.second].y);
					drawGraphNode->drawLine(vec1, vec2, Color4F::GRAY);
				}
			}
		}
		if (needDrawVertices) {
			for (auto& vertex : vertices) {
				vec1.set(vertex.second.x, vertex.second.y);
				drawGraphNode->drawDot(vec1, 4, Color4F::RED);
			}
		}
	}

	/*drawPathNode->clear();
	if (!path.parts.empty()) {
		for (auto iter = path.parts.begin(); iter != path.parts.end() - 1; ++iter) {
			EdgeModel& edge = edges[iter->second];

			auto& vertex1 = vertices[edge.vertices.first];
			vec1.set(vertex1.x, vertex1.y);

			auto& vertex2 = vertices[edge.vertices.second];
			vec2.set(vertex2.x, vertex2.y);

			if (edge.hugging) {
				auto& obstacle = obstacles[edge.obstacle];
				Vec2 center(obstacle.x, obstacle.y);
				Vec2 lel = vec2 - center;
				drawCircleSegment(
					drawPathNode,
					center,
					obstacle.radius,
					Util::convertAngleTo2Pi(lel.getAngle()),
					edge.weight,
					36,
					Color4F::ORANGE);
			}
			else {
				drawPathNode->drawLine(vec1, vec2, Color4F::ORANGE);
			}
			drawPathNode->drawDot(vec1, 10.0f, Color4F::BLUE);
			drawPathNode->drawDot(vec2, 10.0f, Color4F::BLUE);
		}
	}*/

	//drawGraphNode->drawDot(startPoint, 5.0f, Color4F::BLUE);
	//drawGraphNode->drawDot(finishPoint, 5.0f, Color4F::BLUE);

}

void ForestDrawerNode::setForest(std::shared_ptr<circular_obstacle_pathfinding::Forest> forest)
{
	this->forest = forest;
}

void ForestDrawerNode::setGraph(std::shared_ptr<circular_obstacle_pathfinding::Graph> graph)
{
	this->graph = graph;
}

void ForestDrawerNode::setAddtitionalWidth(float additionalWidth) {
	this->additionalWidth = additionalWidth;
	needToReinitGraph = true;
}

void ForestDrawerNode::drawCircleSegment(cocos2d::DrawNode* drawNode, const Vec2& center, float radius, float angleFrom, float length, int segments, const Color4F& color) {
	float angle = length / radius;
	if (angle < FLT_EPSILON) {
		return;
	}
	float unitStep = CC_DEGREES_TO_RADIANS(2 * M_PI * radius / segments);
	int realSegments = angle / unitStep;
	if (angle / unitStep - realSegments > FLT_EPSILON) realSegments++;
	if (realSegments == 0) {
		return;
	}
	if (realSegments == 1) {
		++realSegments;
	}
	unitStep = angle / realSegments;
	int pointNum = realSegments + 1;
	float rads = angleFrom;
	float x = center.x + radius * cosf(rads);
	float y = center.y + radius * sinf(rads);
	Vec2 s(x, y);
	for (int index = 1; index < pointNum; ++index) {
		rads = unitStep * index + angleFrom;
		x = center.x + radius * cosf(rads);
		y = center.y + radius * sinf(rads);
		Vec2 f(x, y);
		drawNode->drawLine(s, f, color);
		s = f;
	}
}

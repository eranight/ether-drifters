#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class EngineConfig {
	public:
		float maxVelocity;
		int num;
		float offset;
	};

	class Engine : public cocos2d::Node {
	private:
		Engine(const EngineConfig& config) : config(config) {}
	public:
		~Engine();
		static Engine* create(const EngineConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		cocos2d::Sprite* createFireSprite();
		void processChangeVelocity(cocos2d::EventCustom* event);
		void processStartAfterburner(cocos2d::EventCustom* event);
		void processStopAfterburner(cocos2d::EventCustom* event);
	private:
		EngineConfig config;
		cocos2d::Sprite* leftFire = nullptr;
		cocos2d::Sprite* rightFire = nullptr;
		bool isRunning;
		cocos2d::Vec2 prevPosition;
		cocos2d::Action* maxVelocityAction = nullptr;
		cocos2d::EventListenerCustom* movementEventListener = nullptr;
		cocos2d::EventListenerCustom* startAfterburnerEventListener = nullptr;
		cocos2d::EventListenerCustom* stopAfterburnerEventListener = nullptr;
	};

}
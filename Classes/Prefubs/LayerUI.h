#pragma once

#include "cocos2d.h"

namespace ether_drifters {

	class LayerUI : public cocos2d::Layer {
	public:
		CREATE_FUNC(LayerUI);
		virtual ~LayerUI();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		cocos2d::Size visibleSize;
		cocos2d::Vec2 origin;
		cocos2d::Vec2 center;

		cocos2d::Menu* menu;

		cocos2d::EventListenerPhysicsContact* physicsEventListener;
	};

}
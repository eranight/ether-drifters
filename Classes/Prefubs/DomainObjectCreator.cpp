#include "DomainObjectCreator.h"
#include "Utils/JsonConvertersUtil.h"
#include "Domain.h"
#include "Portal.h"
#include "Meteor.h"
#include "Skeleton.h"
#include "SimpleMonster.h"

using namespace cocos2d;
using namespace ether_drifters;

Domain* DomainObjectCreator::createDomain(const rapidjson::Value& val) {
	DomainConfig config;
	config.radius = val["radius"].GetFloat();

	Domain* domain = Domain::create(config);
	return domain;
}

Portal* DomainObjectCreator::createPortal(const rapidjson::Value& val) {
	PortalConfig config;
	config.destClasterId = val["path"]["cluster_id"].GetString();
	config.destDomainId  = val["path"]["domain_id"].GetString();
	config.outPortalId   = val["path"]["out_portal_id"].GetString();
	config.color = JsonConvertersUtil::convertColor(val["color"]);

	Portal* portal = Portal::create(config);
	return portal;
}

Meteor* DomainObjectCreator::createMeteor(const rapidjson::Value& val) {
	MeteorConfig config;
	if (val.HasMember("sprite_frame")) {
		config.spriteFrame = val["sprite_frame"].GetString();
	}
	else {
		config.spriteFrame = "meteorBrown_big3";
	}
	if (val.HasMember("direction")) {
		config.direction = JsonConvertersUtil::convertCoordinates(val["direction"]);
		config.randomizeDirection = false;
	}
	else {
		config.randomizeDirection = true;
	}
	config.resourcePoints = val["resource_points"].GetInt();
	config.strengthPoints = config.resourcePoints;
	config.size = MeteorSize::l_size;
	Meteor* meteor = Meteor::create(config);
	return meteor;
}

Skeleton* DomainObjectCreator::createSkeleton(const rapidjson::Value& val) {
	Skeleton* skeleton = Skeleton::create();
	return skeleton;
}

SimpleMonster* DomainObjectCreator::createSimpleMonster(const rapidjson::Value& val) {
	SimpleMonster* simpleMonster = SimpleMonster::create();
	return simpleMonster;
}
#pragma once

#include "cocos2d.h"

namespace ether_drifters
{

	class ViewNode;
	class Direction;
	class DamageDealer;

	class LaserConfig {
	public:
		float width;
		float damage;
		DamageDealer* damageDealer;
	};

	class Laser : public cocos2d::Node {
	private:
		Laser(const LaserConfig& config) : config(config) {}
	public:
		static Laser* create(const LaserConfig& config);
		bool init() override;
	public:
		float getLength() { return length; }
		void setLength(float length);
	private:
		LaserConfig config;
		ViewNode* view = nullptr;
		cocos2d::Node* startLaserPart = nullptr;
		cocos2d::Node* middleLaserPart = nullptr;
		cocos2d::Node* endLaserPart = nullptr;
		Direction* direction = nullptr;
		float length = 1.0f;
	};

}
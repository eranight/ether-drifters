#pragma once

#include "cocos2d.h"
#include "Utils/CircularObstaclePathfinding/Forest.h"

class ForestDrawerNode : public cocos2d::Node {
public:
	CREATE_FUNC(ForestDrawerNode);
public:
	bool init() override;
	void update(float dt) override;
	void setForest(std::shared_ptr<circular_obstacle_pathfinding::Forest> forest);
	void setGraph(std::shared_ptr<circular_obstacle_pathfinding::Graph> graph);
	void enableVerticesDrawing(bool enable) { needDrawVertices = enable; }
	void enableSurfingEdgesDrawing(bool enable) { needDrawSurfingEdges = enable; }
	void enableHuggingEdgesDrawing(bool enable) { needDrawHuggingEdges = enable; }
	void setAddtitionalWidth(float additionalWidth);
private:
	void drawCircleSegment(cocos2d::DrawNode* drawNode, const cocos2d::Vec2& center, float radius, float angleFrom, float length, int segments, const cocos2d::Color4F& color);
private:
	std::shared_ptr<circular_obstacle_pathfinding::Forest> forest;
	std::shared_ptr<circular_obstacle_pathfinding::Graph> graph;
	cocos2d::DrawNode* drawObstaclesNode;
	cocos2d::DrawNode* drawGraphNode;
	cocos2d::DrawNode* drawPathNode;
	bool needDrawVertices;
	bool needDrawSurfingEdges;
	bool needDrawHuggingEdges;
	float additionalWidth;
	bool needToReinitGraph;
};
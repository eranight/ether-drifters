#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class ResourceBar;

	class Skeleton : public cocos2d::Node {
	public:
		CREATE_FUNC(Skeleton);
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		ResourceBar* resourceBar;
	};

}
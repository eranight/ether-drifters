#include "UncertaintyVortex.h"
#include "Common/ViewNode.hpp"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/FocusEventListener.h"
#include "Events/TrackedEventData.h"
#include "Events/InputEventData.h"
#include "Prefubs/Cannon.hpp"
#include "Components/PlayerController.hpp"
#include "Components/Tracked.h"
#include "Components/Obstacle.h"

using namespace cocos2d;
using namespace ether_drifters;

UncertaintyVortex* UncertaintyVortex::create(const UncertaintyVortexConfig& config) {
	auto ret = new (std::nothrow) UncertaintyVortex(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

UncertaintyVortex::~UncertaintyVortex()
{
	CC_SAFE_RELEASE_NULL(trackedEventListener);
}

bool UncertaintyVortex::init()
{
	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(config.radius, config.radius);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	view->runAction(RepeatForever::create(RotateBy::create(15.0f, 360.0f)));
	addChild(view);

	trackedEventListener = TrackedEventListener::create(this);
	trackedEventListener->retain();

	auto sprite = Sprite::create("vortex1.png");
	HelperMethodsUtil::adjustScaleToSize(sprite, contentSize * 2.0f);
	/*sprite->setScaleX(config.radius * 2.0f / sprite->getContentSize().width);
	sprite->setScaleY(config.radius * 2.0f / sprite->getContentSize().height);*/
	view->addChild(sprite);

	auto center = contentSize * 0.5f;
	addComponent(Obstacle::create(Obstacle::PATH_OBSTACLE | Obstacle::FIRE_OBSTACLE,
		{ { Vec2::ZERO, config.affectingRadius } }));

	auto visionPhysicsBody = PhysicsBody::create();
	visionPhysicsBody->addShape(HelperMethodsUtil::createVisionPhysicsShape(Vec2::ZERO, 72, 360.0f, config.affectingRadius));
	visionPhysicsBody->setDynamic(false);
	visionPhysicsBody->setCategoryBitmask(ContactMask::tracking);
	visionPhysicsBody->setContactTestBitmask(ContactMask::tracked);
	Node* vision = Node::create();
	vision->setPhysicsBody(visionPhysicsBody);
	addChild(vision, 0, VISION);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);

	return true;
}

void UncertaintyVortex::onEnter()
{
	Node::onEnter();

	trackedEventListener->onFocusedEvent = CC_CALLBACK_1(UncertaintyVortex::focus, this);
	trackedEventListener->onUnfocusedEvent = CC_CALLBACK_1(UncertaintyVortex::unfocus, this);
}

void UncertaintyVortex::onExit()
{
	Node::onExit();

	trackedEventListener->onFocusedEvent = nullptr;
	trackedEventListener->onUnfocusedEvent = nullptr;
}

void UncertaintyVortex::update(float dt)
{
	Node::update(dt);
}

void UncertaintyVortex::focus(TrackedEventData* eventData)
{
	PlayerController* component = HelperMethodsUtil::findComponent<PlayerController>(eventData->getTracked()->getOwner());
	if (component != nullptr) {
		Modification<std::string, PlayerController*>* modification =
			Modification<std::string, PlayerController*>::create(CC_CALLBACK_3(UncertaintyVortex::modify, this));
		component->getInputActioModifiable()->addModification(modification);
		activeModifications.insert((unsigned)&*component, modification);
		component->resetRotation();
	}
}

void UncertaintyVortex::unfocus(TrackedEventData* eventData)
{
	PlayerController* component = HelperMethodsUtil::findComponent<PlayerController>(eventData->getTracked()->getOwner());
	if (component != nullptr) {
		Modification<std::string, PlayerController*>* modification = activeModifications.at((unsigned)&*component);
		if (modification != nullptr) {
			component->getInputActioModifiable()->removeModification(modification);
			activeModifications.erase((unsigned)&*component);
		}
		component->resetRotation();
	}
}

std::string UncertaintyVortex::modify(std::string src, std::string res, PlayerController* component)
{
	if (src == INPUT_RIGHT) {
		return INPUT_LEFT;
	}
	else if (src == INPUT_LEFT) {
		return INPUT_RIGHT;
	}
	return src;
}

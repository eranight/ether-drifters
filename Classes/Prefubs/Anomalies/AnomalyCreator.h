#pragma once
#include "json/document.h"
#include "BlackHole.h"
#include "UncertaintyVortex.h"

#include <map>
#include <functional>

namespace ether_drifters {

	class AnomalyCreator {
	private:
		AnomalyCreator();
	public:
		static cocos2d::Node* create(const rapidjson::Value& val);
	private:
		static void enrichWithComponents(cocos2d::Node* node, const rapidjson::Value& val);
		BlackHole* createBlackHole(const rapidjson::Value& val);
		UncertaintyVortex* createUncertaintyVortex(const rapidjson::Value& val);
	private:
		static AnomalyCreator anomalyCreator;
		std::unordered_map<std::string, std::function<cocos2d::Node* (const rapidjson::Value&)>> mapper;
	};

}

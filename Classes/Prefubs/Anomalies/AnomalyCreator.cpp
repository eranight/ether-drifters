#include "AnomalyCreator.h"
#include "Components/ComponentCreator.h"

using namespace cocos2d;
using namespace rapidjson;
using namespace ether_drifters;

AnomalyCreator AnomalyCreator::anomalyCreator;

AnomalyCreator::AnomalyCreator() {
	mapper["black_hole"] = std::bind(&AnomalyCreator::createBlackHole, this, std::placeholders::_1);
	mapper["uncertainty_vortex"] = std::bind(&AnomalyCreator::createUncertaintyVortex, this, std::placeholders::_1);
}

Node* AnomalyCreator::create(const rapidjson::Value& val) {
	CCASSERT(val.HasMember("type"), "jsonNode must has type member");
	std::string type = val["type"].GetString();
	auto funcIter = anomalyCreator.mapper.find(type);
	if (funcIter != anomalyCreator.mapper.end()) {
		return funcIter->second(val);
	}
	CCLOG("anomaly type %s not found", type);
	return nullptr;
}

void AnomalyCreator::enrichWithComponents(Node* node, const rapidjson::Value& val) {
	if (val.HasMember("components")) {
		auto components = val["components"].GetArray();
		for (int index = 0; index < components.Size(); ++index) {
			Component* component = ComponentCreator::create(components[index]);
			if (component != nullptr) {
				node->addComponent(component);
			}
		}
	}
}

BlackHole* AnomalyCreator::createBlackHole(const rapidjson::Value& val) {
	BlackHoleConfig config;
	config.affectingRadius = val["affecting_radius"].GetFloat();
	config.gravity = val["gravity"].GetFloat();
	config.eventHorizonRadius = val["event_horizon_radius"].GetFloat();
	config.eventHorizonVelocityAffecting = val["event_horizon_velocity_affecting"].GetFloat();

	auto blackHole = BlackHole::create(config);
	enrichWithComponents(blackHole, val);
	return blackHole;
}

UncertaintyVortex* AnomalyCreator::createUncertaintyVortex(const rapidjson::Value& val) {
	UncertaintyVortexConfig config;
	config.radius = val["radius"].GetFloat();
	config.affectingRadius = val["affecting_radius"].GetFloat();

	auto uncertaintyVortex = UncertaintyVortex::create(config);
	enrichWithComponents(uncertaintyVortex, val);
	return uncertaintyVortex;
}
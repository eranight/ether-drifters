#include "BlackHole.h"
#include "Common/ViewNode.hpp"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Tracking.h"
#include "Components/Tracked.h"
#include "Components/Damage.h"
#include "Components/Health.h"
#include "Components/Obstacle.h"
#include "Events/FocusEventListener.h"
#include "Events/TrackedEventData.h"
#include "Modification/Modifiable.h"

using namespace cocos2d;
using namespace ether_drifters;

BlackHole::~BlackHole() {
	CC_SAFE_RELEASE_NULL(trackedEventListener);
}

BlackHole* BlackHole::create(const BlackHoleConfig& config) {
	auto ref = new (std::nothrow) BlackHole(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool BlackHole::init() {
	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(config.affectingRadius, config.affectingRadius);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);


	auto blackHoleSprite = Sprite::create("black_hole.png");
	HelperMethodsUtil::adjustScaleToSize(blackHoleSprite, Size(config.eventHorizonRadius, config.eventHorizonRadius) * 2.0f);

	auto ps = ParticleGalaxy::create();
	ps->setEmitterMode(ParticleSystem::Mode::RADIUS);
	ps->setStartRadius(config.eventHorizonRadius * 0.7f);
	ps->setEndRadius(config.affectingRadius);
	ps->setEndRadiusVar(config.affectingRadius * 0.1f);
	ps->setRotatePerSecond(40.0f);
	ps->setRotatePerSecondVar(20.0f);
	ps->setTotalParticles(1000.0f);
	ps->setStartColor(Color4F(0.76f, 0.25f, 0.12f, 1.0f));
	ps->setEndSize(20.0f);
	ps->setPosition(Vec2::ZERO);

	view->addChild(ps);
	view->addChild(blackHoleSprite);

	auto viewPhysicsBody = PhysicsBody::createCircle(config.eventHorizonRadius);
	viewPhysicsBody->setDynamic(false);
	viewPhysicsBody->setCategoryBitmask(ContactMask::hit);
	viewPhysicsBody->setContactTestBitmask(ContactMask::body);
	view->setPhysicsBody(viewPhysicsBody);

	trackedEventListener = TrackedEventListener::create(this);
	trackedEventListener->retain();

	addComponent(Obstacle::create(Obstacle::PATH_OBSTACLE | Obstacle::FIRE_OBSTACLE, { { Vec2::ZERO, config.affectingRadius } }));

	auto visionPhysicsBody = PhysicsBody::create();
	visionPhysicsBody->addShape(HelperMethodsUtil::createVisionPhysicsShape(Vec2::ZERO, 72, 360.0f, config.affectingRadius));
	visionPhysicsBody->setDynamic(false);
	visionPhysicsBody->setCategoryBitmask(ContactMask::tracking);
	visionPhysicsBody->setContactTestBitmask(ContactMask::tracked);
	Node* vision = Node::create();
	vision->setPhysicsBody(visionPhysicsBody);
	addChild(vision, 0, VISION);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void BlackHole::onEnter() {
	Node::onEnter();

	trackedEventListener->onFocusedEvent = CC_CALLBACK_1(BlackHole::focus, this);
	trackedEventListener->onUnfocusedEvent = CC_CALLBACK_1(BlackHole::unfocus, this);
}

void BlackHole::onExit() {
	Node::onExit();

	trackedEventListener->onFocusedEvent = nullptr;
	trackedEventListener->onUnfocusedEvent = nullptr;
	trackedEventListener->onInterruptedEvent = nullptr;
}

void BlackHole::focus(TrackedEventData* eventData) {
	Movement* movement = HelperMethodsUtil::findComponent<Movement>(eventData->getTracked()->getOwner());
	if (movement != nullptr) {
		Modification<Vec2, Movement*>* modification =
			Modification<Vec2, Movement*>::create(CC_CALLBACK_3(BlackHole::modify, this));
		movement->getVelocityVectorModifiable()->addModification(modification);
		movementOnModificationMapper.insert((unsigned)&*movement, modification);
	}
}

void BlackHole::unfocus(TrackedEventData* eventData) {
	Movement* movement = HelperMethodsUtil::findComponent<Movement>(eventData->getTracked()->getOwner());
	if (movement != nullptr) {
		Modification<Vec2, Movement*>* modification = movementOnModificationMapper.at((unsigned)&*movement);
		if (modification != nullptr) {
			movement->getVelocityVectorModifiable()->removeModification(modification);
			movementOnModificationMapper.erase((unsigned)&*movement);
		}
	}
}

Vec2 BlackHole::modify(Vec2 src, Vec2 res, Movement* movement) {
	Node* trackedNode = movement->getOwner();
	Vec2 distanceVector = getPosition() - trackedNode->getPosition();
	float len = distanceVector.length();
	if (len > config.eventHorizonRadius) {
		Vec2 a = distanceVector.getNormalized() * config.gravity;
		Vec2 l = res + a;
		return res.lerp(l, 1.0f);
	}
	else {
		return res * config.eventHorizonVelocityAffecting;
	}
}
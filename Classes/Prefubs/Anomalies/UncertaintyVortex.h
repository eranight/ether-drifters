#pragma once

#include "cocos2d.h"
#include "Modification/Modification.h"

namespace ether_drifters {

	class TrackedEventData;
	class TrackedEventListener;
	class PlayerController;

	class UncertaintyVortexConfig {
	public:
		float radius;
		float affectingRadius;
	};

	class UncertaintyVortex : public cocos2d::Node {
	private:
		UncertaintyVortex(const UncertaintyVortexConfig& config) : config(config) {}
	public:
		static UncertaintyVortex* create(const UncertaintyVortexConfig& config);
		~UncertaintyVortex();
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	private:
		void focus(TrackedEventData* eventData);
		void unfocus(TrackedEventData* eventData);
		std::string modify(std::string src, std::string res, PlayerController* component);
	private:
		UncertaintyVortexConfig config;
		TrackedEventListener* trackedEventListener = nullptr;

		cocos2d::Map<unsigned, Modification<std::string, PlayerController*>*> activeModifications;
	};

}
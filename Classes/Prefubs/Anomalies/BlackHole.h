#pragma once
#include "cocos2d.h"
#include <memory>
#include <map>
#include "Modification/Modification.h"
#include "Components/Movement.h"

namespace ether_drifters {

	class TrackedEventData;
	class TrackedEventListener;
	class Movement;

	class BlackHoleConfig {
	public:
		float affectingRadius;
		float gravity;
		float eventHorizonRadius;
		float eventHorizonVelocityAffecting;
	};

	class BlackHole : public cocos2d::Node {
	public:
		BlackHole(const BlackHoleConfig& config) : config(config), trackedEventListener(nullptr) {}
		virtual ~BlackHole();
	public:
		static BlackHole* create(const BlackHoleConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		void focus(TrackedEventData* eventData);
		void unfocus(TrackedEventData* eventData);
		cocos2d::Vec2 modify(cocos2d::Vec2 src, cocos2d::Vec2 res, Movement* movement);
	private:
		BlackHoleConfig config;
		TrackedEventListener* trackedEventListener;
		cocos2d::Map<unsigned, Modification<cocos2d::Vec2, Movement*>*> movementOnModificationMapper;
	};

}
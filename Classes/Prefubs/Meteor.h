#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	enum class MeteorSize {
		s_size,
		m_size,
		l_size
	};

	class MeteorConfig {
	public:
		std::string spriteFrame;
		bool randomizeDirection;
		cocos2d::Vec2 direction;
		MeteorSize size;
		int resourcePoints;
		double strengthPoints;
	};

	class ResourceBar;

	class Meteor : public cocos2d::Node {
	protected:
		Meteor(const MeteorConfig& config) : config(config) {}
	public:
		static Meteor* create(const MeteorConfig& config);
		~Meteor();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		void destroy();
		void createPieces();
	private:
		MeteorConfig config;
		ResourceBar* resourceBar;
	};

}
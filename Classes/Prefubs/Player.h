#ifndef __SHIP_H__
#define __SHIP_H__

#include "cocos2d.h"

namespace ether_drifters {

	class PlayerConfig {
	public:
		std::pair<double, double> hp;
		double repairPower;
		float repairInterval;
		float repairDelay;
		float velocity;
		float acceleration;
		float rotation;
		double damage;
		float cooldown;
		float lifeTime;
		float shellVelocity;
		std::string shellViewFilename;
		float visionAreaRadius;
		float miningRate;
		double miningPower;
		float miningAreaRadius;
		std::pair<int, int> crew;
		float crewRepairBonus;
		float crewCanonBonus;
		float crewMiningBonus;
		std::map<double, double> crewLooseChances;
	};

	class HealthBar;

	class Player : public cocos2d::Node {
	protected:
		Player(const PlayerConfig& config) : config(config) {}
	public:
		static Player* create(const PlayerConfig& config);
		~Player();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		PlayerConfig config;
		HealthBar* healthBar;
	};

}

#endif //__SHIP_H__
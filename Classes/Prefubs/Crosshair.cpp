#include "Crosshair.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

using namespace cocos2d;
using namespace ether_drifters;

const std::string Crosshair::NAME = "crosshair";

Crosshair* Crosshair::create(const CrosshairConfig& config) {
	auto ref = new (std::nothrow) Crosshair(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Crosshair::init() {
	if (!Node::init()) {
		return false;
	}
	auto crosshair = Sprite::create("crosshair.png");
	crosshair->setColor(config.color);
	crosshair->runAction(
		RepeatForever::create(RotateBy::create(5.0f, 360.0f))
	);
	addChild(crosshair, 0, VIEW);

	setCameraMask(DOMAIN_UI_CAMERA_MASK);

	setName(NAME);
	return true;
}

void Crosshair::setColor(const Color3B& color)
{
	Node::setColor(color);
	HelperMethodsUtil::getViewIfExists(this)->setColor(color);
}

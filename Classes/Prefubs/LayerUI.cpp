#include "LayerUI.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/Mined.h"
#include "Events/MinedEventData.h"
#include "Ether.h"

using namespace cocos2d;
using namespace ether_drifters;

LayerUI::~LayerUI() {
	physicsEventListener->release();
}

bool LayerUI::init() {
	Layer::init();

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	center = origin + visibleSize * 0.5f;

	menu = Menu::create();
	menu->setPosition(center - Vec2(0, 84.0f));
	addChild(menu);

	physicsEventListener = EventListenerPhysicsContact::create();
	physicsEventListener->retain();
	physicsEventListener->onContactBegin = [this](PhysicsContact & contact) {
		auto nodes = HelperMethodsUtil::fetchNodesWithNames(contact, "player", "domain");
		if (nodes.empty()) {
			return false;
		}
		
		Sprite* dialog = Sprite::createWithSpriteFrameName("dialog_normal");
		Sprite* emote = Sprite::createWithSpriteFrameName("emote_exclamation");
		SpriteBatchNode * batch = SpriteBatchNode::create("Spritesheets/emotes_sheet.png");
		batch->addChild(dialog);
		batch->addChild(emote);
		batch->setCameraMask(getCameraMask());
		batch->setPosition(center + Vec2(0, 84.0f));
		addChild(batch, 0, "warning");

		return true;
	};
	physicsEventListener->onContactSeparate = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesWithNames(contact, "player", "domain");
		if (nodes.empty()) {
			return;
		}
		removeChildByName("warning");
	};

	return true;

}

static int l = 0;
static Map<std::string, Mined*> minedMap;
static std::string currentMiningName;

void LayerUI::onEnter() {
	Layer::onEnter();
	menu->setCameraMask(getCameraMask());
	_eventDispatcher->addCustomEventListener(FOCUS_MINED_EVENT, [this](EventCustom* event) {
		MinedEventData* minedEventData = reinterpret_cast<MinedEventData*>(event->getUserData());
		if (minedEventData == nullptr) {
			CCLOG("received object is null");
			return;
		}

		MenuItemImage* button = MenuItemImage::create();

		Sprite* dialogNormal = Sprite::createWithSpriteFrameName("dialog_normal");
		auto p = Sprite::createWithSpriteFrameName("pick");
		p->setPosition((Vec2)dialogNormal->getContentSize() * 0.5f + Vec2(0, 6));
		p->setRotation(180);
		dialogNormal->addChild(p);
		button->setNormalImage(dialogNormal);

		Sprite* dialogSelected = Sprite::createWithSpriteFrameName("dialog_selected");
		p = Sprite::createWithSpriteFrameName("pick");
		p->setPosition((Vec2)dialogSelected->getContentSize() * 0.5f + Vec2(0, 6));
		p->setRotation(180);
		dialogSelected->addChild(p);
		button->setSelectedImage(dialogSelected);

		Mined* mined = minedEventData->getMined();
		button->setCallback([mined, this](Ref* sender) {
			if (!currentMiningName.empty()) {
				MenuItemImage* startedChild = dynamic_cast<MenuItemImage*>(menu->getChildByName(currentMiningName));
				String* state = dynamic_cast<String*>(startedChild->getUserObject());
				state->_string = STOP_MINING_EVENT;
				MinedEventData eventData(Ether::getInstance()->getCurrentDomain()->getChildByName(PLAYER), mined);
				this->getEventDispatcher()->dispatchCustomEvent(state->_string, &eventData);
			}
			MenuItemImage* child = dynamic_cast<MenuItemImage*>(sender);
			String* state = dynamic_cast<String*>(child->getUserObject());
			MinedEventData eventData(Ether::getInstance()->getCurrentDomain()->getChildByName(PLAYER), mined);
			this->getEventDispatcher()->dispatchCustomEvent(state->_string, &eventData);
			state->_string = state->_string == START_MINING_EVENT ? STOP_MINING_EVENT : START_MINING_EVENT;
			if (state->_string == STOP_MINING_EVENT) {
				currentMiningName = child->getName();
			}
			else {
				currentMiningName = "";
			}
		});
		button->setCameraMask(getCameraMask());
		button->setRotation(-180);

		auto nm = StringUtils::format("%d", l++);
		button->setUserObject(String::create(START_MINING_EVENT));
		minedMap.insert(nm, mined);

		menu->addChild(button, 0, nm);
		menu->alignItemsHorizontally();
	});
	auto destroyPickButtonLambda = [this](EventCustom* event) {
		MinedEventData* minedEventData = reinterpret_cast<MinedEventData*>(event->getUserData());
		if (minedEventData == nullptr) {
			CCLOG("received object is null");
			return;
		}
		for (auto pair : minedMap) {
			if (pair.second == minedEventData->getMined()) {
				MenuItemImage* child = dynamic_cast<MenuItemImage*>(menu->getChildByName(pair.first));
				String* state = dynamic_cast<String*>(child->getUserObject());
				if (state->_string == STOP_MINING_EVENT) {
					state->_string = START_MINING_EVENT;
				}
				menu->removeChild(child, true);
				menu->alignItemsHorizontally();
				minedMap.erase(pair.first);
				break;
			}
		}
	};
	_eventDispatcher->addCustomEventListener(UNFOCUS_MINED_EVENT, destroyPickButtonLambda);
	_eventDispatcher->addCustomEventListener(INTERRUPT_MINING_EVENT, destroyPickButtonLambda);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(physicsEventListener, this);
}

void LayerUI::onExit() {
	Layer::onExit();

	_eventDispatcher->removeCustomEventListeners(FOCUS_MINED_EVENT);
	_eventDispatcher->removeCustomEventListeners(UNFOCUS_MINED_EVENT);
	_eventDispatcher->removeCustomEventListeners(INTERRUPT_MINING_EVENT);

	_eventDispatcher->removeEventListener(physicsEventListener);
}
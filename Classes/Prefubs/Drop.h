#pragma once
#include "cocos2d.h"
#include "Utils/Constants.h"

namespace ether_drifters {

	class ViewNode;

	class DropConfig {
	public:
		DropType type;
		int quantity;
		float physicsBodyDelay;
		float lifeTime;
		std::string spriteFrame;
	};

	class Drop : public cocos2d::Node {
	private:
		Drop(const DropConfig& config) : config(config) {}
	public:
		static Drop* create(const DropConfig& config);
		bool init() override;
	private:
		static cocos2d::Size getSizeForType(const DropType& type);
	private:
		DropConfig config;
	};

}
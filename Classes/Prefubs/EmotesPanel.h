#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class EmotesPanel : public cocos2d::Node {
	public:
		static EmotesPanel* create(std::map<std::string, std::pair<std::string, std::string>> eventNamesOnEmotes);
		bool init(std::map<std::string, std::pair<std::string, std::string>> eventNamesOnEmotes);
		void onEnter() override;
		void onExit() override;
	private:
		void alignItemsHorizontally();
		void alignItemsHorizontallyWithPadding(float padding);
	private:
		std::map<std::string, std::pair<std::string, std::string>> eventNamesOnEmotes;
		cocos2d::Vector<cocos2d::EventListenerCustom *> customEventListeners;
		cocos2d::Sprite* dialogSprite;
		cocos2d::Sprite* emoteSprite;
	};

}
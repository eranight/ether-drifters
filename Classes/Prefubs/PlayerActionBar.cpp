#include "PlayerActionBar.h"
#include "Events/MinedEventData.h"
#include "Events/TrackedEventData.h"
#include "Components/Tracked.h"
#include "Components/Health.h"
#include "Components/Mined.h"
#include "Utils/HelperMethodsUtil.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

PlayerActionBar::~PlayerActionBar() {
	CC_SAFE_RELEASE_NULL(minedEventListener);
	CC_SAFE_RELEASE_NULL(trackedEventListener);
}

bool PlayerActionBar::init() {
	if (!Node::init()) {
		return false;
	}

	contentWidth = SpriteFrameCache::getInstance()->getSpriteFrameByName("dialog_normal")->getOriginalSize().width;

	/*menu = Menu::create();
	if (menu == nullptr) {
		return false;
	}
	menu->setPosition(Vec2::ZERO);
	addChild(menu, 0, "menu");*/

	minedEventListener = MinedEventListener::create(this, false);
	minedEventListener->onFocusedEvent = [this](MinedEventData* eventData) {
		Mined* mined = eventData->getMined();
		MenuItem* item = createMenuItem("pick");
		item->setCallback([this, mined](Ref* sender) {
			MinedEventData eventData(getParent(), mined);
			_eventDispatcher->dispatchCustomEvent(START_MINING_EVENT, &eventData);
			dynamic_cast<MenuItem*>(sender)->setEnabled(false);
			});
		Menu* menu = getNodeMenu(mined->getOwner());
		menu->addChild(item, 0, StringUtils::format("mined_0x%x", mined));
		align(menu);
	};
	minedEventListener->onUnfocusedEvent = [this](MinedEventData* eventData) {
		Mined* mined = eventData->getMined();
		Menu* menu = getNodeMenu(mined->getOwner());
		menu->removeChildByName(StringUtils::format("mined_0x%x", mined));
		align(menu);
	};
	minedEventListener->onInterruptedEvent = [this](InterruptMiningEventData* eventData) {
		Mined* mined = eventData->getMined();
		Menu* menu = getNodeMenu(mined->getOwner());
		if (eventData->getReason() == InterruptMiningReason::switched) {
			dynamic_cast<MenuItem*>(menu->getChildByName(StringUtils::format("mined_0x%x", mined)))->setEnabled(true);
		}
		else {
			menu->removeChildByName(StringUtils::format("mined_0x%x", mined));
			align(menu);
		}
	};
	minedEventListener->retain();

	trackedEventListener = TrackedEventListener::create(this, false);
	//trackedEventListener->onFocusedEvent = [this](TrackedEventData* eventData) {
	//	Tracked* tracked = eventData->getTracked();
	//	Health* health = HelperMethodsUtil::findComponent<Health>(tracked->getOwner());
	//	if (health == nullptr) {
	//		return;
	//	}
	//	MenuItem* item = createMenuItem("emote_cross");
	//	item->setCallback([this, tracked](Ref* sender) {
	//		TrackedEventData eventData(getParent(), tracked);
	//		_eventDispatcher->dispatchCustomEvent(START_TRACKING_EVENT, &eventData);
	//		DamageDealerEventData ddEventData(getParent(), tracked->getOwner()->getPosition());
	//		_eventDispatcher->dispatchCustomEvent(PRODUCE_ATTACK_EVENT, &ddEventData);
	//		MenuItem* mitem = dynamic_cast<MenuItem*>(sender);
	//		/*mitem->setEnabled(false);
	//		mitem->runAction(
	//			Sequence::createWithTwoActions(
	//				DelayTime::create(2.0f),
	//				CallFunc::create([mitem]() {mitem->setEnabled(true); })
	//			)
	//		);*/
	//		});
	//	Menu* menu = getNodeMenu(tracked->getOwner());
	//	menu->addChild(item, 0, StringUtils::format("tracked_0x%x", tracked));
	//	align(menu);
	//};
	//trackedEventListener->onUnfocusedEvent = [this](TrackedEventData* eventData) {
	//	Tracked* tracked = eventData->getTracked();
	//	Health* health = HelperMethodsUtil::findComponent<Health>(tracked->getOwner());
	//	if (health == nullptr) {
	//		return;
	//	}
	//	Menu* menu = getNodeMenu(tracked->getOwner());
	//	menu->removeChildByName(StringUtils::format("tracked_0x%x", tracked));
	//	align(menu);
	//};
	//trackedEventListener->onInterruptedEvent = [this](InterruptTrackingEventData* eventData) {
	//	Tracked* tracked = eventData->getTracked();
	//	if (eventData->getReason() != InterruptTrackingReason::switched) {
	//		Menu* menu = getNodeMenu(tracked->getOwner());
	//		menu->removeChildByName(StringUtils::format("tracked_0x%x", tracked));
	//		align(menu);
	//	}
	//};
	trackedEventListener->retain();

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

Menu* PlayerActionBar::getNodeMenu(Node* node) {
	Menu* menu = dynamic_cast<Menu*>(node->getChildByName(NODE_MENU));
	if (menu == nullptr) {
		menu = Menu::create();
		Size contentSize = HelperMethodsUtil::getContentSize(node);
		float y = -std::max(contentSize.width, contentSize.height) * 0.5f - 19.0f;
		menu->setPosition(Vec2(0.0f, y));
		menu->setCameraMask(DOMAIN_UI_CAMERA_MASK);
		node->addChild(menu, 0, NODE_MENU);
	}
	return menu;
}

MenuItem* PlayerActionBar::createMenuItem(const std::string& frameName) {
	MenuItemImage* button = MenuItemImage::create();
	button->setNormalImage(getDialog("dialog_normal", frameName));
	button->setSelectedImage(getDialog("dialog_selected", frameName));
	button->setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return button;
}

Node* PlayerActionBar::getDialog(const std::string& dialogFrameName, const std::string& actionFrameName) {
	Sprite* dialogImage = Sprite::createWithSpriteFrameName(dialogFrameName);
	dialogImage->setRotation(180.0f);

	Sprite* actionImage = Sprite::createWithSpriteFrameName(actionFrameName);
	actionImage->setPosition(Vec2(0, -6));

	SpriteBatchNode* batchNode = SpriteBatchNode::create("Spritesheets/emotes_sheet.png");
	batchNode->addChild(dialogImage);
	batchNode->addChild(actionImage);
	batchNode->setContentSize(dialogImage->getContentSize());
	batchNode->setPosition((Vec2)batchNode->getContentSize() * 0.5f);
	return batchNode;
}

void PlayerActionBar::align(cocos2d::Menu* menu) {
	float maxPadding = contentWidth * 3.0f + 10.0f;
	float p = (maxPadding - contentWidth * menu->getChildrenCount()) / std::max(menu->getChildrenCount() - 1, 1L);
	float padding = std::min(p, 5.0f);
	menu->alignItemsHorizontallyWithPadding(padding);
}

#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class HaloConfig {
	public:
		float radius;
		int quantity;
		float velocity;
		cocos2d::Color3B color;
		float scale = 1.0f;
		int clockWise = 1;
	};

	class Halo : public cocos2d::Node {
	private:
		Halo(const HaloConfig& config) : config(config) {}
	public:
		~Halo();
		static Halo* create(const HaloConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		HaloConfig config;
		cocos2d::Action* rotateAction;
	};

}
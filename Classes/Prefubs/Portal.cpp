#include "Portal.h"
#include "Common/ViewNode.hpp"
#include "Components/Movement.h"
#include "Components/Touchable.h"
#include "Events/PortalEventData.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"

USING_NS_CC;
using namespace ether_drifters;

Portal* Portal::create(const PortalConfig& config) {
	auto ref = new (std::nothrow) Portal(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Portal::init() {
	if (!Node::init())
		return false;
	
	auto contentSize = Size(64.0f, 64.0f);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	auto sprite = Sprite::createWithSpriteFrameName("spiral.png");
	sprite->setColor(config.color);
	view->addChild(sprite);

	addComponent(Touchable::create(contentSize));

	physicsListener = EventListenerPhysicsContact::create();
	physicsListener->onContactBegin = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesByContactMasks(contact, ContactMask::portal, ContactMask::portable);
		if (nodes.empty() || nodes.at(0) != this) {
			return false;
		}
		else if (nodes.at(1)->getName() == "player") {
			candidates.pushBack(nodes.at(1));
			return true;
		}
		else {
			return false;
		}
	};

	auto physicsBody = PhysicsBody::createCircle(19.2f);
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::body | ContactMask::portal);
	physicsBody->setContactTestBitmask(ContactMask::portable | ContactMask::domain);
	setPhysicsBody(physicsBody);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void Portal::onEnter() {
	Node::onEnter();

	Node* view = getChildByName("view");
	view->runAction(RepeatForever::create(RotateBy::create(20.0f, 360.0f)));

	float p = view->getContentSize().width * 0.6f;
	destination = Vec2::ZERO - (getPosition().getNormalized() * p);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(physicsListener, this);
}

void Portal::onExit() {
	Node::onExit();
	
	_eventDispatcher->removeEventListenersForTarget(this);
}

void Portal::update(float dt) {
	Node::update(dt);
	if (!candidates.empty()) {
		for (Node* node : candidates) {
			teleport(node);
		}
		candidates.clear();
	}
}

void Portal::teleport(cocos2d::Node* playerNode) {
	PortalPath path(config.destClasterId, config.destDomainId, config.outPortalId);
	PortalEventData eventData(this, playerNode, path);
	_eventDispatcher->dispatchCustomEvent(PORTAL_EVENT, &eventData);
}

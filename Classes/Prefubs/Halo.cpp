#include "Halo.h"

using namespace cocos2d;
using namespace ether_drifters;

Halo::~Halo() {
	CC_SAFE_RELEASE_NULL(rotateAction);
	CCLOG("Halo destroyed");
}

Halo* Halo::create(const HaloConfig& config) {
	auto* ref = new (std::nothrow) Halo(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool Halo::init() {
	if (!Node::init()) {
		return false;
	}

	SpriteBatchNode* spriteBatch = SpriteBatchNode::create("Spritesheets/white_smoke.png", config.quantity);
	float deltaAngle = CC_DEGREES_TO_RADIANS(360.0f / config.quantity);
	for (int iter = 0; iter < config.quantity; ++iter) {
		int num = iter % 25;
		std::string name = StringUtils::format("whitePuff%02d", num);
		Sprite* smoke = Sprite::createWithSpriteFrameName(name);
		smoke->setScale(config.scale);
		smoke->setColor(config.color);
		float angle = deltaAngle * iter;
		Vec2 position = Vec2(config.radius * cos(angle), config.radius * sin(angle));
		smoke->setPosition(position);
		spriteBatch->addChild(smoke);
	}
	addChild(spriteBatch);

	rotateAction = RepeatForever::create(RotateBy::create(360.0f / config.velocity, config.clockWise * 360.0f));
	rotateAction->retain();

	return true;
}

void Halo::onEnter() {
	Node::onEnter();

	runAction(rotateAction);
}

void Halo::onExit() {
	Node::onExit();

	stopAction(rotateAction);
}
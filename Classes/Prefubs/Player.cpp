#include "Player.h"
#include "Common/ViewNode.hpp"
#include"Components/Direction.h"
#include "Components/Movement.h"
#include "Components/PlayerController.hpp"
#include "Components/Mining.h"
#include "Components/Mined.h"
#include "Components/Tracked.h"
#include "Components/Tracking.h"
#include "Components/Health.h"
#include "Components/DamageDealer.h"
#include "Components/Picker.h"
#include "Components/Regeneration.h"
#include "Components/Crew.h"
#include "Components/DropSupplier.h"
#include "Components/Touchable.h"
#include "Components/TargetingShellController.hpp"
#include "Components/Skills/SharpReversalSkill.hpp"
#include "Components/Skills/AfterburnerSkill.hpp"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "EmotesPanel.h"
#include "PlayerActionBar.h"
#include "Events/DomainEventData.h"
#include "Events/CrewEventData.h"
#include "HealthBar.h"
#include "StatInformer.h"
#include "Engine.h"
#include "DebugDrawNode.h"
#include "BoardsIndicator.hpp"
#include "Cannon.hpp"
#include "Skills/SingleSalvo.hpp"
#include "Skills/Buckshot.hpp"
#include "Projectiles/CannonBall.hpp"
#include "json/document.h"
#include "Scripts/MachineGunSkillScript.hpp"
#include "Scripts/BeamSkillScript.hpp"

using namespace cocos2d;
using namespace ether_drifters;
using namespace rapidjson;

Player* Player::create(const PlayerConfig& config) {
	auto ref = new (std::nothrow) Player(config);
	if (ref != nullptr && ref->init()) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

Player::~Player() {
	CCLOG("player destroyed");
}

bool Player::init() {
	if (!Node::init()) {
		return false;
	}

	auto contentSize = Size(113.0f, 66.0f);

	auto view = ViewNode::create();
	view->getUserObject()->setContentSize(contentSize);
	addChild(view);

	auto hull = Sprite::createWithSpriteFrameName("ship (6).png");
	hull->setRotation(-90.0f);
	view->addChild(hull, 1);

	EngineConfig engineConfig;
	engineConfig.maxVelocity = this->config.velocity;
	engineConfig.num = 2;
	engineConfig.offset = contentSize.height * 0.1f;
	auto engine = Engine::create(engineConfig);
	engine->setRotation(90.0f);
	engine->setPosition(Vec2(-contentSize.width * 0.4f, 0.0f));
	view->addChild(engine, 0);
	auto physicsBody = PhysicsBody::createBox(Size(contentSize.width * 0.8f, contentSize.height * 0.4f));
	physicsBody->setDynamic(false);
	physicsBody->setCategoryBitmask(ContactMask::body | ContactMask::portable | ContactMask::tracked | ContactMask::picker);
	physicsBody->setContactTestBitmask(ContactMask::domain | ContactMask::portal | ContactMask::tracking | ContactMask::hit | ContactMask::drop);
	view->setPhysicsBody(physicsBody);

	CannonConfig cannonConfig;
	cannonConfig.visionAngle = 170.0f;
	cannonConfig.cooldown = 1.2f;
	cannonConfig.debug = false;
	auto leftCannon = Cannon::create(cannonConfig);
	leftCannon->setPosition(Vec2(0.0f, contentSize.height * 0.6f));
	leftCannon->setRotation(-90.0f);
	view->addChild(leftCannon, 3);

	auto rightCannon = Cannon::create(cannonConfig);
	rightCannon->setPosition(Vec2(0.0f, -contentSize.height * 0.6f));
	rightCannon->setRotation(90.0f);
	view->addChild(rightCannon, 3);

	CannonConfig machineGunCannonConfig;
	machineGunCannonConfig.cooldown = 2.0f;
	machineGunCannonConfig.visionAngle = 270.0f;
	machineGunCannonConfig.debug = false;
	auto machineGunCannon = Cannon::create(machineGunCannonConfig);
	machineGunCannon->setPosition(Vec2(contentSize.width * 0.25f, 0.0f));
	view->addChild(machineGunCannon, 3);

	CannonConfig beamCannonConfig;
	beamCannonConfig.cooldown = 2.0f;
	beamCannonConfig.visionAngle = 0.0f;
	beamCannonConfig.debug = false;
	auto beamCannon = Cannon::create(beamCannonConfig);
	beamCannon->setPosition(Vec2(contentSize.width * 0.4f, 0.0f));
	view->addChild(beamCannon, 3);

	addComponent(Direction::create(Vec2::UNIT_X));
	_componentContainer->add(Movement::create());

	DamageDealer* damageDealer = DamageDealer::create({});
	_componentContainer->add(damageDealer);

	PlayerControllerConfig playerConfig;
	playerConfig.maxVelocity = this->config.velocity;
	playerConfig.acceleration = this->config.acceleration;
	playerConfig.rotVelocity = this->config.rotation;
	playerConfig.cannons.insert(Board::left, leftCannon);
	playerConfig.cannons.insert(Board::right, rightCannon);
	playerConfig.shellSuppliers.push_back([this]() {
		SingleSalvoConfig singleSalvoConfig;
		singleSalvoConfig.shellContectSize = Size(10.0f, 10.0f);
		singleSalvoConfig.shellNodeSupplier = [this](const Vec2 dir) {
			CannonBallConfig cannonBallConfig;
			cannonBallConfig.damage = this->config.damage;
			cannonBallConfig.velocity = this->config.shellVelocity;
			cannonBallConfig.lifeTime = this->config.lifeTime;
			cannonBallConfig.damageDealer = HelperMethodsUtil::findComponent<DamageDealer>(this);
			cannonBallConfig.direction = dir;
			cannonBallConfig.size = Size(10.0f, 10.0f);
			cannonBallConfig.targetMode = DamageTargetMode::any;

			return CannonBall::create(cannonBallConfig);
		};

		return SingleSalvo::create(singleSalvoConfig);
	});
	playerConfig.shellSuppliers.push_back([this]() {
		BuckshotConfig buckshotConfig;
		buckshotConfig.damage = this->config.damage;
		buckshotConfig.velocity = 900.0f;
		buckshotConfig.lifeTime = 0.5f;
		buckshotConfig.dealer = HelperMethodsUtil::findComponent<DamageDealer>(this);
		buckshotConfig.quantityWaves = 3;
		buckshotConfig.quantityPerWave = 6;
		buckshotConfig.dispersionAngle = 20.0f;
		buckshotConfig.delayBeforeWave = 0.02;

		return Buckshot::create(buckshotConfig);
	});

	MachineGunSkillScriptConfig machineGunSkillScriptConfig;
	machineGunSkillScriptConfig.cannon = machineGunCannon;
	machineGunSkillScriptConfig.cannonVisionAngle = machineGunCannonConfig.visionAngle;
	machineGunSkillScriptConfig.damage = 0.0f;
	machineGunSkillScriptConfig.dealer = damageDealer;
	machineGunSkillScriptConfig.debug = false;
	machineGunSkillScriptConfig.delay = 0.09f;
	machineGunSkillScriptConfig.dispersionAngle = 5.0f;
	machineGunSkillScriptConfig.lifeTime = 1.0f;
	machineGunSkillScriptConfig.quantity = 9.0f;
	machineGunSkillScriptConfig.velocity = 900.0f;
	playerConfig.specialSkills.pushBack(MachineGunSkillScript::create(machineGunSkillScriptConfig));

	BeamSkillScriptConfig beamSkillScriptConfig;
	beamSkillScriptConfig.activeDuration = 2.0f;
	beamSkillScriptConfig.cannon = beamCannon;
	beamSkillScriptConfig.damage = 0.0f;
	beamSkillScriptConfig.dealer = damageDealer;
	beamSkillScriptConfig.deployDuration = 0.2f;
	beamSkillScriptConfig.foldDuration = 0.2f;
	beamSkillScriptConfig.length = 250.0f;
	beamSkillScriptConfig.width = 5.0f;
	playerConfig.specialSkills.pushBack(BeamSkillScript::create(beamSkillScriptConfig));

	_componentContainer->add(PlayerController::create(playerConfig));
	

	MiningConfig miningConfig;
	miningConfig.rate = this->config.miningRate;
	miningConfig.power = this->config.miningPower;
	_componentContainer->add(Mining::create(miningConfig));
	_componentContainer->add(Tracked::create());
	_componentContainer->add(Tracking::create());
	HealthConfig config;
	config.hitPoints = this->config.hp.first;
	config.currentHitPoints = this->config.hp.second;
	_componentContainer->add(Health::create(config));
	_componentContainer->add(Picker::create());
	RegenerationConfig regenerationConfig;
	regenerationConfig.heal = this->config.repairPower;
	regenerationConfig.interval = this->config.repairInterval;
	regenerationConfig.delay = this->config.repairDelay;
	_componentContainer->add(Regeneration::create(regenerationConfig));
	CrewConfig crewConfig;
	crewConfig.maxQuantity = this->config.crew.first;
	crewConfig.currentQuantity = this->config.crew.second;
	crewConfig.repairRateBonus = this->config.crewRepairBonus;
	crewConfig.canonCooldownBonus = this->config.crewCanonBonus;
	crewConfig.miningRateBonus = this->config.crewMiningBonus;
	crewConfig.looseCrewChances = this->config.crewLooseChances;
	_componentContainer->add(Crew::create(crewConfig));
	std::vector<Vec2> survivorDropPoints;
	survivorDropPoints.push_back(Vec2(-contentSize.width * 0.4f, contentSize.height * 0.5f + 11.0f));
	survivorDropPoints.push_back(Vec2(-(contentSize.width * 0.5f + 11.0f), 0.0f));
	survivorDropPoints.push_back(Vec2(-contentSize.width * 0.4f, -(contentSize.height * 0.5f + 11.0f)));
	DropSupplierConfig dropSupplierConfig;
	dropSupplierConfig.eventName = CHANGED_CREW_QUANTITY_EVENT;
	dropSupplierConfig.points = survivorDropPoints;
	dropSupplierConfig.spriteFrames = { "crew (1).png", "crew (2).png", "crew (3).png", "crew (4).png", "crew (5).png", "crew (6).png" };
	dropSupplierConfig.type = DropType::survivor;
	dropSupplierConfig.quantity = 1;
	dropSupplierConfig.lifeTime = 5.0;
	dropSupplierConfig.physicsBodyDelay = 1.0f;
	_componentContainer->add(DropSupplier::create(dropSupplierConfig));
	_componentContainer->add(Touchable::create(contentSize * 0.8f));
	_componentContainer->add(SharpReversalSkill::create(playerConfig.rotVelocity * 5.0f));
	AfterburnerSkillConfig forsageSkillConfig;
	forsageSkillConfig.velocity = playerConfig.maxVelocity * 2.0f;
	forsageSkillConfig.acceleration = playerConfig.acceleration * 4.0f;
	forsageSkillConfig.endVelocity = playerConfig.maxVelocity * 0.25f;
	forsageSkillConfig.duration = 5.0f;
	_componentContainer->add(AfterburnerSkill::create(forsageSkillConfig));

	auto emotesMap = std::map<std::string, std::pair<std::string, std::string>>();
	emotesMap[BEGIN_TOUCH_DOMAIN_BORDER_EVENT] = std::make_pair<std::string, std::string>("dialog_normal", "emote_exclamation");
	auto emotesPanel = EmotesPanel::create(emotesMap);
	float m = std::max(contentSize.width, contentSize.height) * 0.7f;
	emotesPanel->setPosition(Vec2(0.0f, m + 26.0f));
	addChild(emotesPanel, 0, "emotes_panel");

	auto actionBar = PlayerActionBar::create();
	actionBar->setPosition(Vec2(0.0f, -m));
	addChild(actionBar, 0, "action_bar");

	float width = std::min(contentSize.width, contentSize.height);
	PointsBarConfig healthBarConfig;
	healthBarConfig.width = width;
	healthBarConfig.maxPoints = this->config.hp.first;
	healthBarConfig.startPoints = this->config.hp.second;
	healthBarConfig.color = Color4F::GREEN;
	healthBar = HealthBar::create(healthBarConfig);
	healthBar->setPosition(Vec2(-width * 0.5f, m));
	healthBar->setVisible(false);
	addChild(healthBar);

	StatInformer* statInformer = StatInformer::create();
	statInformer->setPosition(Vec2(10.0f, m) + Vec2(width * 0.5f, 0.0f));
	addChild(statInformer);

	BoardsIndicatorConfig boardsIndicatorConfig;
	boardsIndicatorConfig.rightBoardPosition = Vec2(0.0f, -contentSize.height * 0.5f);
	boardsIndicatorConfig.rightBoardText = "E";
	boardsIndicatorConfig.leftBoardPosition = -boardsIndicatorConfig.rightBoardPosition;
	boardsIndicatorConfig.leftBoardText = "Q";
	boardsIndicatorConfig.length = contentSize.width;
	BoardsIndicator* boardsIndicator = BoardsIndicator::create(boardsIndicatorConfig);
	addChild(boardsIndicator);

	Node* visionRight = Node::create();
	Vec2 startVisionPoint(Vec2(contentSize.height * 0.5f, contentSize.width * -0.5f));
	auto visionPhysicsBody = PhysicsBody::create();
	visionPhysicsBody->addShape(HelperMethodsUtil::createVisionPhysicsShape(startVisionPoint, 12, 100.0f, this->config.visionAreaRadius));
	visionPhysicsBody->setDynamic(false);
	visionPhysicsBody->setCategoryBitmask(ContactMask::tracking);
	visionPhysicsBody->setContactTestBitmask(ContactMask::tracked);
	visionRight->setPhysicsBody(visionPhysicsBody);
	visionRight->setRotation(90.0f);
	view->addChild(visionRight);

	Node* miningAreaRight = Node::create();
	auto miningAreaPhysicsBody = PhysicsBody::createCircle(this->config.miningAreaRadius);
	miningAreaPhysicsBody->setDynamic(false);
	miningAreaPhysicsBody->setCategoryBitmask(ContactMask::mining);
	miningAreaPhysicsBody->setContactTestBitmask(ContactMask::mined);
	miningAreaRight->setPhysicsBody(miningAreaPhysicsBody);
	view->addChild(miningAreaRight);

	Node* visionLeft = Node::create();
	startVisionPoint = Vec2(contentSize.height * 0.5f, contentSize.width * -0.5f);
	visionPhysicsBody = PhysicsBody::create();
	visionPhysicsBody->addShape(HelperMethodsUtil::createVisionPhysicsShape(startVisionPoint, 12, 100.0f, this->config.visionAreaRadius));
	visionPhysicsBody->setDynamic(false);
	visionPhysicsBody->setCategoryBitmask(ContactMask::tracking);
	visionPhysicsBody->setContactTestBitmask(ContactMask::tracked);
	visionLeft->setPhysicsBody(visionPhysicsBody);
	visionLeft->setRotation(-90.0f);
	view->addChild(visionLeft);

#if ED_DEBUG
	addChild(DebugDrawNode::create());
#endif

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	return true;
}

void Player::onEnter() {
	Node::onEnter();

	Vec2 currentPosition = getPosition();
	if (currentPosition.length() > MATH_EPSILON) {
		Vec2 reverse = -currentPosition;
		HelperMethodsUtil::findComponent<Direction>(this)->setDirection(reverse);
		HelperMethodsUtil::findComponent<Movement>(this)->setDirection(reverse);
	}
}

void Player::onExit() {
	Node::onExit();
	unscheduleUpdate();
}

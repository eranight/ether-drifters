#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class StatInfo {
	public:
		int code;
		int qunatity;
		cocos2d::Color4B color;
	};

	class StatInformer : public cocos2d::Node {
	public:
		CREATE_FUNC(StatInformer);
		virtual ~StatInformer();
		bool init() override;
		void onEnter() override;
		void onExit() override;
	private:
		void pushStat(int code, int num, const cocos2d::Color4B& color);
		void putStat(float dt);
	private:
		cocos2d::Vector<cocos2d::Node*> statsQueue;
		std::vector<StatInfo> statInfoQueue;
		cocos2d::Action* showStatAction;

		cocos2d::EventListenerCustom* takingDamageEventListener;
		cocos2d::EventListenerCustom* takingHealingEventListener;
		cocos2d::EventListenerCustom* minedResourcesEventListener;
		cocos2d::EventListenerCustom* pickedDropEventListener;
	};

}
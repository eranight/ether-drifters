#include "BoardsIndicator.hpp"
#include "Common/ViewNode.hpp"
#include "Events/SelectEventData.h"
#include "Events/HealthEventData.h"
#include "Components/Direction.h"
#include "Components/Tracking.h"
#include "Components/Tracked.h"
#include "Components/Health.h"
#include "Utils/HelperMethodsUtil.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

BoardsIndicator::~BoardsIndicator()
{
	CC_SAFE_RELEASE_NULL(selectEventListener);
	CC_SAFE_RELEASE_NULL(deathEventListener);
}

BoardsIndicator* BoardsIndicator::create(const BoardsIndicatorConfig& config)
{
	auto ret = new (std::nothrow) BoardsIndicator(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool BoardsIndicator::init()
{
	if (!Node::init()) {
		return false;
	}

	rightBoard = BoardIndicator::create(config.length, config.rightBoardText, true);
	rightBoard->setPosition(config.rightBoardPosition);
	addChild(rightBoard);

	leftBoard = BoardIndicator::create(config.length, config.leftBoardText, false);
	leftBoard->setPosition(config.leftBoardPosition);
	addChild(leftBoard);

	selectEventListener = EventListenerCustom::create(SELECT_EVENT, CC_CALLBACK_1(BoardsIndicator::processSelectEvent, this));
	selectEventListener->retain();

	deathEventListener = EventListenerCustom::create(DEATH_EVENT, CC_CALLBACK_1(BoardsIndicator::processDeathEvent, this));
	deathEventListener->retain();

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	scheduleUpdate();
	return true;
}

void BoardsIndicator::onEnter()
{
	Node::onEnter();

	getEventDispatcher()->addEventListenerWithFixedPriority(selectEventListener, 1);
	getEventDispatcher()->addEventListenerWithFixedPriority(deathEventListener, 1);

	owner = getParent();
	direction = HelperMethodsUtil::findComponent<Direction>(owner);
	tracking = HelperMethodsUtil::findComponent<Tracking>(owner);

	ownerView = dynamic_cast<ViewNode*>(HelperMethodsUtil::getViewIfExists(owner));
	rightBoard->setViewOwner(ownerView);
	leftBoard->setViewOwner(ownerView);

	setVisible(false);
}

void BoardsIndicator::onExit()
{
	Node::onExit();

	getEventDispatcher()->removeEventListener(selectEventListener);
	getEventDispatcher()->removeEventListener(deathEventListener);
}

void BoardsIndicator::update(float dt)
{
	Node::update(dt);

	this->setRotation(ownerView->getRotation());

	if (currentTarget == nullptr) {
		return;
	}

	select();
}

void BoardsIndicator::processSelectEvent(cocos2d::EventCustom* event)
{
	SelectEventData* eventData = reinterpret_cast<SelectEventData*>(event->getUserData());
	if (eventData->getOwner() == owner) {
		Node* selectedNode = eventData->getAffected();
		if (selectedNode->getName()._Starts_with("enemy") && eventData->isSelected()) {
			setVisible(true);
			currentTarget = selectedNode;
			currentTarget->retain();
		}
		else {
			setVisible(false);
			if (currentActiveBoard != nullptr) {
				currentActiveBoard->activate(false);
				currentActiveBoard = nullptr;
			}
			if (currentTarget != nullptr) {
				CC_SAFE_RELEASE_NULL(currentTarget);
			}
		}
	}
}

void BoardsIndicator::select()
{
	Tracked* tracked = HelperMethodsUtil::findComponent<Tracked>(currentTarget);
	if (tracking->containInVision(tracked)) {
		float angle = calculateAngle(currentTarget);
		BoardIndicator* nextActiveBoard = angle < 0.0f ? rightBoard : leftBoard;
		if (currentActiveBoard != nullptr && currentActiveBoard != nextActiveBoard) {
			currentActiveBoard->activate(false);
			currentActiveBoard = nextActiveBoard;
			currentActiveBoard->activate(true);
		}
		else if (currentActiveBoard != nextActiveBoard) {
			currentActiveBoard = nextActiveBoard;
			currentActiveBoard->activate(true);
		}
	}
	else if (currentActiveBoard != nullptr) {
		currentActiveBoard->activate(false);
		currentActiveBoard = nullptr;
	}
}

void BoardsIndicator::processDeathEvent(EventCustom* event)
{
	HealthEventData* eventData = reinterpret_cast<HealthEventData*>(event->getUserData());
	if (eventData->getOwner() == currentTarget) {
		if (currentActiveBoard != nullptr) {
			currentActiveBoard->activate(false);
			currentActiveBoard = nullptr;
		}
		CC_SAFE_RELEASE_NULL(currentTarget);
		setVisible(false);
	}
}

float BoardsIndicator::calculateAngle(Node* selectedNode)
{
	Vec2 dir = direction->getDirection();
	Vec2 toSelectedDir = selectedNode->getPosition() - owner->getPosition();
	return dir.getAngle(toSelectedDir);
}

BoardIndicator* BoardIndicator::create(float lenght, const std::string& labelText, bool rightSide)
{
	auto ret = new (std::nothrow) BoardIndicator();
	if (ret != nullptr && ret->init(lenght, labelText, rightSide)) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool BoardIndicator::init(float length, const std::string& labelText, bool rightSide)
{
	if (!Node::init()) {
		return false;
	}

	this->length = length;
	float halfLength = length * 0.5f;

	view = DrawNode::create(4.0f);
	view->drawLine(Vec2(-halfLength, 0.0f), Vec2(halfLength, 0.0f), Color4F::GRAY);
	view->setColor(Color3B::GRAY);
	addChild(view);

	label = Label::createWithTTF(labelText, "fonts/arial.ttf", 20.0f);
	label->setAlignment(TextHAlignment::CENTER);
	float y = label->getTTFConfig().fontSize * 0.7f;
	if (rightSide) {
		y *= -1;
	}
	label->setPosition(Vec2(-halfLength, y));
	label->setColor(Color3B::GRAY);
	addChild(label);

	scheduleUpdate();
	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

void BoardIndicator::update(float dt)
{
	Node::update(dt);

	label->setRotation(-ownerView->getRotation());
}

void BoardIndicator::activate(bool activate)
{
	Color3B nextColor = Color3B::GRAY;
	if (activate) {
		nextColor = Color3B::RED;
	}
	view->clear();
	view->drawLine(Vec2(-length * 0.5f, 0.0f), Vec2(length * 0.5f, 0.0f), Color4F(nextColor));
	label->setColor(nextColor);
}

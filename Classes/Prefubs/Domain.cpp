#include "Domain.h"
#include "Common/ViewNode.hpp"
#include "Meteor.h"
#include "Skeleton.h"
#include "SimpleMonster.h"
#include "Portal.h"
#include "Player.h"
#include "DomainObjectTouchLayer.h"
#include "Halo.h"
#include "Anomalies/BlackHole.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Events/DomainEventData.h"
#include "Anomalies/AnomalyCreator.h"
#include "json/document.h"

USING_NS_CC;
using namespace ether_drifters;

Domain::~Domain() {
	CCLOG("In the destructor of Domain. %p", this);
	CC_SAFE_RELEASE_NULL(player);
	CC_SAFE_RELEASE_NULL(physicsListener);
}

Domain* Domain::create(const DomainConfig& config) {
	auto ret = new (std::nothrow) Domain(config);
	if (ret != nullptr && ret->init()) {
		ret->autorelease();
	}
	else {
		CC_SAFE_DELETE(ret);
	}
	return ret;
}

bool Domain::init() {
	if (!Node::init()) {
		return false;
	}

	float radius = config.radius;

	auto back = Sprite::create("wall5.png");
	back->setScale(radius * 2 / back->getContentSize().height);

	Rect rect = Rect(Vec2::ZERO, Size(radius * 3.5, radius * 3.5));
	auto farStarMap = Sprite::createWithTexture(HelperMethodsUtil::createLoopTexture("star_map_mask.png"), rect);
	auto nearStarMap = Sprite::createWithTexture(HelperMethodsUtil::createLoopTexture("star_map_mask2.png"), rect);
	auto cloudsFrontSprite = Sprite::createWithTexture(HelperMethodsUtil::createLoopTexture("space_clouds_mask.png"), rect);

	parallax = ParallaxNode::create();
	parallax->addChild(farStarMap, 0, Vec2(0.0f, 0.0f), Vec2(radius, radius) * 0.8f);
	parallax->addChild(nearStarMap, 0, Vec2(0.1f, 0.1f), Vec2(radius, radius) * 0.8f);
	parallax->addChild(cloudsFrontSprite, 0, Vec2(0.5f, 0.5f), Vec2(radius, radius) * 0.8f);

	auto front = ClippingNode::create();
	auto frontStencil = DrawNode::create();
	frontStencil->drawSolidCircle(Vec2::ZERO, radius, 360, 200, Color4F::WHITE);
	front->setStencil(frontStencil);
	front->addChild(parallax);

	auto view = ViewNode::create();
	addChild(view);

	view->addChild(back);
	view->addChild(front);

	const int vertexesNumber = 72;
	Vec2 points[vertexesNumber];
	float dAngle = CC_DEGREES_TO_RADIANS(360.0f) / vertexesNumber;
	float angle = 0.0f;
	for (int vertexesCounter = 0; vertexesCounter < vertexesNumber; ++vertexesCounter, angle += dAngle)
	{
		points[vertexesCounter].x = radius * 1.01f * cos(angle);
		points[vertexesCounter].y = radius * 1.01f * sin(angle);
	}
	edge = PhysicsBody::createEdgePolygon(points, vertexesNumber, PhysicsMaterial(0.0f, 0.0f, 0.0f), 2.0f);
	edge->setDynamic(false);
	edge->setCategoryBitmask(ContactMask::domain);
	edge->setContactTestBitmask(ContactMask::body | ContactMask::hit);
	this->setPhysicsBody(edge);

	physicsListener = EventListenerPhysicsContact::create();
	physicsListener->onContactBegin = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesFromPhysicsContact(contact, this, ContactMask::domain);
		if (nodes.empty()) {
			return false;
		}
		Node* node = nodes.at(1);
		bool inside = isInside(node);
		DomainEventData eventData(this, node, inside);
		_eventDispatcher->dispatchCustomEvent(BEGIN_TOUCH_DOMAIN_BORDER_EVENT, &eventData);
		return true;
	};
	physicsListener->onContactSeparate = [this](PhysicsContact& contact) {
		auto nodes = HelperMethodsUtil::fetchNodesFromPhysicsContact(contact, this, ContactMask::domain);
		if (nodes.empty()) {
			return;
		}
		Node* node = nodes.at(1);
		bool inside = isInside(node);
		DomainEventData eventData(this, node, inside);
		_eventDispatcher->dispatchCustomEvent(END_TOUCH_DOMAIN_BORDER_EVENT, &eventData);
	};
	physicsListener->retain();

	auto domainObjectTouchLayer = DomainObjectTouchLayer::create();
	domainObjectTouchLayer->setCameraMask(DOMAIN_CAMERA_MASK);
	addChild(domainObjectTouchLayer, 198);

	/*auto mistSprite = Sprite::create("wall7.png");
	mistSprite->setScale(radius * 2 / mistSprite->getContentSize().height);
	mistSprite->setOpacity(164.0f);
	mistSprite->runAction(RepeatForever::create(RotateBy::create(720.0f, 360.0f)));
	mistSprite->setCameraMask(DOMAIN_CAMERA_MASK);
	addChild(mistSprite, 199);*/

	HaloConfig haloConfig;
	haloConfig.color = Color3B(0, 162, 232);
	haloConfig.quantity = 80;
	haloConfig.radius = config.radius * 1.025f;
	haloConfig.scale = config.radius * 0.0005f;
	haloConfig.velocity = 1.0f;
	auto halo = Halo::create(haloConfig);
	halo->setCameraMask(DOMAIN_CAMERA_MASK);
	addChild(halo, 199);

	haloConfig.color = Color3B(0, 87, 232);
	haloConfig.quantity = 120;
	haloConfig.radius = config.radius * 1.07f;
	haloConfig.velocity = 0.5f;
	haloConfig.clockWise = -1;
	halo = Halo::create(haloConfig);
	halo->setCameraMask(DOMAIN_CAMERA_MASK);
	addChild(halo, 199);

	view->setCameraMask(DOMAIN_CAMERA_MASK);
	setCameraMask(DOMAIN_CAMERA_MASK, false);
	setTag(DOMAIN_TAG);
	return true;
}

void Domain::onEnter() {
	Node::onEnter();
	player = getChildByName("player");
	if (player != nullptr) {
		player->retain();
	}
	auto currentScene = Director::getInstance()->getRunningScene();
	for (auto node : currentScene->getChildren()) {
		if (node->getName() == PLAYER_CAMERA_NAME || node->getName() == DOMAIN_UI_CAMERA_NAME) {
			cameras.pushBack(dynamic_cast<Camera*>(node));
		}
	}
	if (!cameras.empty()) {
		parallax->setPosition(parallax->getPosition() - cameras.at(0)->getPosition());
	}
	_eventDispatcher->addEventListenerWithSceneGraphPriority(physicsListener, this);
	scheduleUpdateWithPriority(10); // guarantee to update after changing player position
}

void Domain::onExit() {
	Node::onExit();
	_eventDispatcher->removeEventListener(physicsListener);
}

void Domain::update(float dt) {
	Node::update(dt);
	if (player != nullptr && !cameras.empty()) {
		Vec2 currentCameraPos = cameras.at(0)->getPosition();
		Vec2 nextCameraPos = convertToWorldSpace(player->getPosition());
		nextCameraPos = currentCameraPos.lerp(nextCameraPos, 0.1f);
		for (auto camera : cameras) {
			camera->setPosition(nextCameraPos);
		}
		Vec2 v = -(nextCameraPos - currentCameraPos);
		parallax->setPosition(parallax->getPosition() + v);
	}
}

bool Domain::isInside(cocos2d::Node* node) {
	Node* view = HelperMethodsUtil::getViewIfExists(node);
	if (view->getPhysicsBody() != nullptr) {
		PhysicsShape* shape = view->getPhysicsBody()->getFirstShape();
		switch (shape->getType())
		{
		case PhysicsShape::Type::POLYGON:
		case PhysicsShape::Type::BOX: {
			PhysicsShapePolygon* polygonShape = dynamic_cast<PhysicsShapePolygon*>(shape);
			for (int index = 0; index < polygonShape->getPointsCount(); ++index) {
				Vec2 pos = polygonShape->getPoint(index);
				pos = view->convertToWorldSpaceAR(pos);
				pos = convertToNodeSpace(pos);
				if (pos.length() < config.radius) {
					return true;
				}
			}
			return false;
		}
		case PhysicsShape::Type::CIRCLE: {
			float rd = dynamic_cast<PhysicsShapeCircle*>(shape)->getRadius();
			Vec2 pos = view->getPosition();
			pos = view->getParent()->convertToWorldSpaceAR(pos);
			pos = convertToNodeSpace(pos);
			return pos.length() < config.radius + rd;
		}
		default:
			CCLOG("domain doesn't handle shape type %d", shape->getType());
			return false;
		}
	}
	else {
		auto bb = view->getBoundingBox();
		return bb.origin.length() < config.radius || (bb.origin + bb.size).length() < config.radius;
	}
}
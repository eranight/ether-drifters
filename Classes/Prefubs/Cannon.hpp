#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters
{

	class CannonConfig {
	public:
		float cooldown = 0.0f;
		float visionAngle;
		std::string spriteName;
		cocos2d::Size contentSize;
		bool debug = false;
	};

	class Cannon : public cocos2d::Node {
		CREATE_FUNC_WITH_CONFIG(Cannon, CannonConfig);
	public:
		bool init() override;
		void onEnter() override;
	public:
		// in degrees in ccw
		void rotate(float angle);
		// in degress in ccw
		float getRotation();
		void reset();
		void fire(const std::function<cocos2d::Node* ()>& shotNodeSupplier);
		bool isRecharging() { return recharging; }
	private:
		void redrawDebugNode();
		void updateRecharged(float dt);
	private:
		bool recharging = false;
		cocos2d::Node* view = nullptr;
		cocos2d::Node* shotNode = nullptr;
		cocos2d::DrawNode* debugDrawNode = nullptr;
	};

}
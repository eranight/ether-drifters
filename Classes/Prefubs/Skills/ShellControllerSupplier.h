#pragma once
#include "Components/ShellControllers.h"

namespace ether_drifters {

	class ShellControllerSupplier {
	public:
		virtual AbstractShellController* get(const ShellControllerConfig& config) = 0;
	};

	class SimpleShellControllerSupplier : public ShellControllerSupplier {
	public:
		AbstractShellController* get(const ShellControllerConfig& config) override {
			return SimpleShellController::create(config);
		}
	};

}
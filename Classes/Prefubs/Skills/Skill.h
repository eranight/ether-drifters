#pragma once
#include "cocos2d.h"
#include "PrefubConverter.h"

namespace ether_drifters {

	enum class Goal {
		none,
		any,
		self,
		target
	};

	enum class GoalComponent {
		node,
		health,
		tracked
	};

	class SkillContext {
	public:
		cocos2d::Node* owner;
		cocos2d::Vec2 startPos;
		cocos2d::Node* target;
	};

	class Skill : public cocos2d::Ref {
	public:
		CREATE_FUNC(Skill);
		bool init() { return true; }

		Goal goal;
		GoalComponent goalComponent;
		PrefubConverter::PrefubType type;
		std::function<PrefubConverter (SkillContext*)> prefubSupplier;
		float cooldown;
		bool selfDirected;
		bool inGlobalCooldown;
	};

}
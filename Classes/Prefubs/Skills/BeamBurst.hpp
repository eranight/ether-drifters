#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"
#include "Common/ProjectileMarkers.hpp"
#include "Prefubs/Projectiles/Beam.hpp"

namespace ether_drifters
{

	class DamageDealer;
	class Domain;
	class Direction;

	class BeamBurstConfig {
	public:
		float deployDuration = 0.0f;
		float activeDuration = 0.0f;
		float foldDuration = 0.0f;
		float damage = 0.0f;
		float length = 0.0f;
		float width = 0.0f;
		DamageDealer* dealer = nullptr;
	};

	class BeamBurst : public cocos2d::Node, public CallbackEndShot {
		CREATE_FUNC_WITH_CONFIG(BeamBurst, BeamBurstConfig);
	public:
		bool init() override;
		void onEnter() override;
		void update(float dt) override;
		void end(const std::function<void()>& callback) override;
	private:
		void recalculateRayPosition();
		void removeRayEvent(cocos2d::Node* ray);
	private:
		Domain* domain = nullptr;

		BeamConfig beamConfig;
		cocos2d::Node* currentBeam = nullptr;
		Direction* beamDirection = nullptr;
		std::function<void()> endCallback;
	};

}
#include "Buckshot.hpp"
#include "Ether.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/DamageDealer.h"

using namespace cocos2d;
using namespace ether_drifters;

static const float DEBUG_LINE_SIZE = 100.0f;

Buckshot::~Buckshot()
{
	if (running) {
		Director::getInstance()->getScheduler()->unschedule(schedule_selector(Buckshot::updateRunning), this);
	}
}

bool Buckshot::init()
{
	if (!Node::init()) {
		return false;
	}

	bulletConfig.damage = config.damage / (config.quantityWaves * config.quantityPerWave);
	bulletConfig.velocity = config.velocity;
	bulletConfig.lifeTime = config.lifeTime;
	bulletConfig.target = nullptr;
	bulletConfig.targetMode = DamageTargetMode::any;
	bulletConfig.contentSize = Size(8.222f, 2.0f);
	bulletConfig.dealer = config.dealer;

	if (config.debug) {
		debugDrawNode = DrawNode::create();

		float halfAngle = CC_DEGREES_TO_RADIANS(config.dispersionAngle * 0.5f);

		debugDrawNode->drawLine(Vec2::ZERO, Vec2(cosf(halfAngle) * DEBUG_LINE_SIZE, sinf(halfAngle) * DEBUG_LINE_SIZE), Color4F::RED);
		debugDrawNode->drawLine(Vec2::ZERO, Vec2(cosf(halfAngle) * DEBUG_LINE_SIZE, -sinf(halfAngle) * DEBUG_LINE_SIZE), Color4F::RED);

		addChild(debugDrawNode, 5);
	}

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void Buckshot::onEnter()
{
	Node::onEnter();

	running = true;
	shotIndex = 0;

	Director::getInstance()->getScheduler()->schedule(
		schedule_selector(Buckshot::updateRunning),
		this,
		config.delayBeforeWave,
		std::clamp(config.quantityWaves - 1, 0, INT32_MAX),
		0.0f,
		false
	);
}

void Buckshot::onExit()
{
	Node::onExit();

	if (running) {
		Director::getInstance()->getScheduler()->unschedule(schedule_selector(Buckshot::updateRunning), this);
	}
}

float Buckshot::duration()
{
	return config.delayBeforeWave * std::clamp(config.quantityWaves - 1, 0, INT32_MAX) + Director::getInstance()->getAnimationInterval();
}

void Buckshot::updateRunning(float dt)
{
	if (!running) {
		return;
	}

	int negativeCounter = 0;
	int positiveCounter = 0;
	int halfQuantity = config.quantityPerWave / 2;
	if (config.quantityPerWave % 2 == 1) {
		++halfQuantity;
	}

	float halfAngle = config.dispersionAngle * 0.5f;

	Vec2 offset = Vec2(8.222f * 0.5f, 0.0f);
	auto domain = Ether::getInstance()->getCurrentDomain();
	Vec2 fromPosition = domain->convertToNodeSpace(convertToWorldSpace(offset));
	for (int index = 0; index < config.quantityPerWave; ++index) {
		float angle;
		if (positiveCounter < halfQuantity) {
			angle = HelperMethodsUtil::randomExplicitAngle(0.0f, halfAngle);
			++positiveCounter;
		}
		else if (negativeCounter < halfQuantity) {
			angle = HelperMethodsUtil::randomExplicitAngle(-halfAngle, 0.0f);
			++negativeCounter;
		}
		else {
			angle = HelperMethodsUtil::randomAngle(0.0f, halfAngle);
		}

		Vec2 rawToPosition = Vec2::UNIT_X.rotateByAngle(Vec2::ZERO, angle);
		Vec2 toPosition = domain->convertToNodeSpace(convertToWorldSpace(rawToPosition + offset));

		bulletConfig.direction = toPosition - fromPosition;

		auto bullet = Bullet::create(bulletConfig);
		bullet->setPosition(fromPosition);
		domain->addChild(bullet, static_cast<int>(DomainObjectLevel::projectiles));
	}

	running = shotIndex++ < config.quantityWaves - 1;
}

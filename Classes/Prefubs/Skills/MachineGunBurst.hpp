#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"
#include "Prefubs/Projectiles/Bullet.hpp"
#include "Common/ProjectileMarkers.hpp"

namespace ether_drifters
{

	class DamageDealer;

	class MachineGunBurstConfig {
	public:
		float damage;
		int quantity;
		float delay;
		float velocity;
		float lifeTime;
		float dispersionAngle;
		DamageDealer* dealer = nullptr;
		bool debug = false;
	};

	class MachineGunBurst : public cocos2d::Node, public FiniteTimeShot {
		CREATE_FUNC_WITH_CONFIG(MachineGunBurst, MachineGunBurstConfig);
	public:
		~MachineGunBurst();
		bool init() override;
		void onEnter() override;
		void onExit() override;
		float duration() override;
	private:
		void updateRunning(float dt);
	private:
		BulletConfig bulletConfig;
		bool running = false;
		int shotIndex = 0;
		cocos2d::DrawNode* debugDrawNode = nullptr;
	};

}
#include "BeamBurst.hpp"
#include "Ether.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/DamageDealer.h"
#include "Components/Direction.h"

using namespace cocos2d;
using namespace ether_drifters;

bool BeamBurst::init()
{
	if (!Node::init()) {
		return false;
	}

	beamConfig.deployDuration = config.deployDuration;
	beamConfig.activeDuration = config.activeDuration;
	beamConfig.foldDuration = config.foldDuration;
	beamConfig.damage = config.damage;
	beamConfig.width = config.width;
	beamConfig.length = config.length;
	beamConfig.dealer = config.dealer;
	beamConfig.onFoldingCompletedCallback = CC_CALLBACK_1(BeamBurst::removeRayEvent, this);

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void BeamBurst::onEnter()
{
	Node::onEnter();

	domain = Ether::getInstance()->getCurrentDomain();

	currentBeam = ether_drifters::Beam::create(beamConfig);
	beamDirection = HelperMethodsUtil::findComponent<Direction>(currentBeam);
	
	recalculateRayPosition();

	domain->addChild(currentBeam, static_cast<int>(DomainObjectLevel::projectiles));

	scheduleUpdate();
}

void BeamBurst::update(float dt)
{
	Node::update(dt);

	if (currentBeam != nullptr) {
		recalculateRayPosition();
	}
}

void BeamBurst::end(const std::function<void()>& callback)
{
	endCallback = callback;
}

void BeamBurst::recalculateRayPosition()
{
	Vec2 fromPosition = domain->convertToNodeSpace(convertToWorldSpace(Vec2::ZERO));
	currentBeam->setPosition(fromPosition);

	Vec2 toPosition = domain->convertToNodeSpace(convertToWorldSpace(Vec2::UNIT_X));
	beamDirection->setDirection(toPosition - fromPosition);
}

void BeamBurst::removeRayEvent(Node* ray)
{
	if (ray == currentBeam) {
		Ether::getInstance()->getCurrentDomain()->removeChild(ray);
		currentBeam = nullptr;
		unscheduleUpdate();
		endCallback();
	}
}

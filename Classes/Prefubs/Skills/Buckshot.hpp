#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"
#include "Prefubs/Projectiles/Bullet.hpp"
#include "Common/ProjectileMarkers.hpp"

namespace ether_drifters
{

	class DamageDealer;

	class BuckshotConfig {
	public:
		float damage;
		int quantityWaves;
		int quantityPerWave;
		float delayBeforeWave;
		float velocity;
		float lifeTime;
		float dispersionAngle;
		DamageDealer* dealer = nullptr;
		bool debug = false;
	};

	class Buckshot : public cocos2d::Node, public FiniteTimeShot {
		CREATE_FUNC_WITH_CONFIG(Buckshot, BuckshotConfig);
	public:
		~Buckshot();
		bool init() override;
		void onEnter() override;
		void onExit() override;
		float duration() override;
	private:
		void updateRunning(float dt);
	private:
		BulletConfig bulletConfig;
		bool running = false;
		int shotIndex = 0;
		cocos2d::DrawNode* debugDrawNode = nullptr;
	};
}
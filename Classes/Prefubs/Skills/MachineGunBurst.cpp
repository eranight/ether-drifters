#include "MachineGunBurst.hpp"
#include "Ether.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Components/DamageDealer.h"

using namespace cocos2d;
using namespace ether_drifters;

static const float DEBUG_LINE_SIZE = 100.0f;

MachineGunBurst::~MachineGunBurst()
{
	if (running) {
		Director::getInstance()->getScheduler()->unschedule(schedule_selector(MachineGunBurst::updateRunning), this);
	}
}

bool MachineGunBurst::init()
{
	if (!Node::init()) {
		return false;
	}

	bulletConfig.damage = config.damage / config.quantity;
	bulletConfig.velocity = config.velocity;
	bulletConfig.lifeTime = config.lifeTime;
	bulletConfig.target = nullptr;
	bulletConfig.targetMode = DamageTargetMode::any;
	bulletConfig.contentSize = Size(16.444f, 4.0f);
	bulletConfig.dealer = config.dealer;

	if (config.debug) {
		debugDrawNode = DrawNode::create();

		float halfAngle = CC_DEGREES_TO_RADIANS(config.dispersionAngle * 0.5f);

		debugDrawNode->drawLine(Vec2::ZERO, Vec2(cosf(halfAngle) * DEBUG_LINE_SIZE, sinf(halfAngle) * DEBUG_LINE_SIZE), Color4F::RED);
		debugDrawNode->drawLine(Vec2::ZERO, Vec2(cosf(halfAngle) * DEBUG_LINE_SIZE, -sinf(halfAngle) * DEBUG_LINE_SIZE), Color4F::RED);

		addChild(debugDrawNode, 5);
	}

	setCameraMask(DOMAIN_CAMERA_MASK);
	return true;
}

void MachineGunBurst::onEnter()
{
	Node::onEnter();

	running = true;
	shotIndex = 0;

	Director::getInstance()->getScheduler()->schedule(
		schedule_selector(MachineGunBurst::updateRunning),
		this,
		config.delay,
		std::clamp(config.quantity - 1, 0, INT32_MAX),
		0.0f,
		false
	);
}

void MachineGunBurst::onExit()
{
	Node::onExit();

	if (running) {
		Director::getInstance()->getScheduler()->unschedule(schedule_selector(MachineGunBurst::updateRunning), this);
	}
}

float MachineGunBurst::duration()
{
	return config.delay * std::clamp(config.quantity - 1, 0, INT32_MAX) + Director::getInstance()->getAnimationInterval();
}

void MachineGunBurst::updateRunning(float dt)
{
	if (!running) {
		return;
	}

	Vec2 offset = Vec2(16.444f * 0.5f, 0.0f);

	auto domain = Ether::getInstance()->getCurrentDomain();
	Vec2 fromPosition = domain->convertToNodeSpace(convertToWorldSpace(offset));

	float half = config.dispersionAngle * 0.5f;
	Vec2 rawToPosition = Vec2::UNIT_X.rotateByAngle(Vec2::ZERO, HelperMethodsUtil::randomAngle(0.0f, half));
	Vec2 toPosition = domain->convertToNodeSpace(convertToWorldSpace(rawToPosition + offset));

	bulletConfig.direction = toPosition - fromPosition;

	auto bullet = Bullet::create(bulletConfig);
	bullet->setPosition(fromPosition);
	domain->addChild(bullet, static_cast<int>(DomainObjectLevel::projectiles));

	running = shotIndex++ < config.quantity - 1;
}

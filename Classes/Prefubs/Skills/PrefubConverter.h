#pragma once
#include "cocos2d.h"

namespace ether_drifters {

	class PrefubConverter {
	public:
		enum class PrefubType {
			node,
			component
		};
	public:
		PrefubConverter(cocos2d::Ref* obj, const PrefubType& type) : obj(obj), type(type) {}
	public:
		cocos2d::Node* asNode() { return dynamic_cast<cocos2d::Node*>(obj); }
		cocos2d::Component* asComponent() { return dynamic_cast<cocos2d::Component*>(obj); }
		PrefubType getType() { return type; }
	private:
		cocos2d::Ref* obj;
		PrefubType type;
	};

}
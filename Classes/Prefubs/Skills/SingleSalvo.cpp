#include "SingleSalvo.hpp"
#include "Ether.h"
#include "Utils/Constants.h"
#include "Utils/HelperMethodsUtil.h"
#include "Common/ViewNode.hpp"
#include "Components/Direction.h"
#include "Components/Movement.h"

using namespace cocos2d;
using namespace ether_drifters;

void SingleSalvo::onEnter()
{
	Node::onEnter();

	scheduleOnce(schedule_selector(SingleSalvo::updateRunning), 0.0f);
}

void SingleSalvo::updateRunning(float dt)
{
	Vec2 offset = Vec2(config.shellContectSize.width * 0.5f, 0.0f);

	auto domain = Ether::getInstance()->getCurrentDomain();
	Vec2 startPosition = domain->convertToNodeSpace(convertToWorldSpace(offset));

	Vec2 rawToPosition = Vec2::UNIT_X;
	Vec2 toPosition = domain->convertToNodeSpace(convertToWorldSpace(rawToPosition + offset));

	Vec2 direction = toPosition - startPosition;

	auto shellNode = config.shellNodeSupplier(direction);
	shellNode->setPosition(startPosition);
	domain->addChild(shellNode, static_cast<int>(DomainObjectLevel::projectiles));
}

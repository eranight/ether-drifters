#pragma once
#include "cocos2d.h"
#include "Utils/HelperMacroses.hpp"

namespace ether_drifters
{

	class SingleSalvoConfig {
	public:
		cocos2d::Size shellContectSize;
		std::function<cocos2d::Node* (const cocos2d::Vec2&)> shellNodeSupplier; // requires direction
	};

	class SingleSalvo : public cocos2d::Node {
		CREATE_FUNC_WITH_CONFIG(SingleSalvo, SingleSalvoConfig);
	public:
		void onEnter() override;
	private:
		void updateRunning(float dt);
	};

}
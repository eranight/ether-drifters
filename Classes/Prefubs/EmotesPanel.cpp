#include "EmotesPanel.h"
#include "Events/EventData.h"
#include "Utils/Constants.h"

using namespace cocos2d;
using namespace ether_drifters;

EmotesPanel* EmotesPanel::create(std::map<std::string, std::pair<std::string, std::string>> eventNamesOnEmotes) {
	auto ref = new (std::nothrow) EmotesPanel();
	if (ref != nullptr && ref->init(eventNamesOnEmotes)) {
		ref->autorelease();
	}
	else {
		CC_SAFE_DELETE(ref);
	}
	return ref;
}

bool EmotesPanel::init(std::map<std::string, std::pair<std::string, std::string>> eventNamesOnEmotes) {
	if (!Node::init()) {
		return false;
	}

	this->eventNamesOnEmotes = eventNamesOnEmotes;

	for (auto keyValuePair : eventNamesOnEmotes) {
		auto customEventListener = EventListenerCustom::create(keyValuePair.first, [this, keyValuePair](EventCustom* custom) {
			EventData* eventData = reinterpret_cast<EventData*>(custom->getUserData());
			if (eventData == nullptr) {
				CCLOG("event data must be not null");
				return;
			}
			if (eventData->getOwner() == getParent()) {
				SpriteBatchNode* batchNode;
				if (dialogSprite == nullptr) {
					batchNode = SpriteBatchNode::create("Spritesheets/emotes_sheet.png");
					dialogSprite = Sprite::createWithSpriteFrameName(keyValuePair.second.first);
					emoteSprite = Sprite::createWithSpriteFrameName(keyValuePair.second.second);
					batchNode->addChild(dialogSprite);
					batchNode->addChild(emoteSprite);
					batchNode->setCameraMask(DOMAIN_UI_CAMERA_MASK);
					addChild(batchNode);
				}
				else {
					dialogSprite->setSpriteFrame(keyValuePair.second.first);
					emoteSprite->setSpriteFrame(keyValuePair.second.second);
					batchNode = dynamic_cast<SpriteBatchNode*>(dialogSprite->getParent());
				}

				batchNode->setVisible(true);
				batchNode->runAction(
					Sequence::createWithTwoActions(
						DelayTime::create(3.0f),
						CallFuncN::create([](Node* node) { node->setVisible(false); })
					)
				);
			}
		});
		customEventListeners.pushBack(customEventListener);
	}

	setCameraMask(DOMAIN_UI_CAMERA_MASK);
	return true;
}

void EmotesPanel::onEnter() {
	Node::onEnter();
	for (auto listener : customEventListeners) {
		_eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
	}
}

void EmotesPanel::onExit() {
	Node::onExit();
	for (auto listener : customEventListeners) {
		_eventDispatcher->removeEventListener(listener);
	}
}

void EmotesPanel::alignItemsHorizontally() {
	alignItemsHorizontallyWithPadding(5.0f);
}

void EmotesPanel::alignItemsHorizontallyWithPadding(float padding) {
	float width = -padding;
	for (const auto& child : _children)
		width += child->getContentSize().width * child->getScaleX() + padding;

	float x = -width / 2.0f;

	for (const auto& child : _children) {
		child->setPosition(x + child->getContentSize().width * child->getScaleX() / 2.0f, 0);
		x += child->getContentSize().width * child->getScaleX() + padding;
	}
}
#pragma once

#include "cocos2d.h"
#include "Portal.h"

namespace ether_drifters {

	class DomainConfig {
	public:
		float radius;
	};

	class Domain : public cocos2d::Node {
	protected:
		Domain(const DomainConfig& config) : config(config), player(nullptr) {}
		virtual ~Domain();
	public:
		static Domain * create(const DomainConfig& config);
		bool init() override;
		void onEnter() override;
		void onExit() override;
		void update(float dt) override;
	public:
		float getRadius() const { return config.radius; }
		bool isInside(cocos2d::Node* node);
	private:
		DomainConfig config;
		cocos2d::PhysicsBody * edge;
		cocos2d::Node * player;
		cocos2d::Vector<cocos2d::Camera*> cameras; // player camera + domain ui camera
		cocos2d::EventListenerPhysicsContact* physicsListener;
		cocos2d::ParallaxNode* parallax;
	};

}
